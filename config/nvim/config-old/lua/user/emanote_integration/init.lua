--[[

The intent of this module is to provide a useable (if rough) integration with
 the 'EmaNote' system for personal websites and note taking.

EmaNote can start a local server that will watch for changes to your notes
 directory and then serve those files in a browsable HTML format to your
 web browser.

Integration features:
  Done:

  Todo:
    High priority:
      - [x] Command to start emanote server and open it in default browser
      - [x] Keybind to update all metadata for current page
      - [x] Keybind to follow link (gd) (not needed, link following provided by mkdnflow)

      - Syntax highlighting and LSP support for YAML frontmatter in markdown files:
        - https://habamax.github.io/2019/03/07/vim-markdown-frontmatter.html
        - https://github.com/ChristianChiarulli/nvim/blob/master/after/syntax/markdown.vim#L105
      - Telescope integration to find all files with a tag
        - Type 1: Fuzzy find tag name within file itself
        - Type 2: Fuzzy search for tag name, enter to select and populate quickfix list with all notes that have that tag

    Medium priority:
      - [x] Keybind to open the Emanote server in a new browser window when in a notes directory
      - emanote supports embedded media files with the same syntax as standard note links. Add a broot integration for selecting a media file to inline in the file the same way that telescope is used to add links between notes.
      - Telescope and broot integrations to fuzzy-find a file and insert a link to it at the cursor (or wrapping the current visual mode selection)

    Low priority:
      - Keybind to navigate to current page in browser (;gx)
      - Keybind to start/stop/restart emanote server
      - Keybind to clean the server build directory

    Might never do this:
      - 'Tag view' mode where only notes tagged with a particular set of tags are visible in the emanote integration. Helpful for focusing on concepts that span a broad range or organizational structures.



--]]

-- local frontmatter = require('user.emanote_integration.frontmatter')
-- frontmatter.setup({
--   enabled = true,
--
--   keymap = {
--     ['<leader>nf'] = { frontmatter.generateFrontmatter },
--   },
--
--   autocmd = {
--     enabled = true,
--     ignore = {
--     },
--   },
-- })

-- local server = require('user.emanote_integration.server')
-- server.setup {
--   enabled = true,
--   open_url = function(url)
--     local cmd = ("firefox --new-window '%s'"):format(url)
--     vim.fn.system(cmd)
--   end
-- }

-- local telescopeIntegration = require('user.emanote_integration.telescope')
-- telescopeIntegration.setup()

-- local autocmd = require('user.lib.autocmd').autocmd
-- autocmd(
--   'EmanoteAutostart',
--   {
--     clear = true,
--
--     { 'VimEnter', {
--       callback = function()
--         if (vim.fn.getcwd() == vim.fn.getenv('NOTES_DIR')) then
--           vim.cmd 'EmanoteStartServer'
--           vim.cmd 'EmanoteOpenServer'
--         end
--       end
--     } }
--   }
-- )
