local make_feature = require('user.util.feature').make_feature_v2
local make_augroup = require('user.util.autocmd').make_augroup
local startupEvents = require('user.features.startup.events')

local M = make_feature({
  name = 'Dashboard',
  description = [[
  This feature opens a greeter on startup whenever there is
  no file to open from stdin or args.
  ]],

  dependencies = { startupEvents },

  dashboardCmd = function()
    vim.notify('No dashboard provider configured')
  end,

  apply = function(self)
    local augroup = make_augroup {
      {
        name = 'ShowDashboard',
        autocmds = {
          {
            events = 'User',
            opts = {
              pattern = startupEvents.events.Loaded,
              callback = function(event)
                local data = event.data or {}

                if data.hasStdin then return end

                local args = vim.fn.argv()

                -- Do show the dashboard when only one parameter is given and it is a directory
                -- (implied) Don't show the dashboard when editing a single file
                if #args == 1 and vim.fn.isdirectory(args[1]) == 0 then return end

                -- Don't show the dashboard when more than one file is given
                if #args > 1 then return end

                if type(self.dashboardCmd) == 'function' then
                  self.dashboardCmd()
                elseif type(self.dashboardCmd) == 'string' then
                  vim.cmd(self.dashboardCmd)
                end
              end
            }
          }
        }
      }
    }

    augroup:apply()
  end
})

return M
