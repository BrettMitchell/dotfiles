local make_feature = require('user.util.feature').make_feature_v2
local telescope = require('user.features.telescope')
local lsp = require('user.features.lsp')

local telescope_handlers = make_feature({
  name = 'LSP: Telescope Handlers',
  deps = { telescope, lsp },

  description = [[
  This feature adds Telescope handlers for LSP user interactions such as
  goto definition, find references, etc.
  ]],

  plugins = { {
    'https://github.com/gbrlsnchs/telescope-lsp-handlers.nvim',
    requires = { 'nvim-telescope/telescope.nvim' },
  } },

  apply = function()
    require('telescope').load_extension('lsp_handlers')
  end
})

return telescope_handlers
