local feat_util = require('user.util.feature')
local table_util = require('user.util.table')
local plugin_util = require('user.util.plugins.lazy')
local dap = require('user.features.dap')
local adapter_util = require('user.features.dap.adapters.util')

local M = {
  name = "DAP :: Adapter 'delve'",

  description = [[
  This feature adds utilities to aid in the installation and startup of
  the 'delve' debug adapter.
  ]],

  adapter_name = 'go',
  install_dir = dap.adapter_install_dir .. '/delve',
  ref = 'main',

  dap_retries = 3,
  dap_timeout = 5,
  dap_port = 8229,

  tsessd_additional_config = {
    mode = 'remote'
  }
}

function M.get_adapter()
  return function(callback, config)
    if config.request == "attach" then
      local adapter = {
        type = "server",
        host = config.host or '127.0.0.1',
        port = config.port or "${port}",
        options = {
          initialize_timeout_sec = M.dap_timeout,
        },
      }

      return callback(adapter)
    end

    local con_options = {
      max_retries = M.dap_retries,
      initialize_timeout_sec = M.dap_timeout,
    }

    if config.request == 'attach' and config.mode == 'remote' and config.host then
      callback({ type = 'server', host = config.host, port = config.port, options = con_options })
      return
    end

    local port = config.port or M.dap_port
    local host = config.host or '127.0.0.1'
    local addr = string.format('%s:%d', host, port)

    local function onread(err, data)
      if err then
        vim.notify('dlv exited with code ' + tostring(err), vim.log.levels.WARN)
      end

      if not data or data == '' then
        return
      end

      if data:find("couldn't start") then
        vim.schedule(function()
          utils.error(data)
        end)
      end

      vim.schedule(function()
        require('dap.repl').append(data)
      end)
    end

    local stdout = vim.loop.new_pipe(false)
    local stderr = vim.loop.new_pipe(false)
    local handle, pid_or_err

    handle, pid_or_err = vim.loop.spawn('dlv', {
      stdio = { nil, stdout, stderr },
      args = { 'dap', '-l', addr },
      initialize_timeout_sec = con_options.initialize_timeout_sec,
      detached = true,
    }, function(code)
      if code ~= 0 then
        vim.schedule(function()
          log('dlv exited', code)
          vim.notify(string.format('dlv exited with exit code: %d', code), 'warn')
          M.dap_port = M.dap_port + 1
        end)
      end

      _ = stdout and stdout:close()
      _ = stderr and stderr:close()
      _ = handle and handle:close()
      stdout = nil
      stderr = nil
      handle = nil
    end)
    assert(handle, 'Error running dlv: ' .. tostring(pid_or_err))
    stdout:read_start(onread)
    stderr:read_start(onread)

    vim.defer_fn(function()
      callback({ type = 'server', host = host, port = port, options = con_options })
    end, 1000)

    -- return {
    --   type = 'server',
    --   port = '${port}',
    --   args = {},
    --   build_flags = "",
    --   executable = {
    --     command = 'dlv',
    --     args = { 'dap', '-l', '127.0.0.1:${port}' },
    --   }
    -- }
  end
end

function M.get_dap_config(self)
  return {
    -- {
    --   name = "Launch " .. M.adapter_name,
    --   type = M.adapter_name,
    --   request = "launch",
    --   program = "${file}"
    -- },
    --
    -- {
    --   type = M.adapter_name,
    --   name = "Launch test",
    --   request = "launch",
    --   mode = "test",
    --   program = "${file}"
    -- },
    --
    -- -- works with go.mod packages and sub packages
    -- {
    --   type = M.adapter_name,
    --   name = "Launch test (go.mod)",
    --   request = "launch",
    --   mode = "test",
    --   program = "./${relativeFileDirname}"
    -- },

    {
      name = 'Attach ' .. M.adapter_name .. ' - Enter port',
      type = M.adapter_name,
      mode = 'remote',
      request = 'attach',
      port = function()
        local port = vim.fn.input('Enter a port: ')
        if port:len() == 0 then
          return require('dap').ABORT
        end
        return port
      end
    },

    {
      name = 'Attach ' .. M.adapter_name .. ' - Pick process',
      type = M.adapter_name,
      mode = 'remote',
      request = 'attach',
      port = function()
        return coroutine.create(function(dap_run_co)
          adapter_util.pick_listening_process('', function(proc_record)
            local port = require('dap').ABORT
            if proc_record ~= nil then
              port = proc_record.port
            end
            coroutine.resume(dap_run_co, port)
          end)
        end)
      end,
    },
  }
end

function M.init()
  local _dap = require('dap')
  _dap.adapters[M.adapter_name] = M.get_adapter()
  _dap.configurations.go = table_util.concat(
    _dap.configurations.go or {},
    M.get_dap_config()
  )
end

function M.setup(config)
  feat_util.assert_deps('DAP Adapter delve', { dap })
  feat_util.apply_config(config, M)
  if not M.install_dir then
    M.install_dir = dap.adapter_install_dir .. '/' .. M.adapter_name
  end
  dap.methods.add_installable(M.adapter_name, M.install)

  plugin_util.add_post_load_cb(function()
    M.init()
  end)
end

function M.remove(opts)
  vim.notify('Not supported', 'info')
end

function M.install()
  dap.hooks.do_hook('pre_install', { name = M.adapter_name })
  vim.system({ 'go', 'install', 'github.com/go-delve/delve/cmd/dlv@latest' }, {}, function(res)
    if res.code ~= 0 then
      vim.notify('Failed to install delve: \nSTDERR:\n' .. res.stderr .. '\nSTDOUT:\n' .. res.stdout, 'error')
      return
    end

    vim.notify('Installed delve', 'info')
    dap.hooks.do_hook('post_install', { name = M.adapter_name })
  end)
end

return M
