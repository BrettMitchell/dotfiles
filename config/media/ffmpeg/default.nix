{ nixpkgs, ... }:
{
  home.packages = with nixpkgs.stable; [
    ffmpeg_7-full
    ffmpegthumbnailer
  ];
}
