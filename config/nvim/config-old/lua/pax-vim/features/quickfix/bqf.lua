local make_feature = require('pax-vim.util.feature').make_feature

local config = {
  bqf_config = {}
}

local M = make_feature({
  name = 'Better Quick Fix',

  description = [[
  This feature configures the Better Quick Fix plugin.

  Some of the features included:
  - Quickfix list preview window
  - Quickfix list filtering
  - Bulk replace
  - Undo bulk action
  ]],

  plugins = {
    {
      'https://github.com/kevinhwang91/nvim-bqf',
      ft = 'qf',
      config = function ()
        require('bqf').setup(config.bqf_config)
      end
    }
  },

  config = config,

  -- keymaps = {},
})

return M
