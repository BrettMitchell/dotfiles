--[[

Feature design:

  Data points to track and support:
    Created date-time
    Updated date-time
    Title
    Description
    Tags

  Requirements:
    On save:
      If frontmatter exists for the current file
        Assign the current date to the 'updated' field. This must be in ISO 8601 format.
        This must not re-order the keys in the frontmatter
      If frontmatter does not exist
        Generate new values for all fields as follows
          created: <iso-8601-date-time>
          updated: <iso-8601-date-time>
          title: Transform filepath relative to root notes folder
          description: <empty-string>
          tags: <empty-list>

    On file moved:
      If the title corresponds to the file's path
        Regenerate the title
      If the title does not correspond to the file's path
        Leave the title as is
      In either case
        Update all relative links to other files within the moved note
        Update all relative links to the file in other notes

  Done:
    - [x] Parse and generate frontmatter programmatically
    - [x] Set up autocommand to keep frontmatter up-to-date
    - [x] Preserve cursor location when updating frontmatter

  Todo:
    - [ ] Support ignored files with a glob pattern
    - [ ] Transform file path into title
    - [ ] Provide consistent key ordering in YAML output: https://github.com/gvvaughan/lyaml/issues/45
    - [ ] Provide keymap to fully re-generate frontmatter on demand

--]]

local autocmd = require('user.lib.autocmd').autocmd
local registerKeymap = require('user.lib.map').registerKeyMap

local function dumpYamlSortedKeys(t)
  -- TODO
  local lyaml = require('lyaml')
  return lyaml.dump({ t })
end

local M = {}

M.defaultConfig = {
  enabled = true,

  keymap = {
    ['<leader>nf'] = { M.generateFrontmatter },
  },

  autocmd = {
    enabled = true,
    ignore = {},
  },
}

local function getFrontmatterLines()
  local currentBufferLines = vim.fn.getbufline('%', 0, '$')
  if (currentBufferLines[1] ~= '---') then
    vim.notify('Invalid frontmatter: First line must be "---"')
    return nil
  end

  local frontmatterLines = {}
  local i = 2
  while currentBufferLines[i] ~= '---' do
    frontmatterLines[i - 1] = currentBufferLines[i]
    i = i + 1
  end

  return frontmatterLines
end

local function getFrontmatterString()
  local frontmatterLines = getFrontmatterLines()
  if not frontmatterLines then return nil end

  local frontmatterString = ''
  for _, line in ipairs(frontmatterLines) do
    frontmatterString = frontmatterString .. '\n' .. line
  end

  return frontmatterString
end

local function getExistingFrontmatter()
  local lyaml = require('lyaml')
  local frontmatterString = getFrontmatterString()
  if not frontmatterString then return nil end

  local parsed = lyaml.load(frontmatterString)
  return parsed
end

local function getTitleFromFilename(filename)
  return filename
end

local function serializeFrontmatter(frontmatter)
  return dumpYamlSortedKeys(frontmatter)
end

local function removeExistingFrontmatter()
  local rawFrontmatter = getFrontmatterLines()
  if not rawFrontmatter then return end

  vim.cmd('0,' .. #rawFrontmatter + 2 .. 'd')
end

local function overwriteLine(lineIndex, newLine)
  vim.cmd(lineIndex .. 'd')
  vim.fn.append(lineIndex - 1, newLine)
end

local function writeFrontmatter(serializedFrontmatter)
  -- Explicitly mark previous position
  vim.cmd("mark `")

  removeExistingFrontmatter()

  local i = 1
  local lines = {}
  for line in serializedFrontmatter:gmatch '[^\n]+' do
    lines[i] = line
    i = i + 1
  end

  vim.fn.append(0, lines)

  -- lyaml terminates blocks with '...', but we want '---'
  overwriteLine(#lines, '---')

  -- Go back to the previously stored position
  vim.api.nvim_feedkeys("``", 'n', false)
end

function M.generateFrontmatter(shouldIgnoreFile)
  return function()
    local filename = vim.fn.expand('%')
    if shouldIgnoreFile(filename) then return end

    local formattedDate = os.date('%Y-%m-%dT%H:%M:%S')

    local existingFrontmatter = getExistingFrontmatter()
    existingFrontmatter = existingFrontmatter or {}
    existingFrontmatter.tags = existingFrontmatter.tags or {}
    existingFrontmatter.description = existingFrontmatter.description or ''
    existingFrontmatter.created = existingFrontmatter.created or formattedDate
    existingFrontmatter.updated = formattedDate

    existingFrontmatter.title = existingFrontmatter.title or getTitleFromFilename(filename)

    local serialized = serializeFrontmatter(existingFrontmatter)
    writeFrontmatter(serialized)
  end
end

function M.onEnter()
  M.generateFrontmatter()
end

-- function M.onMove()
--   M.generateFrontmatter()
--   M.updateLinksInFile()
--   M.updateLinksInOtherFiles()
-- end

M.autocmdId = nil

function M.registerAutocmd(ignorePatterns)
  ignorePatterns = ignorePatterns or {}

  local function shouldIgnoreFile(file)
    for _, pattern in ipairs(ignorePatterns) do
      if file:match(pattern) then return true end
    end
    return false
  end

  M.autocmdId = autocmd(
    'EmanoteFrontmatterOnSave',
    {
      clear = true,

      { 'BufWritePre', {
        pattern = '*.md',
        callback = M.generateFrontmatter(shouldIgnoreFile),
      } }
    }
  )
end

function M.unregisterAutocmd()
  if (M.autocmdId) then
    vim.api.nvim_del_autocmd(M.autocmdId)
  end
end

function M.setup(config)
  config = config or M.defaultConfig

  if config.enabled then
    if config.keymap then
      registerKeymap({ 'n' }, config.keymap)
    end

    if config.autocmd and config.autocmd.enabled then
      M.registerAutocmd(config.autocmd.ignore)
    end
  end
end

return M
