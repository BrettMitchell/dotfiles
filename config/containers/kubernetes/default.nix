{ nixpkgs, ... }:
{
  home.packages = with nixpkgs.stable; [
    kubernetes
    minikube
    kubernetes-helm
    kubectl
    kubectl-convert
    k9s
  ];
}
