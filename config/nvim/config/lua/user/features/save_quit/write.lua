local keymap = require('user.util.keymap')

local write = {
  name = 'Write Files',

  description = [[
  This feature adds easier keymaps for writing the files.
  ]],

  keymap = keymap {
    { 'n', '<space>w', '<cmd>w<cr>', { desc = 'Write current file' } },
    { 'n', '<space>W', '<cmd>wa<cr>', { desc = 'Write all files' } },
  }
}

function write.setup()
  write.keymap.apply()
end

return write
