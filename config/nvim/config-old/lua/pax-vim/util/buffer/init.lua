return {
  append_lines = require('pax-vim.util.buffer.append_lines'),
  get_buf_lines = require('pax-vim.util.buffer.get_buf_lines'),
  insert_text_at_cursor = require('pax-vim.util.buffer.insert_text_at_cursor'),
  make_lines = require('pax-vim.util.buffer.make_lines'),
  replace_text_range = require('pax-vim.util.buffer.replace_text_range'),
}
