return function(hex)
  if hex:sub(1, 1) == '#' then
    hex = hex:sub(2)
  end

  if #hex ~= 6 then
    return nil
  end

  local color = {}
  for i = 0, 2 do
    local val_str = hex:sub(2 * i + 1, 2 * i + 2)
    color[i + 1] = tonumber(val_str, 16)
  end

  return color
end
