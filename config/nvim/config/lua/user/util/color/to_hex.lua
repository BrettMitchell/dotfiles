return function(rgb)
  if type(rgb) ~= 'table' then
    return nil
  end

  local hex = '#'
  for _, color_value in ipairs(rgb) do
    local segment = string.format('%x', color_value)
    if #segment == 1 then
      segment = '0' .. segment
    end
    hex = hex .. segment
  end

  return hex
end
