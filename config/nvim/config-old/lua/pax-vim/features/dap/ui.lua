local make_feature = require('pax-vim.util.feature').make_feature_v2
local dap = require('pax-vim.features.dap')
local make_keymap = require('pax-vim.util.keymap')

local M = make_feature({
  name = 'DAP :: UI',
  dependencies = { dap },

  description = [[
  This feature provides a pre-built UI for interacting with the
  debugger in neovim.
  ]],

  plugins = {
    {
      'https://github.com/rcarriga/nvim-dap-ui',
      dependencies = { 'https://github.com/mfussenegger/nvim-dap' },
      config = function()
        local dapMdl = require("dap")
        local dapui = require("dapui")

        dapui.setup({
          controls = { enabled = false },

          icons = {
            collapsed = "",
            expanded = "",
            current_frame = "",
          },

          layouts = {
            {
              elements = {
                {
                  id = "repl",
                  size = 0.5
                },
              },
              position = "left",
              size = 50
            },

            {
              elements = {
                {
                  id = "stacks",
                  size = 0.5
                },
              },
              position = "left",
              size = 50
            },

            {
              elements = {
                {
                  id = "scopes",
                  size = 0.9
                },
                {
                  id = "watches",
                  size = 0.1
                },
              },
              position = "left",
              size = 50
            },
          },
        })

        local keymap = make_keymap {
          eval = { { 'n', 'x' }, ';e', function () dapui.eval() end, { desc = 'Evaluate expression' } },
          toggle_repl = { 'n', ';r', function () dapui.toggle({ layout = 1 }) end, { desc = 'Toggle debug REPL'} },
          toggle_call_stack = { 'n', ';c', function () dapui.toggle({ layout = 2 }) end, { desc = 'Toggle debug callstack panel'} },
          toggle_values = { 'n', ';v', function () dapui.toggle({ layout = 3 }) end, { desc = 'Toggle debug values panel'} },
        }

        dapMdl.listeners.after.event_initialized["dap-ui-keymaps"] = function()
          keymap.overlay()
        end

        dapMdl.listeners.before.event_terminated["dap-ui-keymaps"] = function()
          keymap.remove_overlay()
          dapui.close()
        end

        dapMdl.listeners.before.event_exited["dap-ui-keymaps"] = function()
          keymap.remove_overlay()
          dapui.close()
        end
      end
    }
  }
})

return M
