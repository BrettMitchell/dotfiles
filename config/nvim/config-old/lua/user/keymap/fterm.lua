local registerKeyMap = require('user.lib.map').registerKeyMap

local M = {}

function M.setup()
  -- Broot menu (through FTerm)
  registerKeyMap({ 'n', 't', 'i', 'v' }, {
    -- Think of 'b' as 'browse'
    ['<M-b>'] = {
      function()
        require('user.termSingleton').lf.cwd:toggle()
      end,
      'Toggle file browser'
    },
  })
end

return M
