local table_util = require('user.util.table')
local buf_util = require('user.util.buffer')
local file_util = require('user.util.file')
local string_util = require('user.util.string')
local keymap = require('user.util.keymap')
local context = require('user.features.notes.context')

-------------
-- Helpers --
-------------

local function get_note_frontmatter_lines(file_lines)
  local frontmatter_lines = {}
  local i = 2

  while i < #file_lines and file_lines[i] ~= '---' do
    frontmatter_lines[i - 1] = file_lines[i]
    i = i + 1
  end

  return frontmatter_lines
end

local function get_note_body_lines(file_lines)
  local found_delim = 0
  local end_fronmatter = 0
  for i, line in ipairs(file_lines) do
    if line == '---' then
      found_delim = found_delim + 1
      if found_delim == 2 then
        end_fronmatter = i
        break
      end
    end
  end

  return { unpack(file_lines, end_fronmatter + 2) }
end

local function parse_frontmatter_lines(frontmatter_lines)
  local frontmatter_string = table_util.join(frontmatter_lines, '\n')
  local parsed = require('lyaml').load(frontmatter_string)
  return parsed
end

------------
-- Public --
------------

local M = {}

-- Buffer API

function M.get_buf_frontmatter(opts)
  opts = opts or {}

  local buf_lines = buf_util.get_buf_lines({ bufnr = opts.bufnr or 0 })
  if not buf_lines or buf_lines[1] ~= '---' then
    return nil
  end

  local frontmatter_lines = get_note_frontmatter_lines(buf_lines)
  return parse_frontmatter_lines(frontmatter_lines)
end

function M.get_buf_body(opts)
  local buf_lines = buf_util.get_buf_lines({ bufnr = opts.bufnr or 0 })
  if not buf_lines then return nil end

  local buf_body = get_note_body_lines(buf_lines)

  return buf_body
end

function M.write_buf_body(opts)
  opts = opts or {}

  local buf_lines = buf_util.get_buf_lines({ bufnr = opts.bufnr or 0 })
  local frontmatter_lines = get_note_frontmatter_lines(buf_lines)
  local body = buf_util.make_lines(opts.body)

  vim.api.nvim_buf_set_lines(
    opts.bufnr or 0,
    #frontmatter_lines + 3,
    -1,
    false,
    body
  )
end

function M.write_buf_frontmatter(opts)
  opts = opts or {}

  local buf_lines = buf_util.get_buf_lines({ bufnr = opts.bufnr or 0 })
  local current_frontmatter_lines = get_note_frontmatter_lines(buf_lines)
  local dumped_frontmatter = M.dump_frontmatter({
    frontmatter = opts.frontmatter,
    split = true,
  })

  vim.api.nvim_buf_set_lines(
    opts.bufnr or 0,
    0,
    #current_frontmatter_lines + 3,
    false,
    dumped_frontmatter
  )
end

function M.map_buf_frontmatter(opts)
  opts = opts or {}

  if type(opts.fn) ~= 'function' then
    vim.notify("Parameter 'fn' must be a function: " .. vim.inspect(opts.fn), 'error')
    return
  end

  local frontmatter = M.get_buf_frontmatter(opts)
  local updated_frontmatter = opts.fn(frontmatter) or frontmatter

  M.write_buf_frontmatter({
    bufnr = opts.bufnr,
    frontmatter = updated_frontmatter,
  })
end

function M.load_buf_keybindings(opts)
  opts = opts or {}

  local frontmatter = M.get_buf_frontmatter(opts)
  if not frontmatter then return end

  local bindings = frontmatter.bindings or {}

  local map_spec = {}

  for key, cmd in pairs(bindings) do
    local normalizedCmd = cmd

    if type(cmd) == 'table' then
      if cmd.keys then
        normalizedCmd = ':normal! ' .. t(cmd.keys)
      end

      if cmd.lua then
        normalizedCmd = 'lua ' .. cmd.lua
      end

      if cmd.cmd then
        normalizedCmd = cmd
      end
    end

    if type(normalizedCmd) ~= 'string' then
      vim.notify('Unsupported command config: ' .. vim.inspect(cmd))
      return
    end

    if normalizedCmd:sub(1, 1) == ':' then
      normalizedCmd = normalizedCmd:sub(2)
    end

    -- Remove any newlines, as vim's command mode doesn't play nicely with them
    normalizedCmd = string.gsub(normalizedCmd, '[\n]+', '')

    table.insert(map_spec, { 'n', key, function() vim.cmd(normalizedCmd) end, { desc = 'Autoloaded keybinding' } })
  end

  keymap(map_spec).apply(vim.fn.bufnr('%'))
end

-- File API

function M.get_file_frontmatter(opts)
  opts = opts or {}

  local file_lines = file_util.get_file_lines({ filename = opts.filename, silent = true })
  if not file_lines or file_lines[1] ~= '---' then
    return nil
  end

  local frontmatter_lines = get_note_frontmatter_lines(file_lines)
  return parse_frontmatter_lines(frontmatter_lines)
end

function M.get_file_body(opts)
  local file_lines = file_util.get_file_lines({ filename = opts.filename })
  if not file_lines then return nil end

  local file_body = get_note_body_lines(file_lines)
  return file_body
end

function M.write_file_frontmatter(opts)
  opts = opts or {}

  local file_body = M.get_file_body(opts)
  if not file_body then return end
  local dumped_frontmatter = M.dump_frontmatter({
    frontmatter = opts.frontmatter,
    split = true,
  })

  local new_file_lines = table_util.concat(dumped_frontmatter, file_body)
  vim.fn.writefile(new_file_lines, opts.filename)
end

function M.write_file_body(opts)
  opts = opts or {}

  local file_lines = file_util.get_file_lines({ filename = opts.filename })
  local frontmatter_lines = get_note_frontmatter_lines(file_lines)
  local new_file_lines = table_util.concat(
    { '---' },
    frontmatter_lines
  )
  new_file_lines = table_util.concat(
    new_file_lines,
    { '---' }
  )
  new_file_lines = table_util.concat(
    new_file_lines,
    buf_util.make_lines(opts.body)
  )

  vim.fn.writefile(new_file_lines, opts.filename)
end

function M.map_file_frontmatter(opts)
  opts = opts or {}

  if type(opts.fn) ~= 'function' then
    vim.notify("Parameter 'fn' must be a function: " .. vim.inspect(opts.fn), 'error')
    return
  end

  local frontmatter = M.get_file_frontmatter(opts)
  local updated_frontmatter = opts.fn(frontmatter) or frontmatter

  M.write_file_frontmatter({
    filename = opts.filename,
    frontmatter = updated_frontmatter,
  })
end

-- General API

function M.generate_frontmatter(opts)
  opts = opts or {}

  local formattedDate = os.date('%Y-%m-%dT%H:%M:%S')

  local tags = opts.tags or {}
  if not opts.tags then
    local context_tags = context:get_auto_tags()
    for _, tag in ipairs(context_tags) do
      if not table_util.includes(tag, tags) then
        table.insert(tags, tag)
      end
    end
  end

  return table_util.merge.deepMergeRight(opts, {
    title = opts.title or vim.fn.expand('%'),
    description = opts.description or '',
    tags = tags,
    created = opts.created or formattedDate,
    updated = formattedDate,
  })
end

function M.dump_frontmatter(opts)
  opts = opts or {}

  local dumped_yaml = require('lyaml').dump({ opts.frontmatter })
  -- lyaml terminates blocks with '...', but we want '---'
  dumped_yaml = dumped_yaml:gsub('%.%.%.\n$', '---\n')
  -- lyaml dumps all empty keys (objects and arrays alike) as empty objects (`{}`)
  -- quarto refuses to interact with frontmatter with tags set to an empty object,
  -- so we satisfy that requirement here.
  dumped_yaml = dumped_yaml:gsub('tags: {}\n', 'tags: []\n')

  if opts.split then
    return string_util.split(dumped_yaml, '\n')
  end

  return dumped_yaml
end

return M
