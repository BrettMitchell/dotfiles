local table_util = require('pax-vim.util.table')
local keymap = require('pax-vim.util.keymap')

local get_clients = vim.lsp.get_clients or vim.lsp.get_active_clients

--[[
--
-- Maintain current active function overload index.
-- Increment and refresh docs with <C-n>.
-- Decrement and refresh docs with <C-p>.
-- Wrap in both cases.
-- Reset index on cursor move. (? maybe there's a better condition for this)
--
--]]

local overload_index = 1
local last_max_overload_index = 1

local function render_single_signature(signature, opts)
  opts = opts or {}
  -- TODO: Unsupported at this time
  -- opts.highlight_params = opts.highlight_params or {}

  local lines = {}

  -- if signature.parameters then
  --   lines = table_util.concat(lines, {
  --     vim.inspect(signature.parameters)
  --   })
  -- end

  if signature.label then
    lines = table_util.concat(lines, {
      -- '---',
      signature.label,
    })
  end

  if type(signature.documentation) == 'string' then
    lines = table_util.concat(lines, {
      signature.documentation,
    })
  elseif type(signature.documentation) == 'table' then
    lines = table_util.concat(lines, {
      signature.documentation.value,
    })
  end

  return lines
end

local function render_signature_help_response(lsp_result, opts)
  opts = opts or {}
  opts.highlight_params = opts.highlight_params or {}
  opts.show_signatures = opts.show_signatures or { 1 }

  local lines = {}
  for i, signature in ipairs(lsp_result.signatures) do
    if table_util.includes(i, opts.show_signatures) then
      if #lines > 0 then
        lines = table_util.concat(lines, { '---' })
      end
      lines = table_util.concat(lines, render_single_signature(signature, opts))
      -- lines[1] = vim.lsp.util.stylize_markdown(lines[1])
      -- lines[1] = lines[1] .. ' - ' .. tostring(i) .. '/' .. tostring(#lsp_result.signatures)
      lines = table_util.concat(lines, {
        tostring(i) .. '/' .. tostring(#lsp_result.signatures),
      })
    end
  end

  return lines
end

local function str_utfindex(line, index, encoding)
  if not line or #line < index then
    return index
  end

  if encoding == 'utf-8' then
    return index
  end

  encoding = encoding or 'utf-16'

  if encoding == 'utf-16' or encoding == 'utf-16' then
    local col32, col16 = vim.str_utfindex(line, index)
    return encoding == 'utf-32' and col32 or col16
  end

  error('Invalid encoding: ' .. vim.inspect(encoding))
end

local function buf_request_all(bufnr, method, params_fn, handler)
  local results = {}
  local exp_reponses = 0
  local reponses = 0

  for _, client in pairs(get_clients({ bufnr = bufnr })) do
    if client.supports_method(method, { bufnr = bufnr }) then
      exp_reponses = exp_reponses + 1
      client.request(method, params_fn(client), function(_, result)
        reponses = reponses + 1
        results[client] = result
        if reponses >= exp_reponses then
          handler(results)
        end
      end, bufnr)
    end
  end
end

local function create_params(bufnr, row, col)
  return function(client)
    local offset_encoding = client.offset_encoding
    local ok, lines = pcall(vim.api.nvim_buf_get_lines, bufnr, row, row + 1, true)

    if not ok then
      print(debug.traceback(string.format('ERROR: row %d is out of range: %s', row, lines)))
    end

    local ccol = lines and str_utfindex(lines[1], col, offset_encoding) or col

    return {
      textDocument = { uri = vim.uri_from_bufnr(bufnr) },
      position = {
        line = row,
        character = ccol
      }
    }
  end
end

local function run(opts, done)
  local row, col = opts.pos[1] - 1, opts.pos[2]

  buf_request_all(
    opts.bufnr,
    'textDocument/signatureHelp',
    create_params(opts.bufnr, row, col),
    function(results)
      for _, result in pairs(results or {}) do
        if result == nil or result.signatures == nil or result.signatures[1] == nil then
          return
        end

        if not result.signatures then
          return
        end

        if overload_index < 1 or overload_index > #result.signatures then
          overload_index = 1
        end

        last_max_overload_index = #result.signatures

        local lines = render_signature_help_response(result, {
          show_signatures = { overload_index }
        })

        return done({ lines = lines, filetype = 'markdown' })
      end

      return done(false)
    end
  )
end

local lsp_signature_help_provider = {
  name = 'LSP Signature Help',
  priority = 10,

  enabled = function(bufnr)
    for _, client in pairs(get_clients()) do
      if client.supports_method('textDocument/signatureHelp', { bufnr = bufnr }) then
        return true
      end
    end
    return false
  end,

  on_render = function(bufnr, _, opts)
    --[[
    -- TODO: Make these keybindings resize the window on the fly
    --       Possibly make these activate in the parent buffer as well, and deactivate when the window closes
    --]]

    local function next_overload()
      overload_index = overload_index + 1
      run(opts, function(res)
        if res then
          vim.bo.modifiable = true
          vim.api.nvim_buf_set_lines(bufnr, 0, -1, true, res.lines)
          vim.bo.modifiable = false
        end
      end)
    end

    local function previous_overload()
      overload_index = overload_index - 1
      if overload_index < 1 then
        overload_index = last_max_overload_index
      end
      run(opts, function(res)
        if res then
          vim.bo.modifiable = true
          vim.api.nvim_buf_set_lines(bufnr, 0, -1, true, res.lines)
          vim.bo.modifiable = false
        end
      end)
    end

    local lsp_overload_keymap = keymap {
      next_overload = { { 'n', 'i' }, '<C-n>', next_overload, { desc = 'Next function overload' } },
      previous_overload = { { 'n', 'i' }, '<C-p>', previous_overload, { desc = 'Previous function overload' } },
    }

    lsp_overload_keymap.apply(bufnr)
  end,

  execute = function(opts, done)
    run(opts, function(res)
      if vim.api.nvim_get_current_buf() ~= opts.bufnr then
        return done(false)
      end
      done(res)
    end)
  end
}

return lsp_signature_help_provider
