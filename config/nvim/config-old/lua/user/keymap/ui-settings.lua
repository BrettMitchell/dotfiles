local registerKeyMap = require('user.lib.map').registerKeyMap

local function toggleLineNumbers()
  vim.cmd('set number! relativenumber!')
  vim.notify('Toggled line numbers')
end

local M = {}

function M.setup()
  -- Visual shortcuts
  registerKeyMap({ 'n' }, {
    ['<space>u'] = {
      g = {
        g = { require('user.gitsigns').toggle, 'Toggle all gitsigns features' },
        s = { require('user.gitsigns').toggleGutterSigns, 'Toggle git signs' },
        b = { require('user.gitsigns').toggleCurrentLineBlame, 'Toggle git blame' },
      },

      n = { toggleLineNumbers, 'Toggle line numbers' },

      s = {
        require('user.statusline').toggle,
        'Toggle status line'
      },
    }
  })

  -- registerKeyMap({ 'v' }, {
  --   ['<A-z>'] = { "<cmd>'<,'>TZNarrow<enter>", 'Zen mode on selection' },
  -- })
end

return M
