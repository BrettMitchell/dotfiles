local make_feature = require('pax-vim.util.feature').make_feature_v2
local augroup = require('pax-vim.util.autocmd').make_augroup

local frontmatter = require('pax-vim.features.notes.frontmatter.v2')

local events = {
  FrontmatterLoaded = 'FrontmatterLoaded',
}

local frontmatter_loaded = make_feature({
  name = 'Frontmatter Loaded User Autocommand',

  description = [[
  This feature provides a user autocommand which parses the frontmatter
  of markdown files on BufEnter, and which emits the parsed frontmatter
  in a User event.
  ]],

  events = events,

  augroups = augroup { {
    name = 'ParseFrontmatter',
    autocmds = { {
      events = 'BufEnter',
      opts = {
        callback = function()
          local ft = vim.bo.filetype
          if ft == 'markdown' or ft == 'quarto' then
            local filename = vim.fn.expand('%:p')
            local parsed_frontmatter = frontmatter.get_file_frontmatter({ filename = filename })

            local event_data = {
              filename = filename,
              frontmatter = parsed_frontmatter
            }

            vim.api.nvim_exec_autocmds('User', {
              pattern = events.FrontmatterLoaded,
              data = event_data,
            })
          end
        end
      }
    } }
  } },

  apply = function(self)
    if require('zk.util').notebook_root(vim.fn.getcwd()) ~= nil then
      self.augroups:apply()
    end
  end
})

return frontmatter_loaded
