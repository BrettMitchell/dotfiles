{
  home.file.".profile".source = ./files/.profile;

  home.file.".bash_profile".source = ./files/.bash_profile;
  home.file.".bashrc".source = ./files/.bashrc;

  home.file.".zprofile".source = ./files/.zprofile;
  home.file.".zshenv".source = ./files/.zshenv;
  home.file.".zshrc".source = ./files/.zshrc;
  home.file.".config/zsh".source = ./files/zsh;
}
