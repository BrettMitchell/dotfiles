#!/usr/bin/env bash

rm_safe "$HOME/.config/broot/conf.hjson"
rm_safe "$HOME/.config/broot/conf.keys.hjson"
rm_safe "$HOME/.config/broot/conf.lf.hjson"
rm_safe "$HOME/.config/broot/conf.shell.hjson"
rm_safe "$HOME/.config/broot/conf.template.hjson"
