local add_plugins = require('pax-vim.util.plugins.lazy').add_plugins

local M = {
  feature = {
    name = 'Completion Source - LaTeX Symbols',

    description = [[
    This feature adds support for LaTeX symbol completions
    ]],
  },

  symbols_source_definition = {
    name = 'latex_symbols',
    option = {
      strategy = 2,
    }
  }
}

function M.setup()
  add_plugins({ { 'https://github.com/kdheepak/cmp-latex-symbols' } })
end

return M

