local make_feature = require('user.util.feature').make_feature_v2

local syntax_highlighting_hjson = make_feature({
  name = 'Syntax Highlighting: HJSON',

  description = [[
  This feature adds syntax higlighing for HJSON files.
  ]],

  plugins = { {
    'https://github.com/hjson/vim-hjson',
  } },
})

return syntax_highlighting_hjson
