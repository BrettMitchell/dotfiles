local feat_util = require('user.util.feature')
local plugin_util = require('user.util.plugins.lazy')
local dap = require('user.features.dap')
local keymap = require('user.util.keymap')

local DAPUI_CONFIG = {
  controls = { enabled = false },

  icons = {
    collapsed = "",
    expanded = "",
    current_frame = "",
  },

  layouts = {
    {
      elements = {
        {
          id = "repl",
          size = 0.5
        },
      },
      position = "left",
      size = 50
    },

    {
      elements = {
        {
          id = "stacks",
          size = 0.5
        },
      },
      position = "left",
      size = 50
    },

    {
      elements = {
        {
          id = "scopes",
          size = 0.9
        },
        {
          id = "watches",
          size = 0.1
        },
      },
      position = "left",
      size = 50
    },
  },
}

local M = {
  name = 'DAP :: UI',

  description = [[
  This feature provides a pre-built UI for interacting with the
  debugger in neovim.
  ]],

  keymap = keymap {
    eval = { { 'n', 'x' }, ';e', function() require('dapui').eval() end, { desc = 'Evaluate expression' } },
    toggle_repl = { 'n', ';r', function() require('dapui').toggle({ layout = 1 }) end, { desc = 'Toggle debug REPL' } },
    toggle_call_stack = { 'n', ';c', function() require('dapui').toggle({ layout = 2 }) end, { desc = 'Toggle debug callstack panel' } },
    toggle_values = { 'n', ';v', function() require('dapui').toggle({ layout = 3 }) end, { desc = 'Toggle debug values panel' } },

    show_frames = {
      'n',
      ';f',
      function()
        local widgets = require('dap.ui.widgets')
        widgets.centered_float(widgets.frames)
      end,
      { desc = 'Navigate debug callstack' },
    },

    show_scopes = {
      'n',
      ';S',
      function()
        local widgets = require('dap.ui.widgets')
        widgets.centered_float(widgets.scopes)
      end,
      { desc = 'Navigate debug callstack' },
    },
  }
}

function M.setup()
  feat_util.assert_deps('DAP UI', { dap })
  plugin_util.add_plugins({
    {
      'https://github.com/rcarriga/nvim-dap-ui',
      event = 'VeryLazy',
      dependencies = {
        'https://github.com/mfussenegger/nvim-dap',
        'https://github.com/nvim-neotest/nvim-nio',
      },
      config = function()
        local dapMdl = require("dap")
        local dapui = require("dapui")

        dapui.setup(DAPUI_CONFIG)

        dapMdl.listeners.after.event_initialized.dap_ui_keymap = function()
          M.keymap.overlay()
        end

        dapMdl.listeners.before.event_terminated.dap_ui_keymap = function()
          M.keymap.remove_overlay()
          dapui.close()
        end

        dapMdl.listeners.before.event_exited.dap_ui_keymap = function()
          M.keymap.remove_overlay()
          dapui.close()
        end
      end
    }
  })
end

return M
