local make_feature = require('user.util.feature').make_feature
local priority = require('user.util.priority')

local state = {
  activate_on_load = false
}

local M = make_feature({
  name = 'Colorschema: Tokyo Night',

  description = [[
  This feature adds the tokyonight theme
  ]],

  plugins = {
    {
      lazy = false,
      priority = priority.theme,
      'https://github.com/folke/tokyonight.nvim',
      config = function ()
        if state.activate_on_load then
          vim.cmd [[ colorscheme tokyonight ]]
        end
      end
    }
  },
})

M.methods = {
  activate = function ()
    state.activate_on_load = true
  end
}

return M
