local string_util = require('user.util.string')
local table_util = require('user.util.table')
local escape_filepath = require('user.util.file').escape_filepath

local M = {}

-- function M.send_text(text, target)
-- end
--

function M.get_tmux_value(value_name)
  local out = vim.fn.system({ 'tmux', 'display-message', '-p', '#{' .. value_name .. '}' })
  local out_split = string_util.split(out, '\n')
  return out_split[1]
end

function M.get_tsessd_state_dir()
  local session_path = M.get_tmux_value('session_path')
  if not session_path then
    return nil
  end

  local suffix = '/tsessd/' .. escape_filepath(session_path)
  local xdg_data_dir = vim.fn.getenv('XDG_DATA_DIR')
  local home = vim.fn.getenv('HOME')

  if xdg_data_dir ~= vim.NIL then
    return xdg_data_dir .. suffix
  elseif home ~= vim.NIL then
    return home .. '/.local/share' .. suffix
  end

  return nil
end

local function run_in_tmux(cmd_prefix, cmd, dir)
  local original_window = M.get_tmux_value('window_id')
  local original_pane = M.get_tmux_value('pane_id')

  dir = dir or vim.fn.getcwd()
  local full_cmd = table_util.concat(cmd_prefix, { '-c', dir })
  full_cmd = table_util.concat(full_cmd, cmd)
  vim.fn.jobstart(full_cmd)

  local new_window = M.get_tmux_value('window_id')
  local new_pane = M.get_tmux_value('pane_id')

  M.focus({
    window_id = original_window,
    pane_id = original_pane,
  })

  return {
    window_id = new_window,
    pane_id = new_pane,
  }
end

function M.focus(opts)
  opts = opts or {}
  if opts.window_id then
    vim.fn.system({ 'tmux', 'select-window', '-t', opts.window_id })
  end
  if opts.pane_id then
    vim.fn.system({ 'tmux', 'select-pane', '-t', opts.pane_id })
  end
end

function M.run_hsplit(cmd, dir)
  return run_in_tmux({ 'tmux', 'split-window', '-h' }, cmd, dir)
end

function M.run_vsplit(cmd, dir)
  return run_in_tmux({ 'tmux', 'split-window', '-v' }, cmd, dir)
end

function M.run_new_window(cmd, dir)
  return run_in_tmux({ 'tmux', 'new-window' }, cmd, dir)
end

function M.running_in_tmux()
  return vim.fn.getenv('TMUX') ~= vim.NIL
end

function M.kill_pane(pane_id)
  vim.fn.system({ 'tmux', 'kill-pane', '-t', pane_id })
end

function M.pick_pane()
  local output = vim.fn.system({ 'tmux', 'display-panes', '-d', '0', "display -p '%%'"})
  local pane = string_util.trim(output)
  if #pane == 0 then return nil end
  if pane:sub(1, 1) ~= '%' then pane = '%' .. pane end
  return pane
end

function M.list_panes()
  local output = vim.fn.system({ 'tmux', 'list-panes' })
  output = string_util.trim(output)
  return string_util.split(output, '\n')
end

return M
