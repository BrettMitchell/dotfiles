local make_feature = require('user.util.feature').make_feature

local M = make_feature({
  name = 'Macro Keybindings',

  description = [[
  This feature adds keybindings to make macros easier to get at.

  It creates a command pallette of macro keys available via
  a double leader prefix.
  ]],

  keymaps = {
    { 'n', 'q', '@', { desc = 'Play back register' } },
    { 'n', 'Q', 'q', { desc = 'Record macro' } },
  }
})

return M
