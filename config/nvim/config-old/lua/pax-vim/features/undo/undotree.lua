local make_feature = require('pax-vim.util.feature').make_feature

local M = make_feature({
  name = 'Undotree',

  description = [[
  This feature adds the undotree plugin. This provides an easy way to
  visualize and interact with the history tree of a file.
  ]],

  plugins = {
    {
      'https://github.com/mbbill/undotree',
    }
  },

  set_config = function(userConfig)
    userConfig = userConfig or {}

    local function set_global(key)
      if userConfig[key] ~= nil then
        vim.g[key] = userConfig[key]
      end
    end

    --[[
       Window layout
       style 1
       +----------+------------------------+
       |          |                        |
       |          |                        |
       | undotree |                        |
       |          |                        |
       |          |                        |
       +----------+                        |
       |          |                        |
       |   diff   |                        |
       |          |                        |
       +----------+------------------------+
       Style 2
       +----------+------------------------+
       |          |                        |
       |          |                        |
       | undotree |                        |
       |          |                        |
       |          |                        |
       +----------+------------------------+
       |                                   |
       |   diff                            |
       |                                   |
       +-----------------------------------+
       Style 3
       +------------------------+----------+
       |                        |          |
       |                        |          |
       |                        | undotree |
       |                        |          |
       |                        |          |
       |                        +----------+
       |                        |          |
       |                        |   diff   |
       |                        |          |
       +------------------------+----------+
       Style 4
       +-----------------------++----------+
       |                        |          |
       |                        |          |
       |                        | undotree |
       |                        |          |
       |                        |          |
       +------------------------+----------+
       |                                   |
       |                            diff   |
       |                                   |
       +-----------------------------------+
    --]]
    set_global('undotree_WindowLayout')

    set_global('undotree_ShortIndicators') -- e.g. using 'd' instead of 'days' to save some space.

    set_global('undotree_SplitWidth') -- undotree window width

    set_global('undotree_DiffpanelHeight') -- diff window height

    set_global('undotree_DiffAutoOpen') -- auto open diff window

    -- if set, let undotree window get focus after being opened, otherwise
    -- focus will stay in current window.
    set_global('undotree_SetFocusWhenToggle')

    set_global('undotree_TreeNodeShape') -- tree node shape.

    set_global('undotree_TreeVertShape') -- tree vertical shape.

    set_global('undotree_TreeSplitShape') -- tree split shape.

    set_global('undotree_TreeReturnShape') -- tree return shape.

    set_global('undotree_DiffCommand')

    set_global('undotree_RelativeTimestamp') -- relative timestamp

    set_global('undotree_HighlightChangedText') -- Highlight changed text

    set_global('undotree_HighlightChangedWithSign') -- Highlight changed text using signs in the gutter

    -- Highlight linked syntax type.
    -- You may chose your favorite through ":hi" command
    set_global('undotree_HighlightSyntaxAdd')

    set_global('undotree_SplitLocation') -- Deprecates the old style configuration.

    set_global('undotree_HelpLine') -- Show help line

    set_global('undotree_CursorLine') -- Show cursorline
  end,

  keymaps = {
    { { 'n' }, '<C-u>', '<cmd>UndotreeToggle<enter>', { desc = 'Toggle undotree' } },
  },
})

return M

