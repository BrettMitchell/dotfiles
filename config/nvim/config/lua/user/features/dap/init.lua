local feat_util = require('user.util.feature')
local plugin_util = require('user.util.plugins.lazy')
local table_util = require('user.util.table')
local make_hook_registry = require('user.util.hooks').make_hook_registry
local make_keymap = require('user.util.keymap')

-- local which_key = require('user.features.ui.whichkey')
local pick_option = require('user.features.input.pick_option')

local state = {
  installable = {}
}

local hooks = make_hook_registry({
  'on_load',
  'pre_install',
  'post_install',
  'pre_attach',
  'post_attach'
})

local function get_default_install_dir()
  return vim.fn.stdpath('data') .. '/dap/adapters'
end

-- Lifted from telescope-dap
-- This andthe breakpoint picker are the only things I need from that plugin,
-- and it overrides the internal 'dap.ui.pick_one' function, which is no bueno.
local pick_configuration = function(opts)
  local actions      = require 'telescope.actions'
  local action_state = require 'telescope.actions.state'
  local finders      = require 'telescope.finders'
  local pickers      = require 'telescope.pickers'
  local previewers   = require 'telescope.previewers'
  local conf         = require('telescope.config').values
  local dap          = require('dap')


  opts = opts or {}
  local filter = opts.filter or function(_, _) return true end
  local results = {}
  for lang, configs in pairs(dap.configurations) do
    local filtered = table_util.filter(configs, function(config)
      return filter(lang, config)
    end)
    vim.list_extend(results, filtered)
  end

  for provider_name, provider in pairs(dap.providers.configs) do
    if provider_name ~= 'dap.global' then
      local configs = provider({})
      local filtered = table_util.filter(configs, function(config)
        return filter(nil, config)
      end)
      vim.list_extend(results, filtered)
    end
  end

  if vim.tbl_isempty(results) then
    vim.notify('No applicable configurations found', 'info')
    return
  end

  pickers.new(opts, {
    prompt_title    = 'Dap Configurations',
    sorter          = conf.generic_sorter(opts),
    previewer       = previewers.display_content.new(opts),

    finder          = finders.new_table {
      results = results,
      entry_maker = function(entry)
        return {
          value = entry,
          display = entry.type .. ': ' .. entry.name,
          ordinal = entry.type .. ': ' .. entry.name,
          preview_command = function(entry, bufnr)
            local output = vim.split(vim.inspect(entry.value), '\n')
            vim.api.nvim_buf_set_lines(bufnr, 0, -1, true, output)
          end
        }
      end,
    },

    attach_mappings = function(prompt_bufnr)
      actions.select_default:replace(function()
        local selection = action_state.get_selected_entry()
        actions.close(prompt_bufnr)
        if selection.value.request == "custom" then
          vim.cmd(selection.value.command)
        else
          dap.run(selection.value)
        end
      end)
      return true
    end,
  }):find()
end

local find_breakpoints = function(opts)
  local builtin = require 'telescope.builtin'
  local dap     = require('dap')


  opts              = opts or {}
  opts.prompt_title = 'Dap Breakpoints'
  dap.list_breakpoints(false)
  builtin.quickfix(opts)
end

local M = {
  name = 'DAP',
  description = [[
  This feature provides debugging facilities. It does not add
  support for any specific language; individual features exist
  for supported languages.
  ]],

  adapter_install_dir = get_default_install_dir(),
  signs = {
    breakpoint = nil,
    stopped = nil,
  },
};

M.hooks = hooks
M.state = state

function M.setup(config)
  feat_util.apply_config(config, M)
  feat_util.check_deps({ pick_option })
  plugin_util.add_plugins({
    {
      -- dir = '~/projects/nvim-plugins/nvim-dap',
      'https://github.com/mfussenegger/nvim-dap',
      event = 'VeryLazy',
      config = function()
        local dap = require('dap')
        dap.listeners.before.event_initialized.vim_dap_session_keymaps = function()
          M.dap_session_keymaps.overlay()
        end
        dap.listeners.before.event_terminated.vim_dap_session_keymaps = function()
          M.dap_session_keymaps.remove_overlay()
        end
        dap.listeners.before.event_exited.vim_dap_session_keymaps = function()
          M.dap_session_keymaps.remove_overlay()
        end
      end
    },
  })

  M.dap_global_keymaps.apply()

  -- which_key.methods.label_group('<space>d', 'Debugging')

  if M.signs.breakpoint then
    vim.fn.sign_define('DapBreakpoint', {
      text = M.signs.breakpoint.icon,
      texthl = M.signs.breakpoint.texthl,
      linehl = M.signs.breakpoint.linehl,
      numhl = M.signs.breakpoint.numhl,
    })
  end

  if M.signs.stopped then
    vim.fn.sign_define('DapStopped', {
      text = M.signs.stopped.icon,
      texthl = M.signs.stopped.texthl,
      linehl = M.signs.stopped.linehl,
      numhl = M.signs.stopped.numhl,
    });
  end

  hooks.do_hook('on_load')
end

-- TODO: Add augroup to enable and disable keymaps when attaching and detaching from
-- a debug session
-- augroups = {},

M.methods = {
  ensure_install_dir = function()
    vim.fn.system({ 'mkdir', '-p', M.adapter_install_dir })
  end,

  add_installable = function(label, install)
    state.installable[label] = install
  end,

  install_adapter = function()
    local labels = {}
    for k, _ in pairs(state.installable) do
      table.insert(labels, k)
    end

    pick_option.methods.pick_from_map({
      header = 'Select a debug adapter to install:',
      items = state.installable
    }, function(selected_adapters)
      if not selected_adapters or #selected_adapters == 0 then
        vim.notify('No adapter selected', 'info')
        return
      end

      for _, selected_adapter in ipairs(selected_adapters) do
        local install = selected_adapter.item
        install()
      end
    end)
  end,

  -- Breakpoints
  toggle_breakpoint = function() require('dap').toggle_breakpoint() end,
  clear_breakpoints = function() require('dap').clear_breakpoints() end,
  add_conditional_breakpoint = function()
    require('dap').set_breakpoint(vim.fn.input('Breakpoint condition: '), nil, nil)
  end,
  add_log_point = function()
    require('dap').set_breakpoint(nil, nil, vim.fn.input('Log point message: '))
  end,

  find_breakpoints = find_breakpoints,

  -- Session commands
  attach = function(opts)
    -- require('dap').continue(opts)
    pick_configuration({
      filter = function(lang, config)
        local bufnr = vim.api.nvim_get_current_buf()
        local filetype = vim.bo[bufnr].filetype
        return (lang == filetype or lang == nil) and config.request == 'attach'
      end
    })
  end,

  launch = function()
    pick_configuration({
      filter = function(lang, config)
        local bufnr = vim.api.nvim_get_current_buf()
        local filetype = vim.bo[bufnr].filetype
        return (lang == filetype or lang == nil) and config.request == 'launch'
      end
    })
  end,

  run_any = function()
    pick_configuration()
  end,

  -- Session commands
  attach_last = function()
    require('dap').run_last()
  end,

  close = function()
    require('dap').disconnect(
      { restart = false, terminateDebuggee = false },
      function() end
    )
  end,

  step_continue = function() require('dap').continue() end,
  step_over = function() require('dap').step_over() end,
  step_into = function() require('dap').step_into() end,
  step_out = function() require('dap').step_out() end,
  step_back = function() require('dap').step_back() end,
  step_up = function() require('dap').up() end,
  step_down = function() require('dap').down() end,
  run_to_cursor = function() require('dap').run_to_cursor() end,
}


M.dap_global_keymaps = make_keymap {
  install_adapter = { 'n', '<space>dI', M.methods.install_adapter, { desc = 'Install a registered debug adapter' } },
  attach = { 'n', '<space>da', M.methods.attach, { desc = 'Attach to a debug session' } },
  launch = { 'n', '<space>dl', M.methods.launch, { desc = 'Launch a new debug session' } },
  run_any = { 'n', '<space>dA', M.methods.run_any, { desc = 'Activate a debug session using any configuration, regardless of type or language' } },
  attach_last = { 'n', '<space>d.', M.methods.attach_last, { desc = 'Attach to the last run debug configuration' } },

  -- Breakpoints
  toggle_breakpoint = { 'n', '<space>bb', M.methods.toggle_breakpoint, { desc = 'Toggle breakpoint' } },
  add_conditional_breakpoint = { 'n', '<space>bc', M.methods.add_conditional_breakpoint, { desc = 'Add conditional breakpoint' } },
  add_log_point = { 'n', '<space>bl', M.methods.add_log_point, { desc = 'Add log point' } },
  clear_breakpoints = { 'n', '<space>bx', M.methods.clear_breakpoints, { desc = 'Clear breakpoints' } },
  find_breakpoints = { 'n', '<space>bf', M.methods.find_breakpoints, { desc = 'Fuzzy find debugger breakpoints' } },
}

M.dap_session_keymaps = make_keymap {
  -- Session commands
  close = { 'n', '<space>dx', M.methods.close, { desc = 'Disconnect debug session' } },
  step_continue = { 'n', ';sc', M.methods.step_continue, { desc = 'Debugger: Step continue' } },
  step_back = { 'n', ';sb', M.methods.step_back, { desc = 'Debugger: Step back' } },
  step_over = { 'n', ';sj', M.methods.step_over, { desc = 'Debugger: Step over' } },
  step_into = { 'n', ';si', M.methods.step_into, { desc = 'Debugger: Step into' } },
  step_out = { 'n', ';so', M.methods.step_out, { desc = 'Debugger: Step out' } },
  up = { 'n', ';sl', M.methods.step_up, { desc = 'Debugger: Step up' } },
  down = { 'n', ';sh', M.methods.step_down, { desc = 'Debugger: Step down' } },
  run_to_cursor = { 'n', ';;', M.methods.run_to_cursor, { desc = 'Debugger: Run to cursor' } },
}

return M
