local make_feature = require('user.util.feature').make_feature

local events = {
  Loaded = 'PaxVimLoaded'
}

local hasStdin = false

local function emitStartupEvents()
  local path = require('path')
  local args = vim.fn.argv()

  local eventData = {
    hasArgs = #args > 0,
    cwdFromArgs = nil,
    hasStdin = hasStdin,
  }

  if not args or #args == 0 then
    vim.api.nvim_exec_autocmds('User', { pattern = events.Loaded, data = eventData })
    return
  end

  local specifiedDir = args[1]

  if (path.isabs(specifiedDir) and vim.fn.isdirectory(specifiedDir)) then
    eventData.pathFromArgs = specifiedDir
  else
    local cwd = vim.fn.getcwd()
    local resolvedToCwd = cwd .. path.DIR_SEP .. specifiedDir
    if (vim.fn.isdirectory(resolvedToCwd)) then
      eventData.pathFromArgs = resolvedToCwd
    end
  end

  vim.api.nvim_exec_autocmds('User', { pattern = events.Loaded, data = eventData })
end

local M = make_feature({
  name = 'Startup Events',
  augroups = {
    name = 'OpenDirectory',
    autocmds = {
      {
        events = 'VimEnter',
        opts = {
          callback = function()
            emitStartupEvents()
          end
        }
      },

      {
        events = 'StdinReadPre',
        opts = {
          callback = function() hasStdin = true end
        }
      }
    }
  }
})

M.events = events

return M
