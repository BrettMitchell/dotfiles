local string_util = require('pax-vim.util.string')
local table_util  = require('pax-vim.util.table')


local frontmatter        = require('pax-vim.features.notes.frontmatter.v2')
local frontmatter_loaded = require('pax-vim.features.notes.frontmatter.autocmd')
local TAGS               = require('pax-vim.features.notes.special_tags')
local zk_query_util      = require('pax-vim.features.notes.auto_gen.zk_query.util')


local format_flat_list = require('pax-vim.features.notes.auto_gen.zk_query.format_flat_list')
-- local format_taxonomy = require('pax-vim.features.notes.auto_gen.zk_query.format_taxonomy')
-- local format_link_tree = require('pax-vim.features.notes.auto_gen.zk_query.format_link_tree')
-- local format_tag_buckets = require('pax-vim.features.notes.auto_gen.zk_query.format_tag_buckets')

local FORMATTERS = {
  ['flat-list'] = format_flat_list,
  ['inspect'] = vim.inspect,
  -- ['taxonomy'] = format_taxonomy.generate_taxonomy_tree,
  -- format_link_tree = format_link_tree,
  -- format_tag_buckets = format_tag_buckets,
}


local M = {}


function M.generate_body(opts)
  local queries = opts.queries or {}

  local new_body = {}
  for _, query in ipairs(queries) do
    local zk_query = {
      select = { 'metadata', 'tags', 'title', 'path' }
    }
    if query.tags then zk_query.tags = query.tags end
    if query.match then zk_query.match = query.match end
    if query.sort then zk_query.sort = query.sort end
    local query_results = zk_query_util.async_zk(opts, zk_query)

    if query['post-sort'] then
      for sort_field, dir in pairs(query['post-sort']) do
        table.sort(query_results, function (a, b)
          if a.metadata[sort_field] ~= nil and b.metadata[sort_field] == nil then
            return true
          end

          if b.metadata[sort_field] ~= nil and a.metadata[sort_field] == nil then
            return false
          end

          if dir > 0 then
            return a.metadata[sort_field] < b.metadata[sort_field]
          else
            return a.metadata[sort_field] > b.metadata[sort_field]
          end
        end)
      end
    end

    local formatter = FORMATTERS[query.format]
    if not formatter then formatter = FORMATTERS['flat-list'] end
    local formatted = formatter(query_results)

    local block = {}
    -- This starts the body with 1 empty line instead of 2
    if #new_body > 0 then table.insert(block, '') end
    if query.name then table.insert(block, { '## ' .. query.name, '' }) end
    table.insert(block, formatted)

    table.insert(new_body, block)
  end

  return new_body
end

function M.populate_query_note(opts)
  opts = opts or {}
  local filename = opts.filename

  local generated_body = {
    '',
    M.generate_body(opts),
    '',
  }

  vim.schedule(function()
    local write = nil
    local current_body = nil

    local current_filename = vim.fn.expand('%:p')
    if current_filename == filename then
      current_body = frontmatter.get_buf_body({ bufnr = 0 })

      write = function(body)
        frontmatter.write_buf_body({
          bufnr = 0,
          body = body
        })
        vim.cmd('write')
      end
    else
      current_body = frontmatter.get_file_body({ filename = filename })

      write = function()
        frontmatter.write_file_body({
          filename = filename,
          body = generated_body
        })
      end
    end

    if not current_body then current_body = {} end
    local chunk = {}
    local preserve = {}
    for _, line in ipairs(current_body) do
      table.insert(chunk, line)
      if line == '---' then
        table.insert(preserve, chunk)
        chunk = {}
      end
    end

    local full_body = { preserve, generated_body }
    write(full_body)
  end)
end

M.generate_body_augroup = {
  name = 'AutogenerateZkQueryBody',
  autocmds = { {
    events = 'User',
    opts = {
      pattern = frontmatter_loaded.events.FrontmatterLoaded,
      callback = function(event)
        local data = event.data or {}

        if vim.bo.readonly then
          return
        end

        if not data.frontmatter
            or not data.frontmatter.queries
        then
          return
        end

        coroutine.wrap(function()
          M.populate_query_note({
            queries = data.frontmatter.queries,
            filename = data.filename
          })
        end)()
      end
    }
  } }
}

return M
