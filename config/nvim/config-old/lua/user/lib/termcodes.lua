
-- Transforms escape sequences automatically handled by VimL
--  into their respective termcodes
-- E.g. '<Tab>' -> an actual tab character
-- Note: This is not necessary for vim.keycodes.set(...), as that
--        function handles termcode sequences internally
local function t (str)
  return vim.api.nvim_replace_termcodes(str, true, true, true)
end

return {
  t = t
}
