-- local zkApi = require('zk.api')

local autocmd              = require('user.lib.autocmd').autocmd
local registerBufferKeyMap = require('user.lib.map').registerBufferKeyMap
local registerKeyMap       = require('user.lib.map').registerKeyMap
local projects             = require('user.notes.projects')

local notes                = require('user.notes')

-- TODO: Load frontmatter and look at creation date.
-- If the file was created in the last 5 minutes, just delete it right away
-- If the file is more than 5 minutes old, prompt to confirm delete for safety
-- At this point the keybinding can probably be changed to ';d' for easier access
local function deleteFile()
  local filename = vim.fn.expand('%:p')
  if not filename then return end
  vim.fn.delete(filename)
  vim.cmd 'bp! | bdelete! #'
end

local M = {}

function M.setup()
  if require("zk.util").notebook_root(vim.fn.expand('%:p')) ~= nil then
    registerKeyMap({ 'n' }, {
      ['<C-space>'] = { notes.telescope.pickNoteByTitle, 'Search for notes by title' },
      ['<space>t'] = {
        t = { notes.telescope.pickNoteByTag, 'Search for notes by tag' },
        n = { notes.telescope.pickNoteByTitle, 'Search for notes by title' },
        o = { notes.telescope.pickTagOrphans, 'Search among notes with no tags by title' },
        O = { notes.telescope.pickLinkOrphans, 'Search among orphaned notes by title' },
        p = { projects.pickProject, 'Pick a project' },
        P = { projects.pickWithinProject, 'Pick a note within the current project' },
      },
      [';n'] = { notes.newNote, 'Create a new blank note' },
      [';p'] = { notes.newProjectNote, 'Create a new file in the currently opened project' },
      [';P'] = { notes.gotoProjectRoot, 'Go to root of current project' },
      [';D'] = { deleteFile, 'Delete a file and remove its buffer' },
      [';j'] = { notes.todaysJournal, 'Open (creating if necessary) todays daily journal note' },
      [';J'] = { notes.tomorrowsJournal, 'Open (creating if necessary) tomorrow daily journal note' },
      [';f'] = { function() notes.multiSearch({ mode = 'title' }) end, 'Initiate multisearch' },
      [';r'] = { notes.refactorTagUnderCursor, 'Rename a tag throughout the zettel' }
    })

    autocmd(
      'MarkdownKeybindings',
      {
        {
          'FileType',
          {
            callback = function()
              if vim.bo.filetype ~= 'markdown' then
                return
              end

              registerBufferKeyMap({ 'n' }, {
                [';b'] = { '<cmd>ZkBacklinks<cr>', 'Search through backlinks in current note' },
                ['<space>tl'] = { '<cmd>ZkLinks<cr>', 'Search through links in current note' },
                [';l'] = { notes.insertLinkFromClipboard,
                  'Insert link under cursor using clipboard text. Supports multiple filenames if separated by newlines.' },
                [';L'] = { function() notes.insertLink() end, 'Insert empty link under cursor' },
                [';t'] = { notes.addTags, 'Add existing tags using a fuzzy finder' },
                [';T'] = { notes.pickTemplate, 'Pick a template from a fuzzy finder and apply it at the cursor' },
              })

              registerBufferKeyMap({ 'v' }, {
                [';s'] = { "<cmd>'<,'>ZkMatch<cr>", 'Search for selected phrase within notes' },
                [';l'] = { notes.insertLinkFromClipboard,
                  'Wrap selected text in link using clipboard text. Supports multiple filenames if separated by newlines (will duplicate selected text for each link generated).' },
                [';L'] = { function() notes.insertLink() end, 'Wrap selected text in empty link' },
              })
            end
          }
        }
      }
    )
  end
end

return M
