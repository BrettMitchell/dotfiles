local make_feature = require('pax-vim.util.feature').make_feature_v2

local M = make_feature({
  name = 'Yank Highlighting',

  description = [[
  This feature briefly hightlights the targeted region on yank.
  
  Use the `duration` property to control how many milliseconds
  the highlight will last for.

  Use the `show_in_visual_mode` property to control whether or
  not the highlight should show up in visual mode.
  ]],

  plugins = { {
    'https://github.com/machakann/vim-highlightedyank',
  } },

  duration = 200,
  show_in_visual_mode = true,

  apply = function(self)
    if type(self.duration) == 'number' then
      vim.g.highlightedyank_highlight_duration = self.duration
    end

    if type(self.show_in_visual_mode) == 'number' then
      vim.g.highlightedyank_highlight_in_visual = self.show_in_visual_mode
    end
  end
})

return M
