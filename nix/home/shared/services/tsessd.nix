{ lib, pkgs, externalPackages, ... }:
{
  systemd.user.services.tsessd = {
    Unit = {
      Description = "tsessd";
    };
    Install = {
      WantedBy = [ "default.target" ];
    };
    Service = {
      UMask = "002";
      Environment = "PATH=${lib.makeBinPath [
        pkgs.tmux
        pkgs.lf
        externalPackages.tsessd.bin
      ]}";
      ExecStart = "${pkgs.zsh}/bin/zsh -l -c 'exec ${externalPackages.tsessd.bin}/bin/tsessd'";
    };
  };
}
