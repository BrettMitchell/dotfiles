local keymap = require('user.util.keymap')

local toggle_fold = {
  name = 'UI Commands: Toggle Fold',

  description = [[
  This feature adds keymaps to toggle folds.
  ]],

  keymap = keymap {
    { 'n', '<C-f>', 'za', { desc = 'Toggle fold under cursor' } },
  },
}

function toggle_fold.setup()
  vim.o.foldlevel = 99
  vim.o.foldlevelstart = 99
  vim.o.foldenable = true

  toggle_fold.keymap.apply()
end

return toggle_fold
