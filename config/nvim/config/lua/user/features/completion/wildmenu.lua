local feat_util = require('user.util.feature')
local add_plugins = require('user.util.plugins.lazy').add_plugins

local M = {
  feature = {
    name = 'Better Wildmenu',

    description = [[
    Enhances wildmenu autocompletion
    ]],
  },
}

function M.setup(config)
  feat_util.apply_config(config or {}, M.feature)

  add_plugins({ {
    'https://github.com/gelguy/wilder.nvim',
    config = function()
      vim.cmd [[call wilder#setup({ 'modes': [':', '/', '?'] })]]
    end
  } })
end

return M
