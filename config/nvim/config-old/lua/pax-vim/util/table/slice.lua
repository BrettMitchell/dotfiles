return function(tbl, start, finish)
  finish = finish or #tbl
  local res = {}
  for i = start, finish do
    table.insert(res, tbl[i])
  end

  return res
end
