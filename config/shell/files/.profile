
# Note: Managing all environment variables in a POSIX-compatible profile
#       makes sure that other applications that run commands (like neovim)
#       have access to the same utilities and functions that an interactive
#       zsh session will.

# Directory settings (TODO: Move these out of git tracking)
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_STATE_HOME="$HOME/.local/state"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_DESKTOP_DIR="$HOME/"
export XDG_DOCUMENTS_DIR="$HOME/files/media/documents"
export XDG_DOWNLOAD_DIR="$HOME/downloads"
export XDG_MUSIC_DIR="$HOME/files/media/music"
export XDG_PICTURES_DIR="$HOME/files/media/images"
export XDG_VIDEOS_DIR="$HOME/files/media/videos"

export USER_BUILD_DIR="$HOME/.build"

export VISUAL='nvim -u ~/.config/nvim/init-visual.lua'
export EDITOR=nvim
export MEDIA_DIR="$HOME/files/media"
export MUSIC_DIR="$XDG_MUSIC_DIR"
export BOOKS_DIR="$XDG_DOCUMENTS_DIR/books"
export MANUALS_DIR="$XDG_DOCUMENTS_DIR/manuals"
export VIDEOS_DIR="$XDG_VIDEOS_DIR/videos"
export SCREEN_CAPTURE_DIR="$XDG_VIDEOS_DIR/videos/screen-recordings"
export PICTURES_DIR="$XDG_PICTURES_DIR"
export SCREENSHOTS_DIR="$XDG_PICTURES_DIR/screenshots"
export DOWNLOADS_DIR="$HOME/downloads"
export NOTES_DIR="$HOME/files/notes"

# Set these in ~/.profile.local
export PROJECTS_DIR=
export THEMES_DIR=
export WALLPAPERS_DIR=
export DOTFILES=

alias nvim-notes='nvim -u "$HOME"/.config/nvim/init.notes.arch.lua'
alias nvim-prompt='nvim -u "$HOME"/.config/nvim/init.prompt.arch.lua'

# if [ -d "$HOME/.nix-profile/share/man" ] ; then
#   MANPATH="$MANPATH:$HOME/.nix-profile/share/man"
# fi

# Load binaries from direct children of bin directory
# This allows installation of shell packages that use
# relative references to share logic without requiring
# an explicit PATH update
for dir in "$HOME/.local/bin"/* ; do
  if [ -d "$dir" ] ; then
    case ":$PATH:" in
      *:"$dir":*)
        ;;
      *)
        export PATH="$PATH:$dir"
    esac
  fi
done

export PATH="$HOME/.local/bin:$PATH"

export CM_DIR="$HOME/.cache/clipmenu"
export CM_MAX_CLIPS=1000

export ZK_NOTEBOOK_DIR="$NOTES_DIR"

export BAT_THEME='ansi'

export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME/npm/config"

export MINIKUBE_HOME="$HOME/.local/state/.minikube"
export MINIKUBE_IN_STYLE=0

export ASDF_DIR="$USER_BUILD_DIR/asdf"
export ASDF_DATA_DIR="$XDG_STATE_HOME/asdf"
export ASDF_CONFIG_FILE="$XDG_CONFIG_HOME/asdf/config"
export ASDF_PYTHON_DEFAULT_PACKAGES_FILE="$XDG_CONFIG_HOME/asdf/default_python_packages"
export ASDF_NPM_DEFAULT_PACKAGES_FILE="$XDG_CONFIG_HOME/asdf/default_npm_packages"

export PASSWORD_STORE_EXTENSIONS_DIR=/usr/lib/password-store/extensions

if command -v sway >/dev/null ; then
  __sway_sock=`fd --glob 'sway-ipc.*.sock' "$XDG_RUNTIME_DIR" | head -n 1`
  if [[ -e "$__sway_sock" ]] ; then
    export SWAYSOCK="$__sway_sock"
  fi
fi

source ~/.config/skim/keybindings.sh
source ~/.config/skim/colors.sh
export SKIM_DEFAULT_OPTIONS="--bind='$SKIM_KEYBINDINGS_MOTION' --color='$SK_COLORS'"

[ -f "$HOME"/.profile.local ] && source "$HOME"/.profile.local

