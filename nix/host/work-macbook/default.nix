{ pkgs, ... }:
{
  # Auto upgrade nix package and the daemon service.
  services.nix-daemon.enable = true;

  documentation.man = {
    enable = true;
  };

  imports = [
    ../../home/shared/services/tsessd-launchd.nix
  ];

  # Necessary for using flakes on this system.
  nix.settings = {
    experimental-features = [ "nix-command" "flakes" ];
    auto-optimise-store = true;
  };

  programs.zsh.enable = true;

  environment.systemPackages = with pkgs; [
    wget
    curl

    git
    gnumake
    gcc
    killall
    coreutils-full
  ];

  # Used for backwards compatibility, please read the changelog before changing.
  # $ darwin-rebuild changelog
  system.stateVersion = 5;
}
