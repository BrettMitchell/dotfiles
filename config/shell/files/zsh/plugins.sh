
# == Vim mode with keybindings much closer to actual Vim
# No 'wait' modifier here so that shell starts with cursor
# that matches initial mode (INSERT)

# == ZSH syntax highlighting
# zinit light zsh-users/zsh-syntax-highlighting

# == Powerline

# == Interactive jq expression builder
# Alt-J : Enter interactive query builder mode. Must be in insert mode.
# Enter : Insert jq command into command line where Alt-J was pressed
# zinit ice depth=1 wait lucid
# zinit light reegnz/jq-zsh-plugin

# == Completions
# zinit ice depth=1
# zinit light zsh-users/zsh-autosuggestions

# == Type-ahead completions
# Keybindings listed at: https://github.com/marlonrichert/zsh-autocomplete
# Horrifying bugs exist in this plugin ATM, so I'm disabling it until I can figure out what's going on

# zinit ice depth=1 wait lucid cloneopts='--branch 23.05.02'
# zinit light marlonrichert/zsh-autocomplete

zinit ice depth=1
zinit light agkozak/zhooks

