local make_feature = require('pax-vim.util.feature').make_feature_v2

local yank_history = make_feature({
  name = 'Yank History',

  description = [[
  This feature adds a yank ring feature which keeps track of previously
  yanked text, and makes it available when pasting.
  ]],

  plugins = { {
    'https://github.com/svermeulen/vim-yoink',
  } },
})

return yank_history
