-- Source: https://github.com/parz3val/pywal-complete.nvim/blob/main/lua/pywal16/config.lua

local hl = require('user.util.highlight')
local TRANSPARENT = 'NONE'

return function(theme)
  hl('LspFloatWinNormal', { bg = TRANSPARENT })
  hl('LspFloatWinBorder', { fg = theme.color.foreground })
  hl('LspSagaBorderTitle', { fg = theme.color.accent1 })
  hl('LspSagaHoverBorder', { fg = theme.color.accent1 })
  hl('LspSagaRenameBorder', { fg = theme.color.accent4 })
  hl('LspSagaDefPreviewBorder', { fg = theme.color.accent4 })
  hl('LspSagaCodeActionBorder', { fg = theme.color.accent1 })
  hl('LspSagaFinderSelection', { fg = theme.color.accent1 })
  hl('LspSagaCodeActionTitle', { fg = theme.color.accent1 })
  hl('LspSagaCodeActionContent', { fg = theme.color.accent6 })
  hl('LspSagaSignatureHelpBorder', { fg = theme.color.accent4 })
  hl('ReferencesCount', { fg = theme.color.accent6 })
  hl('DefinitionCount', { fg = theme.color.accent6 })
  hl('DefinitionIcon', { fg = theme.color.accent1 })
  hl('ReferencesIcon', { fg = theme.color.accent1 })
  hl('TargetWord', { fg = theme.color.accent1 })
end
