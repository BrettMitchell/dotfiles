local make_feature = require('user.util.feature').make_feature
local plenary = require('user.features.util.plenary')

local pick_static = require('user.features.telescope.pick_static')
-- local pickDynamic = require('user.features.telescope.pickDynamic')
-- local pickFiles = require('user.features.telescope.pickFiles')
-- local pickFileContents = require('user.features.telescope.pickFileContents')
-- local pickBuffers = require('user.features.telescope.pickBuffers')
-- local pickHelp = require('user.features.telescope.pickHelp')
-- local pickRegisters = require('user.features.telescope.pickRegisters')
-- local pickKeymaps = require('user.features.telescope.pickKeymaps')
-- local pickCommands = require('user.features.telescope.pickCommands')
-- local pickCommandHistory = require('user.features.telescope.pickCommandHistory')
--
-- local previewFile = require('user.features.telescope.previewFile')
-- local previewText = require('user.features.telescope.previewText')
-- local previewCommand = require('user.features.telescope.previewCommand')

local methods = require('user.features.telescope.methods-tmp')

local config = {
  layout = nil,
  keymapSet = nil,
  preview_enabled = true,
  refresh_view = nil,
}

local M = make_feature({
  name = 'Telescope',
  description = [[
  This feature defines telescope bindings and utilities
  ]],
  dependencies = { plenary },
  plugins = {
    {
      'https://github.com/nvim-telescope/telescope.nvim',
      event = 'VeryLazy',

      config = function()
        -- Override picker constructor to force ascending sort for horizontal layout
        local pickers = require('telescope.pickers')
        local OLD_pickers_new = pickers.new
        pickers.new = function(self, opts, ...)
          if opts.layout_strategy == 'horizontal' or opts.layout_strategy == nil then
            opts.sorting_strategy = 'ascending'
          end
          local picker = OLD_pickers_new(self, opts, ...)
          return picker
        end

        local telescope = require('telescope')
        local keys = require('user.features.telescope.keys')

        local ok, Layout = pcall(require, 'nui.layout')
        local _, Popup = pcall(require, 'nui.popup')
        local TSLayout = require('telescope.pickers.layout')

        local defaults = {
          default_mappings = {},
          mappings = {
            i = keys.getInsertMode(),
          },
        }

        if ok then
          local function make_popup(options)
            local popup = Popup(options)
            function popup.border:change_title(title)
              popup.border.set_text(popup.border, "top", title)
            end

            return TSLayout.Window(popup)
          end

          defaults.mappings.i['<C-S-p>'] = function()
            config.preview_enabled = not config.preview_enabled
            if config.refresh_view then
              config.refresh_view()
            end
          end

          defaults.layout_strategy = "horizontal"

          defaults.layout_config = {
            horizontal = {
              size = {
                width = "90%",
                height = "90%",
              },
            },
            vertical = {
              size = {
                width = "90%",
                height = "90%",
              },
            },
          }

          defaults.create_layout = function(picker)
            local border = {
              results = {
                top_left = "┌",
                top = "─",
                top_right = "┬",
                right = "│",
                bottom_right = "",
                bottom = "",
                bottom_left = "",
                left = "│",
              },
              results_patch = {
                minimal_reversed = {
                  top_left = "├",
                  top_right = "┤",
                  bottom_right = "┘",
                  bottom = "─",
                  bottom_left = "└",
                },
                minimal = {
                  top_left = "┌",
                  top_right = "┐",
                },
                horizontal = {
                  top_left = "├",
                  top_right = "┤",
                  bottom_right = "┴",
                  bottom = "─",
                  bottom_left = "└",
                },
                vertical = {
                  top_left = "├",
                  top_right = "┤",
                },
              },
              prompt = {
                top_left = "├",
                top = "─",
                top_right = "┤",
                right = "│",
                bottom_right = "┘",
                bottom = "─",
                bottom_left = "└",
                left = "│",
              },
              prompt_patch = {
                minimal_reversed = {
                  top_left = "┌",
                  top_right = "┐",
                  bottom_right = "",
                  bottom = "",
                  bottom_left = "",
                },
                minimal = {
                  bottom_right = "┘",
                },
                horizontal = {
                  top_left = "┌",
                  top_right = "┬",
                  bottom_right = "",
                  bottom = "",
                  bottom_left = "",
                },
                vertical = {
                  bottom_right = "┘",
                },
              },
              preview = {
                top_left = "┌",
                top = "─",
                top_right = "┐",
                right = "│",
                bottom_right = "┘",
                bottom = "─",
                bottom_left = "└",
                left = "│",
              },
              preview_patch = {
                minimal_reversed = {},
                minimal = {},
                horizontal = {
                  bottom = "─",
                  bottom_left = "",
                  bottom_right = "┘",
                  left = "",
                  top_left = "",
                },
                vertical = {
                  bottom = "",
                  bottom_left = "",
                  bottom_right = "",
                  left = "│",
                  top_left = "┌",
                },
              },
            }

            local function pad_string(s)
              if s and s:len() > 0 then
                return ' ' .. s .. ' '
              end
              return s
            end

            local results = make_popup({
              focusable = false,
              border = {
                style = border.results,
                text = {
                  top = pad_string(picker.results_title),
                  top_align = "center",
                },
              },
              win_options = {
                winhighlight = "Normal:Normal",
              },
            })

            local prompt = make_popup({
              enter = true,
              border = {
                style = border.prompt,
                text = {
                  top = pad_string(picker.prompt_title),
                  top_align = "center",
                },
              },
              win_options = {
                winhighlight = "Normal:Normal",
              },
            })

            local preview = make_popup({
              focusable = false,
              border = {
                style = border.preview,
                text = {
                  top = pad_string(picker.preview_title),
                  top_align = "center",
                },
              },
            })

            local box_by_kind = {
              vertical = Layout.Box({
                Layout.Box(preview, { grow = 1 }),
                Layout.Box(results, { grow = 1 }),
                Layout.Box(prompt, { size = 3 }),
              }, { dir = "col" }),

              horizontal = Layout.Box({
                Layout.Box({
                  Layout.Box(prompt, { size = 2 }),
                  Layout.Box(results, { grow = 1 }),
                }, { dir = "col", size = "60%" }),
                Layout.Box(preview, { size = "40%" }),
              }, { dir = "row" }),

              minimal = Layout.Box({
                Layout.Box(results, { grow = 1 }),
                Layout.Box(prompt, { size = 3 }),
              }, { dir = "col" }),

              minimal_reversed = Layout.Box({
                Layout.Box(prompt, { size = 2 }),
                Layout.Box(results, { grow = 1 }),
              }, { dir = "col" }),
            }

            local function get_box()
              local strategy = picker.layout_type or 'horizontal'
              if not config.preview_enabled then
                strategy = strategy == 'minimal_reversed' and 'minimal_reversed' or 'minimal'
              end

              if strategy and box_by_kind[strategy] then
                return box_by_kind[strategy], strategy
              end

              local height, width = vim.o.lines, vim.o.columns
              local box_kind = "horizontal"
              if width < 100 then
                box_kind = "vertical"
                if height < 40 then
                  box_kind = "minimal"
                end
              end
              return box_by_kind[box_kind], box_kind
            end

            local function prepare_layout_parts(layout, box_type)
              layout.results = results
              results.border:set_style(border.results_patch[box_type])

              layout.prompt = prompt
              prompt.border:set_style(border.prompt_patch[box_type])

              if box_type == "minimal" or box_type == "minimal_reversed" then
                layout.preview = nil
              else
                layout.preview = preview
                preview.border:set_style(border.preview_patch[box_type])
              end
            end

            -- Ensures API compatibility between vanilla telescope config and
            -- NUI popup config
            local function convert_dimension(value)
              if type(value) == 'string' then return value end
              if type(value) == 'number' then
                if value < 1 then
                  return tostring(value * 100) .. '%'
                end
                return tostring(value)
              end
              return value
            end

            local function get_layout_size(box_kind)
              if picker.layout_config.width and picker.layout_config.height then
                return {
                  width = convert_dimension(picker.layout_config.width),
                  height = convert_dimension(picker.layout_config.height),
                }
              end

              if box_kind == "minimal" or box_kind == "minimal_reversed" then
                return picker.layout_config.vertical.size
              end

              return picker.layout_config.horizontal.size
            end

            local box, box_kind = get_box()
            local layout = Layout({
              relative = "editor",
              position = "50%",
              size = get_layout_size(box_kind),
            }, box)

            layout.picker = picker
            prepare_layout_parts(layout, box_kind)

            local layout_update = layout.update
            function layout:update()
              local box, box_kind = get_box()
              prepare_layout_parts(layout, box_kind)
              layout_update(self, { size = get_layout_size(box_kind) }, box)
            end

            config.refresh_view = function()
              layout:update()
            end

            return TSLayout(layout)
          end
        end

        telescope.setup({ defaults = defaults })
      end
    }
  },
  config = config,
  keymaps = {
    { 'n', '<space>ff', function() methods.cwdFilesByName() end,        { desc = 'Search files by name' } },
    { 'n', '<space>fl', function() methods.currentFileLspSymbols() end, { desc = 'Search document symbols' } },
    { 'n', '<space>fs', function() methods.cwdFilesByContent() end,     { desc = 'Fuzzy search within project files' } },
    { 'n', '<space>fS', function() methods.currentFileContents() end,   { desc = 'Fuzzy search in current file' } },
    { 'n', '<space>fb', function() methods.buffers() end,               { desc = 'Fuzzy find open buffer' } },
    { 'n', '<space>fh', function() methods.help() end,                  { desc = 'Fuzzy find help' } },
    { 'n', '<space>fj', function() methods.jump_list() end,             { desc = 'Fuzzy find jump list' } },
    { 'n', '<space>fm', function() methods.keymaps() end, {
      desc =
      'Fuzzy find keymaps and their descriptions'
    } },
    { 'n', '<space>f;', function() methods.command_history() end, { desc = 'Fuzzy find previously executed command' } },
  }
})

M.methods = {
  --   -- General
  pick_static = pick_static,
  --   pickDynamic = pickDynamic,
  --
  --   -- Specific
  --   pickFiles = pickFiles,
  --   pickFileContents = pickFileContents,
  --   pickBuffers = pickBuffers,
  --   pickHelp = pickHelp,
  --   pickRegisters = pickRegisters,
  --   pickKeymaps = pickKeymaps,
  --   pickCommands = pickCommands,
  --   pickCommandHistory = pickCommandHistory,
  --
  --   -- Easier previewers
  --   previewFile = previewFile,
  --   previewText = previewText,
  --   previewCommand = previewCommand,
}

-- M.methods = methods

M.layouts = {}

M.keymapSets = {}

return M
