local M = {}

function M.configureDap()
  local dap = require("dap")
  local dapui = require("dapui")

  dapui.setup()
  require('nvim-dap-virtual-text').setup()

  dap.listeners.after.event_initialized["dapui_config"] = function()
    dapui.open()
  end

  dap.listeners.before.event_terminated["dapui_config"] = function()
    dapui.close()
  end

  dap.listeners.before.event_exited["dapui_config"] = function()
    dapui.close()
  end

  vim.fn.sign_define('DapBreakpoint', { text = '-', texthl = '', linehl = '', numhl = '' });
  vim.fn.sign_define('DapStopped', { text = '>', texthl = '', linehl = '', numhl = '' });

  require('user.dap.nodejs').setup(dap)
end

function M.attach()
  -- Temp: In the future, infer the proper debugger from file type
  require('user.dap.nodejs').attach()
end

return M
