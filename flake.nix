{
  description = "System configuration";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";
    nixpkgs-unstable.url = "github:NixOS/nixpkgs/nixos-unstable";

    nix-darwin.url = "github:LnL7/nix-darwin";
    nix-darwin.inputs.nixpkgs.follows = "nixpkgs";

    # flake-utils.url = "github:numtide/flake-utils"
    home-manager = {
      url = "github:nix-community/home-manager/release-24.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    home-manager-unstable = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs-unstable";
    };

    nur = {
      url = "github:nix-community/NUR";
    };

    tmux-tpm = {
      url = "git+https://github.com/tmux-plugins/tpm.git";
      flake = false;
    };

    tsessd = {
      # Note: Cannot use 'path:' here, because only 'git+file' works with submodules
      url = "git+file:src/session-daemon/tmux-session-daemon";
      # url = "git+ssh://git@gitlab.com/personal-system/tmux-session-daemon/3082fee353907188fe88994fd3084dac444a5644";
    };
  };

  outputs = inputs@{ self, ... }:
    let
    in {
      root = "${self}";
      nixosConfigurations = {
        t490 = let
          system = "x86_64-linux";
          hostname = "t490";

          nixpkgsConfig = {
            inherit system;
            config.allowUnfree = true;
            overlays = [
              (import ./config/media/mpv/mpv-custom-overlay.nix).overlay
            ];
          };

          nixpkgs-stable = import inputs.nixpkgs nixpkgsConfig;
          nixpkgs = {
            stable = nixpkgs-stable;
            unstable = import inputs.nixpkgs-unstable nixpkgsConfig;
            nur = import inputs.nur {
              pkgs = nixpkgs-stable;
              nurpkgs = nixpkgs-stable;
            };
          };

          externalPackages = {
            tmux-tpm = inputs.tmux-tpm;
            tsessd = inputs.tsessd.packages.${system};
            nvr = (import ./src/nvr-bin/install.nix).packages.${system};
          };
        in inputs.nixpkgs.lib.nixosSystem {
          inherit system;
          modules = [
            ./nix/host/t490
            inputs.home-manager.nixosModules.home-manager
            {
              home-manager = {
                users.brettm = import ./nix/home/users/brettm;
                extraSpecialArgs = {
                  inherit hostname nixpkgs externalPackages;
                  root = inputs.self;
                };
              };
            }
            {
              _module.args = {
                inherit hostname inputs nixpkgs;
                root = inputs.self;
              };
            }
          ];
        };
      };

      darwinConfigurations = {
        work-macbook = let
          system = "aarch64-darwin";
          hostname = "work-macbook";

          nixpkgsConfig = {
            inherit system;
            config.allowUnfree = true;
            # overlays = [
            #   (import ./config/media/mpv/mpv-custom-overlay.nix).overlay
            # ];
          };

          nixpkgs-stable = import inputs.nixpkgs nixpkgsConfig;
          nixpkgs = {
            stable = nixpkgs-stable;
            unstable = import inputs.nixpkgs-unstable nixpkgsConfig;
            nur = import inputs.nur {
              pkgs = nixpkgs-stable;
              nurpkgs = nixpkgs-stable;
            };
          };

          externalPackages = {
            tmux-tpm = inputs.tmux-tpm;
            tsessd = inputs.tsessd.packages.${system};
            nvr = (import ./src/nvr-bin/install.nix).packages.${system};
          };
        in inputs.nix-darwin.lib.darwinSystem {
          inherit system;
          modules = [
            ./nix/host/work-macbook
            inputs.home-manager.darwinModules.home-manager
            {
              home-manager = {
                useGlobalPkgs = true;
                useUserPackages = true;
                users.brettm = import ./nix/home/users/brettm-darwin;
                extraSpecialArgs = {
                  inherit system hostname nixpkgs externalPackages;
                  root = inputs.self;
                };
              };
            }
            {
              _module.args = {
                inherit system hostname inputs nixpkgs externalPackages;
                root = inputs.self;
              };
            }
          ];
        };
      };
    };
  }
