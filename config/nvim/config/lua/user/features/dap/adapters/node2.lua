local make_feature = require('user.util.feature').make_feature_v2
local table_util = require('user.util.table')
local includes = require('user.util.table.includes')
local dap = require('user.features.dap')

local M = {}

local config = {
  install_dir = nil
}

local function get_adapter()
  local installDir = config.install_dir or dap.adapter_install_dir .. '/vscode-node-debug2'

  return {
    type = 'executable',
    command = 'node',
    args = { installDir .. '/out/src/nodeDebug.js' },
  }
end

local function get_dap_config()
  return {
    {
      name = 'Launch ' .. M.adapter_name,
      type = 'node2',
      request = 'launch',
      program = '${file}',
      cwd = vim.fn.getcwd(),
      sourceMaps = true,
      protocol = 'inspector',
      console = 'integratedTerminal',
    },

    {
      -- For this to work you need to make sure the node process is started with the `--inspect` or `--inspect-brk` flag.
      name = 'Attach ' .. M.adapter_name .. ' - Enter port',
      type = 'node2',
      request = 'attach',
      port = function()
        local port = vim.fn.input('Enter a port: ')
        if port:len() == 0 then
          return require('dap').ABORT
        end
        return port
      end
    },

    -- These don't seem to work at all: https://github.com/microsoft/vscode-node-debug2/issues/51
    -- {
    --   -- For this to work you need to make sure the node process is started with the `--inspect` or `--inspect-brk` flag.
    --   name = 'Attach (pick)',
    --   type = 'node2',
    --   request = 'attach',
    --   processId = require 'dap.utils'.pick_process,
    -- },

    -- {
    --   -- For this to work you need to make sure the node process is started with the `--inspect` or `--inspect-brk` flag.
    --   name = 'Attach (auto)',
    --   type = 'node2',
    --   request = 'attach',
    --   cwd = vim.fn.getcwd(),
    --   sourceMaps = true,
    --   protocol = 'inspector',
    --   skipFiles = { '<node_internals>/**/*.js' },
    -- },
  }
end

local function init()
  local _dap = require('dap')
  _dap.adapters.node2 = get_adapter()
  _dap.configurations.javascript = table_util.concat(
    _dap.configurations.javascript or {},
    get_dap_config()
  )
end

local methods = {
  get_adapter = get_adapter,
  get_dap_config = get_dap_config,
  init = init,

  remove = function(opts)
    opts = opts or {}

    if not config.install_dir then
      vim.notify("Cannot remove directory '" .. config.install_dir .. "'", 'error')
      return
    end

    if not opts.no_confirm then
      local confirmation = vim.fn.input("Are you sure you want to permanantly delete the directory '" ..
      config.install_dir .. "'? (y/n) : ")
      if not includes(confirmation, { 'y', 'Y' }) then
        vim.notify('Aborted by user')
        return
      end
    end

    vim.notify("Removing directory '" .. config.install_dir .. "'")
    vim.fn.system({ 'rm', '-rf', config.install_dir })
  end,

  install = function()
    local shell = os.getenv('SHELL')

    if (vim.fn.isdirectory(config.install_dir) == 1) then
      vim.notify('https://github.com/microsoft/vscode-node-debug2.git already installed to ' .. config.install_dir,
        'info')
      return
    end

    dap.hooks.do_hook('pre_install', { name = 'node2' })

    vim.notify('Installing node2 debug adapter: ' .. vim.inspect({ shell = shell, installDir = config.install_dir }))

    -- TODO: Use plenary to make this async. This is a pretty awful experience right now.
    vim.notify(vim.fn.system({ 'mkdir', '-p', config.install_dir }))
    vim.notify(vim.fn.system({ 'git', 'clone', 'https://github.com/microsoft/vscode-node-debug2.git', config.install_dir }))
    vim.notify(
      vim.fn.system({ shell, '-c',
        ("cd '%s' && npm install && NODE_OPTIONS=--no-experimental-fetch npm run build"):format(config.install_dir) })
    )

    vim.notify('Finished installing node2 debug adapter')

    dap.hooks.do_hook('post_install', { name = 'node2' })
  end,
}

M = make_feature({
  name = "DAP :: Adapter 'node2'",
  deps = { dap },

  description = [[
  This feature adds utilities to aid in the installation and startup of
  the 'node2' debug adapter.
  ]],

  adapter_name = 'node2',

  config = config,

  apply = function(self)
    if not config.install_dir then
      self.config.install_dir = dap.adapter_install_dir .. '/node2'
    end

    dap.methods.add_installable('node2', methods.install)
    methods.init()
  end
})

M.methods = methods

return M
