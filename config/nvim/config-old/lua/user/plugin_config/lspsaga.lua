-- TODO: Load standard border character config from telescope and try to use that
--       in LSPSaga border config.

return {
  config = function()
    local getTheme = require('user.lib.theme').getTheme
    local theme = getTheme()

    local ui = {
      colors = {
        --float window normal background color
        normal_bg = '#1d1536',
        --title background color
        title_bg  = '#afd700',
        red       = '#e95678',
        magenta   = '#b33076',
        orange    = '#FF8700',
        yellow    = '#f7bb3b',
        green     = '#afd700',
        cyan      = '#36d0e0',
        blue      = '#61afef',
        purple    = '#CBA6F7',
        white     = '#d1d4cf',
        black     = '#1c1c19',
      },
    }

    if theme ~= nil then
      ui = {
        colors = {
          --float window normal background color
          normal_bg = theme.special.background,
          --title background color
          title_bg  = theme.special.background,
          red       = theme.special.foreground,
          magenta   = theme.special.foreground,
          orange    = theme.special.foreground,
          yellow    = theme.special.foreground,
          green     = theme.special.foreground,
          cyan      = theme.special.foreground,
          blue      = theme.special.foreground,
          purple    = theme.special.foreground,
          white     = theme.special.foreground,
          black     = theme.special.foreground,
        }
      }
    end

    require('lspsaga').setup({
      ui = ui,

      scroll_preview = {
        scroll_down = 'j',
        scroll_up = 'k',
      },

      finder = {
        open = { 'o', '<CR>' },
        vsplit = 's',
        split = 'i',
        tabe = 't',
        quit = { 'q', '<ESC>' },
      },

      definition_action_keys = {
        edit = '<CR>',
        vsplit = '<M-CR>/',
        split = '<M-CR>\\',
        tabe = '',
        quit = { 'q', '<ESC>' },
      },

      code_action = {
        num_shortcut = true,
        keys = {
          quit = '<ESC>',
          exec = '<CR>',
        }
      },

      lightbulb = {
        enable = false,
        -- enable_in_insert = false,
        -- sign = true,
        -- sign_priority = 20,
        -- virtual_text = true,
      },

      outline = {
        -- win_position = 'right',
        -- win_with = '',
        -- win_width = 30,
        -- show_detail = true,
        -- auto_preview = true,
        -- auto_refresh = true,
        -- auto_close = true,
        -- custom_sort = nil,
        keys = {
          jump = '<cr>',
          expand_collapse = '<cr>',
          quit = 'q',
        },
      },

      symbol_in_winbar = {
        enable = true,
        separator = ' ',
        hide_keyword = true,
        show_file = true,
        folder_level = 2,
        respect_root = false,
        color_mode = true,
      },

      rename = {
        quit = '<esc>',
        exec = '<cr>',
        whole_project = false,
      },
    })
  end
}
