local make_feature = require('user.util.feature').make_feature_v2
local keymap = require('user.util.keymap')

local function move_by_indent_size(dir)
  local indent_size = vim.opt.tabstop._value
  if not vim.opt.expandtab._value then
    indent_size = 1
  end

  local arrows = ''
  for _ = 1, indent_size do
    arrows = arrows .. '<' .. dir .. '>'
  end

  return arrows
end

local indent = make_feature({
  name = 'Edit Commands: Indent',

  description = [[
  This feature adds keymaps for indenting text
  ]],

  keymap = keymap {
    { 'n', '>', '>>', { desc = 'Indent' } },
    { 'n', '<', '<<', { desc = 'Dedent' } },
    { 'v', '>', '>gv', { desc = 'Indent' } },
    { 'v', '<', '<gv', { desc = 'Dedent' } },

    {
      'i',
      '<C->>',
      function ()
        vim.cmd('normal >')
        vim.api.nvim_input(move_by_indent_size('right'))
      end,
      { desc = 'Indent' }
    },

    {
      'i',
      '<M-<>',
      function ()
        vim.cmd('normal <')
        vim.api.nvim_input(move_by_indent_size('left'))
      end,
      { desc = 'Dedent' }
    },
  },

  apply = function (self)
    self.keymap.apply()
  end
})

return indent

