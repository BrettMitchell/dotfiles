return function(str)
  local without_leading = string.gsub(str, '^%s+', '')
  return without_leading
end
