local base = require('user.features.theme.pax-colors.base')
local treesitter = require('user.features.theme.pax-colors.treesitter')
local galaxyline = require('user.features.theme.pax-colors.galaxyline')
local flash = require('user.features.theme.pax-colors.flash')
local telescope = require('user.features.theme.pax-colors.telescope')
local gitsigns = require('user.features.theme.pax-colors.gitsigns')
local lspsaga = require('user.features.theme.pax-colors.lspsaga')

local pax_colors = {
  name = 'Pax Colors',

  description = [[
  This feature will eventually replace the 'pywal' colorscheme,
  but for now, it serves only as a supplement to clean up some
  contrast issues.
  ]],
}

function pax_colors.reset()
  -- require("pywal").setup {}
  pax_colors.load()
end

function pax_colors.load()
  local json = require('lunajson')

  -- TODO: Add error notifications here. Right now this just silently exits without letting
  -- you know that something's wrong

  local theme_dir = vim.fn.getenv('THEMES_DIR')
  if theme_dir == vim.NIL then
    vim.notify('THEMES_DIR environment variable is not set', 'error')
    return
  end

  local current_theme_name_file = theme_dir .. '/current'
  if vim.fn.filereadable(current_theme_name_file) == 0 then
    vim.notify(current_theme_name_file .. ' is not readable', 'error')
    return
  end

  local current_theme_file = vim.fn.readfile(current_theme_name_file)[1] .. '/value_store.json'
  if vim.fn.filereadable(current_theme_file) == 0 then
    vim.notify(current_theme_file .. ' is not readable', 'error')
    return
  end

  local current_theme_values_arr = vim.fn.readfile(current_theme_file)
  local current_theme_values = ''
  for _, line in ipairs(current_theme_values_arr) do
    current_theme_values = current_theme_values .. line
  end

  local theme = json.decode(current_theme_values)

  base(theme)
  treesitter(theme)
  galaxyline(theme)
  flash(theme)
  telescope(theme)
  gitsigns(theme)
  lspsaga(theme)
end

function pax_colors.setup()
  pax_colors.load()
end

return pax_colors
