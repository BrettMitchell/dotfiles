local M = {}

-- TODO: Make this more declarative
--       Individual functions should all accept notebook directory as
--       a parameter rather than referring to this module.
--       Also, this code shouldn't even know that NOTES_DIR is a thing.
--       Let the shell do that.
local notes_dir = vim.fn.getenv('NOTES_DIR')
if vim.fn.isdirectory(notes_dir) == 0 then
  notes_dir = vim.fn.getcwd()
end

function M.set_notes_dir(new_dir)
  notes_dir = new_dir
end

function M.getNotesDir(opts)
  opts = opts or {}
  if opts.notesDir then return opts.notesDir end
  return notes_dir
end

return M
