local shallow_copy = require('user.util.table.shallow_copy')

return function(cos)
  cos = shallow_copy(cos)
  while true do
    if #cos == 0 then break end
    for i=1, #cos do
      local _, res = coroutine.resume(cos[i])
      if not res then
        table.remove(cos, i)
        break
      end
    end
  end
end
