local closeWindow = require('user.utils').closeWindow

local M = {}

M.gutterEnabled = true

function M.toggleGutterSigns()
  vim.cmd('Gitsigns toggle_signs')
  vim.notify('Toggled git gutter signs')
end

function M.toggleCurrentLineBlame()
  vim.cmd('Gitsigns toggle_current_line_blame')
  vim.notify('Toggled git blame')
end

function M.toggle()
  M.gutterEnabled = not M.gutterEnabled
  if M.gutterEnabled then
    require('gitsigns').attach()
  else
    require('gitsigns').detach()
  end
  vim.notify('Toggled gitsigns plugin')
end

function M.enable()
  M.gutterEnabled = true
  require('gitsigns').attach()
  vim.notify('Enabled gitsigns plugin')
end

function M.disable()
  M.gutterEnabled = false
  require('gitsigns').detach()
  vim.notify('Disabled gitsigns plugin')
end

-- local function getVisibleBuffers()
--   local wins = vim.api.nvim_list_wins()
--   local bufs = {}
--
--   for _, win in ipairs(wins) do
--     table.insert(bufs, vim.api.nvim_win_get_buf(win))
--   end
--
--   return bufs
-- end

local function getWinForFilenamePattern(pattern)
  for _, win in ipairs(vim.api.nvim_list_wins()) do
    local buf = vim.api.nvim_win_get_buf(win)
    local filename = vim.api.nvim_buf_get_name(buf)
    if filename:find(pattern) then
      return win, buf, filename
    end
  end

  return nil, nil, nil
end

M.config = {
  current_line_blame_opts = {
    delay = 0
  },

  on_attach = function(bufnr)
    local function map(mode, l, r, opts)
      opts = opts or {}
      opts.buffer = bufnr
      vim.keymap.set(mode, l, r, opts)
    end

    local n = { 'n' }
    local nv = { 'n', 'v' }
    local ox = { 'o', 'x' }

    map(ox, 'ih', ':<C-U>Gitsigns select_hunk<CR>')

    map(n, '<space>gm', '<cmd>Gvdiffsplit!<cr>') -- Open a 3-way merge editor
    map(n, '<space>ghp', '<cmd>Gitsigns preview_hunk<cr>') -- Preview previous version of hunk in a floating window

    map(nv, '<space>gs', '<cmd>Gitsigns stage_hunk<cr>') -- Stage hunk under cursor
    map(n, '<space>gS', '<cmd>Gitsigns stage_buffer<cr>') -- Stage current buffer
    map(nv, '<space>gr', '<cmd>Gitsigns reset_hunk<cr>') -- Reset hunk under cursor
    map(n, '<space>gR', '<cmd>Gitsigns reset_buffer<cr>') -- Reset current buffer

    map(n, '[h', '<cmd>Gitsigns next_hunk<cr>') -- Next hunk in the current file
    map(n, ']h', '<cmd>Gitsigns prev_hunk<cr>') -- Previous hunk in the current file

    -- Merge-specific bindings

    local filename = vim.fn.expand('%')
    local isFugitive = filename:match('^fugitive://')
    -- local isWorkingCopy = filename:match('^fugitive://.*//1')
    local isTarget = filename:match('^fugitive://.*//2')
    local isMerge = filename:match('^fugitive://.*//3')

    local function quitAll()
      local targetWin = getWinForFilenamePattern('^fugitive://.*//2')
      local mergeWin = getWinForFilenamePattern('^fugitive://.*//3')

      -- If in merge mode, we want to quit merge mode before closing the entire file
      if targetWin then vim.api.nvim_win_close(targetWin, false) end
      if mergeWin then vim.api.nvim_win_close(mergeWin, false) end

      -- If not in merge mode, we want to have normal close file behavior
      if not targetWin and not mergeWin then
        closeWindow(0)
      end
    end

    map(n, '<space>q', quitAll) -- Quit all members of the 3-way merge editor

    -- Saving inside a Fugitive merge window should update the actual file using the working copy
    -- if isFugitive then
    --   map(n, 'KK')
    -- end

    if isFugitive and (isTarget or isMerge) then
      map(n, '<space>ga', '<cmd>diffput<cr>') -- Put changes under cursor in working copy
    end

    map(n, '<space>gah', '<cmd>diffget //2<cr>') -- Accept //2 conflict contents
    map(n, '<space>gal', '<cmd>diffget //3<cr>') -- Accept //3 conflict contents

    -- Maybe incorporate stage hunk into process here, but maybe just use the existing binding

    return M.gutterEnabled
  end
}

return M
