return {
  pickNote = require('user.notes.telescope.pickNote').pickNote,
  multiSearch = require('user.notes.telescope.multiSearch').multiSearch,
  pickNoteByTag = require('user.notes.telescope.zkBuiltins').pickNoteByTag,
  pickNoteByTitle = require('user.notes.telescope.zkBuiltins').pickNoteByTitle,
  pickTagOrphans = require('user.notes.telescope.pickNote').pickTagOrphans,
  pickLinkOrphans = require('user.notes.telescope.pickNote').pickLinkOrphans,
}
