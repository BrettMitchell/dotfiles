return {
  config = function()
    local alpha = require 'alpha'
    local dashboard = require 'alpha.themes.dashboard'

    dashboard.config.opts.noautocmd = true

    dashboard.section.header.val = require('user.static').ascii_art.nvim_banner_text1

    dashboard.section.buttons.val = {
      dashboard.button("e", "  > New file", ":ene<CR>"),
      dashboard.button("f", "  > Find file", ":Telescope find_files<CR>"),
      dashboard.button("r", "  > Recent", ":Telescope oldfiles<CR>"),
    }

    alpha.setup(dashboard.config)
  end
}
