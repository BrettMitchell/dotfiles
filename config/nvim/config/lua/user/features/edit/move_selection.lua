local make_feature = require('user.util.feature').make_feature_v2

local move_selection = make_feature({
  name = 'Editing Command: Move Selection',

  description = [[
  This feature adds the ability to move a visual selection or individual
  line up, down, left, or right.
  ]],

  plugins = { { 'https://github.com/matze/vim-move' } }
})

return move_selection
