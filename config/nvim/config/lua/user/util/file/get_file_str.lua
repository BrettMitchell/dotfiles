return function(opts)
  opts = opts or {}

  if vim.fn.filereadable(opts.filename) ~= 1 then
    if not opts.silent then
      vim.notify('Cannot read file: ' .. opts.filename, 'error')
    end
    return
  end

  local blob = vim.fn.readblob(opts.filename)
  return tostring(blob)
end
