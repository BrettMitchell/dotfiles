local feat_util = require('user.util.feature')
local augroup = require('user.util.autocmd').make_augroup
local startup = require('user.features.startup.events')

local set_cwd = {
  name = 'Static Session Working Directory',

  description = [[
  This feature sets neovim's working directory at startup based
  on the first parameter supplied from the command line.

  If no argument is provided, this feature does not activate.

  If a directory is provided as the first parameter, it is used
  as the working directory. This will work if the path is absolute
  or if it is given relative to the shell's working directory.
  ]],

  augroup = augroup {
    {
      name = 'SetCwd',
      autocmds = {
        {
          events = 'User',
          opts = {
            pattern = startup.events.Loaded,
            callback = function(event)
              if not event.data
                  or not event.data.pathFromArgs
                  or vim.fn.isdirectory(event.data.pathFromArgs) ~= 1 then
                return
              end

              vim.cmd('cd ' .. event.data.pathFromArgs)
              vim.g.__user__cwd__ = event.data.pathFromArgs
            end
          }
        }
      }
    },

    {
      name = 'PreserveCwd',
      autocmds = {
        {
          events = 'BufEnter',
          opts = {
            callback = function()
              if not vim.g.__user__cwd__ then return end
              vim.cmd('cd ' .. vim.g.__user__cwd__)
            end
          }
        }
      }
    }
  }
}

function set_cwd.setup(config)
  feat_util.apply_config(config or {}, set_cwd)
  set_cwd.augroup:apply()
end

return set_cwd
