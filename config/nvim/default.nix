{ config, root, ... }:
{
  home.file.".config/nvim".source = ./config;
}
