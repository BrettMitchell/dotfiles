--[[

Feature description:

  This module is intended to provide the user with a way of loading key bindings that are specific to notes only when the current working directory is known to be a notes directory.
  This module will require that the user specify the directory as a notes directory.

  Todo:
    - [ ] When opening a file that is within a note directory, attach configured mappings as buffer local

--]]

local autocmd = require('user.lib.autocmd').autocmd
local noteDirectories = require('user.emanote_integration.noteDirectories')

local M = {}

function M.setup(config)
  config = config or {}

  autocmd(
    'AttachLocalMappings',
    {
      clear = true,

      { 'BufEnter', {
        callback = function()
          local currentBufnr = vim.fn.bufnr('%')
        end
      } }
    }
  )

end

return M

