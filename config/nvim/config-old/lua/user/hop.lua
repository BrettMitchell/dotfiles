local M = {}

function M.lineVertical()
  require('hop').hint_vertical {}
end

function M.lineBegin()
  require('hop').hint_lines {}
end

function M.wordBegin()
  require('hop').hint_words {
    hint_position = require('hop.hint').HintPosition.BEGIN
  }
end

function M.wordEnd()
  require('hop').hint_words {
    hint_position = require('hop.hint').HintPosition.END
  }
end

function M.singleCharBefore()
  require('hop').hint_char1 {
    direction = require('hop.hint').HintDirection.BEFORE_CURSOR,
    current_line_only = true
  }
end

function M.singleCharAfter()
  require('hop').hint_char1 {
    direction = require('hop.hint').HintDirection.AFTER_CURSOR,
    current_line_only = true
  }
end

function M.fCharBefore()
  require('hop').hint_char1 {
    direction = require('hop.hint').HintDirection.BEFORE_CURSOR,
    current_line_only = false,
  }
end

function M.fCharAfter()
  require('hop').hint_char1 {
    direction = require('hop.hint').HintDirection.AFTER_CURSOR,
    current_line_only = false,
  }
end

function M.tCharBefore()
  require('hop').hint_char1 {
    direction = require('hop.hint').HintDirection.BEFORE_CURSOR,
    current_line_only = false,
    hint_offset = -1,
  }
end

function M.tCharAfter()
  require('hop').hint_char1 {
    direction = require('hop.hint').HintDirection.AFTER_CURSOR,
    current_line_only = false,
    hint_offset = -1,
  }
end

function M.nChar()
  vim.cmd [[ HopPattern ]]
end

return M
