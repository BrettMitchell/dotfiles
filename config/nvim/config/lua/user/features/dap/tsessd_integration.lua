local feat_util = require('user.util.feature')
local plugin_util = require('user.util.plugins.lazy')
local system_binary = require('user.util.feature.system_binary_dependency').system_binary
local tmux_util = require('user.util.tmux')
local table_util = require('user.util.table')
local string_util = require('user.util.string')
local adapter_util = require('user.features.dap.adapters.util')

local adapters = require('user.features.dap.adapters')

local M = {
  name = 'DAP :: tsessd integration',

  description = [[
  This feature integrates nvim-dap with tsessd's managed process functionality.

  It adds a dynamic config provider which identifies valid debug targets via process tags.
  ]],

  debugger_type_associations = {
    binary = {
      node = adapters.js_vscode.adapter_name,
      dlv = adapters.delve.adapter_name,
    },
    -- well_known_files = { },
  }
}

function M.setup()
  feat_util.assert_deps('DAP tsessd integration', {
    system_binary('tsess'),
    system_binary('tmux'),
  })

  if vim.fn.getenv('TMUX') == vim.NIL then
    return
  end

  plugin_util.add_post_load_cb(function()
    require('dap').providers.configs.tsessd = M.get_configs
  end)
end

--[[
This function retrieves all tsessd manaaged processes
tagged with the 'debugger' well-known tag
--]]
function M.discover_debug_targets()
  local session_name = tmux_util.get_tmux_value('session_name')
  local result = vim.system({ 'tsess', 'proc-show', session_name, '-F', 'json' }):wait()
  local ok, parsed = pcall(vim.fn.json_decode, result.stdout)
  if not ok then return {} end
  local targets = {}

  for _, proc in ipairs(parsed) do
    local tags = proc.metadata.tags or {}
    if table_util.includes('debugger', tags) then
      table.insert(targets, proc)
    end
  end

  return targets
end

--[[
In the absence of an explicit debug adapter tag, this function
guesses the adapter type using the configured association map
--]]
local function guess_adapter(proc)
  local binary_name = proc.cmd[1]
  local adapter_name = M.debugger_type_associations.binary[binary_name]
  if adapter_name then return adapter_name end
  return 'Unknown adapter'
end

local function get_config(proc)
  local adapter_name = guess_adapter(proc)
  for _, tag in ipairs(proc.metadata.tags) do
    if string_util.starts_with(tag, 'dap:') then
      adapter_name = string_util.replace_start(tag, 'dap:', '')
    end
  end

  local adapter = adapters[adapter_name]
  if not adapter then
    return nil
  end

  local proc_records = adapter_util.get_ports_from_pids({ proc.pid })
  if not proc_records or #proc_records == 0 then
    return nil
  end

  local proc_record = proc_records[1]

  local config = {
    type = adapter.adapter_name,
    request = 'attach',
    name = 'Attach tsessd - ' .. proc.pid .. '@localhost:' .. tostring(proc_record.port),
    port = proc_record.port
  }

  if adapter.tsessd_additional_config then
    config = table_util.merge.mergeLeft(config, adapter.tsessd_additional_config)
  end

  show(config)

  return config
end

--[[
This function produces a list of DAP configurations for attaching to
existing debugger processes.
--]]
function M.get_configs()
  local targets = M.discover_debug_targets()
  local configs = table_util.map(targets, get_config)
  local valid_configs = table_util.filter(configs, function(config)
    return config ~= nil
  end)
  return valid_configs
end

return M
