return {
  config = function()
    require('FTerm').setup {
      border = 'double',
      hl = 'Normal',
      dimensions = {
        width = 0.8,
        height = 0.8,
      },
    }
  end
}
