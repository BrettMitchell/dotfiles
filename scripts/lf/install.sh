#!/usr/bin/env bash

for script in "$module"/scripts/* ; do
  ln_safe "$script" "$HOME/.local/bin/"`basename "$script"`
done

