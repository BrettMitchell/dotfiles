local actions = require('telescope.actions')
local helpers = require('user.features.telescope.helpers')

local function handleSelection(cb)
  return function(prompt_bufnr)
    local selectedItems = helpers.getMultiSelection(prompt_bufnr)
    actions.close(prompt_bufnr)

    return cb(selectedItems, prompt_bufnr)
  end
end

local function make_attach_mappings(mappings)
  if type(mappings) ~= 'table' then
    return function() end
  end

  return function(_, map)
    for mode, mapTargets in pairs(mappings) do
      -- Skip 'select_default' as special case
      if mode ~= 'select_default' then
        if type(mapTargets) ~= 'table' then
          error 'Mappings must be a table of the form "{ [mode] = { [key] = action } }"'
        end

        for key, action in pairs(mapTargets or {}) do
          map(mode, key, action)
        end
      end
    end

    if mappings.select_default then
      actions.select_default:replace(handleSelection(mappings.select_default))
    end

    return true
  end
end

return {
  make_attach_mappings = make_attach_mappings
}

