local dap = require('dap')
local dapUiVariables = require('dap.ui')

local userDap = require('user.dap')
local map = require('user.lib.map')

local M = {}

function M.setupStatic()
  map.registerKeyMap({ 'n' }, {
    ['<M-d>'] = { require('dapui').toggle, 'Open debugger' },
    ['<space>d'] = {
      b = { dap.toggle_breakpoint, 'Toggle breakpoint on current line' },
      C = { dap.clear_breakpoints, 'Clear all breakpoints' },
      ['.'] = { dap.run_last, 'Re-run the last used debug adapter / configuration' },
      a = { userDap.attach, 'Attach debugger session' },
      x = { dap.close, 'Detatch from current session' },
    },
  });
end

function M.setupOnAttach()
  -- https://github.com/mfussenegger/nvim-dap/wiki/Cookbook#map-k-to-hover-while-session-is-active
  map.defineGlobalOverwriteMap('dap', { 'n' }, {
    [';j'] = { dap.step_into, 'Step in' },
    [';k'] = { dap.step_out, 'Step out' },
    [';n'] = { dap.step_over, 'Step over' },
    [';c'] = { dap.continue, 'Continue' },
    [';i'] = { function()
      dapUiVariables.hover(function() return vim.fn.expand('<cexpr>') end)
    end, 'Re-run the last used debug adapter / configuration' },
  });

  dap.listeners.after['event_initialized']['me'] = function()
    map.globalOverwrite('dap')
  end

  dap.listeners.after['event_terminated']['me'] = function()
    map.globalRestore('dap')
  end
end

return M
