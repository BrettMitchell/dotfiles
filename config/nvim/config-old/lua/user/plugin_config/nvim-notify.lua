_G.notifySeverityFilter = {}

local M = {
  notifySeverityFilter = {},
  messageFilters = {},
}

function M.setFilter(filter)
  M.notifySeverityFilter = filter or {}
end

function M.addMessageFilter(fn)
  table.insert(M.messageFilters, fn)
end

function M.config(self)
  local notify = require('notify')

  notify.setup {
    render = 'minimal',
    stages = 'fade',
    top_down = false,
  }

  vim.notify = function(msg, severity)
    for _, filter in pairs(self.messageFilters) do
      if filter(msg) then return end
    end

    if #self.notifySeverityFilter == 0 then

      notify(msg, severity)

    else

      for _, v in ipairs(self.notifySeverityFilter) do
        if v == severity then
          notify(msg, severity)
        end
      end

    end
  end
end

return M
