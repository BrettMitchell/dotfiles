return {
  append_lines = require('pax-vim.util.file.append_lines'),
  get_file_lines = require('pax-vim.util.file.get_file_lines'),
  replace_text_range = require('pax-vim.util.file.replace_text_range'),
}
