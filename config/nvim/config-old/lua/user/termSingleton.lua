-- API Reference : https://github.com/numToStr/FTerm.nvim/blob/master/lua/FTerm/terminal.lua
local Terminal = require('FTerm.terminal')
local Utils = require('FTerm.utils')
local server_name = require('user.server').server_name
local t = require('user.lib.termcodes').t

local brootConfFile = vim.fn.tempname() .. '.hjson'
local brootConf = {
  '{',
  '  verbs: [',
  '    {',
  '      invocation: terminal',
  '      key: enter',
  [[      external: nvr --servername ]] ..
      server_name ..
      [[ -l --remote-send ":lua require('user.termSingleton').closeActive()<enter>:silent e {file}<enter>"]],
  '      leave_broot: false',
  '      apply_to: file',
  '    }',
  '    {',
  '      invocation: terminal',
  '      key: alt-enter',
  [[      external: nvr --servername ]] ..
      server_name ..
      [[ -l --remote-send ":lua require('user.termSingleton').closeActive()<enter>:silent vsplit {file}<enter>:lua require('user.broot').refocusBroot()<enter>"]],
  '      leave_broot: false',
  '      apply_to: file',
  '    }',
  '    {',
  '      invocation: terminal',
  '      key: ctrl-alt-enter',
  [[      external: nvr --servername ]] ..
      server_name ..
      [[ -l --remote-send ":lua require('user.termSingleton').closeActive()<enter>:silent split {file}<enter>:lua require('user.broot').refocusBroot()<enter>"]],
  '      leave_broot: false',
  '      apply_to: file',
  '    }',
  '  ]',
  '}',
}
vim.fn.writefile(brootConf, brootConfFile)

local terms = {}

local function terminalSingleton(termName)
  local iface = { termName = termName }

  function iface.init(self, opts)
    local ft = opts and opts.ft or 'term/' .. self.termName
    self.term = Terminal:new()
    self.term:setup {
      ft = ft,
      cmd = opts.cmd
    }
    terms[#terms + 1] = self.term
  end

  function iface.isOpen(self)
    return self.term
  end

  function iface.toggle(self)
    if not self.term then
      print('Terminal not initialized')
      return
    end

    self.term:toggle()
  end

  function iface.open(self)
    if not self.term then
      iface:init()
    end

    self.term:open()
  end

  function iface.close(self)
    if not self.term then
      return
    end

    self.term:close()
  end

  function iface.sendKeys(self, keys)
    if not self.term then
      print('Terminal ' .. self.termName .. ' not open')
      return
    end

    -- Use custom send to channel instead of brootTerm:run
    -- because brootTerm:run appends a <CR> termcode
    vim.api.nvim_chan_send(
      self.term.terminal,
      keys
    )
  end

  return iface
end

local function brootForDir(dir)
  dir = dir or vim.fn.getcwd()

  local brootCmd = "cd '" .. dir .. "' ; broot --conf ~/.config/broot/conf.nvim.hjson\\;" .. brootConfFile
  local iface = terminalSingleton('broot::' .. dir)
  iface:init({ cmd = brootCmd, ft = 'broot' })

  function iface.setBrootFilter(newFilter)
    iface:sendKeys(newFilter)
  end

  function iface.focusCurrentFile()
    -- %:.
    --  %  -> Current file
    --  :. -> Relative to current directory, if possible
    local file = vim.fn.expand('%:.')

    if vim.fn.filereadable(file) == 0 then
      return
    end

    local escapedFile = file:gsub('/', '\\/')

    iface:open()
    iface:sendKeys(escapedFile)
  end

  return iface
end

local broot = {
  cwd = brootForDir(),
  notes = brootForDir(vim.fn.getenv('NOTES_DIR')),
}

local function lfForDir(dir)
  dir = dir or vim.fn.getcwd()

  local selectionPathTempFile = vim.fn.tempname()
  local lastDirTempFile = vim.fn.tempname()
  local lfCmd = "cd '" ..
      dir ..
      "' ; lf --config ~/.config/lf/nvim.lfrc -last-dir-path=" ..
      lastDirTempFile .. ' -selection-path=' .. selectionPathTempFile
  local iface = terminalSingleton('lf::' .. dir)
  iface:init({ cmd = lfCmd, ft = 'lf' })

  -- function iface.focusCurrentFile()
  --   -- %:.
  --   --  %  -> Current file
  --   --  :. -> Relative to current directory, if possible
  --   local file = vim.fn.expand('%:.')
  --
  --   if vim.fn.filereadable(file) == 0 then
  --     return
  --   end
  --
  --   local escapedFile = file:gsub('/', '\\/')
  --
  --   iface:open()
  --   iface:sendKeys(escapedFile)
  -- end

  return iface
end

local lf = {
  cwd = lfForDir(),
}

local function closeActive()
  for _, term in ipairs(terms) do
    term:close()
  end
end

local function switchToOrToggle(targetTerm)
  if Utils.is_win_valid(targetTerm.term.win) then return end

  for _, term in ipairs(terms) do
    term:close()
  end

  targetTerm:open()
end

return {
  broot = broot,
  lf = lf,
  closeActive = closeActive,
  switchToOrToggle = switchToOrToggle,
  terminalSingleton = terminalSingleton,
}
