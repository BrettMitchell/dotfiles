{ nixpkgs, ... }:
{
  home.packages = with nixpkgs.stable; [
    tor-browser
  ];
}
