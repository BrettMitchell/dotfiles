--[[

Feature design:

  Requirements:
    - [x] Command to start emanote server
    - [x] Command to stop emanote server
    - [x] Command to emanote server in default browser
    - [x] Parameterize command to open server in browser

--]]

local open = require('user.lib.open').open

local M = {}

M.defualtConfig = {
  enabled = true,
  port = 9010,
  open_url = open,
  key_map = {}
}

M.serverJobId = nil

function M.start(port)
  port = port or M.defualtConfig.port
  M.serverJobId = vim.fn.jobstart(("emanote run --port '%s'"):format(port))
end

function M.stop()
  if M.serverJobId then
    vim.fn.jobstop(M.serverJobId)
  end
end

function M.openInBrowser(config)
  config = config or {}
  local port = config.port or M.defualtConfig.port
  local open_url = config.open_url or M.defualtConfig.open_url

  local serverUrl = ("localhost:%s"):format(port)
  if serverUrl then
    open_url(serverUrl)
  end
end

function M.setup(config)
  config = config or M.defualtConfig

  if config.enabled then
    -- if config.key_map then
    --   registerKeyMap(config.key_map)
    -- end

    vim.api.nvim_create_user_command('EmanoteStartServer', function () M.start(config.port) end, {})
    vim.api.nvim_create_user_command('EmanoteStopServer', M.stop, {})
    vim.api.nvim_create_user_command('EmanoteOpenServer', function () M.openInBrowser(config) end, {})
  end
end

return M
