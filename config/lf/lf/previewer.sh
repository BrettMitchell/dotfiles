#!/usr/bin/env bash

mux-preview "$@"
exit $?

file=$1
w=$2
h=$3
x=$4
y=$5

dflt() {
  [ -f "$file" ] && bat --color=always --style=plain -- "$file";
  [ -d "$file" ] && tree "$file" -La 1;
}

previewByExtension() {
  case "$file" in
    *.md|*.qmd) glow -s dark "$file" ;;
    *.tgz|*.tar.gz) tar tzf "$file";;
    *.tar.bz2|*.tbz2) tar tjf "$file";;
    *.tar.txz|*.txz) xz --list "$file";;
    *.tar) tar tf "$file";;
    *.zip|*.jar|*.war|*.ear|*.oxt) unzip -l "$file";;
    *.rar) unrar l "$file";;
    *.7z) 7z l "$file";;
    *.[1-8]) man "$file" | col -b ;;
    *.o) nm "$file" | less ;;
    *.torrent) transmission-show "$file";;
    *.iso) iso-info --no-header -l "$file";;
    *odt,*.ods,*.odp,*.sxw) odt2txt "$file";;
    *.doc) catdoc "$file" ;;
    *.docx) docx2txt "$file" - ;;
    *) dflt ;;
  esac
}

kittyRenderImage() {
  yLocal="$2"
  kitten icat --silent --stdin no --transfer-mode file --place "${w}x${h}@${x}x${yLocal}" "$1" < /dev/null > /dev/tty;
}

# Aligned image preview with media info
kittyImagePreview() {
  # local info=`mediainfo "$file"`
  # local lines=`printf '%s' "$info" | wc -l`
  # printf '%s' "$info"

  # kittyRenderImage "$file" $(( $y + $lines + 1 ))
  kittyRenderImage "$file" $y

  # Non-zero exit code required to signal to lf that the preview needs to reload each time
  # Otherwise, the preview for an image will load only when the cursor first visits the file
  exit 127
}

kittyVideoThumbnail() {
  # local info=`mediainfo "$file"`
  # local lines=`printf '%s' "$info" | wc -l`
  # printf '%s' "$info"

  ffmpegthumbnailer -s 0 -o /tmp/thumbnail.jpg -i "$file"
  # kittyRenderImage /tmp/thumbnail.jpg $(( $y + $lines + 1 ))
  kittyRenderImage "$file" $y
  rm /tmp/thumbnail.jpg

  exit 127
}

kittyPdfPreview() {
  echo "Pass"
}

previewByMimeType() {
  local mimetype=`xdg-mime query filetype "$file"`;

  case "$mimetype" in
    image/vnd.djvu) ;;
    image/*) kittyImagePreview ;;
    video/*) kittyVideoThumbnail ;;
    application/pdf) pdftotext "$file" - ;;
    *) previewByExtension ;;
  esac
}

previewByMimeType
