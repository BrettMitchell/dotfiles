local zkApi = require('zk.api')

local pickers = require('telescope.pickers')
local finders = require('telescope.finders')
local sorters = require('telescope.sorters')

local attachMappings = require('user.telescope.attachMappings')
local theme = require('user.telescope.theme')

local zkFinder = require('user.notes.telescope.util.zkFinder')
local noteUtil = require('user.notes.util')

local function zkPick(opts)
  opts = opts or {}

  local mappings = opts.mappings or {}

  local finderConfig = {
    getQuery = opts.getQuery
  }

  if opts.entry_maker then
    finderConfig.entry_maker = opts.entry_maker
  end

  local function getSorter()
    if (opts.highlighter) then
      return sorters.Sorter:new({
        scoring_function = function () return 1 end,
        highlighter = opts.highlighter,
      })
    end

    return sorters.highlighter_only()
  end

  local sorter = opts.sorter or getSorter()

  local picker = pickers.new {
    default_text = opts.default_text,
    results_title = opts.title or '',
    prompt_title = opts.prompt_title or '',
    finder = zkFinder.new_zk_finder(finderConfig),
    sorter = sorter,
    previewer = opts.previewer or nil,
    attach_mappings = attachMappings.make_attach_mappings(mappings),
  }

  picker:find(theme.horizontal)
end

local function zkPickTags(opts)
  opts = opts or {}

  local mappings = opts.mappings or {}

  zkApi.tag.list(noteUtil.getNotesDir(), opts.query or {}, function(err, tags)
    if (err) then
      vim.notify('Encountered an error while running zk: ' .. vim.inspect(err), 'error')
      return
    end

    local picker = pickers.new {
      default_text = opts.default_text,
      results_title = opts.results_title or '',
      prompt_title = opts.prompt_title or '',

      attach_mappings = attachMappings.make_attach_mappings(mappings),
      previewer = opts.previewer or nil,
      sorter = sorters.get_generic_fuzzy_sorter(),

      finder = finders.new_table {
        results = tags,
        entry_maker = function(tag)
          return {
            value = tag,
            display = tag.name,
            ordinal = tag.name,

            preview_command = function(entry, bufnr)
              vim.api.nvim_buf_set_lines(bufnr, 0, -1, true, entry.value.files)
            end
          }
        end
      }
    }

    picker:find(theme.horizontal)
  end)
end

return {
  zkPick = zkPick,
  zkPickTags = zkPickTags,
}
