#!/usr/bin/env bash

set -a
. "$(dirname "$0")/lib.sh"
set +a

function get_remove_scripts {
  fd --type x remove ./config ./scripts ./src \
    | sed -E 's:^\./::' \
    | sed -E 's:/remove\.sh$::'
}

function remove {
  local target=`cat -`
  if [[ -z "$target" ]] ; then
    return 0
  fi

  printf "\n> Removing module '$target'"
  printf "\n\n"

  export module="$root/$target"
  "$module/remove.sh"

  printf "\nDONE\n"
}

function main {
  local target="$1"

  if [[ "$target" == all ]] ; then
    get_remove_scripts | remove
    return 0
  fi

  if [[ ! -z "$target" ]] ; then
    printf '%s' "$target" | remove
    return 0
  fi

  get_remove_scripts | sk | remove
}

main "$@"

