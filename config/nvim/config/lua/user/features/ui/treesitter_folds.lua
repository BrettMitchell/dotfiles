local make_augroup = require('user.util.autocmd').make_augroup

local config = {
  ignore_file = function() return false end,
  start_open = true
}

local M = {
  name = 'Treesitter Folding',

  description = [[
  This feature creates folds using treesitter and optionally
  unfolds them.
  ]],

  config = config,

  augroups = make_augroup({
    {
      name = 'TreeSitterExprFolding',
      autocmds = {
        {
          events = {
            'BufAdd',
            'BufEnter',
            'BufNew',
            'BufNewFile',
            'BufReadPost',
            'BufWinEnter',
            'UIEnter'
          },

          opts = {
            callback = function()
              if config.ignore_file() then return end

              vim.schedule(function ()
                vim.opt_local.foldmethod = 'expr'
                vim.opt_local.foldexpr = 'nvim_treesitter#foldexpr()'
                if config.start_open then
                  vim.cmd [[ silent! :%foldopen! ]]
                end
              end)
            end
          }
        },

        {
          -- A silly hack for force nvim to recompute folds
          events = { 'TextChanged', 'TextChangedI' },
          opts = {
            callback = function()
              if config.ignore_file() then return end
              vim.opt_local.foldmethod = 'expr'
            end
          }
        }
      }
    }
  })
}

function M.setup()
  M.augroups:apply()
end

return M
