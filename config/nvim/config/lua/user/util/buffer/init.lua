return {
  append_lines = require('user.util.buffer.append_lines'),
  get_buf_lines = require('user.util.buffer.get_buf_lines'),
  insert_text_at_cursor = require('user.util.buffer.insert_text_at_cursor'),
  make_lines = require('user.util.buffer.make_lines'),
  replace_text_range = require('user.util.buffer.replace_text_range'),
}
