local add_post_load_cb = require('user.util.plugins.lazy').add_post_load_cb
local augroup = require('user.util.autocmd').make_augroup

local frontmatter = require('user.features.notes.frontmatter.v2')

local events = {
  FrontmatterLoaded = 'FrontmatterLoaded',
}

local frontmatter_loaded = {
  name = 'Frontmatter Loaded User Autocommand',

  description = [[
  This feature provides a user autocommand which parses the frontmatter
  of markdown files on BufEnter, and which emits the parsed frontmatter
  in a User event.
  ]],

  events = events,

  augroups = augroup { {
    name = 'ParseFrontmatter',
    autocmds = { {
      events = { 'BufEnter', 'FocusGained' },
      opts = {
        callback = function()
          local ft = vim.bo.filetype
          if ft == 'markdown' or ft == 'quarto' then
            local filename = vim.fn.expand('%:p')
            local parsed_frontmatter = frontmatter.get_file_frontmatter({ filename = filename })

            local event_data = {
              filename = filename,
              frontmatter = parsed_frontmatter
            }

            vim.api.nvim_exec_autocmds('User', {
              pattern = events.FrontmatterLoaded,
              data = event_data,
            })
          end
        end
      }
    } }
  } },
}

function frontmatter_loaded.setup()
  add_post_load_cb(function()
    frontmatter_loaded.augroups:apply()
  end)
end

return frontmatter_loaded
