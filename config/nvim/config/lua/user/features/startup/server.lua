local M = {
  name = 'Server',

  description = [[
  This feature starts the neovim server with a known file name and then
  makes that filename available to other modules through a method.
  ]],
}

function M.get_server(self)
  return self.server_name
end

function M.setup()
  local existing_servers = vim.fn.serverlist()
  if #existing_servers > 0 then
    M.server_name = existing_servers[1]
  else
    M.server_name = vim.fn.serverstart()
  end
end

return M
