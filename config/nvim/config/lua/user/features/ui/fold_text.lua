local make_augroup = require('user.util.autocmd').make_augroup

local fold_text = {
  name = 'UI Settings: Fold text',

  description = [[
  This feature makes fold text a little nicer to look at.
  ]],

  augroup = make_augroup({
    name = 'SetFoldText',
    autocmds = {
      {
        events = {
          'BufAdd',
          'BufEnter',
          'BufNew',
          'BufNewFile',
          'BufReadPost',
          'BufWinEnter',
          'UIEnter'
        },

        opts = {
          callback = function()
            vim.opt_local.foldtext = 'getline(v:foldstart) . " [" . (v:foldend - v:foldstart + 1) . "] "'
          end
        }
      }
    }
  })
}

function fold_text.setup()
  fold_text.augroup:apply()
end

return fold_text
