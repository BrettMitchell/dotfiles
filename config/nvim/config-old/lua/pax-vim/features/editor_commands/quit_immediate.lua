local make_feature = require('pax-vim.util.feature').make_feature_v2
local keymap = require('pax-vim.util.keymap')

local quit_normal = make_feature({
  name = 'System Commands: Quit (immediate)',

  description = [[
  This feature adds easier keymaps for exiting the editor.

  This version is intended for use when nvim serves as a
  prompting application, where just one file will be edited
  and then the application will be closed.
  ]],

  keymap = keymap {
    { 'n', '<C-c>', '<cmd>qa!<cr>', { desc = 'Cancel without saving' } },
    { 'n', '<space>Q', '<cmd>qa!<cr>', { desc = 'Cancel without saving' } },
    { 'n', '<space>q', '<cmd>qa<cr>', { desc = 'Exit' } },
    { 'n', '<space>x', '<cmd>wqa<cr>', { desc = 'Write and exit' } },
  },

  apply = function (self)
    self.keymap.apply()
  end
})

return quit_normal

