local add_plugins = require('pax-vim.util.plugins.lazy').add_plugins
local keymap = require('pax-vim.util.keymap')
-- local make_feature = require('pax-vim.util.feature').make_feature

local M = {
  feature = {
    name = 'Editing command: Break Object',

    description = [[
    This feature adds an editing command for breaking an inline object
    out onto multiple lines.

    This is accomplished using the 'trevJ' plugin from AckslD, which is
    essentially the inverse of the 'join line' operation which is bound
    to 'J' in vanilla Vim.
    ]],

    keymap = keymap {
      { 'n', '<space>es', '<cmd>TSJSplit<cr>', { desc = 'Split object to multiple lines' } },
      { 'n', '<space>eJ', '<cmd>TSJJoin<cr>', { desc = 'Join object to one line' } },
    },
  }
}

function M.setup()
  M.feature.keymap.apply()

  add_plugins({ {
    'https://github.com/Wansmer/treesj',
    config = function()
      require('treesj').setup({
        ---@type boolean Use default keymaps (<space>m - toggle, <space>j - join, <space>s - split)
        use_default_keymaps = false,

        ---@type boolean Node with syntax error will not be formatted
        check_syntax_error = true,

        ---If line after join will be longer than max value,
        ---@type number If line after join will be longer than max value, node will not be formatted
        max_join_length = 120,

        ---Cursor behavior:
        ---hold - cursor follows the node/place on which it was called
        ---start - cursor jumps to the first symbol of the node being formatted
        ---end - cursor jumps to the last symbol of the node being formatted
        ---@type 'hold'|'start'|'end'
        cursor_behavior = 'hold',

        ---@type boolean Notify about possible problems or not
        notify = true,

        ---@type boolean Use `dot` for repeat action
        dot_repeat = true,

        ---@type nil|function Callback for treesj error handler. func (err_text, level, ...other_text)
        on_error = nil,

        ---@type table Presets for languages
        -- langs = {}, -- See the default presets in lua/treesj/langs
      })
    end
  } })
end

return M
