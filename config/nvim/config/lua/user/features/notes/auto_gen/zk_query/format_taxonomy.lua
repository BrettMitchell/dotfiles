local stringUtil = require('user.util.string')
local table_util = require('user.util.table')


local M = {}

-- Assumes tags are given in sorted order
local function add_missing_tag_paths(tag_names)
  local dense_tag_list = {}

  -- local previous_tag_name = root_tag_name
  for _, tag_name in ipairs(tag_names) do
    local segments = stringUtil.split(tag_name, '.')
    local acc = ''
    for i, segment in ipairs(segments) do
      local suffix = segment
      if i > 1 then suffix = '.' .. suffix end
      acc = acc .. suffix
      if not table_util.includes(acc, dense_tag_list) then
        table.insert(dense_tag_list, acc)
      end
    end
  end

  return dense_tag_list
end

local function get_notes_for_tag(tag, notes)
  local tagged_notes = {}
  for _, note in ipairs(notes) do
    if table_util.includes(tag, note.tags or {}) then
      table.insert(tagged_notes, note)
    end
  end
  return tagged_notes
end

local function assemble_tree(ordered_tags, notes)
  local tree = { tag_name = '', notes = {}, tags = {} }
  local path = { tree }

  -- All untagged notes go in the root node
  for _, note in ipairs(notes) do
    if not note.tags or #note.tags == 0 then
      table.insert(tree.notes, note)
    end
  end

  for _, tag in ipairs(ordered_tags) do
    -- Unwind until a parent tag name is found (hard stop at unnamed root node)
    while #path > 1 and not stringUtil.starts_with(tag, path[#path].tag_name) do
      table.remove(path, #path)
    end

    local current = path[#path]

    local notes_for_tag = get_notes_for_tag(tag, notes)
    local new = { tag_name = tag, notes = notes_for_tag, tags = {} }
    table.insert(current.tags, new)
    table.insert(path, new)
  end

  return tree
end

local function format_link(note)
  return '[' .. note.title .. '](../' .. note.path .. ')'
end

local function render_tree(tree, indent)
  indent = indent or 0
  local indent_str = string.rep(' ', indent * 2)

  local lines = {}
  for _, note in ipairs(tree.notes) do
    table.insert(lines, indent_str .. '- ' .. format_link(note))
  end

  for _, subtree in ipairs(tree.tags) do
    table.insert(lines, indent_str .. '- ' .. subtree.tag_name)
    lines = table_util.concat(lines, render_tree(subtree, indent + 1))
  end

  return lines
end

function M.generate_taxonomy_tree(notes)
  if not notes then return {} end

  -- Get sorted, unique tag names from notes
  local tag_names = {}
  for _, note in ipairs(notes) do
    for _, tag in ipairs(note.tags or {}) do
      if not table_util.includes(tag, tag_names) then
        table.insert(tag_names, tag)
      end
    end
  end
  table.sort(tag_names)

  -- The list may omit some levels of the hierarchy:
  -- 'a.b'
  -- 'a.c.d.e'
  --
  -- We want:
  -- ''
  -- 'a'
  -- 'a.b'
  -- 'a.c'
  -- 'a.c.d'
  -- 'a.c.d.e'
  local dense_tag_list = add_missing_tag_paths(tag_names)

  local tree = assemble_tree(dense_tag_list, notes)
  local lines = render_tree(tree)
  return lines
end

return M
