return function(str)
  local without_trailing = string.gsub(str, '%s+$', '')
  return without_trailing
end
