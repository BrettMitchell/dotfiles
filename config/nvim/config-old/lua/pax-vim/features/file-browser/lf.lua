local make_feature = require('pax-vim.util.feature').make_feature
local terminal = require('pax-vim.features.terminal')
local system_binary = require('pax-vim.util.feature.system_binary_dependency').system_binary

local state = {
  lf_id = nil,
  on_id_set = function (_) end
}

local function set_id(lf_id)
  state.lf_id = lf_id
  state.on_id_set(lf_id)
  state.on_id_set = function () end
end

local config = {
  lf_config_file = vim.fn.getenv('HOME') .. '/.config/lf/pax-vim.lfrc',
  lf_dir = nil,
  recenter_key_sequence = '<C-\\><C-n>0i'
}

local function focus()
  local term = terminal.methods.getTerminal('filebrowser-lf')
  term:focus()
end

local function open()
  local term = terminal.methods.getTerminal('filebrowser-lf')
  term:open()
end

local function close()
  terminal.methods.getTerminal('filebrowser-lf'):close()
end

local function toggle()
  local term = terminal.methods.getTerminal('filebrowser-lf')
  term:toggle()
end

-- TODO: Either move this to user-space, or add a default
-- config that includes the required lf definition.
local function focusCurrentFileInLf()
  local filename = vim.fn.expand('%:p')
  if string.len(filename) == 0 then
    vim.notify('Buffer is not saved to a file', 'error')
    return
  end

  local function issue_focus_command(lf_id)
    vim.fn.system(('lf -remote "send ' .. lf_id .. ' cd-dir-or-file \'%s\'"'):format(filename))
  end

  -- Note: If the file browser has already been opened at least once, then we
  -- have an lf instance id available and we can go ahead and issue the focus
  -- command.
  --
  -- If the browser has not been opened yet, we do not have a way of identifying
  -- our browser instance, and we need to open it, wait for it to register its
  -- id, and THEN issue the focus command.

  open()
  if state.lf_id == nil then
    state.on_id_set = issue_focus_command
  else
    issue_focus_command(state.lf_id)
  end
end

local lf = make_feature({
  name = 'LF Integration',

  description = [[
  This feature provides a persistent 'lf' file browser instance in a
  terminal overlay.

  Note on 'recenter_key_sequence' option:

  LF specifically seems to cause alignment scrolling issues
  when restoring a floating terminal, so we feed a key
  sequence to NVim when opening this specific terminal
  designed to scroll back to the beginning of the buffer
  and re-enter insert mode.

  This key sequence can be overridden with 'recenter_key_sequence'
  if you do something weird to your mappings, but in the vast
  majority of cases the default value (`<C-\><C-n>0i`) should work.
  ]],

  dependencies = { terminal, system_binary('lf') },

  config = config,

  keymaps = {
    { { 'n', 't', 'i', 'x' }, '<C-space>', toggle, { desc = 'Toggle LF file browser' } },
    { { 'n', 'x', 'i' }, '<C-b>', focusCurrentFileInLf, { desc = 'Focus the current file in LF' } }
  },

  on_activate = function()
    local dir = config.lf_dir or vim.fn.getcwd()

    local selectionPathTempFile = vim.fn.tempname()
    local lastDirTempFile = vim.fn.tempname()
    local lfCmd = "cd '" ..
        dir ..
        "' ; lf -config '" .. config.lf_config_file .. "' -last-dir-path=" ..
        lastDirTempFile .. ' -selection-path=' .. selectionPathTempFile

    local iface = terminal.methods.makeTerminal({
      name = 'filebrowser-lf',
      cmd = lfCmd,
      ft = 'term-filebrowser-lf'
    })

    if iface == nil then
      vim.notify('Could not create LF terminal instance')
      return
    end

    iface:init()
  end,

  augroups = {
    {
      name = 'RecenterLf',
      autocmds = {
        {
          -- 'BufWinEnter' is REQUIRED here; 'BufEnter' fires before
          -- the user can enter insert mode, which causes window focus
          -- and mode weirdness when first opening the terminal
          events = 'BufWinEnter',

          opts = {
            callback = function ()
              if vim.bo.filetype == 'term-filebrowser-lf' then
                vim.api.nvim_input(config.recenter_key_sequence)
              end
            end
          }
        }
      }
    }
  }
})

lf.state = state

lf.methods = {
  open = open,
  close = close,
  toggle = toggle,
  focus = focus,
  set_id = set_id
}

return lf
