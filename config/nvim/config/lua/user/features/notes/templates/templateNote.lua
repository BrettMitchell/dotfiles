local getNotesByTags = require('user.features.notes.explore.plumbing').getNotesByTags
local pickNote = require('user.features.notes.telescope').pickNote

local M = {}

M.newTemplateNote = {
  -- frontmatter is called second, so it will have a selection value
  frontmatter = function(_, _, cb)
    return cb(nil, {
      tags = { 'meta.type.template' },
      templateFrontmatter = { tags = {} },
    });
  end,
  -- build is called first, so it is responsible for picking the template note
  build = function(_, _, cb)
    return cb(nil, '')
  end
}

M.fromTemplateNote = {
  -- What a hack. Refactor this garbage.
  selection = nil,

  -- frontmatter is called second, so it will have a selection value
  frontmatter = function(_, _, cb)
    if not M.fromTemplateNote.selection then
      return cb('Nothing selected!')
    end

    local res = M.fromTemplateNote.selection.frontmatter

    M.fromTemplateNote.selection = nil
    return cb(nil, res)
  end,

  -- build is called first, so it is responsible for picking the template note
  build = function(_, _, cb)
    getNotesByTags(
      {
        tags = { 'meta.type.template' },
        select = { 'title', 'absPath', 'body', 'metadata' }
      },

      function(err, notes)
        if #notes == 0 then return cb('No template notes found') end
        if err then return cb(err) end

        pickNote(notes, {
          mappings = { select_default = function(noteSelection)
            if #noteSelection == 0 then return cb('No note selected') end
            if #noteSelection > 1 then return cb('More than one note selected') end

            local picked = noteSelection[1]
            M.fromTemplateNote.selection = {
              -- Note: Something in zk seems to lowercase this key
              frontmatter = picked.value.metadata.templatefrontmatter
            }

            return cb(nil, picked.value.body)
          end }
        })
      end
    )
  end
}

return M
