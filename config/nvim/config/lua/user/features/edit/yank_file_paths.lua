local keymap = require('user.util.keymap')
local table_util = require('user.util.table')

local quote_pattern = "['\"](.+)['\"]"
-- Note: Not sure about colon, but I've thrown it in just to be safe.
-- I can't think of a case where colon is syntactically significant in a way that would ambiguate filenames.
local word_pattern = "[^-_:./%w]?([-_:./%w]+)[^-_:./%w]?"
local patterns = {
  quote_pattern,
  word_pattern,
}

local function resolve(root_path, filename)
  if filename[1] == '/' then
    return filename
  end
  return vim.fn.system({ 'realpath', root_path .. '/' .. filename }):gsub('%s+$', '')
end

local function extract_filenames(root_path, line)
  local paths = {}
  for _, pattern in ipairs(patterns) do
    for match in string.gmatch(line, pattern) do
      local resolved = resolve(root_path, match)
      if vim.fn.filereadable(resolved) == 1 or (match ~= '.' and match ~= '..' and vim.fn.isdirectory(resolved) == 1) then
        table.insert(paths, match)
      end
    end
  end
  return paths
end

local function get_filenames(lines, path_type)
  local root_path = vim.fn.expand('%:p')
  root_path = root_path:gsub('/[^/]*$', '')
  if #root_path == 0 then
    root_path = vim.fn.getcwd()
  end

  local paths = {}
  for _, line in ipairs(lines) do
    local new_paths = extract_filenames(root_path, line)
    paths = table_util.concat(paths, new_paths)
  end

  local final = {}
  for _, path in ipairs(paths) do
    if path_type == 'absolute' then
      path = resolve(root_path, path)
    end
    table.insert(final, path)
  end

  return final
end

local function yank_filenames(lines, path_type)
  local filenames = get_filenames(lines, path_type)
  local str = table_util.join(filenames, '\n')
  vim.fn.setreg('+', str)
  vim.fn.setreg('"', str)
end

local M = {
  feature = {
    name = 'Yank file paths',

    description = [[
    This feature adds a keybinding to yank all the file paths in your current selection
    or in the current file if there is no selection
    ]],

    get_filenames = get_filenames,
    yank_filename = yank_filenames,

    yank_abs_filename_in_file = function() yank_filenames(vim.fn.getline(0, '$'), 'absolute') end,
    yank_abs_filename_in_selection = function() yank_filenames(_G.getVisualSelection(), 'absolute') end,

    yank_rel_filename_in_file = function() yank_filenames(vim.fn.getline(0, '$'), 'relative') end,
    yank_rel_filename_in_selection = function() yank_filenames(_G.getVisualSelection(), 'relative') end,
  }
}

M.keymap = keymap {
  { 'n', ';yfa', M.feature.yank_abs_filename_in_file,      { desc = 'Yank absolute filename' } },
  { 'v', ';yfa', M.feature.yank_abs_filename_in_selection, { desc = 'Yank absolute filename' } },
  { 'n', ';yfr', M.feature.yank_rel_filename_in_file,      { desc = 'Yank relative filename' } },
  { 'v', ';yfr', M.feature.yank_rel_filename_in_selection, { desc = 'Yank relative filename' } },
}

function M.setup()
  M.keymap.apply()
end

return M
