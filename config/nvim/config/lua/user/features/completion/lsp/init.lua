local feat_util = require('user.util.feature')
local add_plugins = require('user.util.plugins.lazy').add_plugins
local lsp = require('user.features.lsp')

local M = {
  name = 'Completion :: LSP',
  dependencies = { lsp },

  description = [[
  Provides LSP autocompletion
  ]],

  source_definition = {
    name = 'nvim_lsp',
  },
}

function M.setup(config)
  feat_util.apply_config(config or {}, M.feature)
  add_plugins({ { 'https://github.com/hrsh7th/cmp-nvim-lsp' } })
  lsp.config.capabilities.all_servers = function()
    return require('cmp_nvim_lsp').default_capabilities()
  end
end

return M
