local make_feature = require('pax-vim.util.feature').make_feature

local M = make_feature({
  name = 'Macro Command Height',

  description = [[
  This feature sets cmdheight to 1 when recording a macro so that
  you can have some visual indicator of what your macro state is.

  Not needed if you do not use cmdheight=0.

  Source: https://www.reddit.com/r/neovim/comments/vfn99v/psa_macros_w_cmdheight_0/
  ]],

  augroups = {
    {
      name = 'MacroCmdheight',
      autocmds = {
        {
          events = 'RecordingEnter',
          opts = {
            callback = function()
              vim.opt_local.cmdheight = 1
            end
          }
        },

        {
          events = 'RecordingLeave',
          opts = {
            callback = function()
              local timer = vim.loop.new_timer()
              -- NOTE: Timer is here because we need to close cmdheight AFTER
              -- the macro is ended, not during the Leave event
              timer:start(
                50,
                0,
                vim.schedule_wrap(function()
                  vim.opt_local.cmdheight = 0
                end)
              )
            end
          }
        },
      }
    }
  }
})

return M
