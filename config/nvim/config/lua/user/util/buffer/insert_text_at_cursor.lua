local unpack = require('user.util.table.unpack')

return function(text)
  local row, column = unpack(vim.api.nvim_win_get_cursor(0))
  if type(text) == 'string' then text = { text } end
  vim.api.nvim_buf_set_text(0, row - 1, column, row - 1, column, text)
end
