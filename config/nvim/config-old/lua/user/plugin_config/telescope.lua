return {
  config = function()
    local telescope = require('telescope')
    local keys = require('user.telescope.keys')

    telescope.setup {
      defaults = {
        default_mappings = {},
        mappings = {
          i = keys.insertMode,
        }
      },

      extensions = {
        emanote = {
          -- mappings = {
          --   n = require('user.emanote_integration.telescope').defaultMappings.i
          -- }
        }
      }
    }

    -- telescope.load_extension('emanote')
  end
}
