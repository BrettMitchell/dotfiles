local autocmd = require('user.lib.autocmd').autocmd
local showDashboard = require('user.utils').showDashboard

-- If opening a directory as an argument, cd to that directory
-- Open Alpha greeter only if input is not piped to stdin

local M = {}

function M.setup()
  local std_in = false

  local function setWorkingDirectory()
    local path = require('path')

    local cwd = vim.fn.getcwd()
    local args = vim.fn.argv()
    if not args or #args == 0 then return end

    local specifiedDir = args[1]

    if (path.isabs(specifiedDir) and vim.fn.isdirectory(specifiedDir)) then
      vim.cmd('cd ' .. specifiedDir)
      vim.g.__user__cwd__ = specifiedDir
      if not std_in then showDashboard() end
      return
    end

    local resolvedToCwd = cwd .. path.DIR_SEP .. specifiedDir
    if (vim.fn.isdirectory(resolvedToCwd)) then
      vim.cmd('cd ' .. resolvedToCwd)
      vim.g.__user__cwd__ = resolvedToCwd
      if not std_in then showDashboard() end
      return
    end
  end

  -- Set current directory to dir specified in CLI argument when opening a project folder
  autocmd(
    'OpenDirectory',
    {
      clear = true,
      { 'VimEnter', {
        callback = setWorkingDirectory
      } },
      { 'StdinReadPre', {
        callback = function() std_in = true end
      } }
    }
  )

  autocmd(
    'PreserveCwd',
    {
      clear = true,
      { 'BufEnter', {
        callback = function()
          if not vim.g.__user__cwd__ then return end
          vim.cmd('cd ' .. vim.g.__user__cwd__)
        end
      } }
    }
  )
end

return M
