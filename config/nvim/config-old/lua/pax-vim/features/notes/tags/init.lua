local zk             = require('zk.api')

local pick_static    = require('pax-vim.features.telescope').methods.pick_static
local string_util    = require('pax-vim.util.string')
local table_util     = require('pax-vim.util.table')

local frontmatter_v1 = require('pax-vim.features.notes.frontmatter')
local frontmatter    = require('pax-vim.features.notes.frontmatter.v2')
local links          = require('pax-vim.features.notes.links')
local util           = require('pax-vim.features.notes.util')
local explore        = require('pax-vim.features.notes.explore.plumbing')
local pickNote       = require('pax-vim.features.notes.telescope').pickNote
local TAGS           = require('pax-vim.features.notes.special_tags')

local taxonomy       = require('pax-vim.features.notes.tags.taxonomy')

local M              = {}

M.taxonomy           = taxonomy

-- Toggle selected tags
local function addTagsToNote(tags)
  local justSelectedTagNames = {}
  for i, tagRecord in pairs(tags) do
    justSelectedTagNames[i] = tagRecord[1]
  end

  frontmatter.map_buf_frontmatter({
    fn = function(fm)
      local new_tags = {}

      -- Remove old tags
      for _, tag in ipairs(fm.tags) do
        if not table_util.includes(tag, justSelectedTagNames) then
          table.insert(new_tags, tag)
        end
      end

      -- Add new tags
      for _, tag in ipairs(justSelectedTagNames) do
        if not table_util.includes(tag, fm.tags) then
          table.insert(new_tags, tag)
        end
      end

      fm.tags = new_tags

      return fm
    end
  })
end

function M.addTags()
  zk.tag.list(util.getNotesDir(), {}, function(err, tags)
    if (err) then
      vim.notify(err)
      return
    end

    local tagNames = {}
    for i, tagRecord in pairs(tags) do
      tagNames[i] = tagRecord.name
    end

    pick_static(tagNames, {
      prompt_title = 'Add tags to current note',
      mappings = { select_default = addTagsToNote },
      layout_strategy = 'minimal_reversed',
      preview = false,
      layout_config = {
        width = 0.4,
        height = 0.7,
      },
    })
  end)
end

-- Important! This does NOT handle the general case for tags.
-- It can only handle frontmatter tags; tags using hash syntax in the note body will be missed.
function M.refactorTag(tagName)
  local newTagName = vim.fn.input({
    prompt = 'New tag name: ',
    default = tagName,
    cancelreturn = 0,
  })

  -- On no input, vim.fn.input will return the cancelreturn value of 0.
  -- If any input was supplied, it will be of type 'string'
  if type(newTagName) == 'number' then
    return
  end

  if string.len(newTagName) == 0 then
    vim.notify('Cannot rename tag to empty string')
    return
  end

  if newTagName == tagName then
    vim.notify('No change to tag name')
    return
  end

  zk.tag.list(util.getNotesDir(), {}, function(tagErr, tags)
    if (tagErr) then
      vim.notify('An error occurred while getting notes with tag ' .. tagName .. ': ' .. vim.inspect(tagErr))
      return
    end

    for _, otherTag in ipairs(tags) do
      if otherTag.name == newTagName then
        vim.notify("Tag '" ..
          newTagName .. "' already exists (" .. otherTag.note_count .. " notes).\nContinuing will merge the two.")

        local mergeResponse = vim.fn.input({
          prompt = 'Continue? (y/n): ',
          cancelreturn = 'n',
        })

        if mergeResponse ~= 'y' then
          vim.notify('Canceled')
          return
        end
      end
    end

    zk.list(util.getNotesDir(), { tags = { tagName }, select = { 'tags', 'title', 'absPath' } },
      function(notesErr, notes)
        if (notesErr) then
          vim.notify('An error occurred while getting notes with tag ' .. tagName .. ': ' .. vim.inspect(notesErr))
          return
        end

        for _, note in ipairs(notes) do
          local newTags = {}
          for _, tag in ipairs(note.tags) do
            if (tag ~= tagName) then
              table.insert(newTags, tag)
            else
              table.insert(newTags, newTagName)
            end
          end

          frontmatter_v1.setAttributes({ tags = newTags }, { merge = false, filename = note.absPath })
        end

        vim.cmd('ZkIndex')
      end)
  end)
end

function M.refactorTagUnderCursor()
  local WUnderCursor = vim.fn.expand('<cWORD>')
  M.refactorTag(WUnderCursor)
end

function M.open_taxonomy(opts)
  opts = table_util.merge.mergeLeft({ title = 'Projects' }, opts or {})

  explore.getNotesByTags(
    {
      tags = {
        TAGS.TAXONOMY,
      },

      select = {
        'absPath',
        'path',
        'title',
        'metadata',
        'tags',
        'rawContent',
      }
    },

    function(err, notes)
      if (err) then
        vim.notify('Encountered an error while finding taxonomies: ' .. vim.inspect(err), 'error')
        return
      end

      pickNote(notes, opts)
    end)
end

local function filterTags(tags, opts)
  opts = opts or {}
  if not opts.filter then return tags end

  local filteredTags = {}
  for _, tag in ipairs(tags) do
    for _, filter in ipairs(opts.filter) do
      if tag == filter then
        table.insert(filteredTags, tag)
      end
    end
  end

  return filteredTags
end

function M.insertTagToTree(tree, tag, notes)
  local segments = string_util.split(tag, '.')

  local treeNode = tree
  for _, segment in ipairs(segments) do
    if not treeNode[segment] then
      treeNode[segment] = { _type = 'node' }
    end

    treeNode = treeNode[segment]
  end

  for _, note in ipairs(notes) do
    if table_util.includes(note.tags or {}, tag) then
      table.insert(treeNode, note)
    end
  end

  return tree
end

function M.generateTagTree(opts, cb)
  opts = opts or {}

  zk.tag.list(util.getNotesDir(opts), {}, function(tagsErr, tags)
    if tagsErr then
      vim.notify('Error encountered while running zk: ' .. vim.inspect(tagsErr))
      return cb(tagsErr, nil)
    end

    local tagNames = table_util.map(tags, function(tag) return tag.name end)
    local filteredTags = filterTags(tagNames, opts)
    local tagTerm = table_util.join(filteredTags, ' OR ')

    zk.list(
      util.getNotesDir(opts),
      { tag = tagTerm, select = { 'tags', 'title', 'path' } },

      function(notesErr, notes)
        if notesErr then
          vim.notify('Error encountered while running zk: ' .. vim.inspect(notesErr))
          return cb(notesErr, nil)
        end

        local tree = {}

        for _, tag in ipairs(filteredTags) do
          tree = M.insertTagToTree(tree, tag, notes)
        end

        local rendered = M.renderTagTree({ tree = tree })

        return cb(nil, { tags = filteredTags, tree = tree, rendered = rendered })
      end
    )
  end)
end

function M.renderTagTree(opts)
  opts = opts or {}

  opts.tree = opts.tree or {}
  opts.indent = opts.indent or -2

  local lines = {}
  local indentLeader = string_util.repeat_str(opts.indent + 0, ' ')

  -- Do not render the empty root tag node
  if opts.tagParts then
    indentLeader = string_util.repeat_str(opts.indent + 2, ' ')
    lines = {
      string_util.repeat_str(opts.indent, ' ') .. '- ' .. table_util.join(opts.tagParts, '.')
    }
  end

  opts.tagParts = opts.tagParts or {}

  for _, note in ipairs(opts.tree) do
    -- TODO: Find a more generic solution. Right now, this is hardcoded to my notes directory structure
    local target = note.path:gsub('^zettelkasten/', '')
    local link = links.makeLink({
      text = note.title,
      target = target,
    })

    local line = indentLeader .. '- ' .. link
    table.insert(lines, line)
  end

  for tagPart, subtree in pairs(opts.tree) do
    if (subtree._type == 'node') then
      local subtreeTagParts = table_util.shallow_copy(opts.tagParts)
      table.insert(subtreeTagParts, tagPart)
      local subtreeLines = M.renderTagTree({
        tree = subtree,
        indent = opts.indent + 2,
        tagParts = subtreeTagParts
      })

      lines = table_util.concat(lines, subtreeLines)
    end
  end

  if lines[#lines] ~= '' then table.insert(lines, '') end

  return lines
end

function M.insertTagTree()
  local fm = frontmatter.get_buf_frontmatter()
  if not fm then return end

  local opts = {}
  if type(fm['tag-vis']) == 'table' then
    opts.filter = fm['tag-vis']
  end

  M.generateTagTree(opts, function(err, res)
    if err then return end
    vim.fn.setreg('+', res.rendered)
    vim.fn.setreg('"', res.rendered)
  end)
end

return M
