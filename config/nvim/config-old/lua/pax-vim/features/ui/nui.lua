local add_plugins = require('pax-vim.util.plugins.lazy').add_plugins

local M = {
  feature = {
    name = 'nui',

    description = [[
    Adds the nui UI widget library
    ]],
  }
}

function M.setup()
  add_plugins({
    { 'https://github.com/MunifTanjim/nui.nvim' }
  })
end

return M
