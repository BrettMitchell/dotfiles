{
  home.file.".config/hypr/hyprland.conf".source = ./hypr/hyprland.conf;
  home.file.".config/hypr/keybindings.conf".source = ./hypr/keybindings.conf;
  home.file.".config/waybar".source = ./waybar;
}
