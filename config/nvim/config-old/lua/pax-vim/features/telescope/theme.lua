-- Reference: https://github.com/nvim-telescope/telescope.nvim/blob/master/lua/telescope/pickers/layout_strategies.lua
--
--
-- TODO: Incorporate this for a more cusomized look
--       https://github.com/nvim-telescope/telescope.nvim/pull/2572/files
--       Probably will require the following utility
--       https://github.com/MunifTanjim/nui.nvim

local mergeLeft = require('user.lib.merge').mergeLeft

-- local borderCharsVertical = {
--   prompt = { "─", "│", "─", "│", "┌", "┐", "┘", "└" },
--   results = { "─", "│", "─", "│", "┌", "┐", "┤", "├" },
--   preview = { "─", "│", "─", "│", "┌", "┐", "┘", "└" },
-- }

local border_thickness = {
  prompt = { 1, 1, 1, 1 },
  results = { 1, 1, 1, 1 },
  preview = { 1, 1, 1, 1 },
}

-- local borderCharsHorizontal = {
--   prompt = { "─", "│", "─", "│", "┌", "┐", "┘", "└" },
--   results = { "─", "│", "─", "│", "┌", "┐", "┘", "└" },
--   preview = { "─", "│", "─", "│", "┌", "┐", "┘", "└" },
-- }

local borderCharsHorizontal = {
  prompt = { " ", " ", "─", "│", "│", " ", "─", "└" },
  results = { "─", " ", " ", "│", "┌", "─", " ", "│" },
  preview = { "─", "│", "─", "│", "┬", "┐", "┘", "┴" },
}

-- local borderCharsHorizontal = {
--   prompt = { "─", "│", "─", "│", "┌", "┐", "┘", "└" },
--   results = { "─", "│", "─", "│", "┌", "┐", "┘", "└" },
--   preview = { "─", "│", "─", "│", "┌", "┐", "┘", "└" },
-- }

local borderCharsCentered = {
  prompt = { "─", "│", " ", "│", "┌", "┐", "│", "│" },
  results = { "─", "│", "─", "│", "├", "┤", "┘", "└" },
  preview = { "─", "│", "─", "│", "┌", "┐", "┘", "└" },
}

local common = {
  winblend = 10
}

local centered = mergeLeft(
  common,
  {
    sorting_strategy = 'ascending',
    layout_strategy = 'center',
    results_title = false,
    preview_title = false,
    layout_config = {
      width = 0.9,
      height = 0.7,
    },
    show_line = false,
    borderchars = borderCharsCentered
  }
)

local horizontal = mergeLeft(
  common,
  {
    -- sorting_strategy = 'ascending',
    layout_strategy = 'horizontal',
    results_title = false,
    preview_title = false,
    layout_config = {
      width = 0.9,
      height = 0.9,
    },
    prompt_prefix = '  ',
    border = border_thickness,
    borderchars = borderCharsHorizontal,

    -- Modified from Telescope repo to add extra space
    get_status_text = function(self, opts)
      local ww = #(self:get_multi_selection())
      local xx = (self.stats.processed or 0) - (self.stats.filtered or 0)
      local yy = self.stats.processed or 0

      local status_icon = ""
      if opts and not opts.completed then
        status_icon = "*"
      end

      if xx == 0 and yy == 0 then
        return status_icon .. ' '
      end

      if ww == 0 then
        return string.format("%s %s / %s ", status_icon, xx, yy)
      else
        return string.format("%s %s / %s / %s ", status_icon, ww, xx, yy)
      end
    end,
  }
)

-- local vertical = {}

-- local cursor = mergeLeft(
--   common,
--   require('telescope.themes').get_cursor {
--     borderchars = {
--       prompt = { "─", "│", " ", "│", "┌", "┐", "│", "│" },
--       results = { "─", "│", "─", "│", "├", "┤", "┘", "└" },
--       preview = { "─", "│", "─", "│", "┌", "┐", "┘", "└" },
--     }
--   }
-- )

return {
  centered = centered,
  -- horizontal = horizontal,
  horizontal = {},
  -- cursor = cursor
}
