local priority = require('user.util.priority')
local lazy = require('user.util.plugins.lazy')

local state = {
  activate_on_load = false
}

local M = {
  name = 'Colorschema: Pywal',

  description = [[
  This feature adds pywal derived colors to nvim

  Note: The color choices made by this plugin are not
  always optimal, and an alternative solution will probably
  be added
  ]],
}

M.methods = {
  activate = function ()
    state.activate_on_load = true
  end
}

function M.setup()
  lazy.add_plugin({
    lazy = false,
    priority = priority.theme,
    'https://github.com/AlphaTechnolog/pywal.nvim',
    name = 'pywal',
    config = function()
      require('pywal').setup()
      if state.activate_on_load then
        vim.cmd [[ colorscheme pywal ]]
      end
    end
  })
end

return M

