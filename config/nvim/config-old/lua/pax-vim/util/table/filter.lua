return function (tbl, fn)
  local res = {}
  for idx, el in ipairs(tbl) do
    if fn(el, idx) then
      table.insert(res, el)
    end
  end
  return res
end
