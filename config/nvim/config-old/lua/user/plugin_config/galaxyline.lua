local M = {}

function M.config()
  local gl = require('galaxyline')

  -- GalaxyLine API modules
  local buffer = require("galaxyline.provider_buffer")
  local fileinfo = require("galaxyline.provider_fileinfo")
  local vcs = require("galaxyline.provider_vcs")
  local condition = require('galaxyline.condition')

  local function can_describe_file()
    local f_type = vim.bo.filetype
    if not f_type or f_type == '' or f_type == 'alpha' or f_type == 'packer' then
      return false
    end
    return true
  end

  local function ft_is(ft_to_check)
    return function()
      return vim.bo.filetype == ft_to_check
    end
  end

  local function AND(...)
    local fns = { ... }

    return function()
      local result = true
      for _, fn in ipairs(fns) do
        result = result and fn()
      end
      return result
    end
  end

  local function NOT(fn)
    return function() return not fn() end
  end

  local has_ft = AND(NOT(ft_is(nil)), NOT(ft_is('')))

  -- local lspsaga = require('lspsaga.symbolwinbar')
  -- local function getSymbols()
  --   return lspsaga:get_winbar()
  -- end

  -- Show project info on the left
  gl.section.left = {
    { FileName = {
      provider = fileinfo.get_current_file_name,
      -- provider = getSymbols,
      condition = AND(
        condition.buffer_not_empty,
        has_ft,
        can_describe_file,
        NOT(ft_is('alpha')),
        NOT(ft_is('packer'))
      ),
      -- highlight = { colors.fg, colors.line_bg, 'bold' }
    } },
  }

  -- Show file info on the right
  gl.section.right = {
    { Branch = {
      provider = vcs.get_git_branch,
      condition = condition.check_git_workspace,
      -- highlight = { '#8FBCBB', colors.line_bg, 'bold' },
    } },

    { FileType = {
      provider = buffer.get_buffer_filetype,
      separator = ' | ',
      -- separator = '',
      condition = AND(
        condition.buffer_not_empty,
        has_ft,
        can_describe_file,
        NOT(ft_is('alpha')),
        NOT(ft_is('packer'))
      ),
      -- condition = can_describe_file,
      -- separator_highlight = { colors.purple, colors.bg },
      -- highlight = { colors.fg, colors.purple }
    } },

    { ScrollPercentage = {
      provider = fileinfo.current_line_percent,
      separator = ' |',
      -- separator_highlight = { colors.line_bg, colors.line_bg },
      -- highlight = { colors.cyan, colors.darkblue, 'bold' },
    } },
  }
end

return M
