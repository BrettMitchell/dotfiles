--[[
--
-- This module exists to automate and formalize the practice
-- of organizing information in taxonomies.
--
-- Features:
-- 1. Automatically generate a taxonomy tree from tags attached
--    to notes
-- 2. Provide a taxonomy navigator using dynamic ZK queries and
--    telescope
-- 3. Provide a method for describing taxonomy members with tag
--    representative notes.
-- 4. Automatically display a list of links to taxonomy members
--    inside of taxonomy representative notes
--
--]]

local zk = require('zk.api')

local stringUtil = require('pax-vim.util.string')
local table_util = require('pax-vim.util.table')

local frontmatter = require('pax-vim.features.notes.frontmatter.v2')
local frontmatter_loaded = require('pax-vim.features.notes.frontmatter.autocmd')
local links = require('pax-vim.features.notes.links')
local util = require('pax-vim.features.notes.util')
local TAGS = require('pax-vim.features.notes.special_tags')

local fsOps = require('pax-vim.features.notes.fsOps')

local M = {}

local function get_missing_tag_names(previous_tag_name, current_tag_name)
  local missing_tag_names = {}

  local prev_segments = stringUtil.split(previous_tag_name, '.')
  local cur_segments = stringUtil.split(current_tag_name, '.')

  local found_different_segment = false
  local current_path = ''
  for index, segment in ipairs(cur_segments) do
    current_path = current_path .. (index == 1 and '' or '.') .. segment

    if prev_segments[index] ~= segment then
      found_different_segment = true
    end

    if found_different_segment and index ~= #cur_segments then
      table.insert(missing_tag_names, current_path)
    end
  end

  return missing_tag_names
end

-- Assumes tags are given in sorted order
local function add_missing_taxa(root_tag_name, tag_names)
  local dense_tag_list = {}

  -- This edge case crops up only when the 'root_tag_name' doesn't exist in any note
  if tag_names[1] ~= root_tag_name then
    dense_tag_list = { root_tag_name }
  end

  local previous_tag_name = root_tag_name
  for _, tag_name in ipairs(tag_names) do
    local missing_tag_names = get_missing_tag_names(previous_tag_name, tag_name)
    dense_tag_list = table_util.concat(dense_tag_list, missing_tag_names)
    table.insert(dense_tag_list, tag_name)
    previous_tag_name = tag_name
  end

  return dense_tag_list
end

local function indent(filter, tag_link_record)
  local trailing_segment = tag_link_record.tag_name:sub(#filter + 1)
  local spaces = ''

  for _ in trailing_segment:gmatch("([^.]+)") do
    spaces = spaces .. '  '
  end

  return spaces .. tag_link_record.list_item
end

local function ensure_links(opts, cb)
  opts = opts or {}

  local taxon_notes = {}
  for _, tag_name in ipairs(opts.tag_names) do
    table.insert(taxon_notes, {
      title = 'Taxon: ' .. tag_name,
      tag_name = tag_name,
    })
  end

  -- Note: A list of tag terms functions like a logical 'AND'
  local taxon_tag_match = table_util.join(opts.tag_names, ' OR ')
  local tag_match = { TAGS.TAXON, taxon_tag_match }

  zk.list(
    util.getNotesDir(opts),
    { tags = tag_match, select = { 'tags', 'title', 'path' } },
    function(err, existing_taxon_notes)
      if err then
        vim.notify('Error encountered while running zk: ' .. vim.inspect(err))
        return cb(err, nil)
      end

      local function get_existing_note(note)
        for _, found_note in ipairs(existing_taxon_notes) do
          local is_taxon = table_util.includes(TAGS.TAXON, found_note.tags)
          local represents_current_tag = table_util.includes(note.tag_name, found_note.tags)

          if represents_current_tag and is_taxon then
            return found_note
          end
        end

        return nil
      end

      for _, taxon_note in ipairs(taxon_notes) do
        local existing_note = get_existing_note(taxon_note)
        if existing_note then
          taxon_note.filename = existing_note.path
        else
          local filename = fsOps.generateNewFilename()
          taxon_note.filename = filename

          vim.fn.writefile({ '' }, filename)

          local fm = frontmatter.generate_frontmatter({
            title = taxon_note.title,
            description = "This note lists all notes which are members of the '" .. taxon_note.tag_name .. "' taxon",
            tags = {
              TAGS.TAXON,
              taxon_note.tag_name,
            },
          })

          frontmatter.write_file_frontmatter({
            filename = filename,
            frontmatter = fm
          })
        end
      end

      -- Create a link for all notes, now that a file exists for each one
      for _, taxon_note in ipairs(taxon_notes) do
        -- TEMPORARY
        taxon_note.link = '[' .. taxon_note.tag_name .. '](' .. taxon_note.filename:gsub('zettelkasten', '.') .. ')'
        -- TEMPORARY

        -- TODO: Generalize note filename semantics. The fact that my notes are found at
        -- `$NOTES_DIR/zettelkasten/` rather than just `$NOTES_DIR` seems to have some
        -- unintended consequences for relative paths and directory logic.
        -- taxon_note.link = '[' .. taxon_note.title .. '](' .. taxon_note.filename .. ')'
      end

      -- Return the taxon note records
      return cb(nil, taxon_notes)
    end
  )
end

function M.generate_taxonomy_tree(opts, cb)
  --[[
  -- Requirements:
  -- 1. Treat 'taxonomy_tag' as the root of the taxonomy
  -- 2. Get all tags prefixed with 'taxonomy_tag'
  -- 3. Generate a markdown list tree
  --]]

  opts = opts or {}

  zk.tag.list(util.getNotesDir(opts), {}, function(tagsErr, tags)
    if tagsErr then
      vim.notify('Error encountered while running zk: ' .. vim.inspect(tagsErr))
      return cb(tagsErr, nil)
    end

    local filtered_tags = {}
    for _, tag in ipairs(tags) do
      if tag.name:sub(1, #opts.filter) == opts.filter then
        table.insert(filtered_tags, tag.name)
      end
    end

    table.sort(filtered_tags)

    -- The list may omit some levels of the hierarchy:
    --  taxa.group1
    --  taxa.group2.group3.group4
    --
    -- We want:
    --  taxa.group1
    --  taxa.group2
    --  taxa.group2.group3
    --  taxa.group2.group3.group4
    local dense_tag_list = add_missing_taxa(opts.filter, filtered_tags)

    -- At this point, we have a tree structure encoded by:
    -- 1. the nested nature of dotted tag paths
    -- 2. the lexical order of the tags in the now sorted 'filtered_tags'

    ensure_links({ tag_names = dense_tag_list }, function(err, tag_links)
      local list_lines = table_util.map(tag_links, function(tag_link_record)
        local list_item = '- ' .. tag_link_record.link
        local indented_item = indent(
          opts.filter,
          {
            tag_name = tag_link_record.tag_name,
            list_item = list_item,
          }
        )

        return indented_item
      end)

      return cb(nil, list_lines)
    end)
  end)
end

function M.populate_taxonomy_tree(taxonomy_tag, filename, cb)
  cb = cb or function () end
  M.generate_taxonomy_tree({ filter = taxonomy_tag }, function(err, list_lines)
    if err then return cb(err, false) end

    list_lines = table_util.concat({ '' }, list_lines)
    list_lines = table_util.concat(list_lines, { '' })

    local current_filename = vim.fn.expand('%:p')
    if current_filename == filename then
      frontmatter.write_buf_body({
        bufnr = 0,
        body = list_lines
      })
      vim.cmd('write')
    else
      frontmatter.write_file_body({
        filename = filename,
        body = list_lines
      })
    end

    cb(nil, true)
  end)
end

M.taxonomy_tree_augroup = {
  name = 'AutogenerateTaxonomyTree',
  autocmds = { {
    events = 'User',
    opts = {
      pattern = frontmatter_loaded.events.FrontmatterLoaded,
      callback = function(event)
        local data = event.data or {}

        if vim.bo.readonly then
          return
        end

        if not data.frontmatter
            or not data.frontmatter.tags
            or not table_util.includes(TAGS.TAXONOMY, data.frontmatter.tags)
            or #data.frontmatter.tags ~= 2
        then
          return
        end

        local taxonomy = data.frontmatter.tags[1]
        if taxonomy == TAGS.TAXONOMY then
          taxonomy = data.frontmatter.tags[2]
        end

        M.populate_taxonomy_tree(
          taxonomy,
          data.filename
        )
      end
    }
  } }
}

function M.generate_taxonomy_member_list(opts, cb)
  --[[
  -- Requirements:
  -- 1. Get all tags tagged with 'tag', except the current one
  -- 2. Generate a markdown list with links to each one
  --]]

  opts = opts or {}

  local tags = {
    'NOT ' .. TAGS.TAXON,
    'NOT ' .. TAGS.TAXONOMY,
  }

  for _, tag in ipairs(opts.tags or {}) do
    table.insert(tags, tag .. ' OR ' .. tag .. '.*')
  end

  zk.list(
    util.getNotesDir(opts),
    { tags = tags, select = { 'title', 'path' } },
    function(err, notes)
      if err then
        vim.notify('Error encountered while getting notes: ' .. vim.inspect(err))
        return
      end

      local list = table_util.map(notes, function(note)
        -- TEMPORARY
        return '- [' .. note.title .. '](' .. note.path:gsub('zettelkasten', '.') .. ')'
        -- TEMPORARY

        -- TODO: Generalize note filename semantics. The fact that my notes are found at
        -- `$NOTES_DIR/zettelkasten/` rather than just `$NOTES_DIR` seems to have some
        -- unintended consequences for relative paths and directory logic.
        -- return '- [' .. note.title .. '](' .. note.path .. ')'
      end)

      return cb(nil, list)
    end
  )
end

function M.populate_taxonomy_member_list(tags, filename, cb)
  cb = cb or function () end
  M.generate_taxonomy_member_list({ tags = tags }, function(err, list_lines)
    if err then return cb(err, false) end

    list_lines = table_util.concat({ '' }, list_lines)
    list_lines = table_util.concat(list_lines, { '' })

    local current_filename = vim.fn.expand('%:p')
    if current_filename == filename then
      frontmatter.write_buf_body({
        bufnr = 0,
        body = list_lines
      })
      vim.cmd('write')
    else
      frontmatter.write_file_body({
        filename = filename,
        body = list_lines
      })
    end

    return cb(nil, true)
  end)
end

M.taxonomy_member_augroup = {
  name = 'AutogenerateTaxonomyMemberList',
  autocmds = { {
    events = 'User',
    opts = {
      pattern = frontmatter_loaded.events.FrontmatterLoaded,
      callback = function(event)
        local data = event.data or {}

        if vim.bo.readonly then
          return
        end

        if not data.frontmatter
            or not data.frontmatter.tags
            or not table_util.includes(TAGS.TAXON, data.frontmatter.tags)
            or #data.frontmatter.tags < 2
        then
          return
        end

        local taxa = {}
        for _, tag in ipairs(data.frontmatter.tags) do
          if tag ~= TAGS.TAXON then
            table.insert(taxa, tag)
          end
        end

        M.populate_taxonomy_member_list(
          taxa,
          data.filename
        )
      end
    }
  } }
}

function M.explore()
end

-- LEGACY API

function M.insertTagToTree(tree, tag, notes)
  local segments = stringUtil.split(tag, '.')

  local treeNode = tree
  for _, segment in ipairs(segments) do
    if not treeNode[segment] then
      treeNode[segment] = { _type = 'node' }
    end

    treeNode = treeNode[segment]
  end

  for _, note in ipairs(notes) do
    if table_util.includes(note.tags or {}, tag) then
      table.insert(treeNode, note)
    end
  end

  return tree
end

function M.generateTagTree(opts, cb)
  opts = opts or {}

  zk.tag.list(util.getNotesDir(opts), {}, function(tagsErr, tags)
    if tagsErr then
      vim.notify('Error encountered while running zk: ' .. vim.inspect(tagsErr))
      return cb(tagsErr, nil)
    end

    local tagNames = table_util.map(tags, function(tag) return tag.name end)
    local filteredTags = filterTags(tagNames, opts)
    local tagTerm = table_util.join(filteredTags, ' OR ')

    zk.list(
      util.getNotesDir(opts),
      { tag = tagTerm, select = { 'tags', 'title', 'path' } },

      function(notesErr, notes)
        if notesErr then
          vim.notify('Error encountered while running zk: ' .. vim.inspect(notesErr))
          return cb(notesErr, nil)
        end

        local tree = {}

        for _, tag in ipairs(filteredTags) do
          tree = M.insertTagToTree(tree, tag, notes)
        end

        local rendered = M.renderTagTree({ tree = tree })

        return cb(nil, { tags = filteredTags, tree = tree, rendered = rendered })
      end
    )
  end)
end

function M.renderTagTree(opts)
  opts = opts or {}

  opts.tree = opts.tree or {}
  opts.indent = opts.indent or -2

  local lines = {}
  local indentLeader = stringUtil.repeat_str(opts.indent + 0, ' ')

  -- Do not render the empty root tag node
  if opts.tagParts then
    indentLeader = stringUtil.repeat_str(opts.indent + 2, ' ')
    lines = {
      stringUtil.repeat_str(opts.indent, ' ') .. '- ' .. table_util.join(opts.tagParts, '.')
    }
  end

  opts.tagParts = opts.tagParts or {}

  for _, note in ipairs(opts.tree) do
    -- TODO: Find a more generic solution. Right now, this is hardcoded to my notes directory structure
    local target = note.path:gsub('^zettelkasten/', '')
    local link = links.makeLink({
      text = note.title,
      target = target,
    })

    local line = indentLeader .. '- ' .. link
    table.insert(lines, line)
  end

  for tagPart, subtree in pairs(opts.tree) do
    if (subtree._type == 'node') then
      local subtreeTagParts = table_util.shallowCopy(opts.tagParts)
      table.insert(subtreeTagParts, tagPart)
      local subtreeLines = M.renderTagTree({
        tree = subtree,
        indent = opts.indent + 2,
        tagParts = subtreeTagParts
      })

      lines = table_util.concat(lines, subtreeLines)
    end
  end

  if lines[#lines] ~= '' then table.insert(lines, '') end

  return lines
end

function M.insertTagTree()
  local fm = frontmatter.parseFrontmatter()
  if not fm then return end

  local opts = {}
  if type(fm['tag-vis']) == 'table' then
    opts.filter = fm['tag-vis']
  end

  M.generateTagTree(opts, function(err, res)
    if err then return end
    vim.fn.setreg('+', res.rendered)
    vim.fn.setreg('"', res.rendered)
  end)
end

return M
