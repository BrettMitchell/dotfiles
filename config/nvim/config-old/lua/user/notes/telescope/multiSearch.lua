local makeTextPreviewer = require('user.telescope.previewer').makeTextPreviewer
local deepMergeLeft     = require('user.lib.merge').deepMergeLeft
local selection         = require('user.notes.telescope.util.selection')
local zkPicker          = require('user.notes.telescope.util.zkPicker')
local zkPreviewer       = require('user.notes.telescope.util.zkPreviewer')
local links             = require('user.notes.links')

local M                 = {}

-- Multi search
--
-- Principles
--   Build up a zk query gradually and interactively re-fetch results
--   Always limit resuls to a reasonable number to keep fetching responsive
--   An empty query should always populate results
--   Fetch without a limit when populating quickfix list with matches

-- This is likely more notes than can be shown in Telescope
local LIMIT             = 100

--[[

Note on searching:
  By full text:
    Full text search terms may be combined as a comma-separated list

  By tags:
    Tags may be combined by concatenating the list of tags

  By title:
    Title searches require a title prefix and go into the match clause - e.g. title:my term

--]]
function M.buildZkQuery(search)
  search = search or {}

  local matchTerms = {}

  if search.title and search.title.value and string.len(search.title.value) > 0 then
    table.insert(matchTerms, 'title:' .. search.title.value)
  end

  if search.fullText and search.fullText.value and string.len(search.fullText.value) > 0 then
    table.insert(matchTerms, 'body:' .. search.fullText.value)
  end

  local query = {
    limit = LIMIT,
    matchStrategy = 'fts',
  }

  if #matchTerms > 0 then
    query.match = matchTerms
  end

  if #search.tags.value > 0 then
    query.tags = search.tags.value
  end

  return query
end

function M.copySearchResultsAsList(searchConfig)
  require('user.notes.explore.plumbing').getNotes(
    M.buildZkQuery(searchConfig),
    function(err, notes)
      if err then
        vim.notify('Error encountered while running zk: ' .. vim.inspect(err))
        return
      end

      local parentDir = vim.fn.expand('%:p:h')

      local list = ''
      for _, note in ipairs(notes) do
        local relativeFilename = links.getRelativeFilename(note.absPath, parentDir)
        list = list .. '\n' .. links.makeLink({ text = note.title, target = relativeFilename })
      end

      vim.fn.setreg('+', list)
      vim.fn.setreg('"', list)
    end
  )
end

function M.multiSearch(opts, cb)
  opts = opts or {}
  opts.mode = opts.mode or 'title'

  if opts.search then
    opts.search = {
      title = opts.search.title or {
        prompt = '',
        value = '',
      },
      fullText = opts.search.fullText or {
        prompt = '',
        value = '',
      },
      tags = opts.search.tags or {
        prompt = '',
        value = {},
      },
    }
  else
    opts.search = {
      title = {
        prompt = '',
        value = '',
      },
      fullText = {
        prompt = '',
        value = '',
      },
      tags = {
        prompt = '',
        value = {}
      },
    }
  end

  local picker = nil
  local otherModeBindings = nil

  local function setQuery(newSearchValue)
    opts.search[opts.mode].prompt = newSearchValue
    if opts.mode ~= 'tags' then
      opts.search[opts.mode].value = newSearchValue
    end
  end

  local function getQuery()
    local query = M.buildZkQuery(opts.search)
    query.select = {
      'absPath',
      'title',
      'tags',
      'snippets'
    }
    return query
  end

  local function gotoMode(gotoModeOpts)
    gotoModeOpts = gotoModeOpts or {}
    if not gotoModeOpts.mode then
      vim.notify('No mode specified', 'error')
      return
    end

    local childOpts = {}
    for k, v in pairs(opts) do
      childOpts[k] = v
    end
    childOpts.mode = gotoModeOpts.mode
    childOpts.previousMode = opts.mode

    M.multiSearch(childOpts, cb)
  end

  local function gotoFuzzyMode()
    vim.notify('Going to fuzzy mode!')
  end

  if opts.mode == 'title' then
    picker = M.progressiveMatchPicker(getQuery, setQuery, 'Find notes by title')
    otherModeBindings = {
      i = {
        ['<M-t>'] = function() gotoMode({ mode = 'tags' }) end,
        ['<M-s>'] = function() gotoMode({ mode = 'fullText' }) end,
        ['<M-f>'] = function() gotoFuzzyMode() end,
      }
    }
  end

  if opts.mode == 'tags' then
    picker = M.progressiveTagPicker('Find notes by tag')

    local function getTagsFromMultiselect(selectedTagEntries)
      local tags = {}
      for _, tagEntry in ipairs(selectedTagEntries) do
        table.insert(tags, tagEntry.value.name)
      end
      return tags
    end

    local function commitTagSelection(gotoModeOpts)
      local selectedTagEntries = selection.getMultiSelection(gotoModeOpts.prompt_bufnr)
      local tags = getTagsFromMultiselect(selectedTagEntries)
      opts.search.tags = { prompt = '', value = tags }
    end

    otherModeBindings = {
      select_default = function(selectedTagEntries)
        if not opts.previousMode then return end
        local tags = getTagsFromMultiselect(selectedTagEntries)
        opts.search.tags = { prompt = '', value = tags }
        gotoMode({ mode = opts.previousMode })
      end,
      i = {
        ['<M-n>'] = function(prompt_bufnr)
          commitTagSelection({ prompt_bufnr = prompt_bufnr })
          gotoMode({ mode = 'title' })
        end,

        ['<M-s>'] = function(prompt_bufnr)
          commitTagSelection({ prompt_bufnr = prompt_bufnr })
          gotoMode({ mode = 'fullText' })
        end,

        ['<M-f>'] = function(prompt_bufnr)
          commitTagSelection({ prompt_bufnr = prompt_bufnr })
          gotoFuzzyMode()
        end
      }
    }
  end

  if opts.mode == 'fullText' then
    picker = M.progressiveMatchPicker(getQuery, setQuery, 'Find notes by full text search')
    otherModeBindings = {
      i = {
        ['<M-n>'] = function() gotoMode({ mode = 'title' }) end,
        ['<M-t>'] = function() gotoMode({ mode = 'tags' }) end,
        ['<M-f>'] = function() gotoFuzzyMode() end,
      }
    }
  end

  if picker == nil then
    vim.notify('Invalid mode given: ' .. opts.mode, 'error')
    return
  end

  local function yankSearch()
    local serializedSearch = vim.inspect(opts.search, { newline = '' })

    vim.fn.setreg('+', serializedSearch)
    vim.fn.setreg('"', serializedSearch)
  end

  local commonBindings = {
    i = {
      ['<M-y>'] = selection.yankAttrOfSelected('absPath'),
      ['<M-Y>'] = yankSearch,
    }
  }
  local allBindings = deepMergeLeft(commonBindings, otherModeBindings)

  picker(
    {
      default_text = opts.search[opts.mode].prompt,
      mappings = deepMergeLeft(opts.mappings or {}, allBindings)
    },
    cb
  )
end

function M.progressiveTagPicker(pickerTitle)
  return function(opts, cb)
    local telescopeOpts = {}

    telescopeOpts.previewer = zkPreviewer.makeZkPreviewer({
      getQuery = function(self, entry)
        return {
          tags = { entry.value.name },
          select = { 'title', 'tags' },
        }
      end,
      getText = function(self, matchingNotes)
        local text = {}

        for _, note in ipairs(matchingNotes or {}) do
          table.insert(text, note.title)
          for _, tagName in ipairs(note.tags or {}) do
            table.insert(text, '  - ' .. tagName)
          end

          table.insert(text, '')
        end

        return text
      end
    })

    telescopeOpts.multiselect = opts.multiselect or false
    telescopeOpts.mappings = opts.mappings or {}
    telescopeOpts.default_text = opts.default_text
    telescopeOpts.prompt_title = pickerTitle

    zkPicker.zkPickTags(telescopeOpts)
  end
end

function M.progressiveMatchPicker(getQuery, onNewQuery, pickerTitle)
  return function(opts, cb)
    local telescopeOpts = {}
    cb = cb or show

    telescopeOpts.entry_maker = function(note)
      local title = note.title or note.path
      return {
        value   = note,
        path    = note.absPath,
        display = title,
        ordinal = title,
      }
    end

    local function getZkMatchHighlights(text)
      local highlights = {}

      -- local hasHighlight = true
      -- local prevousHighlightEnd = 1
      -- while hasHighlight do
      --   local hl_start = string.find(text, '<zk:match>', prevousHighlightEnd, true)
      --   local _, hl_end = string.find(text, '</zk:match>', prevousHighlightEnd, true)
      --   if hl_start then
      --     table.insert(highlights, { start = hl_start, finish = hl_end })
      --   else
      --     hasHighlight = false
      --     prevousHighlightEnd = hl_end + 1
      --   end
      -- end

      return highlights
    end

    telescopeOpts.highlighter = getZkMatchHighlights

    telescopeOpts.previewer = makeTextPreviewer({
      getText = function(_, entry)
        -- return vim.inspect(entry.value)
        local snippets = entry.value.snippets or {}

        -- local highlights = getZkMatchHighlights(text)

        return snippets
        -- return snippets, highlights
      end,
    })

    -- telescopeOpts.previewer = makeFilePreviewer(
    --   function(entry) return entry.value.absPath end,
    --   function(entry) return entry.value.title or entry.value.path end
    -- )

    telescopeOpts.getQuery = function(prompt)
      onNewQuery(prompt)
      return getQuery()
    end

    telescopeOpts.multiselect = opts.multiselect or false
    telescopeOpts.mappings = deepMergeLeft(
      { select_default = function(res) cb(nil, res) end },
      opts.mappings or {}
    )

    telescopeOpts.default_text = opts.default_text
    telescopeOpts.prompt_title = pickerTitle

    zkPicker.zkPick(telescopeOpts)
  end
end

return M
