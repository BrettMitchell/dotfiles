local M = {}

M.weekday = {
  frontmatter = function(_, _, cb)
    return cb(nil, {
      tags = { 'journal' }
    })
  end,

  build = function(existingBody, _, cb)
    return cb(nil, [[
# Reflect
## Retrospect
## Introspect
## Prospect

# Todo
## Morning Routine
- [ ] Brush teeth
- [ ] Shower
- [ ] Make tea
- [ ] Wake Madeline up at 6:30am

## Personal Items
- [ ] 

## Work Items
- [ ] 

]])
  end
}

M.weekend = {
  frontmatter = function(_, _, cb)
    return cb(nil, {
      tags = { 'journal' }
    })
  end,

  build = function(existingBody, _, cb)
    return cb(nil, [[
# Reflect
## Retrospect
## Introspect
## Prospect

# Todo
## Morning Routine
- [ ] Brush teeth
- [ ] Shower
- [ ] Make tea
- [ ] Wake Madeline up at 6:30am

## Personal Items
- [ ] 

]])
  end
}

M.auto = {
  frontmatter = function(existingFrontmatter, ctx, cb)
    return cb(nil, {
      title = ctx.date,
      tags = { 'journal' },
    })
  end,

  build = function(existingBody, ctx, cb)
    -- Weekends
    -- Sunday = 1, Saturday = 7
    if ctx.date.wday == 1 or ctx.date.wday == 7 then
      M.weekend.build(existingBody, ctx, cb)
    end

    -- Weekdays
    return M.weekday.build(existingBody, ctx, cb)
  end
}

return M
