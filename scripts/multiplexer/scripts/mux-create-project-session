#!/usr/bin/env bash

function get_prefix {
  tr : $'\n' <<<"$1" \
    | sed -e 'N;s/^\(.*\).*\n\1.*$/\1/'
}

function main {
  local dir="$1"
  local session_name="$dir"

  local prefix=`get_prefix "$NOTES_DIR:$PROJECTS_DIR"`
  if [[ "${session_name:0:${#prefix}}" == "$prefix" ]] ; then
    session_name=`printf '%s\n' "${session_name:${#prefix}}" | sed 's/^\///'`
  fi
  session_name=`printf "$session_name" | sed -E 's/\.|:/_/g; s:/$::g'`

  # Create session if it does not exist yet
  local init="$dir"/.tmux_init.sh
  local bare_repo_init="$dir"/../.tmux_init.sh

  if ! tmux has-session -t ="$session_name" &> /dev/null ; then
    tmux new-session -d -s "$session_name" -c "$dir"

    # Perform all globally registered init scripts
    if [[ ! -z "$TMUX_GLOBAL_SESSION_INIT" ]] ; then
      IFS=: \
        read -r -d '' \
          -a global_init_scripts \
          < <(printf '%s:\0' "$TMUX_GLOBAL_SESSION_INIT")

      for init_script in "${global_init_scripts[@]}" ; do
        "$init_script" "$session_name" "$dir"
      done
    fi

    if [[ -f "$init" ]] ; then
      "$init" "$session_name" "$dir"
    fi
    if [[ -d "$dir/../.bare" ]] && [[ -f "$bare_repo_init" ]] ; then
      "$bare_repo_init" "$session_name" "$dir"
    fi
  fi

  # Not in tmux session
  if [[ -z "$TMUX" ]] ; then
    tmux attach -t "=$session_name"
  # In tmux session
  else
    tmux switch-client -t "=$session_name"
  fi
}

main "$@"

