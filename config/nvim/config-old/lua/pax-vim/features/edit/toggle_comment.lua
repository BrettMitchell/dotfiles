local make_feature = require('pax-vim.util.feature').make_feature_v2
local keymap = require('pax-vim.util.keymap')

-- tmux does something weird to C-/
local function getCtrlSlash()
  -- iTerm also does something weird to C-/
  if vim.loop.os_uname().sysname == 'Darwin' then
    return '<C-c>'
  end

  local tmuxEnvVar = vim.fn.getenv('TMUX')
  if tmuxEnvVar ~= vim.NIL and tmuxEnvVar:len() > 0 then
    return '<C-_>'
  end
  return '<C-/>'
end

local toggle_comment = make_feature({
  name = 'Edit Command: Toggle Comment',

  description = [[
  This feature adds the ability to toggle comments
  ]],

  plugins = { { 'https://github.com/numToStr/Comment.nvim' } },

  init = function(self)
    self.config = {
      ---LHS of toggle mappings in NORMAL mode
      ---@type table
      toggler = {
        ---Line-comment toggle keymap
        line = '<space>ecc',
        ---Block-comment toggle keymap
        block = '<space>ebc',
      },

      ---LHS of operator-pending mappings in NORMAL mode
      ---LHS of mapping in VISUAL mode
      ---@type table
      opleader = {
        ---Line-comment keymap
        line = '<space>ec',
        ---Block-comment keymap
        block = '<space>eb',
      },

      ---LHS of extra mappings
      ---@type table
      extra = {
        ---Add comment on the line above
        above = '<space>ecO',
        ---Add comment on the line below
        below = '<space>eco',
        ---Add comment at the end of line
        eol = '<space>ecA',
      },

      ---Create basic (operator-pending) and extended mappings for NORMAL + VISUAL mode
      ---NOTE: If `mappings = false` then the plugin won't create any mappings
      mappings = {
        ---Operator-pending mapping
        ---Includes `gcc`, `gbc`, `gc[count]{motion}` and `gb[count]{motion}`
        ---NOTE: These mappings can be changed individually by `opleader` and `toggler` config
        basic = true,
        ---Extra mapping
        ---Includes `gco`, `gcO`, `gcA`
        extra = true,
      },
    }

    self.keymap = keymap {
      {
        { 'n', 'o' },
        getCtrlSlash(),
        '<plug>(comment_toggle_linewise_current)',
        { desc = 'Comment single line from normal mode', noremap = false }
      },

      {
        'i',
        getCtrlSlash(),
        '<C-o><plug>(comment_toggle_linewise_current)',
        { desc = 'Comment single line from normal mode', noremap = false }
      },

      {
        'x',
        getCtrlSlash(),
        '<plug>(comment_toggle_linewise_visual)gv',
        { desc = 'Comment visual block and return to visual mode', noremap = false }
      }
    }
  end,

  apply = function (self)
    require('Comment').setup(self.config)
    self.keymap.apply()
  end
})

return toggle_comment
