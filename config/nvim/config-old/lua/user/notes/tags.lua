local zk = require('zk.api')

local pickOptions = require('user.telescope.pickOption').pickOption
local stringUtil = require('user.lib.string')
local tableUtil = require('user.lib.table')

local frontmatter = require('user.notes.frontmatter')
local links = require('user.notes.links')
local util = require('user.notes.util')

local M = {}

local function addTagsToNote(tags)
  local justSelectedTagNames = {}
  for i, tagRecord in pairs(tags) do
    justSelectedTagNames[i] = tagRecord[1]
  end

  frontmatter.setAttributes({ tags = justSelectedTagNames }, { merge = true })
end

function M.addTags()
  zk.tag.list(util.getNotesDir(), {}, function(err, tags)
    if (err) then
      vim.notify(err)
      return
    end

    local tagNames = {}
    for i, tagRecord in pairs(tags) do
      tagNames[i] = tagRecord.name
    end

    pickOptions(tagNames, {
      title = 'Add tags to current note',
      mappings = {
        select_default = addTagsToNote,
      }
    })
  end)
end

-- Important! This does NOT handle the general case for tags.
-- It can only handle frontmatter tags; tags using hash syntax in the note body will be missed.
function M.refactorTag(tagName)
  local newTagName = vim.fn.input({
    prompt = 'New tag name: ',
    default = tagName,
    cancelreturn = 0,
  })

  -- On no input, vim.fn.input will return the cancelreturn value of 0.
  -- If any input was supplied, it will be of type 'string'
  if type(newTagName) == 'number' then
    return
  end

  if string.len(newTagName) == 0 then
    vim.notify('Cannot rename tag to empty string')
    return
  end

  if newTagName == tagName then
    vim.notify('No change to tag name')
    return
  end

  zk.tag.list(util.getNotesDir(), {}, function(tagErr, tags)
    if (tagErr) then
      vim.notify('An error occurred while getting notes with tag ' .. tagName .. ': ' .. vim.inspect(tagErr))
      return
    end

    for _, otherTag in ipairs(tags) do
      if otherTag.name == newTagName then
        vim.notify("Tag '" ..
        newTagName .. "' already exists (" .. otherTag.note_count .. " notes).\nContinuing will merge the two.")

        local mergeResponse = vim.fn.input({
          prompt = 'Continue? (y/n): ',
          cancelreturn = 'n',
        })

        if mergeResponse ~= 'y' then
          vim.notify('Canceled')
          return
        end
      end
    end

    zk.list(util.getNotesDir(), { tags = { tagName }, select = { 'tags', 'title', 'absPath' } },
      function(notesErr, notes)
        if (notesErr) then
          vim.notify('An error occurred while getting notes with tag ' .. tagName .. ': ' .. vim.inspect(notesErr))
          return
        end

        for _, note in ipairs(notes) do
          local newTags = {}
          for _, tag in ipairs(note.tags) do
            if (tag ~= tagName) then
              table.insert(newTags, tag)
            else
              table.insert(newTags, newTagName)
            end
          end

          frontmatter.setAttributes({ tags = newTags }, { merge = false, filename = note.absPath })
        end

        vim.cmd('ZkIndex')
      end)
  end)
end

function M.refactorTagUnderCursor()
  local WUnderCursor = vim.fn.expand('<cWORD>')
  M.refactorTag(WUnderCursor)
end

local function filterTags(tags, opts)
  opts = opts or {}
  if not opts.filter then return tags end

  local filteredTags = {}
  for _, tag in ipairs(tags) do
    for _, filter in ipairs(opts.filter) do
      if tag == filter then
        table.insert(filteredTags, tag)
      end
    end
  end

  return filteredTags
end

function M.insertTagToTree(tree, tag, notes)
  local segments = stringUtil.split(tag, '.')

  local treeNode = tree
  for _, segment in ipairs(segments) do
    if not treeNode[segment] then
      treeNode[segment] = { _type = 'node' }
    end

    treeNode = treeNode[segment]
  end

  for _, note in ipairs(notes) do
    if tableUtil.includes(note.tags or {}, tag) then
      table.insert(treeNode, note)
    end
  end

  return tree
end

function M.generateTagTree(opts, cb)
  opts = opts or {}

  zk.tag.list(util.getNotesDir(opts), {}, function(tagsErr, tags)
    if tagsErr then
      vim.notify('Error encountered while running zk: ' .. vim.inspect(tagsErr))
      return cb(tagsErr, nil)
    end

    local tagNames = tableUtil.map(tags, function(tag) return tag.name end)
    local filteredTags = filterTags(tagNames, opts)
    local tagTerm = tableUtil.join(filteredTags, ' OR ')

    zk.list(
      util.getNotesDir(opts),
      { tag = tagTerm, select = { 'tags', 'title', 'path' } },

      function(notesErr, notes)
        if notesErr then
          vim.notify('Error encountered while running zk: ' .. vim.inspect(notesErr))
          return cb(notesErr, nil)
        end

        local tree = {}

        for _, tag in ipairs(filteredTags) do
          tree = M.insertTagToTree(tree, tag, notes)
        end

        local rendered = M.renderTagTree({ tree = tree })

        return cb(nil, { tags = filteredTags, tree = tree, rendered = rendered })
      end
    )
  end)
end

function M.renderTagTree(opts)
  opts = opts or {}

  opts.tree = opts.tree or {}
  opts.indent = opts.indent or -2

  local lines = {}
  local indentLeader = stringUtil.repeatStr(opts.indent + 0, ' ')

  -- Do not render the empty root tag node
  if opts.tagParts then
    indentLeader = stringUtil.repeatStr(opts.indent + 2, ' ')
    lines = {
      stringUtil.repeatStr(opts.indent, ' ') .. '- ' .. tableUtil.join(opts.tagParts, '.')
    }
  end

  opts.tagParts = opts.tagParts or {}

  for _, note in ipairs(opts.tree) do
    -- TODO: Find a more generic solution. Right now, this is hardcoded to my notes directory structure
    local target = note.path:gsub('^zettelkasten/', '')
    local link = links.makeLink({
      text = note.title,
      target = target,
    })

    local line = indentLeader .. '- ' .. link
    table.insert(lines, line)
  end

  for tagPart, subtree in pairs(opts.tree) do
    if (subtree._type == 'node') then
      local subtreeTagParts = tableUtil.shallowCopy(opts.tagParts)
      table.insert(subtreeTagParts, tagPart)
      local subtreeLines = M.renderTagTree({
        tree = subtree,
        indent = opts.indent + 2,
        tagParts = subtreeTagParts
      })

      lines = tableUtil.concat(lines, subtreeLines)
    end
  end

  if lines[#lines] ~= '' then table.insert(lines, '') end

  return lines
end

function M.insertTagTree()
  local fm = frontmatter.parseFrontmatter()
  if not fm then return end

  local opts = {}
  if type(fm['tag-vis']) == 'table' then
    opts.filter = fm['tag-vis']
  end

  M.generateTagTree(opts, function(err, res)
    if err then return end
    vim.fn.setreg('+', res.rendered)
    vim.fn.setreg('"', res.rendered)
  end)
end

return M
