return function(tbl, sep)
  local res = ''
  for index, el in ipairs(tbl) do
    if index == 1 then
      res = el
    else
      res = res .. sep .. el
    end
  end
  return res
end
