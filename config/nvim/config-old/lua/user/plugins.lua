local ensure_packer = function()
  local fn = vim.fn
  local install_path = fn.stdpath('data') .. '/site/pack/packer/start/packer.nvim'
  if fn.empty(fn.glob(install_path)) > 0 then
    fn.system({ 'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path })
    vim.cmd [[packadd packer.nvim]]
    return true
  end
  return false
end

local packer_bootstrap = ensure_packer()

local ok, packer = pcall(require, 'packer')
if not ok then
  return
end

--[[

This list can be populated again with -> ':r!u-get-nvim-plugin-commits'

]]
--

packer.startup(function(use, use_rocks)
  use_rocks 'lua-path'
  use_rocks 'lyaml'
  use_rocks 'lunajson'

  if packer_bootstrap then
    require('packer').sync()
  end
end)
