local stringUtil = require('pax-vim.util.string')
local table_util = require('pax-vim.util.table')


local async_zk      = require('pax-vim.features.notes.auto_gen.zk_query.util').async_zk
local async_zk_tags = require('pax-vim.features.notes.auto_gen.zk_query.util').async_zk_tags
local frontmatter   = require('pax-vim.features.notes.frontmatter.v2')
local TAGS          = require('pax-vim.features.notes.special_tags')
local fsOps         = require('pax-vim.features.notes.fsOps')


local M = {}

local function get_missing_tag_names(previous_tag_name, current_tag_name)
  local missing_tag_names = {}

  local prev_segments = stringUtil.split(previous_tag_name, '.')
  local cur_segments = stringUtil.split(current_tag_name, '.')

  local found_different_segment = false
  local current_path = ''
  for index, segment in ipairs(cur_segments) do
    current_path = current_path .. (index == 1 and '' or '.') .. segment

    if prev_segments[index] ~= segment then
      found_different_segment = true
    end

    if found_different_segment and index ~= #cur_segments then
      table.insert(missing_tag_names, current_path)
    end
  end

  return missing_tag_names
end

-- Assumes tags are given in sorted order
local function add_missing_taxa(root_tag_name, tag_names)
  local dense_tag_list = {}

  -- This edge case crops up only when the 'root_tag_name' doesn't exist in any note
  if tag_names[1] ~= root_tag_name then
    dense_tag_list = { root_tag_name }
  end

  local previous_tag_name = root_tag_name
  for _, tag_name in ipairs(tag_names) do
    local missing_tag_names = get_missing_tag_names(previous_tag_name, tag_name)
    dense_tag_list = table_util.concat(dense_tag_list, missing_tag_names)
    table.insert(dense_tag_list, tag_name)
    previous_tag_name = tag_name
  end

  return dense_tag_list
end

local function indent(filter, tag_link_record)
  local trailing_segment = tag_link_record.tag_name:sub(#filter + 1)
  local spaces = ''

  for _ in trailing_segment:gmatch("([^.]+)") do
    spaces = spaces .. '  '
  end

  return spaces .. tag_link_record.list_item
end

local function ensure_links(opts)
  opts = opts or {}

  local taxon_notes = {}
  for _, tag_name in ipairs(opts.tag_names) do
    table.insert(taxon_notes, {
      title = 'Taxon: ' .. tag_name,
      tag_name = tag_name,
    })
  end

  -- Note: A list of tag terms functions like a logical 'AND'
  local taxon_tag_match = table_util.join(opts.tag_names, ' OR ')
  local tag_match = { TAGS.TAXON, taxon_tag_match }

  local existing_taxon_notes = async_zk(
    opts,
    { tags = tag_match, select = { 'tags', 'title', 'path' } }
  )
  if existing_taxon_notes == nil then return end

  local function get_existing_note(note)
    for _, found_note in ipairs(existing_taxon_notes) do
      local is_taxon = table_util.includes(TAGS.TAXON, found_note.tags)
      local represents_current_tag = table_util.includes(note.tag_name, found_note.tags)

      if represents_current_tag and is_taxon then
        return found_note
      end
    end

    return nil
  end

  for _, taxon_note in ipairs(taxon_notes) do
    local existing_note = get_existing_note(taxon_note)
    if existing_note then
      taxon_note.filename = existing_note.path
    else
      local filename = fsOps.generateNewFilename()
      taxon_note.filename = filename

      vim.fn.writefile({ '' }, filename)

      local fm = frontmatter.generate_frontmatter({
        title = taxon_note.title,
        description = "This note lists all notes which are members of the '" .. taxon_note.tag_name .. "' taxon",
        tags = {
          TAGS.TAXON,
          taxon_note.tag_name,
        },
      })

      frontmatter.write_file_frontmatter({
        filename = filename,
        frontmatter = fm
      })
    end
  end

  -- Create a link for all notes, now that a file exists for each one
  for _, taxon_note in ipairs(taxon_notes) do
    -- TEMPORARY
    taxon_note.link = '[' .. taxon_note.tag_name .. '](' .. taxon_note.filename:gsub('zettelkasten', '.') .. ')'
    -- TEMPORARY

    -- TODO: Generalize note filename semantics. The fact that my notes are found at
    -- `$NOTES_DIR/zettelkasten/` rather than just `$NOTES_DIR` seems to have some
    -- unintended consequences for relative paths and directory logic.
    -- taxon_note.link = '[' .. taxon_note.title .. '](' .. taxon_note.filename .. ')'
  end

  -- Return the taxon note records
  return taxon_notes
end

function M.generate_taxonomy_tree(opts)
  --[[
  -- Requirements:
  -- 1. Treat 'taxonomy_tag' as the root of the taxonomy
  -- 2. Get all tags prefixed with 'taxonomy_tag'
  -- 3. Generate a markdown list tree
  --]]

  opts = opts or {}

  local tags = async_zk_tags(opts, {})
  if not tags then return {} end

  local filtered_tags = {}
  for _, tag in ipairs(tags) do
    if tag.name:sub(1, #opts.filter) == opts.filter then
      table.insert(filtered_tags, tag.name)
    end
  end

  table.sort(filtered_tags)

  -- The list may omit some levels of the hierarchy:
  --  taxa.group1
  --  taxa.group2.group3.group4
  --
  -- We want:
  --  taxa.group1
  --  taxa.group2
  --  taxa.group2.group3
  --  taxa.group2.group3.group4
  local dense_tag_list = add_missing_taxa(opts.filter, filtered_tags)

  -- At this point, we have a tree structure encoded by:
  -- 1. the nested nature of dotted tag paths
  -- 2. the lexical order of the tags in the now sorted 'filtered_tags'

  local tag_links = ensure_links({ tag_names = dense_tag_list })
  local list_lines = table_util.map(tag_links, function(tag_link_record)
    local list_item = '- ' .. tag_link_record.link
    local indented_item = indent(
      opts.filter,
      {
        tag_name = tag_link_record.tag_name,
        list_item = list_item,
      }
    )

    return indented_item
  end)

  return list_lines
end

return M
