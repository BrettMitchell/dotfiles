------------
-- Server --
------------

require('pax-vim.features.startup.server').setup()

-----------------
-- Set options --
-----------------

require('user.setopts')

----------------------
-- Global functions --
----------------------

require('user.global_functions')

--------------
-- LuaRocks --
--------------

require('pax-vim.features.luarocks').setup({
  luarocks_install_path = '/opt/homebrew/share/lua/5.4',
  -- luarocks_install_cpath = function()
  --   local cmd_out = vim.fn.system({ 'rtx', 'where', 'lua', '5.1' })
  --   local lua_dir = vim.split(cmd_out, '\n')[1]
  --   return lua_dir .. '/luarocks/lib/lua/5.1'
  -- end,
  quiet = true,
  ensure_installed = {
    'lua-path',
    'lyaml',
    'lunajson',
    -- 'luautf8',
  }
})

---------------
-- Utilities --
---------------

require('pax-vim.features.util.plenary').setup()

-------------
-- Startup --
-------------

require('pax-vim.features.startup.events').setup()
require('pax-vim.features.startup.set_cwd').setup()

---------------
-- Dashboard --
---------------

require('pax-vim.features.dashboard.alpha').setup({
  header = require('user.static').ascii_art.nvim_banner_text1,
  buttons = {
    { "e", "  > New file",  ":ene<CR>" },
    { "f", "  > Find file", ":Telescope find_files<CR>" },
    { "r", "  > Recent",    ":Telescope oldfiles<CR>" },
  }
})

require('pax-vim.features.startup.dashboard').setup({ dashboardCmd = 'Alpha' })

----------------
-- Treesitter --
----------------

require('pax-vim.features.treesitter').setup({
  treesitter_config = {
    -- parser_install_dir = vim.fn.getenv('ASDF_DIR') .. '/share/nvim/treesitter_packages',

    auto_install = true,

    highlight = {
      enable = true,
    },

    autotag = {
      enable = true
    }
  }
})

--------
-- UI --
--------

require('pax-vim.features.theme.tokyonight').setup()
require('pax-vim.features.theme.tokyonight').methods.activate()

require('pax-vim.features.ui.treesitter_folds').setup({
  start_open = true,
  -- Don't set fold method for buffers managed by True Zen's narrow mode
  ignore_file = function()
    return vim.b.tz_narrowed_buffer == 'true'
  end
})

require('pax-vim.features.ui.toggle_wrap').setup()
require('pax-vim.features.ui.folding').setup()

require('pax-vim.features.ui.statusline').setup()

require('pax-vim.features.ui.notifications').setup({
  override_global_notify = true,
  plugin_config = {
    render = 'minimal',
    stages = 'fade',
    top_down = false,
  },
})

require('pax-vim.features.ui.highlightedyank').setup({
  duration = 120
})

require('pax-vim.features.ui.whichkey').setup({
  timeout = 300,
  silent_toggle = false,
  whichkey_config = {
    plugins = {
      marks = true,          -- shows a list of your marks on ' and `
      registers = true,      -- shows your registers on " in NORMAL or <C-r> in INSERT mode
      presets = {
        operators = false,   -- adds help for operators like d, y, ...
        motions = true,      -- adds help for motions
        text_objects = true, -- help for text objects triggered after entering an operator
        g = true,            -- bindings for prefixed with g
      },
    },
    icons = {
      breadcrumb = "»", -- symbol used in the command line area that shows your active key combo
      separator = "➜", -- symbol used between a key and it's label
      group = "+",      -- symbol prepended to a group
    },
    popup_mappings = {
      scroll_down = "<C-j>", -- binding to scroll down inside the popup
      scroll_up = "<C-k>",   -- binding to scroll up inside the popup
    },
    window = {
      border = "none",          -- none, single, double, shadow
      position = "bottom",      -- bottom, top
      margin = { 1, 0, 1, 0 },  -- extra window margin [top, right, bottom, left]
      padding = { 1, 2, 1, 2 }, -- extra window padding [top, right, bottom, left]
      winblend = 0,             -- value between 0-100 0 for fully opaque and 100 for fully transparent
    },
    layout = {
      height = { min = 4, max = 25 }, -- min and max height of the columns
      width = { min = 20, max = 50 }, -- min and max width of the columns
      spacing = 2,                    -- spacing between columns
      align = "left",                 -- align columns left, center or right
    },
  }
})

---------
-- LSP --
---------

require('pax-vim.features.lsp').setup({
  plugin_config = {
    lspconfig = {},
    mason = {
      ui = {
        border = 'single'
      }
    },
    mason_lspconfig = {
      automaticInstallation = false,
      ensure_installed = { 'lua_ls', 'tsserver' },
    }
  },
  hooks = {
    all_servers = {
      on_attach = {
        function(client, bufnr)
          if client.name == 'tsserver' then
            client.server_capabilities.documentFormattingProvider = false
          end
        end
      }
    },
    servers = {}
  }
})

require('pax-vim.features.lsp.lspsaga').setup({
  scroll_preview = {
    scroll_down = '<C-j>',
    scroll_up = '<C-k>',
  },
  finder = {
    open = { 'o', '<CR>' },
    vsplit = 's',
    split = 'i',
    tabe = 't',
    quit = { 'q', '<ESC>' },
  },
  definition_action_keys = {
    edit = '<CR>',
    vsplit = '<C-CR>/',
    split = '<C-CR>\\',
    tabe = '',
    quit = { 'q', '<ESC>' },
  },
  code_action = {
    num_shortcut = true,
    keys = {
      quit = '<ESC>',
      exec = '<CR>',
    }
  },
  lightbulb = { enable = false, },
  outline = {
    -- win_position = 'right',
    -- win_with = '',
    -- win_width = 30,
    -- show_detail = true,
    -- auto_preview = true,
    -- auto_refresh = true,
    -- auto_close = true,
    -- custom_sort = nil,
    keys = {
      jump = '<cr>',
      expand_collapse = '<cr>',
      quit = 'q',
    },
  },
  symbol_in_winbar = {
    enable = true,
    separator = ' ',
    hide_keyword = true,
    show_file = true,
    folder_level = 2,
    respect_root = false,
    color_mode = true,
  },
  rename = {
    quit = '<esc>',
    exec = '<cr>',
    whole_project = false,
  },
})

require('pax-vim.features.lsp.code_actions').setup()
require('pax-vim.features.lsp.formatting').setup()
require('pax-vim.features.lsp.rename_symbol').setup()
require('pax-vim.features.lsp.hover_docs').setup()
require('pax-vim.features.lsp.outline').setup()
-- require('pax-vim.features.lsp.call_hierarchy').setup()
require('pax-vim.features.lsp.general_nav').setup()
require('pax-vim.features.lsp.diagnostics_nav').setup()
require('pax-vim.features.lsp.diagnostics_virtual_text').setup()
require('pax-vim.features.lsp.diagnostics_floating_window').setup()
-- require('pax-vim.features.lsp.signature_help').setup()
require('pax-vim.features.lsp.telescope_handlers').setup()
require('pax-vim.features.lsp.progress_notifications').setup()

-------------
-- Folding --
-------------

------------
-- Macros --
------------

-- require('pax-vim.features.macros.macro_cmdheight').setup()
require('pax-vim.features.macros.keymaps').setup()
-- require('pax-vim.features.macros.motion_repeat').setup()

-------------------------
-- Integrated terminal --
-------------------------

require('pax-vim.features.terminal').setup({
  fterm_config = {
    border = 'double',
    hl = 'Normal',
    dimensions = {
      width = 0.8,
      height = 0.8,
    },
  }
})

------------------
-- File browser --
------------------

require('pax-vim.features.file-browser.lf').setup({
  lf_config_file = os.getenv('HOME') .. '/.config/lf/pax-vim.lfrc',
})

---------------
-- Telescope --
---------------

require('pax-vim.features.telescope').setup()

--------------------------
-- User Input Utilities --
--------------------------

require('pax-vim.features.input.pick_option').setup({ driver = 'telescope' })
-- require('pax-vim.features.input.text').setup({ driver = 'native' })

---------
-- DAP --
---------

require('pax-vim.features.dap').setup({
  adapter_install_dir = os.getenv('HOME') .. '/.build/neovim-artifacts/dap-install',

  signs = {
    breakpoint = {
      icon = '-',
      texthl = '',
      linehl = '',
      numhl = '',
    },

    stopped = {
      icon = '>',
      texthl = '',
      linehl = '',
      numhl = '',
    }
  }
})

require('pax-vim.features.dap.ui').setup()
require('pax-vim.features.dap.telescope_integration').setup()
require('pax-vim.features.dap.virtual_text_variable_values').setup()

require('pax-vim.features.dap.adapters.node2').setup()

---------
-- Git --
---------

require('pax-vim.features.git.git').setup()
-- require('pax-vim.features.git.telescope_integration').setup()

----------------
-- Navigation --
----------------

require('pax-vim.features.nav.base').setup()

require('pax-vim.features.nav.harpoon').setup()

require('pax-vim.features.nav.word_motions').setup()

require('pax-vim.features.nav.clever_f').setup({
  clever_f_not_overwrites_standard_mappings = 1, -- Set custom mappings
  -- clever_f_across_no_line = 1 -- Search current line only
  clever_f_smart_case = 0,
  clever_f_fix_key_direction = 1, -- Predictable find direction (no inverting direction when last match was backwards)
})

require('pax-vim.features.nav.hop').setup({
  case_insensitive = false,
  teasing = false,
})

-- local labels = {
--   's', 'd', 'f', 'j', 'k', 'l',
--   'g', 'h', 'v', 'n', 'r', 'u',
--   'e', 'i', 'w', 'o', 'a', ';',
--   'c', 'm', 'x', ',', 'z', '.',
-- }
require('pax-vim.features.nav.leap').setup({
  -- labels = labels,
  -- safe_labels = labels,

  equivalence_classes = {
    ' \t\r\n',
    '`"\'',
  },
})

--------------
-- Quickfix --
--------------

require('pax-vim.features.quickfix.trouble').setup()

require('pax-vim.features.quickfix.bqf').setup()

require('pax-vim.features.quickfix.qf').setup({
  c = {
    auto_close = false,     -- Automatically close location/quickfix list if empty
    auto_follow = false,    -- Follow current entry, possible values: prev,next,nearest, or false to disable
    auto_follow_limit = 8,  -- Do not follow if entry is further away than x lines
    follow_slow = false,    -- Only follow on CursorHold
    auto_open = false,      -- Automatically open list on QuickFixCmdPost
    auto_resize = true,     -- Auto resize and shrink location list if less than `max_height`
    max_height = 8,         -- Maximum height of location/quickfix list
    min_height = 5,         -- Minimum height of location/quickfix list
    wide = true,            -- Open list at the very bottom of the screen, stretching the whole width.
    number = false,         -- Show line numbers in list
    relativenumber = false, -- Show relative line numbers in list
    unfocus_close = false,  -- Close list when window loses focus
    focus_open = false,     -- Auto open list on window focus if it contains items
  },
  l = {
    auto_close = false,     -- Automatically close location/quickfix list if empty
    auto_follow = false,    -- Follow current entry, possible values: prev,next,nearest, or false to disable
    auto_follow_limit = 8,  -- Do not follow if entry is further away than x lines
    follow_slow = false,    -- Only follow on CursorHold
    auto_open = false,      -- Automatically open list on QuickFixCmdPost
    auto_resize = true,     -- Auto resize and shrink location list if less than `max_height`
    max_height = 8,         -- Maximum height of location/quickfix list
    min_height = 5,         -- Minimum height of location/quickfix list
    wide = true,            -- Open list at the very bottom of the screen, stretching the whole width.
    number = false,         -- Show line numbers in list
    relativenumber = false, -- Show relative line numbers in list
    unfocus_close = false,  -- Close list when window loses focus
    focus_open = false,     -- Auto open list on window focus if it contains items
  },
})

require('pax-vim.features.quickfix.quickfix_keymaps').setup()

---------------------------
-- Filetype Integrations --
---------------------------

require('pax-vim.features.ft.just').setup()
require('pax-vim.features.ft.hjson').setup()

----------
-- Undo --
----------

require('pax-vim.features.undo.undotree').setup()

----------
-- Edit --
----------

require('pax-vim.features.edit.auto_indent').setup()
require('pax-vim.features.edit.break_object').setup()
require('pax-vim.features.edit.move_selection').setup()
require('pax-vim.features.edit.toggle_comment').setup()

require('pax-vim.features.edit.indent').setup()
require('pax-vim.features.edit.word_case_toggle').setup()
require('pax-vim.features.edit.system_clipboard').setup()
require('pax-vim.features.edit.join_lines').setup()

---------------------
-- Editor Controls --
---------------------

require('pax-vim.features.editor_commands.write').setup()
require('pax-vim.features.editor_commands.quit_normal').setup()

------------------
-- Yank History --
------------------

require('pax-vim.features.yank_history').setup()

------------------
-- Text Objects --
------------------

require('pax-vim.features.text_objects.surround').setup({
  highlight = {
    duration = false
  }
})

-----------------
-- Completions --
-----------------

local luasnip = require('pax-vim.features.completion.snippets.luasnip')
local cmp_buffer_text = require('pax-vim.features.completion.buffer_text')
local cmp_lsp = require('pax-vim.features.completion.lsp')
local cmp_lsp_signature_help = require('pax-vim.features.completion.lsp.signature_help')
local cmp_path = require('pax-vim.features.completion.path')
local cmp_cmdline = require('pax-vim.features.completion.command_line')
local cmp_dap = require('pax-vim.features.completion.dap')

luasnip.setup({
  show_node_indicator = { enable = true }
})
cmp_buffer_text.setup()
cmp_lsp.setup()
cmp_path.setup()
cmp_cmdline.setup()
cmp_lsp_signature_help.setup()
cmp_dap.setup()

require('pax-vim.features.completion.cmp').setup({
  snippet_engine = luasnip,

  sources = {
    default = {
      {
        cmp_lsp,
        cmp_lsp_signature_help,
        cmp_path,
        luasnip.source_definitions.cmp_luasnip,
        luasnip.source_definitions.cmp_luasnip_choice,
      },

      { cmp_buffer_text }
    },

    cmdline = { {
      cmp_cmdline,
    } },

    search = { {
      cmp_lsp,
      cmp_buffer_text,
    } },
  }
})

-----------
-- Notes --
-----------

require('pax-vim.features.notes.zk').setup()
require('pax-vim.features.notes.mkdnflow').setup()
require('pax-vim.features.notes').setup({
  dashboard_note_name = 'Dashboard',
  picker_ignore_tags = {
    'meta.type.taxon',
    'meta.type.taxonomy',
  }
})
require('pax-vim.features.notes.frontmatter.autocmd').setup()

--------------------
-- Plugin manager --
--------------------

require('pax-vim.features.plugin_manager').setup()
local lazy = require('pax-vim.util.plugins.lazy')
lazy.bootstrap()
lazy.setup()

------------------
-- Key mappings --
------------------

-- require('user.keymap.dap').setupStatic()
-- require('user.keymap.dap').setupOnAttach()

-- require('user.keymap.editing').setup()
-- require('user.keymap.editor-actions').setupWrite()
-- require('user.keymap.editor-actions').setupQuit()

-- require('user.keymap.folding').setup()
-- require('user.keymap.formatting').setup()
-- require('user.keymap.fterm').setup()

-- require('user.keymap.git').setup()
-- require('user.keymap.harpoon').setup()

-- require('user.keymap.lsp').setup()
-- require('user.keymap.movement').setup()

-- require('user.keymap.notes').setup()
-- require('user.keymap.quickfix').setup()
-- require('user.keymap.reload-config').setup()
require('user.keymap.search').setup()
-- require('user.keymap.telescope').setup()
-- require('user.keymap.trouble').setup()
require('user.keymap.ui-settings').setup()
-- require('user.keymap.undotree').setup()
require('user.keymap.windows').setup()


