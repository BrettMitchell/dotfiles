local make_feature = require('pax-vim.util.feature').make_feature

local config = {
  luarocks_install_path = nil,
  ensure_installed = {}
}

local M = make_feature({
  name = 'LuaRocks Support',

  description = [[
  This feature enables LuaRocks support anywhere in your neovim config.

  This is necessary as a standalone feature if you wish to use LuaRocks
  because lazy.nvim does not support it.

  This requires that you install luarocks manually (I recommend 'rtx' for
  this.)

  Requires that you set the 'luarocks_install_path' configuration property.
  This value can be found by running `luarocks config` and looking for
  the 'deploy_lua_dir' value in the command's output.

  Note that is is a VERY basic feature definition, and all installations are done
  serially and synchronously.
  ]],

  set_config = function(userConfig)
    if not userConfig.luarocks_install_path then
      vim.notify("Config property 'luarocks_install_path' is required.")
    end

    config.luarocks_install_path = userConfig.luarocks_install_path
    if type(config.luarocks_install_path) == 'function' then
      config.luarocks_install_path = config.luarocks_install_path()
    end

    config.luarocks_install_cpath = userConfig.luarocks_install_cpath
    if type(config.luarocks_install_cpath) == 'function' then
      config.luarocks_install_cpath = config.luarocks_install_cpath()
    end

    config.ensure_installed = userConfig.ensure_installed or config.ensure_installed
    config.quiet = userConfig.quiet or config.quiet
  end,

  on_activate = function()
    if config.luarocks_install_path then
      package.path = package.path .. ';' .. config.luarocks_install_path .. '/?/?.lua'
      package.path = package.path .. ';' .. config.luarocks_install_path .. '/?.lua'
      package.path = package.path .. ';' .. config.luarocks_install_path .. '/?/init.lua'
    end

    if config.luarocks_install_cpath then
      package.cpath = package.cpath .. ';' .. config.luarocks_install_cpath .. '/?/?.so'
      package.cpath = package.cpath .. ';' .. config.luarocks_install_cpath .. '/?.so'
    end

    -- Next: Fix cpath so that yaml library can load

    -- local status, loader = pcall(require, 'luarocks.loader')
    -- if not status then
    --   vim.notify("Cannot find LuaRocks: require('luarocks.loader') failed")
    --   return
    -- end

    local function chunk_output(output)
      return vim.split(output, '\n')
    end

    local function get_installed_luarocks()
      local cmdOut = vim.fn.system({ 'luarocks', 'list', '--porcelain' })
      local outLines = chunk_output(cmdOut)

      local installed = {}
      for _, line in ipairs(outLines) do
        for pkgName in string.gmatch(line, '([^\t]+)\t([^\t]+)\t([^\t]+)\t([^\t]+)') do
          table.insert(installed, pkgName)
        end
      end

      return installed
    end

    local installed = get_installed_luarocks()

    local function is_installed(rock)
      for _, installedRock in ipairs(installed or {}) do
        if installedRock == rock then
          return true
        end
      end

      return false
    end

    for _, rock in ipairs(config.ensure_installed) do
      if is_installed(rock) then
        if not config.quiet then
          vim.notify('Rock already installed: ' .. rock)
        end
      else
        if not config.quiet then
          vim.notify('Installing rock: ' .. rock)
        end

        vim.fn.system({ 'luarocks', 'install', rock })
      end
    end
  end
})

return M
