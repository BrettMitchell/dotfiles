local make_feature = require('user.util.feature').make_feature_v2

local zk = make_feature({
  name = 'Notes :: zk integration',

  description = [[
  This feature adds and configures the mkdnflow plugin, which comes
  with many convenience features related to markdown editing and
  navigation.
  ]],

  plugins = { {
    'https://github.com/mickael-menu/zk-nvim',
  } },

  init = function(self)
    self.config = {
      picker = 'telescope',

      lsp = {
        -- `config` is passed to `vim.lsp.start_client(config)`
        config = {
          cmd = { "zk", "lsp" },
          name = "zk",
          -- on_attach = ...
          -- etc, see `:h vim.lsp.start_client()`
        },

        -- automatically attach buffers in a zk notebook that match the given filetypes
        auto_attach = {
          enabled = true,
          filetypes = { "markdown" },
        },
      },
    }
  end,

  apply = function (self)
    require('zk').setup(self.config)
  end
})

return zk
