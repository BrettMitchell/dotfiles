local feat_util = require('user.util.feature')
local add_plugins = require('user.util.plugins.lazy').add_plugins

local M = {
  name = 'Trouble',

  description = [[
  This feature adds a quickfix style tray containing LSP diagnostics
  ]],

  plugins = {
    {
      'https://github.com/folke/trouble.nvim',
      requires = 'kyazdani42/nvim-web-devicons',
      config = function()
        require('trouble').setup(config.trouble_config)
      end
    }
  },

  trouble_config = {},

  keymaps = {
    { 'n', '<C-p>',   ':TroubleToggle document_diagnostics<enter>',  { desc = 'Trouble (document)' }, },
    { 'n', '<C-S-p>', ':TroubleToggle workspace_diagnostics<enter>', { desc = 'Trouble (workspace)' }, }
  }
}

function M.setup(config)
  feat_util.apply_config(config or {}, M)
  add_plugins({
    {
      'https://github.com/folke/trouble.nvim',
      requires = 'kyazdani42/nvim-web-devicons',
      config = function()
        require('trouble').setup(M.trouble_config)
      end
    }
  })
end

return M
