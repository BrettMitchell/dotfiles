local starts_with = require('user.util.string.starts_with')

return function(str, prefix, replacement)
  if not starts_with(str, prefix) then
    return str
  end

  return replacement .. str:sub(#prefix + 1)
end
