#!/usr/bin/env bash

if ! command -v go > /dev/null ; then
  printf '%s\n' 'go is not installed' > /dev/stderr
  exit
fi

pushd "$module/tmux-session-daemon/" > /dev/null
zig build
popd > /dev/null

