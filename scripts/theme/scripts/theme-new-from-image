#!/usr/bin/env bash

function main {
  if [[ ! -d "$THEMES_DIR" ]] ; then
    printf '%s\n' 'Environment variable THEMES_DIR does not specify a directory' > /dev/stderr
    exit 1
  fi

  if [[ ! -d "$WALLPAPERS_DIR" ]] ; then
    printf '%s\n' 'Environment variable WALLPAPERS_DIR is not set to a directory' > /dev/stderr
    exit 1
  fi

  local themeName="$1"
  if [[ -z "$themeName" ]] ; then
    printf '%s\n' "Theme name is required" > /dev/stderr
    exit 1
  fi

  if [[ -e "$THEMES_DIR/$themeName" ]] ; then
    printf '%s\n' "Theme $THEMES_DIR/$themeName already exists" > /dev/stderr
    exit 1
  fi

  local imageFile="$2"

  if [[ -z "$imageFile" ]] ; then
    imageFile=`ls "$WALLPAPERS_DIR" | pick`
  fi

  if [[ ! -f "$imageFile" ]]; then
    imageFile=`pwd`"/$1"
  fi

  if [ ! -f "$imageFile" ]; then
    printf '%s\n' "Image '$imageFile' is not a file" > /dev/stderr
    exit 1
  fi

  imageFile=`realpath "$imageFile"`
  local imageFileRelativeName=`replace "$WALLPAPERS_DIR" '' "$imageFile"`

  shift; shift
  extraArgs="$@"

  # Generate and get wal colors
  wal -n -i "$imageFile" $extraArgs &> /dev/null
  . "$HOME/.cache/wal/colors.sh"
  rm -rf "$HOME/.cache/wal"

  # Create theme directory scaffolding
  mkdir -p "$THEMES_DIR/$themeName"

  # Set initial theme values from generated wal colors
  local valueStore=$(cat << EOF
{
  "wallpaper": {
    "list": ["$imageFileRelativeName"],
    "current": "$imageFileRelativeName",
    "randomizeOnLoad": true,
    "randomizeOnSchedule": false
  },

  "color": {
    "background1": "${background,,}",
    "background2": "${background,,}",
    "foreground1": "${foreground,,}",
    "foreground2": "${foreground,,}",

    "text": "${color1,,}",
    "text_accent": "${color1,,}",
    "background_selected": "${color2,,}",
    "text_selected": "${color9,,}",
    "cursor": "${cursor,,}",
    "cursor_text": "${background,,}",

    "accent1": "${color3,,}",
    "accent2": "${color4,,}",
    "accent3": "${color5,,}",
    "accent4": "${color6,,}",
    "accent5": "${color7,,}",
    "accent6": "${color8,,}",

    "error": "#ffc0b9",
    "warning": "#fce094",
    "success": "#b4f6c0",
    "info": "#a6dbff"
  },

  "font": {
    "notification": "Victor Mono",
    "topbar": "Victor Mono",
    "topbar_icons": "Material Icons",
    "browser": "Noto Sans",

    "terminal": "VictorMono Nerd Font Mono",
    "terminal_bold": "VictorMono Nerd Font Mono",
    "terminal_italic": "VictorMono Nerd Font Mono",
    "terminal_bold_italic": "VictorMono Nerd Font Mono",

    "zathura": "VictorMono Nerd Font Mono",
    "vscode": "VictorMono Nerd Font Mono"
  },

  "window": {
    "corner_radius": 0,
    "gap": 10,
    "border": 1
  },

  "notification": {
    "corner_radius": 0,
    "border": 1,
    "gap": 10,
    "max_notifications": 0
  },

  "topbar": {
    "corner_radius": 0,
    "gap": 25,
    "height": 30,
    "desktop_indicator": {
      "mode": "icon",
      "active": "O",
      "inactive_occupied": "o",
      "inactive_empty": "."
    }
  },

  "terminal": {
    "prompt": "",
    "tmux": {
      "status_bar_mode": "minimal",
      "window_indicator": {
        "mode": "icon",
        "active": "━",
        "inactive": "━"
      }
    }
  }
}
EOF
)

  printf "$valueStore" > "$THEMES_DIR/$themeName/value_store.json"

  theme-set-current "$themeName"
}

main "$@"

