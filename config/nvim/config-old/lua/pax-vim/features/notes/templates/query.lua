local concat = require('pax-vim.util.table.concat')
local TAGS = require('pax-vim.features.notes.special_tags')

local M = {}

M.query = {
  frontmatter = function(prev, _, cb)
    return cb(nil, {
      title = prev.title or 'QUERY',
      tags = concat(prev.tags or {}, { TAGS.QUERY }),
      queries = { { tags = { 'wont-match', 'NOT wont-match' }, match = '' } }
    })
  end,
  build = function(existingBody, _, cb) return cb(nil, existingBody) end
}

return M
