local make_feature = require('pax-vim.util.feature').make_feature_v2

local cmp_path = make_feature({
  name = 'Completion :: Command Line',

  description = [[
  This feature adds a cmp completion source for command line options.
  ]],

  source_definition = { name = 'cmdline' },

  plugins = {
    { 'https://github.com/hrsh7th/cmp-cmdline' }
  },
})

return cmp_path
