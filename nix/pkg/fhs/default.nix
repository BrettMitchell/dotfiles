{ pkgs, nixpkgs, ... }:
let
  base = pkgs.appimageTools.defaultFhsEnvArgs;
in
  pkgs.buildFHSEnv (base // {
    name = "fhs";
    targetPkgs = pkgs: (
      # pkgs.buildFHSUserEnv provides only a minimal FHS environment,
      # lacking many basic packages needed by most software.
      # Therefore, we need to add them manually.
      #
      # pkgs.appimageTools provides basic packages required by most software.
      (base.targetPkgs nixpkgs.stable) ++ [
        nixpkgs.stable.zsh
        nixpkgs.stable.git
        nixpkgs.stable.curl
      ]
    );
    # setuidPrograms = [ nixpkgs.stable.lsof ];
    profile = "export FHS=1";
    runScript = "zsh -c";
    extraOutputsToInstall = ["dev"];
  })
