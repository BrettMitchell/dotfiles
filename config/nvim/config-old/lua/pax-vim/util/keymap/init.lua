local includes = require('pax-vim.util.table.includes')
local unpack = require('pax-vim.util.table.unpack')

--[[
--
--  Helpers
--
--]]

local function register_modespec(keyspec, modes)
  local modespec = keyspec[1]
  if type(modespec) == 'string' then
    modespec = { modespec }
  end

  for _, mode in ipairs(modespec) do
    if not includes(modespec, modes) then
      table.insert(modes, mode)
    end
  end
end

local function get_existing_keymaps(modes, bufnr)
  local existing_keymaps = {}

  for _, mode in ipairs(modes) do
    existing_keymaps[mode] = {}

    if bufnr == nil then
      existing_keymaps[mode] = vim.api.nvim_get_keymap(mode)
    end

    if type(bufnr) == 'number' then
      existing_keymaps[mode] = vim.api.nvim_buf_get_keymap(bufnr, mode)
    end
  end

  return existing_keymaps
end

local function find_original_keyspec(existing_keymaps, mode, lhs)
  if not existing_keymaps[mode] then return nil end

  for _, existing_keymap in ipairs(existing_keymaps[mode]) do
    if existing_keymap.lhs == lhs then
      return existing_keymap
    end
  end

  return nil
end

local function unmap_keyspec(keyspec)
  local modespec = keyspec[1]
  if type(modespec) ~= 'table' then
    modespec = { modespec }
  end

  local lhs = keyspec[2]
  for _, mode in ipairs(modespec) do
    vim.keymap.del(mode, lhs)
  end
end

local function unmap_buf_keyspec(bufnr, keyspec)
  local modespec = keyspec[1]
  if type(modespec) ~= 'table' then
    modespec = { modespec }
  end

  local lhs = keyspec[2]
  for _, mode in ipairs(modespec) do
    vim.keymap.del(mode, lhs, { buffer = bufnr })
  end
end

--[[
--
--  Public Interface
--
--]]

local function keymap(spec)
  -- Extract mode names from spec

  local MODES = {}

  for _, keyspec in pairs(spec) do
    register_modespec(keyspec, MODES)
  end

  -- 'MODES' now contains all unique mode names declared in
  -- the spec

  local function defines_mapping_for(lhs)
    for _, keyspec in pairs(spec) do
      if keyspec[2] == lhs then
        return true
      end
    end

    return false
  end

  local function apply(bufnr)
    if bufnr == nil then
      for _, keyspec in pairs(spec) do
        vim.keymap.set(unpack(keyspec))
      end

      return
    end

    if type(bufnr) == 'number' then
      for _, keyspec in pairs(spec) do
        local modes = keyspec[1]
        local lhs = keyspec[2]
        local rhs = keyspec[3]
        local opts = keyspec[4] or {}
        opts.buffer = bufnr
        vim.keymap.set(modes, lhs, rhs, opts)
      end

      return
    end

    vim.notify('keymap.apply :: Invalid bufnr value provided, must be number - ' .. vim.inspect(bufnr), 'error')
  end

  local function remove(bufnr)
    if bufnr == nil then
      for _, keyspec in pairs(spec) do
        unmap_keyspec(keyspec)
      end

      return
    end

    if type(bufnr) == 'number' then
      for _, keyspec in pairs(spec) do
        unmap_buf_keyspec(bufnr, keyspec)
      end

      return
    end

    vim.notify('keymap.remove :: Invalid bufnr value provided, must be number - ' .. vim.inspect(bufnr), 'error')
  end

  local global_restore_cache = nil
  local per_buffer_restore_cache = {}

  local function backup_keyspec(keyspec, existing_keymaps, cache_container, bufnr)
    local modespec = keyspec[1]
    if type(modespec) ~= 'table' then
      modespec = { modespec }
    end

    local lhs = keyspec[2]

    for _, mode in ipairs(modespec) do
      local original_keyspec = find_original_keyspec(existing_keymaps, mode, lhs)
      if not original_keyspec then return end

      table.insert(cache_container, original_keyspec)
      if type(bufnr) == 'number' then
        vim.keymap.del(mode, lhs, { buffer = bufnr })
      else
        vim.keymap.del(mode, lhs)
      end
    end
  end

  local function is_active(bufnr)
    if bufnr == nil then
      return global_restore_cache ~= nil
    end

    return per_buffer_restore_cache[bufnr] ~= nil
  end

  local function overlay(bufnr)
    -- Global maps
    if bufnr == nil then
      if global_restore_cache ~= nil then
        vim.notify('keymap.overlay :: Cannot overlay in global keymap space - overlay already applied', 'error')
        return
      end

      local existing_keymaps = get_existing_keymaps(MODES)
      global_restore_cache = {}

      for _, keyspec in pairs(spec) do
        backup_keyspec(keyspec, existing_keymaps, global_restore_cache)
      end

      apply()
      return
    end

    -- Buffer local maps
    if type(bufnr) == 'number' then
      if per_buffer_restore_cache[bufnr] ~= nil then
        vim.notify('keymap.overlay :: Cannot overlay in current buffer - overlay already applied', 'error')
        return
      end

      local existing_keymaps = get_existing_keymaps(MODES, bufnr)
      per_buffer_restore_cache[bufnr] = {}

      for _, keyspec in pairs(spec) do
        backup_keyspec(keyspec, existing_keymaps, per_buffer_restore_cache[bufnr], bufnr)
      end

      apply(bufnr)
      return
    end

    vim.notify('keymap.overlay :: Invalid bufnr value provided, must be number - ' .. vim.inspect(bufnr), 'error')
  end

  local function restore_cache()
    for _, restore_keymap in ipairs(global_restore_cache) do
      vim.keymap.set(
        restore_keymap.mode,
        restore_keymap.lhs,
        restore_keymap.callback or restore_keymap.rhs,
        {
          silent = restore_keymap.silent == 1,
          noremap = restore_keymap.noremap == 1,
          nowait = restore_keymap.nowait == 1,
          desc = restore_keymap.desc
        }
      )
    end
  end

  local function restore_buf_cache(bufnr)
    for _, keymaps in pairs(per_buffer_restore_cache[bufnr]) do
      for _, restore_keymap in ipairs(keymaps) do
        vim.keymap.set(
          restore_keymap.mode,
          restore_keymap.lhs,
          restore_keymap.callback or restore_keymap.rhs,
          {
            buffer = restore_keymap.buffer,
            silent = restore_keymap.silent == 1,
            noremap = restore_keymap.noremap == 1,
            nowait = restore_keymap.nowait == 1,
            desc = restore_keymap.desc
          }
        )
      end
    end
  end

  local function remove_overlay(bufnr)
    if bufnr == nil then
      if global_restore_cache == nil then
        vim.notify('keymap.remove_overlay :: Cannot restore keymap - overlay not applied', 'error')
        return
      end

      remove()
      restore_cache()
      global_restore_cache = nil
      return
    end

    if type(bufnr) == 'number' then
      if per_buffer_restore_cache[bufnr] == nil then
        vim.notify('keymap.remove_overlay :: Cannot restore keymap in current buffer - overlay not applied', 'error')
        return
      end

      remove(bufnr)
      restore_buf_cache(bufnr)
      per_buffer_restore_cache[bufnr] = nil
      return
    end

    vim.notify('keymap.remove_overlay :: Invalid bufnr value provided, must be number - ' .. vim.inspect(bufnr), 'error')
  end

  return {
    spec = spec,
    modes = MODES,
    defines_mapping_for = defines_mapping_for,
    apply = apply,
    remove = remove,
    overlay = overlay,
    remove_overlay = remove_overlay,
    is_active = is_active,
  }
end

-- local demo = keymap {
--   demo_keymap = { { 'n', 'v' }, '<M-p>', function() show('Hi there') end, { desc = 'This is a test' } }
-- }
--
-- function keymap.on()
--   demo.overlay()
-- end
--
-- function keymap.off()
--   demo.remove_overlay()
-- end
--
-- function keymap.buf_on(bufnr)
--   demo.overlay(bufnr)
-- end
--
-- function keymap.buf_off(bufnr)
--   demo.remove_overlay(bufnr)
-- end

return keymap
