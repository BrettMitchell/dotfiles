return function(tbl, fn)
  local res = {}
  for i, v in ipairs(tbl) do
    table.insert(res, fn(v, i, tbl))
  end
  return res
end
