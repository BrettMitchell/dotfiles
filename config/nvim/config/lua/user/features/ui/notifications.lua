local feat_util = require('user.util.feature')
local add_plugins = require('user.util.plugins.lazy').add_plugins

local M = {
  feature = {
    name = 'Notifications',

    description = [[
    Unobtrusive notifications
    ]],
  },
}

function M.setup(config)
  feat_util.apply_config(config or {}, M.feature)

  add_plugins({ {
    'https://github.com/vigoux/notifier.nvim',
    config = function()
      require('notifier').setup({
        notify = {
          clear_time = 2500
        }
      })

      local old_notify = vim.notify

      vim.notify = function(msg, level, opts)
        if type(level) == 'string' then
          level = vim.log.levels[level:upper()]
        end

        old_notify(msg, level, opts)
      end
    end
  } })
end

return M
