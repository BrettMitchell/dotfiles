#!/usr/bin/env bash

rm_safe "$HOME/.config/waybar"
rm_safe "$HOME/.config/sway/config"
