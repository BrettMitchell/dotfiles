
export SKIM_KEYBINDINGS_MOTION='ctrl-r:toggle-sort,ctrl-d:half-page-down,ctrl-u:half-page-up,ctrl-h:backward-char,ctrl-l:forward-char,ctrl-j:down,ctrl-k:up,ctrl-shift-j:pgdn,ctrl-shift-k:pgup';

export SKIM_KEYBINDINGS_PREVIEW='ctrl-p:toggle-preview,ctrl-i:preview-page-up,ctrl-o:preview-page-down'

export SKIM_KEYBINDINGS_SELECT='enter:execute-silent(printf "mode1%s" {})+abort,ctrl-enter:execute-silent(printf "mode2%s" {})+abort';

export SKIM_KEYBINDINGS_MULTISELECT='ctrl-t:toggle+down,ctrl-shift-t:toggle+up,ctrl-i:toggle-all,ctrl-c:deselect-all'
