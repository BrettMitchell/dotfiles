local js_vscode = require('user.features.dap.adapters.js_vscode')
local node2 = require('user.features.dap.adapters.node2')
local delve = require('user.features.dap.adapters.delve')

return {
  js_vscode = js_vscode,
  node2 = node2,
  delve = delve,

  by_adapter_name = {
    [js_vscode.adapter_name] = js_vscode,
    [node2.adapter_name] = node2,
    [delve.adapter_name] = delve,
  }
}
