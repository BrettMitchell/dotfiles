local make_feature = require('pax-vim.util.feature').make_feature
local priority = require('pax-vim.util.priority')

local state = {
  activate_on_load = false
}

local M = make_feature({
  name = 'Colorschema: Pywal',

  description = [[
  This feature adds pywal derived colors to nvim

  Note: The color choices made by this plugin are not
  always optimal, and an alternative solution will probably
  be added
  ]],

  plugins = {
    {
      lazy = false,
      priority = priority.theme,
      'https://github.com/AlphaTechnolog/pywal.nvim',
      name = 'pywal',
      config = function()
        require('pywal').setup()
        if state.activate_on_load then
          vim.cmd [[ colorscheme pywal ]]
        end
      end
    }
  },
})

M.methods = {
  activate = function ()
    state.activate_on_load = true
  end
}

return M

