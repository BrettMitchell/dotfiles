local feat_util = require('pax-vim.util.feature')
local keymap = require('pax-vim.util.keymap')
local add_plugins = require('pax-vim.util.plugins.lazy').add_plugins

local M = {
  feature = {
    name = 'Hover',

    description = [[
    This feature adds nice-looking hover popups for a variety of info providers.
    ]],

    keymap = keymap {
      hover_doc = { { 'n', 'x' }, '<space>lh', function() require('hover').hover({ name = 'LSP' }) end,
        { desc = 'Show documentation overlay' }, },
      signature_help = { { 'n', 'x' }, '<space>ls', function() require('hover').hover({ name = 'LSP Signature Help' }) end,
        { desc = 'Show signature help overlay' }, },
    },
  }
}

function M.setup(config)
  feat_util.apply_config(config or {}, M.feature)

  add_plugins({
    {
      -- 'https://github.com/brettmitchelldev/hover.nvim',
      dir = '/home/brettm/projects/software/nvim-plugins/hover.nvim',
      config = function()
        local hover = require('hover')

        hover.setup({
          init = function()
            local lsp_hover_provider = require('hover.providers.lsp')
            hover.register(lsp_hover_provider)

            local lsp_signature_help_provider = require('pax-vim.features.ui.hover_lsp_signature_help_provider')
            hover.register(lsp_signature_help_provider)
          end,
          preview_opts = {
            border = { "┌", "─", "┐", "│", "┘", "─", "└", "│" },
          },
          preview_window = false,
          title = false,
        })
      end
    }
  })

  M.feature.keymap.apply()
end

return M
