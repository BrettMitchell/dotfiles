local lazy = require('user.util.plugins.lazy')

local M = {
  name = 'Autopairs',

  description = [[
  This feature autopair support.
  ]]
}

function M.setup()
  lazy.add_plugins({
    {
      'https://github.com/windwp/nvim-autopairs',
      event = 'VeryLazy',
      config = function()
        local npairs = require("nvim-autopairs")

        npairs.setup {
          enable_check_bracket_line = false
        }

        npairs.add_rules(require('nvim-autopairs.rules.endwise-elixir'))
        npairs.add_rules(require('nvim-autopairs.rules.endwise-lua'))
        npairs.add_rules(require('nvim-autopairs.rules.endwise-ruby'))
      end
    },

    {
      'https://github.com/windwp/nvim-ts-autotag',
      event = 'VeryLazy',
      config = function()
        require('nvim-ts-autotag').setup()
      end
    },
  })
end

return M
