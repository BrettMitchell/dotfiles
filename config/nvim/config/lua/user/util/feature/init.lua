local lazy = require('user.util.plugins.lazy')
local autocmd = require('user.util.autocmd')
local keymap = require('user.util.keymap')

local M = {}

function M.is_feature(item)
  local item_is_feature = type(item) == 'table' and
      type(item.name) == 'string' and
      type(item.description) == 'string' and
      item.plugins and
      item.config and
      item.augroups and
      item.keymaps and
      item.methods and
      item.setup_seq and
      item.status and
      type(item.setup) == 'function' and
      type(item.set_config) == 'function' and
      type(item.on_activate) == 'function' and
      type(item.on_deactivate) == 'function' and
      type(item.register_init_callback) == 'function' and
      type(item.register_plugins) == 'function' and
      type(item.unregister_plugins) == 'function' and
      type(item.register_keymaps) == 'function' and
      type(item.unregister_keymaps) == 'function' and
      type(item.register_autocmds) == 'function' and
      type(item.unregister_autocmds) == 'function' and
      type(item.teardown) == 'function'

  return item_is_feature
end

local function check_deps(feature)
  if type(feature.dependencies) ~= 'table' then return true end

  local valid = true
  for _, dep in ipairs(feature.dependencies) do
    if type(dep) == 'table' then
      if type(dep.dependencies) == 'table' then
        valid = valid and check_deps(dep.dependencies)
      end

      -- Standalone dependency definition
      if type(dep.is_satisfied) == 'function' then
        valid = valid and dep.is_satisfied(feature)
        -- Standalone dependency definition
      elseif type(dep.is_satisfied) == 'boolean' then
        valid = valid and dep.is_satisfied
        -- Dependency on another feature
      elseif type(dep.status) == 'table' then
        valid = valid and dep.status.setup_active
      end
    end
  end

  return valid
end

local function registerAugroups(augroupSpec)
  if type(augroupSpec) ~= 'table' then return end

  if not autocmd.is_augroup_spec(augroupSpec) then
    local groupIds = {}
    for _, spec in ipairs(augroupSpec) do
      local groupId = registerAugroups(spec)[1]
      table.insert(groupIds, groupId)
    end
    return groupIds
  else
    local groupId = autocmd.augroup(augroupSpec)
    return { groupId }
  end
end

local function unregisterAutocmds(augroupIds)
  for _, augroupId in ipairs(augroupIds) do
    vim.api.nvim_del_augroup_by_id(augroupId)
  end
end

local DEFAULT_SETUP_SEQ = {
  config = true,
  plugins = true,
  autocmds = true,
  keymaps = true,
  on_activate = true,
}

function M.make_feature(feature)
  feature = feature or {}

  feature.name = feature.name or 'Unnamed feature'
  feature.description = feature.description or ''
  feature.config = feature.config or {}
  feature.plugins = feature.plugins or {}
  feature.augroups = feature.augroups or {}
  feature.methods = feature.methods or {}
  feature.setup_seq = vim.tbl_deep_extend('force', DEFAULT_SETUP_SEQ, feature.setup_seq or {})

  -- keymaps may be passed in already initialized with
  -- the 'keymap' helper
  feature.keymaps = feature.keymaps or keymap {}
  if type(feature.keymaps.apply) ~= 'function' then
    feature.keymaps = keymap(feature.keymaps)
  end

  feature.set_config = feature.set_config or
      function(user_config)
        for k, v in pairs(user_config or {}) do
          if type(v) == 'table' and type(feature.config[k]) == 'table' then
            feature.config[k] = vim.tbl_deep_extend('force', feature.config[k], v or {})
          else
            feature.config[k] = v
          end
        end
      end

  feature.on_activate = feature.on_activate or
      function()
      end

  feature.on_deactivate = feature.on_deactivate or
      function()
      end

  feature.on_setup = feature.on_setup or
      function()
      end

  feature.status = {
    autocmds_active = false,
    keymaps_active = false,
    plugins_active = false,
    setup_active = false,
  }

  feature.register_init_callback = function()
    lazy.add_post_load_cb(function()
      if feature.setup_seq.keymaps then
        feature.register_keymaps()
      end

      if feature.setup_seq.autocmds then
        feature.register_autocmds()
      end

      if feature.setup_seq.on_activate then
        feature.on_activate()
      end
    end)
  end

  feature.register_plugins = function()
    for _, plugin in ipairs(feature.plugins) do
      lazy.add_plugin(plugin)
    end
    feature.status.plugins_active = true;
  end

  feature.register_keymaps = function()
    feature.keymaps.apply()
    feature.status.keymaps_active = true;
  end

  feature.register_autocmds = function()
    feature.augroupIds = registerAugroups(feature.augroups)
    feature.status.autocmds_active = true;
  end

  feature.unregister_plugins = function()
    for _, plugin in ipairs(feature.plugins) do
      local lazy_plugin = find_lazy_plugin(plugin)
      lazy.deactivate(lazy_plugin)
    end
    feature.status.plugins_active = false;
  end

  feature.unregister_keymaps = function()
    feature.keymaps.remove()
    feature.status.keymaps_active = false;
  end

  feature.unregister_autocmds = function()
    unregisterAutocmds(feature.augroupIds)
    feature.augroupIds = {}
    feature.status.autocmds_active = false;
  end

  local function setup(config)
    if not check_deps(feature) then
      local short = {}
      for _, dep in ipairs(feature.dependencies) do
        if type(dep) == 'table' and dep.name then
          table.insert(short, dep.name)
        else
          table.insert(short, dep)
        end
      end

      vim.notify("Dependencies not satisfied for feature '" ..
        feature.name ..
        "'. Make sure dependencies are loaded before the feature that requires them : dependencies = " ..
        vim.inspect(short))
      return
    end

    if feature.setup_seq.config then
      feature.set_config(config)
    end

    if feature.setup_seq.plugins then
      feature.register_plugins()
    end

    feature.register_init_callback()
    feature.on_setup()

    feature.status.setup_active = true
  end

  local function teardown()
    if feature.status.plugins_active then
      feature.unregister_plugins()
    end

    if feature.status.keymaps_active then
      feature.unregister_keymaps()
    end

    if feature.status.autocmds_active then
      feature.unregister_autocmds()
    end

    feature.on_deactivate()

    feature.status.setup_active = false
  end

  feature.setup = setup
  feature.teardown = teardown

  return feature
end

function M.check_deps(deps)
  local valid = true
  for _, dep in ipairs(deps) do
    if type(dep) == 'table' then
      if type(dep.deps) == 'table' then
        valid = valid and check_deps(dep.deps)
      end

      -- Standalone dependency definition
      if type(dep.is_satisfied) == 'function' then
        valid = valid and dep.is_satisfied()
        -- Standalone dependency definition
      elseif type(dep.is_satisfied) == 'boolean' then
        valid = valid and dep.is_satisfied
        -- Dependency on another feature
      elseif type(dep.__setup_complete) == 'boolean' then
        valid = valid and dep.__setup_complete
      end
    end
  end

  return valid
end

function M.assert_deps(feature_name, deps)
  if not M.check_deps(deps) then
    local short = {}
    for _, dep in ipairs(deps) do
      if type(dep) == 'table' and dep.name then
        table.insert(short, dep.name)
      else
        table.insert(short, dep)
      end
    end

    vim.notify("Dependencies not satisfied for feature '" ..
      feature_name ..
      "'. Make sure dependencies are loaded before the feature that requires them : dependencies = " ..
      vim.inspect(short))
    return
  end
end

local function check_deps_v2(feature)
  if type(feature.deps) ~= 'table' then return true end
  return M.check_deps(feature.deps)
end

--[[
-- Feature lifecycle:
--  1. Feature is defined in source file with 'make_feature'
--  2. Feature is configured by user in init file
--  3. Dependencies are checked
--  4. Plugins are registered with lazy
--  5. After all other features are configured, and lazy is bootstrapped and loaded:
--    1. Feature is constructed with 'init'
--    2. User configuration is applied to constructed feature instance
--    3. Feature is applied with 'apply'. This is where functionality should be constructed
--]]
function M.make_feature_v2(feature)
  if type(feature) ~= 'table' then
    vim.notify('make_feature :: No argument provided', 'error')
    return feature
  end

  if type(feature.name) ~= 'string' then
    vim.notify("make_feature :: Option 'name' is required and must be a string: " .. vim.inspect(feature.name), 'error')
    return feature
  end

  if feature.deps and type(feature.deps) ~= 'table' then
    vim.notify("make_feature :: Option 'deps' must be a table when provided: " .. vim.inspect(feature.deps), 'error')
    return feature
  end

  if feature.plugins and type(feature.plugins) ~= 'table' then
    vim.notify("make_feature :: Option 'plugins' must be a table when provided: " .. vim.inspect(feature.plugins),
      'error')
    return feature
  end

  if feature.init and type(feature.init) ~= 'function' then
    vim.notify("make_feature :: Option 'init' must be a function when provided: " .. vim.inspect(feature.init), 'error')
    return feature
  end

  if feature.apply and type(feature.apply) ~= 'function' then
    vim.notify("make_feature :: Option 'apply' must be a function when provided: " .. vim.inspect(feature.apply), 'error')
    return feature
  end

  feature.setup = function(config_producer)
    -- Normalize user-supplied configuration
    -- Can be either function, table, or nil
    if type(config_producer) == 'fn' then
      -- config_producer = config_producer
    elseif type(config_producer) == 'table' then
      local overrides = config_producer
      config_producer = function(default_config)
        for k, v in pairs(overrides) do
          if type(v) == 'table' and type(default_config[k]) == 'table' then
            default_config[k] = vim.tbl_deep_extend('force', default_config[k], v)
          else
            default_config[k] = v
          end
        end
        return default_config
      end
    elseif config_producer == nil then
      config_producer = function(config) return config end
    else
      vim.notify(
        feature.name ..
        ' :: Invalid setup configuration provided, must be a table or a function: ' .. vim.inspect(config_producer),
        'error')
      return
    end

    -- Check dependencies
    if not check_deps_v2(feature) then
      local short = {}
      for _, dep in ipairs(feature.deps) do
        if type(dep) == 'table' and dep.name then
          table.insert(short, dep.name)
        else
          table.insert(short, dep)
        end
      end

      vim.notify("Dependencies not satisfied for feature '" ..
        feature.name ..
        "'. Make sure dependencies are loaded before the feature that requires them : dependencies = " ..
        vim.inspect(short))
      return
    end

    -- Register plugins
    if feature.plugins then
      for _, plugin in ipairs(feature.plugins) do
        -- Inject feature into any config callbacks defined on plugins
        local original_config = plugin.config
        plugin.config = function()
          if type(original_config) == 'function' then
            original_config(feature)
          end
        end
        lazy.add_plugin(plugin)
      end
    end

    -- Register system init callback
    lazy.add_post_load_cb(function()
      local initialized_feature = feature
      if type(initialized_feature.init) == 'function' then
        initialized_feature = initialized_feature:init()
      end
      if not initialized_feature then
        initialized_feature = feature
      else
        if not M.is_feature_v2(initialized_feature) then
          vim.notify('make_feature.init :: When returning a new value, that value must be a valid feature definition. Got ' .. vim.inspect(initialized_feature), 'error')
          return
        end
      end

      feature = config_producer(initialized_feature)
      if not feature then
        feature = initialized_feature
      end
      if not M.is_feature_v2(feature) then
        vim.notify('make_feature.setup :: When returning a new value, that value must be a valid feature definition. Got ' .. vim.inspect(feature), 'error')
        return
      end

      if type(feature.apply) == 'function' then
        feature:apply()
      end
    end)

    feature.__setup_complete = true
  end

  feature.__setup_complete = false

  return feature
end

function M.is_feature_v2(item)
  local item_is_feature =
      type(item) == 'table' and
      type(item.name) == 'string' and
      type(item.description) == 'string' and
      (not item.plugins or type(item.plugins) == 'table') and
      (not item.deps or type(item.deps) == 'table') and
      (not item.init or type(item.init) == 'function') and
      (not item.apply or type(item.apply) == 'function') and
      type(item.setup) == 'function'

  return item_is_feature
end



function M.apply_config(user_config, feature)
  -- Normalize user-supplied configuration
  -- Can be either function, table, or nil
  if type(user_config) == 'fn' then
    local updated = user_config(feature)
    if updated == nil then
      return feature
    else
      return updated
    end
  elseif type(user_config) == 'table' then
    for k, v in pairs(user_config) do
      if type(v) == 'table' and type(feature[k]) == 'table' then
        feature[k] = vim.tbl_deep_extend('force', feature[k], v)
      else
        feature[k] = v
      end
    end
    return feature
  elseif user_config == nil then
    return feature
  else
    vim.notify(
      feature.name ..
      ' :: Invalid setup configuration provided, must be a table or a function: ' .. vim.inspect(user_config),
      'error')
    return feature
  end
end

return M
