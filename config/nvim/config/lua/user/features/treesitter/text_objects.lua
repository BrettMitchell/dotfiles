local add_plugins = require('user.util.plugins.lazy').add_plugins
local keymap = require('user.util.keymap')

local M = {
  feature = {
    name = 'Word Motions',

    description = [[
    This feature adds new text objects and motions with treesitter queries
    ]],
  },
}

function M.setup()
  add_plugins({
    {
      'https://github.com/nvim-treesitter/nvim-treesitter-textobjects',
      after = 'https://github.com/nvim-treesitter/nvim-treesitter',
      event = 'VeryLazy',
      config = function()
        vim.schedule(function()
          local ts_repeat_move = require('nvim-treesitter.textobjects.repeatable_move')
          keymap {
            repeat_motion = { { 'n', 'x', 'o' }, '<C-j>', ts_repeat_move.repeat_last_move, { desc = 'Repeat last treesitter movement' } },
            repeat_motion_opposite = { { 'n', 'x', 'o' }, '<C-k>', ts_repeat_move.repeat_last_move_opposite, { desc = 'Repeat last treesitter movement in the opposite direction' } },
          }.apply()

          require('nvim-treesitter.configs').setup({
            textobjects = {
              select = {
                enable = true,
                -- disable = { 'zig' },

                lookahead = true,

                keymaps = {
                  ['a='] = { query = '@assignment.outer', { desc = 'Select whole assignment' } },
                  ['i='] = { query = '@assignment.inner', { desc = 'Select RHS of assignment' } },
                  ['r='] = { query = '@assignment.rhs', { desc = 'Select LHS of assignment' } },

                  ['aA'] = { query = '@attribute.outer', query_group = 'custom_textobjects', { desc = 'Select outer part of attribute' } },
                  ['iA'] = { query = '@attribute.inner', query_group = 'custom_textobjects', { desc = 'Select inner part of attribute' } },

                  ['aa'] = { query = '@parameter.outer', { desc = 'Select outer part of parameter/argument' } },
                  ['ia'] = { query = '@parameter.inner', { desc = 'Select inner part of parameter/argument' } },

                  ['am'] = { query = '@comment.outer', { desc = 'Select comment' } },
                  ['im'] = { query = '@comment.outer', { desc = 'Select comment' } },

                  ['ac'] = { query = '@conditional.outer', { desc = 'Select outer part of conditional' } },
                  ['ic'] = { query = '@conditional.inner', { desc = 'Select inner part of conditional' } },

                  ['ai'] = { query = '@call.outer', { desc = 'Select outer part of invokation' } },
                  ['ii'] = { query = '@call.inner', { desc = 'Select inner part of invokation' } },

                  ['af'] = { query = '@function.outer', { desc = 'Select outer part of function definition' } },
                  ['if'] = { query = '@function.inner', { desc = 'Select inner part of function definition' } },

                  ['al'] = { query = '@loop.outer', { desc = 'Select outer part of loop' } },
                  ['il'] = { query = '@loop.inner', { desc = 'Select inner part of loop' } },

                  ['aC'] = { query = '@class.outer', { desc = 'Select outer part of class' } },
                  ['iC'] = { query = '@class.inner', { desc = 'Select inner part of class' } },
                }
              },

              move = {
                enable = true,
                -- disable = { 'zig' },

                set_jumps = true,

                goto_next_start = {
                  [']='] = { query = '@assignment.outer', { desc = 'Next assignment start' } },
                  [']A'] = { query = '@attribute.outer', query_group = 'custom_textobjects', { desc = 'Next attribute start' } },
                  [']a'] = { query = '@parameter.inner', { desc = 'Next parameter/argument start' } },
                  [']m'] = { query = '@comment.outer', { desc = 'Next comment start' } },
                  [']c'] = { query = '@conditional.outer', { desc = 'Next conditional start' } },
                  [']i'] = { query = '@call.outer', { desc = 'Next function invokation start' } },
                  [']f'] = { query = '@function.outer', { desc = 'Next function definition start' } },
                  [']l'] = { query = '@loop.outer', { desc = 'Next loop start' } },
                  [']C'] = { query = '@class.outer', { desc = 'Next class start' } },
                },

                goto_next_end = {},

                goto_previous_start = {
                  ['[='] = { query = '@assignment.outer', { desc = 'Previous assignment start' } },
                  ['[A'] = { query = '@attribute.outer', query_group = 'custom_textobjects', { desc = 'Previous attribute start' } },
                  ['[a'] = { query = '@parameter.inner', { desc = 'Previous parameter/argument start' } },
                  ['[m'] = { query = '@comment.outer', { desc = 'Previous comment start' } },
                  ['[c'] = { query = '@conditional.outer', { desc = 'Previous conditional start' } },
                  ['[i'] = { query = '@call.outer', { desc = 'Previous function invokation start' } },
                  ['[f'] = { query = '@function.outer', { desc = 'Previous function definition start' } },
                  ['[l'] = { query = '@loop.outer', { desc = 'Previous loop start' } },
                  ['[C'] = { query = '@class.outer', { desc = 'Previous class start' } },
                },

                goto_previous_end = {}
              },
            },
          })
        end)
      end
    },
  })
end

return M
