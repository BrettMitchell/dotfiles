local pickers = require('telescope.pickers')
local sorters = require('telescope.sorters')
local finders = require('telescope.finders')

local theme = require('user.telescope.theme')
local attachMappings = require('user.telescope.attachMappings')

local function pickOption(items, opts)
  opts = opts or {}

  local mappings = opts.mappings or {}

  local finderConfig = { results = items }
  if opts.entry_maker then
    finderConfig.entry_maker = opts.entry_maker
  end

  local picker = pickers.new {
    results_title = opts.title or '',
    finder = finders.new_table(finderConfig),
    sorter = sorters.get_fuzzy_file(),
    previewer = opts.previewer or nil,
    prompt_title = opts.prompt_title or '',
    attach_mappings = attachMappings.make_attach_mappings(mappings),
  }

  picker:find(theme.horizontal)
end

return {
  pickOption = pickOption
}
