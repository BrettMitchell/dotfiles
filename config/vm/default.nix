{ nixpkgs, ... }:
{
  home.packages = with nixpkgs.stable; [
    qemu
    quickemu
    libvirt
  ];
}
