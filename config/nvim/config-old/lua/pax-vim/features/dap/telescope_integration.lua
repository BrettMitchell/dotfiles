local make_feature = require('pax-vim.util.feature').make_feature_v2
local dap = require('pax-vim.features.dap')
local telescope = require('pax-vim.features.telescope')

-- Reference for future use when creating more advanced, ergonomic attach interaction
-- https://github.com/nvim-telescope/telescope-dap.nvim/blob/master/lua/telescope/_extensions/dap.lua#L62

local M = make_feature({
  name = 'DAP :: Variable Values in Virtual Text',
  deps = { dap, telescope },

  description = [[
  This feature shows variable values in virtual text at the end of the
  line where the variable is defined or referenced.
  ]],

  plugins = {
    {
      'https://github.com/nvim-telescope/telescope-dap.nvim',
      dependencies = { 'https://github.com/nvim-telescope/telescope.nvim' },
      config = function ()
        require('telescope').load_extension('dap')
      end
    }
  }
})

return M

