local make_feature = require('user.util.feature').make_feature

local config = {
  leap_config = {}
}

local M = make_feature({
  name = 'Leap',

  description = [[
  This feature adds a jump-to-digraph hint motion to neovim.
  ]],

  plugins = {
    {
      'https://github.com/ggandor/leap.nvim',
      config = function()
        require('leap').setup(config.leap_config)
      end
    },
  },

  config = config,
})

return M
