local filter = require('pax-vim.util.table.filter')

local make_hook_registry = function(opts)
  opts = opts or {}
  local hook_names = opts.hook_names or nil
  local hooks = {}

  local hook_name_valid = function(hook_name)
    if hook_names == nil then return true end

    for _, known_hook_name in ipairs(hook_names) do
      if known_hook_name == hook_name then
        return true
      end
    end

    return false
  end

  local has_hook = function(hook_name, fn)
    if not hook_name_valid then
      return false
    end

    for _, registered_hook in ipairs(hooks[hook_name] or {}) do
      if registered_hook == fn then
        return true
      end
    end

    return false
  end

  local add_hook = function(hook_name, fn)
    if not hook_name_valid then
      vim.notify('Invalid hook name: ' .. vim.inspect(hook_name), 'error')
      return
    end

    if has_hook(hook_name, fn) then return end

    if not hooks[hook_name] then
      hooks[hook_name] = {}
    end

    table.insert(hooks[hook_name], fn)
  end

  local del_hook = function(hook_name, fn)
    if not hook_name_valid then
      vim.notify('Invalid hook name: ' .. vim.inspect(hook_name), 'error')
      return
    end

    for _, registered_hook in ipairs(hooks[hook_name] or {}) do
      if registered_hook == fn then
        hooks[hook_name] = filter(hooks[hook_name], function(registered_fn)
          return registered_fn ~= fn
        end)
      end
    end
  end

  local do_hook = function(hook_name, ...)
    if not hook_name_valid then
      vim.notify('Invalid hook name: ' .. vim.inspect(hook_name), 'error')
      return
    end

    for _, registered_hook in ipairs(hooks[hook_name] or {}) do
      registered_hook(...)
    end
  end

  return {
    add_hook = add_hook,
    del_hook = del_hook,
    has_hook = has_hook,
    do_hook = do_hook,
  }
end

return {
  make_hook_registry = make_hook_registry
}
