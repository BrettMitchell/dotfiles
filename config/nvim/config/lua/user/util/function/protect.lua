local M = {}

function M.protect(fn)
  return function(...)
    return pcall(fn, ...)
  end
end

return M
