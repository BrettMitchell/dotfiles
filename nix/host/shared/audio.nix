{ options, config, pkgs, lib, ... }:
{
  environment.systemPackages = with pkgs; [
    pulsemixer
    pavucontrol
    wireplumber
  ];

  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;

  services.pipewire = {
    enable = true;
    # package = pkgs.pulseaudioFull;

    alsa.enable = true;
    pulse.enable = true;
    # jack.enable = true;
    wireplumber.enable = true;
  };
}
