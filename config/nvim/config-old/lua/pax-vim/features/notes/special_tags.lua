return {
  EFFORT = 't.efforts',
  TAXONOMY = 'taxonomy',
  TAXON = 'taxon',
  WEB_RESOURCE = 'resource.web',
  FILE_RESOURCE = 'resource.file',

  -- TODO: Use these as limiting factors rather than direct relationships
  -- I.e., when viewing a list of notes in the graph traversal dialog, I want to be
  --       able to just see the facts, or just see the processes, or just see the
  --       intents, and then search among the results, rather than searching for
  --       'fact' and seeing facts etc.
  INDEX = 'index',
  TASK = 'task',
  PLANNING = 'planning',
  INTENT = 'intent',
  FACT = 'fact',
  LIFE_EVENT = 'life-event',
  JOURNAL = 'journal',
  PROCESS = 'process',
  QUERY = 'query',
  CURRENT = 'current',
  SAVED_CONTEXT = 'saved-context',

  -- Need to retire this at some point
  SAVED_SEARCH = 'search',

  NOISY_TAG_NAMES = {
    'saved-context',
    'query',
    'taxon',
    'taxonomy',

    'index',
    'task',
    'planning',
    'intent',
    'fact',
    'life-event',
    'journal',
    'process',
    'current',
  },
}
