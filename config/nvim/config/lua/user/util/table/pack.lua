
if type(table.pack) == 'function' then
  return table.pack
end

if type(pack) == 'function' then
  return pack
end

return function(...)
  local t = { ... }
  t.n = select('#', ...)
  return t
end
