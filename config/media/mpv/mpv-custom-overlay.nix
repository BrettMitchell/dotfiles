{
  overlay = (final: prev: {
    mpv-custom = prev.mpv.override {
      scripts = [
        # final.mpvScripts.modernx-zydezu
        final.mpvScripts.uosc
        final.mpvScripts.thumbfast
        final.mpvScripts.quality-menu
        final.mpvScripts.seekTo
        final.mpvScripts.mpris
        # final.mpvScripts.dynamic-crop
      ];
    };
  });
}
