local feat_util = require('pax-vim.util.feature')
local add_plugins = require('pax-vim.util.plugins.lazy').add_plugins

local M = {
  feature = {
    name = 'Alpha (dashboard)',

    description = [[
    This feature configures the Alpha dashboard plugin.
    ]],

    header = nil,
    buttons = nil,
  }
}

function M.setup(config)
  M.feature = feat_util.apply_config(config, M.feature)

  add_plugins({
    {
      'https://github.com/goolord/alpha-nvim',

      dependencies = {
        'https://github.com/nvim-tree/nvim-web-devicons'
      },

      config = function()
        local alpha = require 'alpha'
        local dashboard = require 'alpha.themes.dashboard'

        dashboard.config.opts.autostart = false

        if M.feature.header then
          dashboard.section.header.val = M.feature.header
        end

        if M.feature.buttons then
          local buttons = {}
          for _, button in ipairs(M.feature.buttons) do
            table.insert(buttons, dashboard.button(unpack(button)))
          end
          dashboard.section.buttons.val = buttons
        end

        alpha.setup(dashboard.config)
      end
    }
  })
end

return M
