return {
  config = function ()
      local gitsigns = require('user.gitsigns')
      local galaxyline = require('galaxyline')

      require('true-zen').setup {
        modes = {
          narrow = {
            callbacks = {
              open_pre = function()
                gitsigns.disable()
                galaxyline.disable_galaxyline()
              end,
              close_pos = function()
                gitsigns.enable()
                galaxyline.load_galaxyline()
              end,
            }
          },

          ataraxis = {
            callbacks = {
              open_pre = function()
                gitsigns.disable()
                galaxyline.disable_galaxyline()
              end,
              close_pos = function()
                gitsigns.enable()
                galaxyline.load_galaxyline()
              end,
            }
          }
        },

        integrations = {
          twilight = true,
        }
      }
  end
}
