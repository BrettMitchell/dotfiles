local make_feature = require('pax-vim.util.feature').make_feature
local keymap = require('pax-vim.util.keymap')
local lsp = require('pax-vim.features.lsp')
local lspsaga = require('pax-vim.features.lsp.lspsaga')

local methods = {
  code_action = function ()
    require('lspsaga.codeaction'):code_action()
  end
}

-- Note: If this keymap is not applied ONLY to LSP enabled buffers,
--       this keybinding will then be active in the code action
--       floating window itself. Calling this binding in that
--       context causes an issue with lspsaga
local code_action_keymap = keymap {
  code_action = { { 'n', 'x' }, '<space>la', methods.code_action, { desc = 'Open code actions menu' } },
}

local M = make_feature({
  name = 'LSP :: Code actions',

  dependencies = { lsp, lspsaga },

  keymaps = code_action_keymap,
  setup_seq = { keymaps = false },

  description = [[
  This feature adds a popup menu for performing LSP-powered code actions.
  ]],

  on_activate = function()
    lsp.methods.add_hook({
      hook_name = 'on_attach',
      callback = function(_, bufnr)
        code_action_keymap.apply(bufnr)
      end,
    })
  end
})

M.methods = methods

return M
