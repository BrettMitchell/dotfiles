local make_feature = require('pax-vim.util.feature').make_feature_v2

local plenary = make_feature({
  name = 'Utility: Plenary',

  description = [[
  This feature adds the commonly used plugin 'plenary'
  ]],

  plugins = { {
    'nvim-lua/plenary.nvim',
  } },
})

return plenary
