local make_feature = require('pax-vim.util.feature').make_feature_v2
local keymap = require('pax-vim.util.keymap')

local system_clipboard = make_feature({
  name = 'Edit Commands: System Clipboard',

  description = [[
  This feature adds keymaps for copying to and pasting from the
  system clipboard.
  ]],

  keymap = keymap {
    { { 'n', 'o', 'v' }, 'y', '"+y', { desc = 'Yank' } },
    { { 'n', 'o' }, 'yy', '"+yy', { desc = 'Yank line' } },
    { { 'n', 'o' }, 'Y', '"+y$', { desc = "Yank to the end of the line" } },
    { 'v', 'Y', '"+Y', { desc = "Yank visual block" } },

    { { 'n', 'v' }, '<space>p', '"+p', { desc = 'Paste from the system clipboard' } },
    { { 'n', 'v' }, '<space>P', '"+P', { desc = 'Paste from the system clipboard' } },

    -- TODO:
    --  - ;yaf -> yank absolute filepath
    --  - ;yad -> yank absolute containing directory
    --  - ;yrf -> yank relative filepath
    --  - ;yrd -> yank relative containing directory
    { 'n', ';yp', "<cmd>let @+ = expand('%:p')<cr>", { desc = 'Yank full file path' } },
    { 'n', ';yP', "<cmd>let @+ = expand('%:p:h')<cr>", { desc = 'Yank containing directory' } },
  },

  apply = function (self)
    self.keymap.apply()
  end
})

return system_clipboard
