
# pip zsh completion start
function _pip_completion {
  local words cword
  read -Ac words
  read -cn cword
  reply=( $( COMP_WORDS="$words[*]" \
             COMP_CWORD=$(( cword-1 )) \
             PIP_AUTO_COMPLETE=1 $words[1] 2>/dev/null ))
}
compctl -K _pip_completion "$HOME/.local/share/rtx/installs/python/3.11.3/bin/python" -m pip
# pip zsh completion end

command -v register-python-argcomplete > /dev/null \
  && command -v pipx > /dev/null \
  && eval "$(register-python-argcomplete pipx)"

command -v kubectl > /dev/null \
  && source <(kubectl completion zsh)

