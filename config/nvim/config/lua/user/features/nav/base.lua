local keymap = require('user.util.keymap')

local base_navigation = {
  name = 'Base Motion Keymaps',

  description = [[
  This feature defines custom motion keymaps for faster navigation
  ]],

  normal_keymap = keymap {
    start_of_line = { { 'n', 'x' }, 'H', '^', { desc = 'Start of line' } },
    end_of_line = { { 'n', 'x' }, 'L', '$', { desc = 'End of line' } },
    down = { { 'n', 'x' }, 'J', '<C-D>', { desc = '15 lines down' } },
    up = { { 'n', 'x' }, 'K', '<C-U>', { desc = '15 lines up' } },
  },

  command_keymap = keymap {
    { 'c', '<C-h>',   '<left>',    { desc = 'Move left by one character', silent = false } },
    { 'c', '<C-l>',   '<right>',   { desc = 'Move right by one character', silent = false } },
    { 'c', '<C-j>',   '<down>',    { desc = 'Go back in command history by one entry', silent = false } },
    { 'c', '<C-k>',   '<up>',      { desc = 'Go forward in command history by one entry', silent = false } },
    { 'c', '<C-e>',   '<C-right>', { desc = 'Move forward by one word', silent = false } },
    { 'c', '<C-b>',   '<C-left>',  { desc = 'Move back by one word },', silent = false } },
    { 'c', '<C-S-h>', '<home>',    { desc = 'Go to the beginning of the line', silent = false } },
    { 'c', '<C-S-l>', '<end>',     { desc = 'Go to the end of the line', silent = false } },
  },

  insert_keymap = keymap {
    { 'i', '<C-h>', '<left>',  { desc = 'Left in insert mode' } },
    { 'i', '<C-j>', '<down>',  { desc = 'Down in insert mode' } },
    { 'i', '<C-k>', '<up>',    { desc = 'Up in insert mode' } },
    { 'i', '<C-l>', '<right>', { desc = 'Right in insert mode' } },
    { 'i', '<C-e>',   '<C-right>', { desc = 'Move forward by one word', silent = false } },
    { 'i', '<C-b>',   '<C-left>',  { desc = 'Move back by one word },', silent = false } },
  }
}

function base_navigation.setup()
  base_navigation.insert_keymap.apply()
  base_navigation.command_keymap.apply()
  base_navigation.normal_keymap.apply()
end

return base_navigation
