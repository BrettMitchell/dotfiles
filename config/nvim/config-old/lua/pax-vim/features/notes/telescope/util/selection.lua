--[[

This module contains the following helper functions:
  getMultiSelection - Gets a table containing all currently selected items

--]]

local action_state = require 'telescope.actions.state'

local M = {}

function M.getSingleSelection(prompt_bufnr)
  local highlightedLine = action_state.get_selected_entry(prompt_bufnr)
  return highlightedLine
end

function M.getMultiSelection(prompt_bufnr, fall_back_to_current_line)
  if type(fall_back_to_current_line) ~= 'boolean' then
    fall_back_to_current_line = true
  end

  local picker = action_state.get_current_picker(prompt_bufnr)
  local multiSelect = picker:get_multi_selection()

  local highlightedLine = action_state.get_selected_entry(prompt_bufnr)
  if (#multiSelect == 0 and fall_back_to_current_line) then
    return { highlightedLine }
  end

  return multiSelect
end

function M.yankAttrOfSelected(attr)
  return function(prompt_bufnr)
    local selectedEntries = M.getMultiSelection(prompt_bufnr)

    local selectedFilenames = {}
    for _, selectedEntry in ipairs(selectedEntries) do
      table.insert(selectedFilenames, selectedEntry.value[attr])
    end

    vim.fn.setreg('+', selectedFilenames)
    vim.fn.setreg('"', selectedFilenames)

    return selectedFilenames
  end
end

return M
