{ config, pkgs, nixpkgs, externalPackages, lib, ... }:

{
  home.username = "brettm";
  home.homeDirectory = lib.mkForce "/Users/brettm";
  home.stateVersion = "23.11";

  imports = [
    # Font
    ../../shared/fonts

    # System components
    # ../../../../config/browser/advanced.nix
    # ../../../../config/media
    ../../../../config/media/ffmpeg/darwin.nix

    # Configuration
    ../../../../config/broot
    ../../../../config/containers/podman
    ../../../../config/containers/docker
    ../../../../config/containers/util
    # ../../../../config/git
    ../../../../config/kitty
    ../../../../config/lf
    ../../../../config/nvim
    ../../../../config/shell
    ../../../../config/skim
    ../../../../config/tmux
    # ../../../../config/wezterm
    # ../../../../config/zathura

    # Scripts
    # ../../../../scripts/desktop
    ../../../../scripts/lf
    ../../../../scripts/multiplexer
    # ../../../../scripts/TEMP
    ../../../../scripts/theme
    ../../../../scripts/util
    ../../../../scripts/lazygit
  ];

  programs.man = {
    enable = true;
    generateCaches = true;
  };

  home.packages = with nixpkgs.stable; [
    externalPackages.tsessd.bin

    # # Shims
    # (import ../../../pkg/fhs { inherit pkgs nixpkgs; })
    # (import ../../../pkg/fhs-nvim { inherit pkgs nixpkgs; })
    nixpkgs.unstable.neovim

    # Shells
    dash

    # Terminal emulator
    wezterm
    kitty

    # # Runtimes, languages, and dev tools
    # python313
    # nixpkgs.unstable.nodejs_22
    luajit
    luajitPackages.luarocks
    luajitPackages.lyaml
    # rustup
    # gdb
    # lldb
    # valgrind
    unixtools.xxd
    bear
    pkg-config

    # # Documentation
    # manix
    # man    
    # man-pages
    # man-pages-posix

    # # Archives
    # zip
    # unzip
    rar
    pigz
    p7zip

    # # General shell utilities
    # bc
    util-linux
    findutils
    skim
    ripgrep
    fd
    fzf
    broot
    eza
    bat
    tmux
    lf
    jq
    yq
    nixpkgs.unstable.mise
    delta
    trash-cli
    lazygit
    tokei
    xsv
    restic
    glow
    moreutils
    lsof
    libcdio

    # Notes
    zk

    # hunspell
    exiftool

    # # System monitoring
    # powertop
    bottom
  ];
}
