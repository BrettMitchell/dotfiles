
## Completions

- https://github.com/hrsh7th/nvim-cmp


## LSP

List of things that go into my LSP setup

- https://github.com/jose-elias-alvarez/null-ls.nvim
  - Provides a modular, pluggable language server to expand the feature set of nvim LSP
- https://github.com/lukas-reineke/lsp-format.nvim
  - Auto-configures auto-format on save with null-ls


List of things that might go into my LSP setup in the future

- https://github.com/jose-elias-alvarez/typescript.nvim

