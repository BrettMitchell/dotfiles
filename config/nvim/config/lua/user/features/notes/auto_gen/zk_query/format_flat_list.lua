local table_util     = require('user.util.table')

return function (notes)
  local list = table_util.map(notes, function(note)
    -- TEMPORARY
    return '- [' .. note.title .. '](' .. note.path:gsub('zettelkasten', '.') .. ')'
    -- TEMPORARY

    -- TODO: Generalize note filename semantics. The fact that my notes are found at
    -- `$NOTES_DIR/zettelkasten/` rather than just `$NOTES_DIR` seems to have some
    -- unintended consequences for relative paths and directory logic.
    -- return '- [' .. note.title .. '](' .. note.path .. ')'
  end)

  return list
end
