local table_util = require('user.util.table')

------------------
-- Base options --
------------------

require('user.setopts')

------------
-- Server --
------------

require('user.features.startup.server').setup()

----------------------
-- Global functions --
----------------------

require('user.global_functions')

--------------
-- LuaRocks --
--------------

local home = vim.fn.getenv('HOME')
require('user.features.luarocks').setup({
  luarocks_install_path = function()
    return {
      home .. '/.luarocks/share/lua/5.1',
      -- This is here to get notes working
      -- This will go away once I create a neovim flake
      -- with this package in it.
      -- It is intentionally fragile so that I have to
      -- continuously fix it, and so that I am incentivized
      -- to remove this horrible hack.
      '/nix/store/rx9pr0bcpjzgknzbnh008lkji6mp97za-luajit2.1-lyaml-6.2.8-1/share/lua/5.1'
    }
  end,
  luarocks_install_cpath = function()
    return {
      home .. '/.luarocks/lib/luarocks/rocks-5.1',
      '/nix/store/rx9pr0bcpjzgknzbnh008lkji6mp97za-luajit2.1-lyaml-6.2.8-1/lib/lua/5.1'
    }
  end,
  quiet = true,
  ensure_installed = {
    'lua-path',
    -- 'lyaml',
    'lunajson',
    -- TODO: Re-enable this when associated issue is resolved in either mkdnflow or which-key
    -- 'luautf8',
  }
})

---------------
-- Base deps --
---------------

require('user.features.util.plenary').setup()

--------------
-- Quickfix --
--------------

require('user.features.quickfix.qf').setup()
require('user.features.quickfix.bqf').setup()
require('user.features.quickfix.quickfix_keymaps').setup()
require('user.features.quickfix.trouble').setup()

----------------
-- Treesitter --
----------------

require('user.features.treesitter').setup()
require('user.features.treesitter.text_objects').setup()
require('user.features.treesitter.context').setup()

--------
-- UI --
--------

-- require('user.features.theme.pywal').setup()
require('user.features.theme.pax-colors').setup()
require('user.features.ui.statusline').setup()
require('user.features.ui.nui').setup()

require('user.features.ui.toggle_wrap').setup()
require('user.features.ui.notifications_old').setup()
require('user.features.ui.hover').setup()
require('user.features.ui.highlightedyank').setup()
require('user.features.ui.folding').setup()
-- require('user.features.ui.whichkey').setup()

-----------------------------
-- Multiplexer integration --
-----------------------------

require('user.features.mux.open_external').setup()
require('user.features.mux.reflist').hook_telescope()

---------
-- Nav --
---------

require('user.features.telescope').setup()
require('user.features.telescope.ui_select_override').setup()

require('user.features.nav.base').setup()
require('user.features.nav.clever_f').setup()
require('user.features.nav.flash').setup()
require('user.features.nav.harpoon').setup()
require('user.features.nav.word_motions').setup()

----------
-- Edit --
----------

require('user.features.edit.auto_indent').setup()
require('user.features.edit.auto_shiftwidth').setup()
require('user.features.edit.break_object').setup()
require('user.features.edit.indent').setup()
require('user.features.edit.join_lines').setup()
require('user.features.edit.move_selection').setup()
require('user.features.edit.system_clipboard').setup()
require('user.features.edit.search_and_replace').setup()
require('user.features.edit.toggle_comment').setup()
require('user.features.edit.word_case_toggle').setup()
require('user.features.edit.yank_file_paths').setup()
require('user.features.undo.undotree').setup()

------------------
-- Text objects --
------------------

require('user.features.text_objects.surround').setup()

---------
-- LSP --
---------

require('user.features.lsp').setup({
  plugin_config = {
    lspconfig = {
      ts_ls = {
        init_options = {
          preferences = {
            disableSuggestions = true,
          },
        },
      }
    },
    mason = {
      ui = {
        border = 'single'
      }
    },
    mason_lspconfig = {
      automaticInstallation = false,
      ensure_installed = {
        'lua_ls',
        'gopls',
        'ts_ls',
        'zls',
        -- TODO: Figure out what the proper name is for this. Neither of these work.
        -- 'eslint-lsp',
        -- 'eslint-language-server',
      },
    }
  },

  hooks = {
    zls = {
      on_attach = {
        function(client, bufnr)
          client.server_capabilities.documentFormattingProvider = false
        end
      }
    },

    tsserver = {
      on_attach = {
        function(client, bufnr)
          client.server_capabilities.documentFormattingProvider = false
        end
      }
    }
  }
})

require('user.features.lsp.lspsaga').setup({
  beacon = { enable = false },
  scroll_preview = {
    scroll_down = '<C-j>',
    scroll_up = '<C-k>',
  },
  finder = {
    open = { 'o', '<CR>' },
    vsplit = 's',
    split = 'i',
    tabe = 't',
    quit = { 'q', '<ESC>' },
  },
  definition_action_keys = {
    edit = '<CR>',
    vsplit = '<C-CR>/',
    split = '<C-CR>\\',
    tabe = '',
    quit = { 'q', '<ESC>' },
  },
  code_action = {
    num_shortcut = true,
    keys = {
      quit = '<ESC>',
      exec = '<CR>',
    }
  },
  lightbulb = { enable = false, },
  outline = {
    -- win_position = 'right',
    -- win_with = '',
    -- win_width = 30,
    -- show_detail = true,
    -- auto_preview = true,
    -- auto_refresh = true,
    -- auto_close = true,
    -- custom_sort = nil,
    keys = {
      jump = '<cr>',
      expand_collapse = '<cr>',
      quit = 'q',
    },
  },
  symbol_in_winbar = {
    enable = false,
    separator = ' ',
    hide_keyword = true,
    show_file = true,
    folder_level = 2,
    respect_root = false,
    color_mode = true,
  },
  rename = {
    quit = '<esc>',
    exec = '<cr>',
    whole_project = false,
  },
})

require('user.features.lsp.code_actions').setup()
require('user.features.lsp.formatting').setup()
require('user.features.lsp.rename_symbol').setup()
require('user.features.lsp.hover_docs').setup()
require('user.features.lsp.outline').setup()
-- require('user.features.lsp.call_hierarchy').setup()
require('user.features.lsp.general_nav').setup()
require('user.features.lsp.diagnostics_nav').setup()
require('user.features.lsp.diagnostics_virtual_text').setup()
require('user.features.lsp.diagnostics_floating_window').setup()
-- require('user.features.lsp.signature_help').setup()
require('user.features.lsp.telescope_handlers').setup()
require('user.features.lsp.progress_notifications').setup()

---------
-- DAP --
---------

require('user.features.dap').setup({
  adapter_install_dir = os.getenv('HOME') .. '/.local/share/nvim/dap-install',

  signs = {
    breakpoint = {
      icon = '-',
      texthl = '',
      linehl = '',
      numhl = '',
    },

    stopped = {
      icon = '>',
      texthl = '',
      linehl = '',
      numhl = '',
    }
  }
})

require('user.features.dap.ui').setup()
-- require('user.features.dap.virtual_text_variable_values').setup()
require('user.features.dap.tsessd_integration').setup()

require('user.features.dap.adapters.node2').setup()
require('user.features.dap.adapters.js_vscode').setup()
require('user.features.dap.adapters.delve').setup()

-----------------
-- Completions --
-----------------

local luasnip = require('user.features.completion.snippets.luasnip')
local latex_snippets = require('user.features.completion.snippets.latex')
latex_snippets.setup()

local cmp_buffer_text = require('user.features.completion.buffer_text')
local cmp_lsp = require('user.features.completion.lsp')
local cmp_lsp_signature_help = require('user.features.completion.lsp.signature_help')
local cmp_path = require('user.features.completion.path')
-- local cmp_dap = require('user.features.completion.dap')
local cmp_emmet = require('user.features.completion.emmet')
local cmp_latex = require('user.features.completion.lang.latex')

require('user.features.completion.autopairs').setup()
-- require('user.features.completion.wildmenu').setup()

local base_sources = {
  {
    require('user.features.notes.quarto').feature.completion_source,
    cmp_lsp.source_definition,
    cmp_lsp_signature_help.source_definition,
    cmp_path.source_definition,
    luasnip.source_definitions.cmp_luasnip,
    luasnip.source_definitions.cmp_luasnip_choice,
  },

  { cmp_buffer_text.source_definition }
}

local markdown_sources = table_util.deep_copy(base_sources)
table.insert(markdown_sources[1], cmp_latex.symbols_source_definition)

local latex_sources = table_util.deep_copy(base_sources)
table.insert(latex_sources[1], cmp_latex.symbols_source_definition)

require('user.features.completion.cmp').setup({
  snippet_engine = luasnip.expand,

  sources = {
    default = base_sources,

    search = { {
      cmp_lsp.source_definition,
      cmp_buffer_text.source_definition,
    } },

    by_filetype = {
      quarto = markdown_sources,
      markdown = markdown_sources,
      latex = latex_sources,
    }
  }
})

luasnip.setup({
  show_node_indicator = { enable = true }
})
cmp_buffer_text.setup()
cmp_lsp.setup()
cmp_path.setup()
cmp_lsp_signature_help.setup()
-- cmp_dap.setup()
cmp_emmet.setup()
cmp_latex.setup()

---------
-- Git --
---------

require('user.features.git').setup()

---------------
-- Save/quit --
---------------

require('user.features.save_quit.write').setup()
require('user.features.save_quit.quit_normal').setup()

-----------
-- Notes --
-----------

require('user.features.notes.mkdnflow').setup()

--------------------
-- Plugin manager --
--------------------

require('user.features.plugin_manager').setup()
local lazy = require('user.util.plugins.lazy')
lazy.bootstrap()
lazy.setup()

------------------
-- Key mappings --
------------------

require('user.windows').setup()

-- This has to go after lazy is initialized, otherwise
-- folds will not be added when directly opening a file.
require('user.features.ui.treesitter_folds').setup()
require('user.features.ui.fold_text').setup()
