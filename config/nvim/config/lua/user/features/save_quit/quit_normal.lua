local keymap = require('user.util.keymap')

local quit_normal = {
  name = 'Quit (normal)',

  description = [[
  This feature adds easier keymaps for exiting the editor.

  This version is intended for long editing sessions where
  you likely want to be more careful about unsaved changes.
  ]],

  keymap = keymap {
    { 'n', '<space>q', '<cmd>q<cr>', { desc = 'Exit editor' } },
  }
}

function quit_normal.setup()
  quit_normal.keymap.apply()
end

return quit_normal
