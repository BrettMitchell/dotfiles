local make_feature = require('user.util.feature').make_feature
local table_util = require('user.util.table')
-- local make_feature = require('user.util.feature').make_feature_v2

local config = {}

local function setup()
  local gl = require('galaxyline')

  -- Use single global status line rather than per-window status line
  vim.opt.laststatus = 2

  -- GalaxyLine API modules
  local buffer = require("galaxyline.provider_buffer")
  local fileinfo = require("galaxyline.provider_fileinfo")
  local vcs = require("galaxyline.provider_vcs")
  local condition = require('galaxyline.condition')

  local function can_describe_file()
    local f_type = vim.bo.filetype
    if not f_type or f_type == '' or f_type == 'alpha' or f_type == 'packer' then
      return false
    end
    return true
  end

  local function is_mode(modes)
    return table_util.includes(vim.fn.mode(), modes)
  end

  local function ft_is(ft_to_check)
    return function()
      return vim.bo.filetype == ft_to_check
    end
  end

  local function AND(...)
    local fns = { ... }

    return function()
      local result = true
      for _, fn in ipairs(fns) do
        result = result and fn()
      end
      return result
    end
  end

  local function NOT(fn)
    return function() return not fn() end
  end

  local has_ft = AND(NOT(ft_is(nil)), NOT(ft_is('')))

  -- Show project info on the left
  gl.section.left = {
    { Branch = {
      provider = function (...)
        local branch = vcs.get_git_branch(...)
        if not branch then
          return '  NOT TRACKED '
        end
        return '  ' .. branch .. ' '
      end,
      condition = AND(
        NOT(ft_is('term-filebrowser-lf'))
      ),
      highlight = 'GalaxyLineBranch',
    } },

    { FileName = {
      provider = function (...)
        return ' ' .. fileinfo.get_current_file_path(...)
      end,

      condition = AND(
        -- condition.buffer_not_empty,
        -- has_ft,
        -- can_describe_file,
        NOT(ft_is('alpha')),
        NOT(ft_is('packer')),
        NOT(ft_is('term-filebrowser-lf'))
      ),

      highlight = 'GalaxyLineFileName'
    } },

    { HighlightReset = {
      provider = function () return '' end,
      highlight = 'GalaxyLineHighlightReset'
    } }
  }

  -- Show file info on the right
  gl.section.right = {
    { FileType = {
      provider = function (...)
        return buffer.get_buffer_filetype(...)
      end,

      separator = ' ',
      -- separator = '',

      condition = AND(
        -- condition.buffer_not_empty,
        -- has_ft,
        -- can_describe_file,
        NOT(ft_is('alpha')),
        NOT(ft_is('packer')),
        NOT(ft_is('term-filebrowser-lf'))
      ),

      separator_highlight = 'GalaxyLineSeparatorHighlight',
      highlight = 'GalaxyLineFileType',
    } },

    { ViMode = {
      provider = function()
        local alias = {
          n = 'NORMAL',
          niI = 'NORMAL iI',
          niR = 'NORMAL iR',
          niV = 'NORMAL iV',

          nt = 'NORMAL TERM',
          ntT = 'NORMAL TERM T',

          no = 'OP PENDING',
          nov = 'OP PENDING v',
          noV = 'OP PENDING V',
          ['no^V'] = 'OP PENDING Ctrl-V',

          R = 'REPLACE',
          Rc = 'REPLACE c',
          Rx = 'REPLACE x',
          Rv = 'REPLACE v',
          Rvc = 'REPLACE vc',
          Rvx = 'REPLACE vx',

          i = 'INSERT',
          ic = 'INSERT c',
          ix = 'INSERT x',

          c = 'COMMAND',
          cv = 'EX',

          v = 'VISUAL',
          vs = 'VISUAL s',
          V = 'VISUAL LINE',
          Vs = 'VISUAL LINE s',
          [''] = 'VISUAL BLOCK',
          ['s'] = 'VISUAL BLOCK s',

          s = 'SELECT',
          S = 'SELECT LINE',
          ['^S'] = 'SELECT BLOCK',

          r = 'INPUT (HIT ENTER)',
          rm = 'INPUT (MORE)',
          ['r?'] = 'INPUT (CONFIRM)',

          ['!'] = 'EXTERNAL',

          t = 'TERMINAL'
        }

        local display = alias[vim.fn.mode()]
        if not display then return ' ' .. vim.fn.mode() .. ' ' end
        return ' ' .. display .. ' '
      end,

      separator = ' ',
      separator_highlight = 'GalaxyLineSeparatorHighlight',
      highlight = 'GalaxyLineViMode',
    } },

    { ScrollPercentage = {
      provider = function (...)
        if not is_mode({ 'v', 'vs', 'V', 'Vs', '', 's' }) then
          local pct = fileinfo.current_line_percent(...)
          -- Ensure the scroll percentage text is always a constant width
          local withZeroPad = pct:gsub('^(%s*)(%d%%)', '%10%2')
          return withZeroPad
        end

        local selection = _G.getVisualSelection(false)
        local total = 0
        if type(selection) == 'table' then
          for _, line in ipairs(selection) do
            total = total + #line
          end
        end
        return ' ' .. total .. 'c, ' .. #selection .. 'l '
      end,

      event = 'CursorMoved',

      condition = AND(
        -- condition.buffer_not_empty,
        -- has_ft,
        -- can_describe_file,
        NOT(ft_is('alpha')),
        NOT(ft_is('packer')),
        NOT(ft_is('term-filebrowser-lf'))
      ),

      separator = ' ',
      separator_highlight = 'GalaxyLineSeparatorHighlight',
      highlight = 'GalaxyLineScrollPercentage',
    } },
  }

  vim.cmd("autocmd CursorMoved * lua require('galaxyline').load_galaxyline()")
end

-- Sources:
--  - https://zignar.net/2022/01/21/a-boring-statusline-for-neovim/

local function make_part(fn_name)
  return "%<%{luaeval(\"require('user.features.ui.statusline')." .. fn_name .. "()\")}%="
end

local M = make_feature({
  name = 'Status line',

  description = [[
  This feature provides a pre-configured status line. Because of this,
  it has been curated for PaxVim compatability, but it is also tailored
  to my personal taste.
  ]],

  -- make_part = make_part,
  --
  -- dap = function ()
  -- end,
  --
  -- filename = function ()
  --   return '%f'
  -- end,
  --
  -- filetype = function ()
  -- end,
  --
  -- vi_mode = function ()
  -- end,
  --
  -- cursor_loc = function ()
  -- end,
  --
  -- git_branch = function ()
  -- end,
  --
  -- render = function()
  --   local parts = {
  --     -- make_part('dap'),
  --     '%f',
  --     make_part('filetype'),
  --     -- make_part('vi_mode'),
  --     -- make_part('cursor_loc'),
  --     -- make_part('git_branch'),
  --   }
  --
  --   return table.concat(parts)
  -- end,
  --
  -- apply = function()
  --   vim.opt.statusline = "%!v:lua.require'user.features.ui.statusline'.render()"
  -- end

  plugins = {
    {
      'https://github.com/glepnir/galaxyline.nvim',
      requires = { 'kyazdani42/nvim-web-devicons', opt = false },
      config = setup
    }
  },

  config = config,
})

return M
