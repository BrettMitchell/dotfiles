local lazy = require('user.util.plugins.lazy')

local M = {
  name = 'Yank Highlighting',

  description = [[
  This feature briefly hightlights the targeted region on yank.

  Use the `duration` property to control how many milliseconds
  the highlight will last for.

  Use the `show_in_visual_mode` property to control whether or
  not the highlight should show up in visual mode.
  ]],

  duration = 100,
  show_in_visual_mode = true,
}

function M.setup()
  lazy.add_plugin({
    'https://github.com/machakann/vim-highlightedyank',
  })

  if type(M.duration) == 'number' then
    vim.g.highlightedyank_highlight_duration = M.duration
  end

  if type(M.show_in_visual_mode) == 'number' then
    vim.g.highlightedyank_highlight_in_visual = M.show_in_visual_mode
  end
end

return M
