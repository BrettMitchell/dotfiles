#!/usr/bin/env bash

ln_safe "$module/waybar" "$HOME/.config/waybar"
ln_safe "$module/sway/config" "$HOME/.config/sway/config"
