#!/usr/bin/env bash

if ! command -v go > /dev/null ; then
  printf '%s\n' 'golang is not installed' > /dev/stderr
  exit
fi

pushd "$module/neovim-remote-go/" > /dev/null
go build ./cmd/neovim-remote
ln_safe `pwd`/neovim-remote "$HOME/.local/bin/nvr"
popd > /dev/null

