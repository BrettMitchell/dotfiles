local make_feature = require('pax-vim.util.feature').make_feature_v2

--[[

TODO:

x Register custom command 'PVTmuxMain':
  x This will register itself to the NVIM_SESSION_CWD and NVIM_SESSION_SERVER_ID session variables
- Stop making self main on init
- To make nvim main, either call 'PVim tmux_main' after starting, or use the '-c' flag when starting
  to do it automatically from a tmux init script (i.e. `nvim ./ -c 'PVTmuxMain'`)

--]]

local function run(command)
  local commandHandle = io.popen(command)
  if not commandHandle then
    error('Could not execute command: ' .. command)
  end
end

local M = make_feature({
  name = 'Server',

  description = [[
  This feature starts the neovim server with a known file name and then
  makes that filename available to other modules through a method.
  ]],

  get_server = function(self)
    return self.server_name
  end,

  apply = function(self)
    local existing_servers = vim.fn.serverlist()
    if #existing_servers > 0 then
      self.server_name = existing_servers[1]
    else
      self.server_name = vim.fn.serverstart()
    end

    local function set_session_vars()
      run('u-set-session-variable NVIM_SESSION_SERVER_ID ' .. self.server_name)
      run('u-set-session-variable NVIM_SESSION_CWD ' .. vim.fn.getcwd())
    end

    vim.api.nvim_create_user_command(
      'PVTmuxMain',
      set_session_vars,
      {}
    )
  end
})

return M
