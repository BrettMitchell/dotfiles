local previewers = require('telescope.previewers')
local pickers = require('telescope.pickers')
local sorters = require('telescope.sorters')
local finders = require('telescope.finders')

local theme = require('user.telescope.theme')
local utils = require('user.utils')

local M = {}

function M.searchNotes()
  vim.notify('This is still a WIP')

  local noteFiles = utils.get_os_command_output({ 'fd', '--search-path=' .. vim.fn.getenv('NOTES_DIR'), '\\.(norg|md)$' })
  local formattedRows = {}

  for _, noteFile in ipairs(noteFiles) do
    local withoutPrefix = noteFile:gsub(
      utils.escapePattern(vim.fn.getenv('NOTES_DIR')),
      ''
    )

    -- local withoutNorgExt = withoutPrefix:gsub('%.norg$', '')
    -- local withoutMdExt = withoutNorgExt:gsub('%.qmd$', '')

    local withoutLeadingSlash = withoutPrefix:gsub('^/', '')

    table.insert(formattedRows, withoutLeadingSlash)
  end

  -- Use keys to store entries to keep unique
  local allEntriesMap = {}

  for _, formattedRow in ipairs(formattedRows) do
    local acc = ''
    for segment in string.gmatch(formattedRow, '([^/]+)/') do
      if acc == '' then acc = segment
      else acc = acc .. '/' .. segment end
      allEntriesMap[acc] = acc
    end

    allEntriesMap[formattedRow] = formattedRow
  end

  local searchItems = {}
  for item in pairs(allEntriesMap) do
    table.insert(searchItems, item)
  end

  local picker = pickers.new {
    results_title = 'Notes',
    finder = finders.new_table(searchItems),
    sorter = sorters.get_fuzzy_file(),
    previewer = previewers.new_buffer_previewer {
      define_preview = function(self, entry, status)
        local restoredFilename = vim.fn.getenv('NOTES_DIR') .. '/' .. entry.value
        return require('telescope.previewers.utils').job_maker(
          { 'cat', restoredFilename },
          self.state.bufnr
        )
      end
    },
  }

  picker:find(theme.horizontal)
end

return M
