local add_plugins = require('user.util.plugins.lazy').add_plugins

local M = {
  feature = {
    name = 'Seasonal Decorations',

    description = [[
    A silly feature adding seasonal decorations to the dashboard based
    on the current date.
    ]],
  }
}

function M.setup()
  add_plugins({ {
    'https://github.com/folke/drop.nvim',
    config = function ()
      local day = tonumber(os.date('%d'))
      local month = tonumber(os.date('%m'))
      local theme = nil
      if (month == 9 and day >= 23) or (month < 12) then
        theme = 'leaves'
      elseif (month == 12) then
        theme = 'xmas'
      end

      if theme then
        require('drop').setup({ theme = theme })
      end
    end
  } })
end

return M
