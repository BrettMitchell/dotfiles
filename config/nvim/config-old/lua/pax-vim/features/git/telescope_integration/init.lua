local make_feature = require('pax-vim.util.feature').make_feature
local git = require('pax-vim.features.git.git')
local telescope = require('pax-vim.features.telescope')

local config = {
  use_delta_diff_viewer = false,
}

local M = make_feature({
  name = 'Telescope Git Integration',

  description = [[
  This feature adds telescope pickers for interacting with git.
  ]],

  dependencies = { git, telescope },

  config = config,

  methods = {
    -- pickBranch = pickBranch,
    --
    -- pickCommit = pickCommit,
    -- pickBufferCommit = pickBufferCommit,
    -- pickGitChanges = pickGitChanges,
    --
    -- pickCommitDelta = pickCommitDelta,
    -- pickBufferCommitDelta = pickBufferCommitDelta,
    -- pickGitChangesDelta = pickGitChangesDelta,
  },

  keymaps = {
    -- { 'n', '<space>gg', '<cmd>Git<cr>', { desc = 'Open Fugitive equivalent of git status' } },
    { '<space>tc', function() telescope.gitHistory() end, { desc = 'Search through commits in the current project' } },
    { '<space>tC', function() telescope.currentFileGitHistory() end, { desc = 'Search through commits made against the current file' } },
    { '<space>tb', function() telescope.gitBranches() end, { desc = 'Search through commits made against the current file' } },
    { '<space>ts', function() telescope.gitStatus() end, { desc = 'Search through active git changes' } },
  }
})

return M

