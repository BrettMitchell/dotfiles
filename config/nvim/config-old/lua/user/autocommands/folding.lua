local autocmd = require('user.lib.autocmd').autocmd

local M = {}

function M.setup()
  -- Enable expr mode fold config support in tree sitter
  local function foldConfig()
    -- Don't set fold method for buffers managed by True Zen's narrow mode
    if vim.b.tz_narrowed_buffer == 'true' then return end

    vim.cmd [[ setlocal foldmethod=expr ]]
    vim.cmd [[ setlocal foldexpr=nvim_treesitter#foldexpr() ]]
    vim.cmd [[ silent! :%foldopen! ]]
  end

  autocmd(
    'TreeSitterExprFolding',
    {
      clear = true,
      { 'BufAdd', 'BufEnter', 'BufNew', 'BufNewFile', 'BufWinEnter', {
        callback = foldConfig,
      } }
    }
  )
end

return M
