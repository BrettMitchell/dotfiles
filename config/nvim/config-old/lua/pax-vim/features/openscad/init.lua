local make_feature = require('pax-vim.util.feature').make_feature_v2
local system_binary = require('pax-vim.util.feature.system_binary_dependency').system_binary
local make_augroup = require('pax-vim.util.autocmd').make_augroup
local make_keymap = require('pax-vim.util.keymap')

local openscad = make_feature({
  name = 'OpenSCAD',

  description = [[
  This feature opens '.scad' files in OpenSCAD automatically.

  It also declares system dependencies on the 'openscad' binary and
  'clang-format' for openscad-lsp (install through Mason).

  This avoids closing and reopening OpenSCAD by sending the current
  buffer contents to a temporary file on save, and watching that
  rather than the currently focused buffer file directly.
  ]],

  dependencies = {
    system_binary('openscad'),
    system_binary('clang-format'),
  },

  tmp_filename = vim.fn.tempname() .. '.scad',
  preview_enabled = true,
  openscad_job = nil,

  preview_openscad_file = function (self, filename)
    if self.openscad_job then
      return
    end

    local cmd = 'openscad ' .. filename
    local ok, res = pcall(vim.fn.jobstart, cmd, { on_exit = function () self.openscad_job = nil end })

    if not ok then
      vim.notify('Error while starting OpenSCAD: ' .. vim.inspect(res))
    else
      self.openscad_job = res
    end
  end,

  stop_openscad_preview = function (self)
    if self.openscad_job then
      vim.fn.jobstop(self.openscad_job)
      self.openscad_job = nil
    end
  end,

  set_preview = function (self, preview_enabled)
    self.preview_enabled = preview_enabled
    if self.preview_enabled then
      self:preview_openscad_file(self.tmp_filename)
    else
      self:stop_openscad_preview()
    end
  end,

  toggle_preview = function (self)
    self.preview_enabled = not self.preview_enabled
    if self.preview_enabled then
      self:preview_openscad_file(self.tmp_filename)
    else
      self:stop_openscad_preview()
    end
  end,

  init = function(self)
    self.keymaps = make_keymap {
      { 'n', '<space>ups', function ()
        vim.notify('Toggled OpenSCAD preview', 'info')
        self:toggle_preview()
      end, { desc = 'Toggle OpenSCAD auto-preview' } }
    }

    self.augroups = make_augroup {
      {
        name = 'OpenSCADPreviewTempFileWrite',
        autocmds = { {
          events = { 'BufWritePost', 'BufEnter' },
          opts = {
            callback = function ()
              if vim.bo.filetype == 'openscad' then
                local filename = vim.fn.expand('%:p')

                local read_ok, read_res = pcall(vim.fn.readfile, filename)
                if not read_ok then vim.notify(read_res) end

                local write_ok, write_res = pcall(vim.fn.writefile, read_res, self.tmp_filename)
                if not write_ok then vim.notify(write_res) end
              end
            end
          }
        } }
      },

      {
        name = 'OpenSCADAutoPreview',
        autocmds = { {
          events = 'BufEnter',
          opts = {
            callback = function ()
              if self.preview_enabled and vim.bo.filetype == 'openscad' then
                self:preview_openscad_file(self.tmp_filename)
              end
            end
          }
        } }
      }
    }
  end,

  apply = function(self)
    self.keymaps.apply()
    self.augroups:apply()
  end
})

return openscad
