-- NOTE: NAIVE implementation. Does not handle cyclic references
local function deep_copy(tbl)
  local res = {}

  for i, v in ipairs(tbl) do
    if type(v) == 'table' then
      res[i] = deep_copy(v)
    else
      res[i] = v
    end
  end

  for k, v in pairs(tbl) do
    if type(v) == 'table' then
      res[k] = deep_copy(v)
    else
      res[k] = v
    end
  end

  return res
end

return deep_copy
