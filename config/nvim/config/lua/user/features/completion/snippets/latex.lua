local luasnip_feature = require('user.features.completion.snippets.luasnip')

local M = {
  name = 'Snippets - LaTeX',
  description = [[
  This feature adds snippets for LaTeX mathematics symbols.
  ]]
}

function M.make_snippets()
  local ls = require('luasnip')

  -- local events = require("luasnip.util.events")
  -- local ai = require("luasnip.nodes.absolute_indexer")
  -- local extras = require("luasnip.extras")
  -- local fmt = require("luasnip.extras.fmt").fmt
  -- local fmta = require("luasnip.extras.fmt").fmta
  -- local conds = require("luasnip.extras.expand_conditions")
  -- local postfix = require("luasnip.extras.postfix").postfix
  -- local types = require("luasnip.util.types")
  -- local parse = require("luasnip.util.parser").parse_snippet
  -- local k = require("luasnip.nodes.key_indexer").new_key

  local s = ls.snippet
  -- local sn = ls.snippet_node
  -- local isn = ls.indent_snippet_node
  local t = ls.text_node
  local i = ls.insert_node
  -- local f = ls.function_node
  -- local c = ls.choice_node
  -- local d = ls.dynamic_node
  -- local r = ls.restore_node
  -- local ms = ls.multi_snippet

  -- local l = extras.lambda
  -- local rep = extras.rep
  -- local p = extras.partial
  -- local m = extras.match
  -- local n = extras.nonempty
  -- local dl = extras.dynamic_lambda

  local snippets = {}

  snippets.math = s('math context', { t "$$ ", i(0), t " $$" })
  snippets.math_multiline = s('math context multiline', {
    t({
      "$$",
      "\\begin{align}",
    }),
    i(0),
    t({
      "\\end{align}",
      "$$",
    }),
  })

  snippets.indefinite_integral = s('indefinite integral', { t "\\int{", i(0), t "}" })
  snippets.definite_integral = s('definite integral', {
    t "\\int_", i(1), t "^", i(2), t "{", i(0), t "}"
  })

  return snippets
end

function M.setup()
  luasnip_feature.add_snippet_builder(function()
    local snippets = M.make_snippets()
    local ls = require('luasnip')

    -- For now, all snippets are supported everywhere
    -- This pattern is used to make it easy to switch that up
    -- if needed.
    local markdown_snippets = {}
    for _, snippet in pairs(snippets) do
      table.insert(markdown_snippets, snippet)
    end

    ls.add_snippets('markdown', markdown_snippets)
    ls.add_snippets('quarto', markdown_snippets)
    ls.add_snippets('latex', markdown_snippets)
  end)
end

return M
