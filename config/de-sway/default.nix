{ nixpkgs, ... }:
{
  home.file.".config/waybar/config.jsonc".source = ./waybar/config.jsonc;
  home.file.".config/waybar/shared".source = ./waybar/shared;
  home.file.".config/waybar/style.css".source = ./waybar/style.css;
  home.file.".config/sway/config".source = ./sway/config;
  # TODO: Parameterize this by hostname
  home.file.".config/shikane/config.toml".source = ./shikane/t490.config.toml;
  home.file.".local/bin/sway-maintain-prev-mark".source = ./sway/sway-maintain-prev-mark;

  home.file.".icons/default".source = "${nixpkgs.unstable.openzone-cursors}/share/icons/OpenZone_Black_Slim";

  home.packages = with nixpkgs.stable; [
    nixpkgs.unstable.swayfx
    nixpkgs.unstable.shikane
    nixpkgs.unstable.sway-easyfocus # https://github.com/edzdez/sway-easyfocus
    nixpkgs.unstable.sway-overfocus # https://github.com/korreman/sway-overfocus
    nixpkgs.unstable.swaysome # https://gitlab.com/hyask/swaysome
    nixpkgs.unstable.waybar
    nixpkgs.unstable.swaylock
    nixpkgs.unstable.wl-clipboard
    nixpkgs.unstable.wev
    nixpkgs.unstable.dotool
    nixpkgs.unstable.wshowkeys
    nixpkgs.unstable.dunst
    nixpkgs.unstable.openzone-cursors
    flameshot
    hyprpicker
    bemenu
    pinentry-bemenu
  ];
}
