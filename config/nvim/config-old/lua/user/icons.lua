local M = {
  nvimTree = {
    symlink_arrow = ' -> ',

    items = {
    },

    folder = {
      closed = '',
      open = '',
    },

    git = {
      unstaged = '*',
      staged = '',
      unmerged = '',
      renamed = '',
      untracked = '?',
      deleted = '',
      ignored = '',
    },
  },
}

return M

