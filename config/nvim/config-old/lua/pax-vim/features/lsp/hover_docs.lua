local make_feature = require('pax-vim.util.feature').make_feature
local keymap = require('pax-vim.util.keymap')
local lsp = require('pax-vim.features.lsp')
local lspsaga = require('pax-vim.features.lsp.lspsaga')
local treesitter = require('pax-vim.features.treesitter')

local methods = {
  hover_doc = function()
    require('lspsaga.hover'):render_hover_doc()
  end
}

local hover_doc_keymap = keymap {
  hover_doc = { { 'n', 'v' }, '<space>li', vim.lsp.buf.hover, { desc = 'Show documentation overlay' } },
}

local M = make_feature({
  name = 'LSP :: Hover documentation',

  dependencies = { lsp, lspsaga, treesitter },

  keymaps = hover_doc_keymap,
  setup_seq = { keymaps = false },

  description = [[
  This feature adds a floating window displaying documentation
  ]],

  plugins = {
    { 'https://github.com/Issafalcon/lsp-overloads.nvim' }
  },

  on_activate = function()
    lsp.methods.add_hook({
      hook_name = 'on_attach',
      callback = function(client, bufnr)
        hover_doc_keymap.apply(bufnr)

        --- Guard against servers without the signatureHelper capability
        if client.server_capabilities.signatureHelpProvider then
          require('lsp-overloads').setup(client, {
            -- UI options are mostly the same as those passed to vim.lsp.util.open_floating_preview
            ui = {
              border = "single",
              wrap = true,      -- Wrap long lines
              -- Events that will close the signature popup window: use {"CursorMoved", "CursorMovedI", "InsertCharPre"} to hide the window when typing
              close_events = { "CursorMoved", "BufHidden", "InsertLeave" },
              focusable = true,                       -- Make the popup float focusable
              offset_x = 0,                           -- Horizontal offset of the floating window relative to the cursor position
              offset_y = 0,                           -- Vertical offset of the floating window relative to the cursor position
              floating_window_above_cur_line = false, -- Attempt to float the popup above the cursor position
              silent = true -- Prevents noisy notifications (make false to help debug why signature isn't working)
            },

            keymaps = {
              next_signature = "<C-S-j>",
              previous_signature = "<C-S-k>",
              next_parameter = "<C-S-l>",
              previous_parameter = "<C-S-h>",
              close_signature = "<ESC>"
            },

            display_automatically = false -- Uses trigger characters to automatically display the signature overloads when typing a method signature
          })
        end
      end,
    })
  end
})

M.methods = methods

return M
