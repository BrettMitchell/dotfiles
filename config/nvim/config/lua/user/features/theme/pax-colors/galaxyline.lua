local hl = require('user.util.highlight')

return function(theme)
  local text = theme.color.foreground1
  local bg = theme.color.background2

  hl('GalaxyLineSeparatorHighlight', {
    fg = text,
    bg = bg,
  })

  hl('GalaxyLineHighlightReset', {
    fg = text,
    bg = bg,
  })

  hl('GalaxyLineFileName', {
    fg = theme.color.accent1,
    bg = bg,
  })

  hl('GalaxyLineFileType', {
    fg = theme.color.accent1,
    bg = bg,
  })

  hl('GalaxyLineViMode', {
    fg = bg,
    bg = theme.color.foreground2,
  })

  hl('GalaxyLineBranch', {
    fg = bg,
    bg = theme.color.foreground2,
  })

  hl('GalaxyLineScrollPercentage', {
    fg = bg,
    bg = theme.color.foreground2,
  })
end
