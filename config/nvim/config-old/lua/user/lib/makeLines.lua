local M = {}

function M.makeLines(text)
  local lines = {}

  if (vim.tbl_islist(text)) then

    for _, elem in ipairs(text) do
      local subLines = M.makeLines(elem)
      for _, line in ipairs(subLines) do
        table.insert(lines, line)
      end
    end

  else
    if type(text) ~= 'string' then
      text = vim.inspect(text)
    end

    for line in text:gmatch('([^\n]+)') do
      table.insert(lines, line)
    end
  end

  return lines
end

return M
