local table_util = require('user.util.table')

------------------
-- Base options --
------------------

require('user.setopts')
-- vim.opt.cmdheight = 0
vim.opt.readonly = true
vim.o.modifiable = false

------------
-- Server --
------------

require('user.features.startup.server').setup()

----------------------
-- Global functions --
----------------------

require('user.global_functions')

--------------
-- LuaRocks --
--------------

local home = vim.fn.getenv('HOME')
require('user.features.luarocks').setup({
  luarocks_install_path = function()
    return {
      home .. '/.luarocks/share/lua/5.1',
      -- This is here to get notes working
      -- This will go away once I create a neovim flake
      -- with this package in it.
      -- It is intentionally fragile so that I have to
      -- continuously fix it, and so that I am incentivized
      -- to remove this horrible hack.
      '/nix/store/rx9pr0bcpjzgknzbnh008lkji6mp97za-luajit2.1-lyaml-6.2.8-1/share/lua/5.1'
    }
  end,
  luarocks_install_cpath = function()
    return {
      home .. '/.luarocks/lib/luarocks/rocks-5.1',
      '/nix/store/rx9pr0bcpjzgknzbnh008lkji6mp97za-luajit2.1-lyaml-6.2.8-1/lib/lua/5.1'
    }
  end,
  quiet = true,
  ensure_installed = {
    'lua-path',
    -- 'lyaml',
    'lunajson',
    -- TODO: Re-enable this when associated issue is resolved in either mkdnflow or which-key
    -- 'luautf8',
  }
})

-----------------------------
-- Multiplexer integration --
-----------------------------

local open_external = require('user.features.mux.open_external')
open_external.open_keymap = nil
open_external.setup()
require('user.features.mux.reflist').setup()

--------------
-- Quickfix --
--------------

require('user.features.quickfix.qf').setup()
require('user.features.quickfix.bqf').setup()
local qf_keybinds = require('user.features.quickfix.quickfix_keymaps')
qf_keybinds.toggle_keymaps = nil
qf_keybinds.setup()

---------------
-- Base deps --
---------------

require('user.features.util.plenary').setup()

----------------
-- Treesitter --
----------------

require('user.features.treesitter').setup()
require('user.features.treesitter.text_objects').setup()

--------
-- UI --
--------

-- require('user.features.theme.pywal').setup()
require('user.features.theme.pax-colors').setup()

require('user.features.ui.toggle_wrap').setup()
require('user.features.ui.highlightedyank').setup()
require('user.features.ui.folding').setup()

---------
-- Nav --
---------

-- This nvim configuration uses telescope internally, but does not
-- support the standard keymaps
local telescope = require('user.features.telescope')
-- TODO: Once the 'make_feature' goofiness is removed, just set this to nil
telescope.keymaps = { apply = function () end }
telescope.setup()

require('user.features.nav.base').setup()
require('user.features.nav.clever_f').setup()
require('user.features.nav.flash').setup()
require('user.features.nav.word_motions').setup()

----------
-- Edit --
----------

require('user.features.edit.system_clipboard').setup()
require('user.features.edit.yank_file_paths').setup()

---------------
-- Save/quit --
---------------

require('user.features.save_quit.write').setup()
require('user.features.save_quit.quit_immediate').setup()

--------------------
-- Plugin manager --
--------------------

require('user.features.plugin_manager').setup()
local lazy = require('user.util.plugins.lazy')
lazy.bootstrap()
lazy.setup()

------------------
-- Key mappings --
------------------

require('user.windows').setup()

-- This has to go after lazy is initialized, otherwise
-- folds will not be added when directly opening a file.
require('user.features.ui.treesitter_folds').setup()
require('user.features.ui.fold_text').setup()
