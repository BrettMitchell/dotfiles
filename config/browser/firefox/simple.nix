{ nixpkgs, ... }:

let
  lock = value: { Value = value; Status = "locked"; };

  rycee-addons = nixpkgs.nur.repos.rycee.firefox-addons;
  buildFirefoxXpiAddon = rycee-addons.buildFirefoxXpiAddon;

  lastpass = buildFirefoxXpiAddon {
    pname = "lastpass-password-manager";
    addonId = "support@lastpass.com";
    version = "4.127.0.1";
    url = "https://addons.mozilla.org/firefox/downloads/file/4246455/lastpass_password_manager-4.127.0.1.xpi";
    sha256 = "225c323fbd862a904a7c1b364a1f5beca792f8a5fa1deca0e9298b0cad7721bf";
    meta = {};
  };

  clearurls = buildFirefoxXpiAddon {
    pname = "clearurls";
    addonId = "{74145f27-f039-47ce-a470-a662b129930a}";
    version = "1.26.1";
    url = "https://addons.mozilla.org/firefox/downloads/file/4064884/clearurls-1.26.1.xpi";
    sha256 = "e20168d63cb1b8ba3ad0de4cdb42c540d99fe00aa9679b59f49bccc36f106291";
    meta = {};
  };

  canvasblocker = buildFirefoxXpiAddon {
    pname = "canvasblocker";
    addonId = "CanvasBlocker@kkapsner.de";
    version = "1.10.1";
    url = "https://addons.mozilla.org/firefox/downloads/file/4262820/canvasblocker-1.10.1.xpi";
    sha256 = "dae3b648f0b559b8b08cdad8adaaba2fcde3aa7baf0ffe9b2cbca5a3373c98b7";
    meta = {};
  };

  decentraleyes = buildFirefoxXpiAddon {
    pname = "decentraleyes";
    addonId = "jid1-BoFifL9Vbdl2zQ@jetpack";
    version = "2.0.19";
    url = "https://addons.mozilla.org/firefox/downloads/file/4255788/decentraleyes-2.0.19.xpi";
    sha256 = "105d65bf8189d527251647d0452715c5725af6065fba67cd08187190aae4a98f";
    meta = {};
  };
in {
  home.file.".config/tridactyl/tridactylrc".source = ./tridactylrc;

  programs.firefox = {
    enable = true;

    package = nixpkgs.unstable.firefox.override {
      nativeMessagingHosts = [
        # Tridactyl native connector
        nixpkgs.stable.tridactyl-native
      ];
    };

    profiles.simple = {
      isDefault = true;

      extensions = [
        lastpass
        rycee-addons.ublock-origin
        clearurls
        canvasblocker
        decentraleyes
      ];
    };

    policies = {
      Preferences = { 
        "browser.contentblocking.category" = lock "strict";
        "extensions.pocket.enabled" = lock false;
        "extensions.screenshots.disabled" = lock true;

        "browser.search.suggest.enabled" = lock false;
        "browser.search.suggest.enabled.private" = lock false;
        "browser.urlbar.suggest.searches" = lock false;
        "browser.urlbar.showSearchSuggestionsFirst" = lock false;

        "signon.rememberSignons" = lock false;
        "browser.formfill.enable" = lock false;

        "privacy.resistFingerprinting" = lock true;
        "privacy.bounceTrackingProtection.enabled" = lock true;

        # Enable extensions on install
        "extensions.autoDisableScopes" = lock true;

        # TODO: Figure out why this doesn't actually apply when generating firefox config
        "identity.fxaccounts.enabled" = lock false;
        "identity.fxaccounts.toolbar.enabled" = lock false;
      };
    };
  };
}
