-- local labels = {
--   's', 'd', 'f', 'j', 'k', 'l',
--   'g', 'h', 'v', 'n', 'r', 'u',
--   'e', 'i', 'w', 'o', 'a', ';',
--   'c', 'm', 'x', ',', 'z', '.',
-- }

return {
  config = function()
    require('leap').setup {
      -- labels = labels,
      -- safe_labels = labels,

      equivalence_classes = {
        ' \t\r\n~!@#$%^&*+=',
        '({[<',
        ')}]>',
        '`"\'',
      },
    }
  end
}
