-- TODO: Add methods for managing conditional and dynamic config attributes
-- https://github.com/hrsh7th/nvim-cmp/wiki/Advanced-techniques#disable--enable-cmp-sources-only-on-certain-buffers

local feature = require('pax-vim.util.feature')
local make_feature = require('pax-vim.util.feature').make_feature_v2
local keymap = require('pax-vim.util.keymap')
local autocmd = require('pax-vim.util.autocmd')
local unpack = require('pax-vim.util.table.unpack')
local deep_copy = require('pax-vim.util.table.deep_copy')

local function get_snippet_expander(snippet_engine)
  if feature.is_feature(snippet_engine) then
    if type(snippet_engine.methods.expand) ~= 'function' then
      vim.notify(
        'Snippet engine feature ' ..
        snippet_engine.name .. ' does not provide cmp expansion handler (must define method.expand)', 'error')
      return
    end

    return snippet_engine.methods.expand
  elseif feature.is_feature_v2(snippet_engine) then
    if type(snippet_engine.expand) ~= 'function' then
      vim.notify(
        'Snippet engine feature ' ..
        snippet_engine.name .. ' does not provide cmp expansion handler (must define expand)',
        'error')
      return
    end

    return snippet_engine.expand
  elseif type(snippet_engine) == 'function' then
    return snippet_engine
  else
    vim.notify('Invalid snippet engine provided: ' .. vim.inspect(snippet_engine), 'error')
    return
  end
end

local cmp = make_feature({
  name = 'Completion :: cmp',

  -- TODO: Add some simple documentation here
  description = [[
  This feature adds completions with the 'cmp' plugin.

  Register desired sources under the `sources` config property.
  Register a snippet engine with the `snippet_engine` config property
  ]],

  plugins = {
    {
      'https://github.com/hrsh7th/nvim-cmp',
    },
  },

  disabled = false,
  snippet_engine = nil,
  sources = {
    default = {},
    cmdline = {},
    search = {},
  },

  toggle = function(self)
    self.disabled = not self.disabled
    if self.disabled then
      require('cmp').close()
      vim.notify('Completions disabled', 'info')
    else
      require('cmp').complete()
      vim.notify('Completions enabled', 'info')
    end
  end,

  apply = function(self)
    if not self.snippet_engine then
      vim.notify(
        'cmp requires a snippet engine. Set config.snippet_engine to a PaxVim snippet feature, or call cmp.methods.register_snippet_engine manually before cmp.setup.',
        'error')
      return
    end

    local _cmp = require('cmp')

    self.keymap = keymap {
      toggle = { { 'n', 'i', 'v', 'c' }, '<C-z>', function() self:toggle() end, { desc = 'Toggle completions' } },
    }

    -- Optional static plugin config
    self.cmp_config = {
      window = {
        completion = _cmp.config.window.bordered(),
        documentation = _cmp.config.window.bordered(),
      },

      mapping = _cmp.mapping.preset.insert {
        ['<C-j>'] = function(fallback)
          if _cmp.visible() then
            _cmp.select_next_item {}
          else
            fallback()
          end
        end,

        ['<C-k>'] = function(fallback)
          if _cmp.visible() then
            _cmp.select_prev_item {}
          else
            fallback()
          end
        end,

        ['<C-S-j>'] = function(fallback)
          if _cmp.visible() then
            _cmp.select_next_item {}
            _cmp.select_next_item {}
            _cmp.select_next_item {}
            _cmp.select_next_item {}
            _cmp.select_next_item {}
          else
            fallback()
          end
        end,

        ['<C-S-k>'] = function(fallback)
          if _cmp.visible() then
            _cmp.select_prev_item {}
            _cmp.select_prev_item {}
            _cmp.select_prev_item {}
            _cmp.select_prev_item {}
            _cmp.select_prev_item {}
          else
            fallback()
          end
        end,

        ['<C-n>'] = function(fallback)
          if _cmp.visible() then
            _cmp.scroll_docs(5)
          else
            fallback()
          end
        end,

        ['<C-p>'] = function(fallback)
          if _cmp.visible() then
            _cmp.scroll_docs(-5)
          else
            fallback()
          end
        end,

        ['<C-S-n>'] = function(fallback)
          if _cmp.visible() then
            _cmp.scroll_docs(15)
          else
            fallback()
          end
        end,

        ['<C-S-p>'] = function(fallback)
          if _cmp.visible() then
            _cmp.scroll_docs(-15)
          else
            fallback()
          end
        end,

        ['<CR>'] = function(fallback)
          if _cmp.visible() and _cmp.get_active_entry() then
            _cmp.confirm({ behavior = _cmp.ConfirmBehavior.Replace, select = false })
          else
            fallback()
          end
        end,
      }
    }

    -- Generate initial config
    local full_config = vim.tbl_extend('force', self.cmp_config or {}, {})
    -- full_config.sources = build_sources(, _cmp)
    full_config.sources = _cmp.config.sources(unpack(deep_copy(self.sources.default)))

    if self.sources.by_filetype then
      for ft, sources in pairs(self.sources.by_filetype) do
        self.sources.by_filetype[ft] = {
          original = sources,
          parsed = _cmp.config.sources(unpack(deep_copy(sources)))
        }
      end

      autocmd.make_augroup {
        name = 'CmpSetFileTypeCompletionSources',
        autocmds = { {
          events = { 'FileType' },
          opts = {
            callback = function ()
              local bufFt = vim.bo.filetype
              for ft, sources in pairs(self.sources.by_filetype) do
                if ft == bufFt then
                  _cmp.setup.buffer {
                    sources = sources.parsed
                  }
                end
              end
            end
          }
        } }
      }:apply()
    end

    -- Register snippet engine
    full_config.snippet = full_config.snippet or {}
    full_config.snippet.expand = get_snippet_expander(self.snippet_engine)


    local cmp_dap_ok, cmp_dap = pcall(require, 'cmp_dap')

    -- Register enabled callback. This is used to toggle the feature
    local user_enabled = full_config.enabled
    full_config.enabled = function(...)
      if self.disabled then return false end

      local is_enabled_for_dap = true
      if cmp_dap_ok then
        is_enabled_for_dap =
            vim.api.nvim_buf_get_option(0, "buftype") ~= "prompt"
            or cmp_dap.is_dap_buffer()
      end

      if type(user_enabled) == 'function' then
        return user_enabled(...) or is_enabled_for_dap
      end

      return is_enabled_for_dap
    end

    _cmp.setup(full_config)

    if self.sources.cmdline then
      _cmp.setup.cmdline(':', {
        mappings = full_config.mappings,
        sources = _cmp.config.sources(unpack(deep_copy(self.sources.cmdline)))
      })
    end

    if self.sources.search then
      _cmp.setup.cmdline('/', {
        mappings = full_config.mappings,
        sources = _cmp.config.sources(unpack(deep_copy(self.sources.search)))
      })
    end

    if self.keymap then
      self.keymap.apply()
    end
  end
})

return cmp
