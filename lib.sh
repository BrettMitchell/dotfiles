#!/usr/bin/env bash

root="$(dirname "$0")"
export root="$(realpath "$root")"

function includes {
  for opt in $2; do
    if [[ "$opt" == "$1" ]]; then
      return 0
    fi
  done

  return 1
}

export -f includes

function ln_safe {
  local source="$1"
  local dest="$2"

  if [[ ! -e "$source" ]] ; then
    printf "Link source does not exist: $source\n"
    return 1
  fi

  if [[ -e "$dest" ]] || [[ -L "$dest" ]] ; then
    printf "Link destination already exists: $dest\n"
    return 1
  fi

  printf "Linking: $source -> $dest\n"

  mkdir -p "$(dirname "$dest")"
  ln -s "$source" "$dest"
}

function rm_safe {
  local target="$1"

  if [[ ! -e "$target" ]] && [[ ! -L "$target" ]] ; then
    printf "rm_safe: Remove target does not exist, nothing to do: $target\n"
    return 0
  fi

  if [[ ! -L "$target" ]] ; then
    printf "rm_safe: Removal target is not a link, refusing to continue: $target\n"
    return 1
  fi

  printf "Removing: $target\n"
  rm "$target"
}

function remove {
  local module_name="$1"
  local system_type="$2"
  local module_dir="$root/modules/$module_name"

  printf "\n> Removing module '$module_name'"
  printf "\n\n"

  if [[ -x "$module_dir/remove.$system_type.sh" ]] ; then
    export module="$module_dir"
    "$module_dir/remove.$system_type.sh"
  elif [[ -x "$module_dir/remove.sh" ]] ; then
    export module="$module_dir"
    "$module_dir/remove.sh"
  else
    printf "> No remove script found: system type = '$system_type'\n"
  fi

  printf "\nDONE\n"
}
export -f remove

