local keymap = require('user.util.keymap')

local quit_immediate = {
  name = 'Quit (immediate)',

  description = [[
  This feature adds easier keymaps for exiting the editor.

  This version is intended for use when nvim serves as a
  prompting application, where just one file will be edited
  and then the application will be closed.
  ]],

  keymap = keymap {
    { 'n', '<C-c>', '<cmd>qa!<cr>', { desc = 'Cancel without saving' } },
    { 'n', '<escape>', '<cmd>qa<cr>', { desc = 'Exit' } },
    { 'n', '<space>q', '<cmd>qa!<cr>', { desc = 'Cancel without saving' } },
    { 'n', '<space>x', '<cmd>wqa<cr>', { desc = 'Write and exit' } },
  }
}

function quit_immediate.setup()
  quit_immediate.keymap.apply()
end

return quit_immediate

