return function(factor, color)
  if type(factor) ~= 'number' or factor < 0 or factor > 1 then
    error('darken requires a factor between 0 and 1')
  end

  if type(color) ~= 'table' then
    error('darken requires a RGB color table')
  end

  local out = {}
  for i = 1, 3 do
    out[i] = (1 - factor) * color[i]
  end
  return out
end
