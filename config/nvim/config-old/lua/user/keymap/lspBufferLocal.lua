local registerBufferKeyMap = require('user.lib.map').registerBufferKeyMap

-- map('n', 'gd', vim.lsp.buf.definition, opts)
-- map('n', 'gD', vim.lsp.buf.declaration, opts)
-- map('n', 'gr', '<cmd>Trouble lsp_references<enter>', opts)
-- map('n', '<M-i>', vim.lsp.buf.hover, opts)
-- map('n', '<space>r', vim.lsp.buf.rename, opts)

local M = {}

local DIR_SPECS = {
  { dirKey = 'h', cmdPrefix = 'leftabove vsplit',  descSuffix = ', direction = left'  },
  { dirKey = 'j', cmdPrefix = 'rightbelow split',  descSuffix = ', direction = down'  },
  { dirKey = 'k', cmdPrefix = 'leftabove split',   descSuffix = ', direction = up'    },
  { dirKey = 'l', cmdPrefix = 'rightbelow vsplit', descSuffix = ', direction = right' },
}

local function splitDirectionalWithCommand (commonCmd, overallDesc)
  if type(commonCmd) ~= 'string' then
    vim.notify('splitDirectionalWithCommand : Command must be a string. Function commands are not supported')
    return {}
  end

  local res = {}
  for _, dirSpec in ipairs(DIR_SPECS) do
    local cmd = '<cmd>' .. dirSpec.cmdPrefix .. ' | ' .. commonCmd .. '<cr>'
    local desc = overallDesc .. dirSpec.descSuffix
    res[dirSpec.dirKey] = { cmd, desc }
  end

  return res
end

function M.map()
  registerBufferKeyMap({ 'n' }, {
    ['<space>r'] = { '<cmd>Lspsaga rename<cr>', 'Rename element under cursor' },

    ['[e'] = { '<cmd>Lspsaga diagnostic_jump_prev<cr>', 'Jump to previous diagnostic' },
    [']e'] = { '<cmd>Lspsaga diagnostic_jump_next<cr>', 'Jump to next diagnostic' },

    gdd = { vim.lsp.buf.definition, 'Go to definition of LSP item' },
    gd = splitDirectionalWithCommand('lua vim.lsp.buf.definition()', 'Go to definition of LSP item in window split'),

    gDD = { vim.lsp.buf.declaration, 'Go to declaration of LSP item' },
    gD = splitDirectionalWithCommand('lua vim.lsp.buf.declaration()', 'Go to declaration of LSP item in window split'),

    gr = { vim.lsp.buf.references, 'Open quickfix menu with references to LSP item' },
  });

  registerBufferKeyMap({ 'n', 'v' }, {
    ['<C-.>'] = { '<cmd>Lspsaga code_action<cr>', 'Open code action menu' },
    ['<M-i>'] = { '<cmd>Lspsaga hover_doc<cr>', 'Open code action menu' },
  })
end

return M
