-- Source: https://github.com/parz3val/pywal-complete.nvim/blob/main/lua/pywal16/config.lua

local hl = require('user.util.highlight')
local TRANSPARENT = 'NONE'

return function(theme)
    -- TSAnnotation        = { };    -- For C++/Dart attributes, annotations that can be attached to the code to denote some kind of meta information.
    -- TSAttribute         = { };    -- (unstable) TODO: docs
    -- TSBoolean           = { };    -- For booleans.
    -- TSCharacter         = { };    -- For characters.
    -- TSComment           = { };    -- For color1 blocks.
    hl('TSNote', { fg = theme.color.transparent, bg = theme.color.accent5 })
    hl('TSComment', { fg = theme.color.accent2 })
    hl('TSWarning', { fg = TRANSPARENT, bg = theme.color.accent5 })
    hl('TSDanger', { fg = TRANSPARENT, bg = theme.color.accent3 })
    hl('TSConstructor', { fg = theme.color.accent6 }) -- For constructor calls and definitions: `= { }` in Lua, and Java constructors.
    -- TSConditional       = { };    -- For keywords related to conditionnals.
    -- TSConstant          = { };    -- For constants
    -- TSConstBuiltin      = { };    -- For constant that are built in the language: `nil` in Lua.
    -- TSConstMacro        = { };    -- For constants that are defined by macros: `NULL` in C.
    -- TSError             = { };    -- For syntax/parser errors.
    -- TSException         = { };    -- For exception related keywords.
    hl('TSField', { fg = theme.color.accent4 }) -- For fields.
    -- TSFloat             = { };    -- For floats.
    -- TSFunction          = { };    -- For function (calls and definitions).
    -- TSFuncBuiltin       = { };    -- For builtin functions: `table.insert` in Lua.
    -- TSFuncMacro         = { };    -- For macro defined fuctions (calls and definitions): each `macro_rules` in Rust.
    -- TSInclude           = { };    -- For includes: `#include` in C, `use` or `extern crate` in Rust, or `require` in Lua.
    hl('TSKeyword', { fg = theme.color.accent6 })     -- For keywords that don't fall in previous categories.
    hl('TSKeywordFunction', { fg = theme.color.accent6 }) -- For keywords used to define a fuction.
    hl('TSLabel', { fg = theme.color.accent1 })       -- For labels: `label:` in C and `:label:` in Lua.
    -- TSMethod            = { };    -- For method calls and definitions.
    -- TSNamespace         = { };    -- For identifiers referring to modules and namespaces.
    -- TSNone              = { };    -- TODO: docs
    -- TSNumber            = { };    -- For all numbers
    hl('TSOperator', { fg = theme.color.accent1 })    -- For any operator: `+`, but also `->` and `*` in C.
    hl('TSParameter', { fg = theme.color.accent5 })   -- For parameters of a function.
    -- TSParameterReference= { };    -- For references to parameters of a function.
    hl('TSProperty', { fg = theme.color.accent4 })    -- Same as `TSField`.
    hl('TSPunctDelimiter', { fg = theme.color.accent1 }) -- For delimiters ie: `.`
    hl('TSPunctBracket', { fg = theme.color.foreground }) -- For brackets and parens.
    hl('TSPunctSpecial', { fg = theme.color.accent1 }) -- For special punctutation that does not fall in the catagories before.
    -- TSRepeat            = { };    -- For keywords related to loops.
    -- TSString            = { };    -- For strings.
    hl('TSStringRegex', { fg = theme.color.accent1 }) -- For regexes.
    hl('TSStringEscape', { fg = theme.color.accent6 }) -- For escape characters within a string.
    -- TSSymbol            = { };    -- For identifiers referring to symbols or atoms.
    -- TSType              = { };    -- For types.
    -- TSTypeBuiltin       = { };    -- For builtin types.
    hl('TSVariableBuiltin', { fg = theme.color.accent4 }) -- Variable names that are defined by the languages, like `this` or `self`.

    -- TSTag               = { };    -- Tags like html tag names.
    -- TSTagDelimiter      = { };    -- Tag delimiter like `<` `>` `/`
    -- TSText              = { };    -- For strings considered text in a markup language.
    hl('TSTextReference', { fg = theme.color.accent2 })
    -- TSEmphasis          = { };    -- For text to be represented with emphasis.
    -- TSUnderline         = { };    -- For text to be represented with an underline.
    -- TSStrike            = { };    -- For strikethrough text.
    -- TSTitle             = { };    -- Text that is part of a title.
    -- TSLiteral           = { };    -- Literal text.
    -- TSURI               = { };    -- Any URI like a link or email.
end
