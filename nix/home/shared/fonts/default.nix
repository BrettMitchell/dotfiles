{ nixpkgs, ... }:
{
  home.packages = with nixpkgs.stable; [
    noto-fonts
    noto-fonts-color-emoji
    (nerdfonts.override { fonts = [ "VictorMono" ]; })
  ];

  fonts.fontconfig = {
    enable = true;
    defaultFonts = {
      monospace = [ "Noto Sans" ];
      serif = [ "Noto Sans" ];
      sansSerif = [ "Noto Sans" ];
      emoji = [ "Noto Color Emoji" ];
    };
  };
}
