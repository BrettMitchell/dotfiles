-- Source: https://github.com/parz3val/pywal-complete.nvim/blob/main/lua/pywal16/config.lua

local hl = require('user.util.highlight')
local TRANSPARENT = 'NONE'

return function(theme)
  -- Local
  hl('StatusLine', {
    fg = theme.color.text,
    bg = theme.color.background1,
  })

  hl('WinSeparator', {
    fg = theme.color.foreground1,
    bg = theme.color.background1,
  })

  hl('Visual', {
    fg = theme.color.text_selected,
    bg = theme.color.background_selected,
  })

  hl('IncSearch', {
    fg = theme.color.text_selected,
    bg = theme.color.background_selected,
  })

  hl('QuickFixLine', {
    fg = theme.color.background1,
    bg = theme.color.accent3,
  })

  -- TODO: Differentiate search and selection colors
  --       This will require a color audit in existing themes
  hl('Search', {
    fg = theme.color.text_selected,
    bg = theme.color.background_selected,
  })

  hl('DiffAdd', { fg = theme.color.text_accent, bg = theme.color.success })
  hl('DiffChange', { fg = theme.color.text_accent, bg = theme.color.info })
  hl('DiffDelete', { fg = theme.color.text_accent, bg = theme.color.error })
  hl('DiffText', { fg = theme.color.text_accent, bg = theme.color.info })

  hl('Cursor', { fg = theme.color.cursor_text, bg = theme.color.cursor })
  hl('lCursor', { fg = theme.color.cursor_text, bg = theme.color.cursor })
  hl('CursorIM', { fg = theme.color.cursor_text, bg = theme.color.cursor })
  hl('TermCursor', { fg = theme.color.cursor_text, bg = theme.color.cursor })
  hl('TermCursorNC', { fg = theme.color.cursor_text, bg = theme.color.cursor })

  -- From source
  hl('Normal', { fg = theme.color.accent6, bg = theme.color.background1 })
  -- hl('StatusLineNC', { fg = TRANSPARENT, bg = theme.color.accent4 })
  -- hl('StatusLine', { fg = TRANSPARENT, bg = theme.color.accent3 })
  hl('SignColumn', { fg = TRANSPARENT, bg = theme.color.background1 })
  hl('MsgArea', { fg = theme.color.foreground, bg = theme.color.background1 })
  hl('ModeMsg', { fg = theme.color.foreground, bg = theme.color.background1 })
  hl('MsgSeparator', { fg = theme.color.foreground, bg = theme.color.background1 })
  hl('SpellBad', { fg = theme.color.accent2 })
  hl('SpellCap', { fg = theme.color.accent6 })
  hl('SpellLocal', { fg = theme.color.accent4 })
  hl('SpellRare', { fg = theme.color.accent6 })
  hl('NormalNC', { fg = theme.color.foreground, bg = theme.color.background1 })
  hl('Pmenu', { fg = theme.color.foreground, bg = theme.color.background1 })
  hl('PmenuSel', { fg = TRANSPARENT, bg = theme.color.accent0 })
  hl('WildMenu', { fg = theme.color.accent1, bg = theme.color.accent4 })
  hl('CursorLineNr', { fg = theme.color.accent1 })
  hl('Comment', { fg = theme.color.accent2, bg = theme.color.background })
  hl('Folded', { fg = theme.color.accent4, bg = theme.color.background1 })
  hl('FoldColumn', { fg = theme.color.accent4, bg = theme.color.background1 })
  hl('LineNr', { fg = theme.color.accent4, bg = theme.color.background1 })
  hl('FloatBorder', { fg = theme.color.foreground, bg = theme.color.background1 })
  hl('Whitespace', { fg = theme.color.accent1, bg = theme.color.accent2 })
  hl('VertSplit', { fg = theme.color.accent4, bg = theme.color.background1 })
  hl('CursorLine', { bg = theme.color.background1 })
  hl('CursorColumn', { bg = theme.color.background1 })
  hl('ColorColumn', { bg = theme.color.background })
  hl('NormalFloat', { fg = theme.color.foreground, bg = theme.color.background })
  -- hl('Visual', { fg = theme.color.foreground, bg = theme.color.accent5 })
  -- hl('VisualNOS', { bg = theme.color.background1 })
  hl('WarningMsg', { fg = theme.color.accent3, bg = theme.color.background1 })
  hl('QuickFixLine', { bg = theme.color.accent2 })
  hl('PmenuSbar', { bg = theme.color.background1 })
  hl('PmenuThumb', { bg = theme.color.accent2 })
  hl('MatchParen', { fg = theme.color.accent4, bg = theme.color.background1 })
  -- hl('Cursor', { fg = theme.color.foreground, bg = theme.color.cursor })
  -- hl('lCursor', { fg = theme.color.foreground, bg = theme.color.cursor })
  -- hl('CursorIM', { fg = theme.color.foreground, bg = theme.color.cursor })
  -- hl('TermCursor', { fg = theme.color.foreground, bg = theme.color.cursor })
  -- hl('TermCursorNC', { fg = theme.color.foreground, bg = theme.color.cursor })
  hl('Conceal', { fg = theme.color.accent4, bg = theme.color.background1 })
  hl('Directory', { fg = theme.color.accent4 })
  hl('SpecialKey', { fg = theme.color.accent4 })
  hl('Title', { fg = theme.color.accent4 })
  hl('ErrorMsg', { fg = theme.color.accent4, bg = theme.color.background1 })
  -- hl('Search', { fg = theme.color.foreground, bg = theme.color.accent2 })
  -- hl('IncSearch', { fg = theme.color.foreground, bg = theme.color.accent3 })
  hl('Substitute', { fg = theme.color.accent1, bg = theme.color.accent6 })
  hl('MoreMsg', { fg = theme.color.accent5 })
  hl('Question', { fg = theme.color.accent5 })
  hl('EndOfBuffer', { fg = theme.color.background, bg = theme.color.background1 })
  hl('NonText', { fg = theme.color.background, bg = theme.color.background })
  hl('Variable', { fg = theme.color.accent3 })
  hl('String', { fg = theme.color.accent4 })
  hl('Character', { fg = theme.color.accent4 })
  hl('Constant', { fg = theme.color.accent3 })
  hl('Number', { fg = theme.color.accent5 })
  hl('Boolean', { fg = theme.color.accent5 })
  hl('Float', { fg = theme.color.accent5 })
  hl('Identifier', { fg = theme.color.accent5 })
  hl('Function', { fg = theme.color.accent3 })
  hl('Operator', { fg = theme.color.accent6 })
  hl('Type', { fg = theme.color.accent5 })
  hl('StorageClass', { fg = theme.color.accent1 })
  hl('Structure', { fg = theme.color.accent6 })
  hl('Typedef', { fg = theme.color.accent6 })
  hl('Keyword', { fg = theme.color.accent4 })
  hl('Statement', { fg = theme.color.accent6 })
  hl('Conditional', { fg = theme.color.accent2 })
  hl('Repeat', { fg = theme.color.accent6 })
  hl('Label', { fg = theme.color.accent4 })
  hl('Exception', { fg = theme.color.accent6 })
  hl('Include', { fg = theme.color.accent6 })
  hl('PreProc', { fg = theme.color.accent6 })
  hl('Define', { fg = theme.color.accent6 })
  hl('Macro', { fg = theme.color.accent6 })
  hl('PreCondit', { fg = theme.color.accent6 })
  hl('Special', { fg = theme.color.accent6 })
  hl('SpecialChar', { fg = theme.color.foreground })
  hl('Tag', { fg = theme.color.accent4 })
  hl('Debug', { fg = theme.color.accent4 })
  hl('Delimiter', { fg = theme.color.foreground })
  hl('SpecialComment', { fg = theme.color.accent2 })
  hl('Ignore', { fg = theme.color.accent1, bg = theme.color.background1 })
  hl('Todo', { fg = theme.color.accent4, bg = theme.color.background1 })
  hl('Error', { fg = theme.color.accent4, bg = theme.color.background1 })
  --hl('TabLine', { fg = theme.color.accent4, bg = theme.color.background })
  --hl('TabLineSel', { fg = theme.color.foreground, bg = theme.color.background })
  hl('TabLineFill', { fg = theme.color.accent4, bg = theme.color.background1 })
  hl('CmpDocumentationBorder', { fg = theme.color.foreground, bg = theme.color.background1 })
  hl('CmpItemAbbr', { fg = theme.color.foreground, bg = theme.color.background1 })
  hl('CmpItemAbbrDeprecated', { fg = theme.color.accent2, bg = theme.color.background1 })
  hl('CmpItemAbbrMatch', { fg = theme.color.accent1, bg = theme.color.background1 })
  hl('CmpItemAbbrMatchFuzzy', { fg = theme.color.accent1, bg = theme.color.background1 })
  hl('CmpItemKind', { fg = theme.color.accent4, bg = theme.color.background1 })
  hl('CmpItemMenu', { fg = theme.color.accent2, bg = theme.color.background1 })

  hl('healthError', { fg = theme.color.accent2 })
  hl('healthSuccess', { fg = theme.color.accent4 })
  hl('healthWarning', { fg = theme.color.accent5 })
end
