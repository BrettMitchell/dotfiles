------------
-- Server --
------------

-- require('user.server')

-----------------
-- Set options --
-----------------

require('user.setopts')

----------------------
-- Global functions --
----------------------

require('user.global_functions')

-------------
-- Plugins --
-------------

require('user.plugins')

------------------
-- Key mappings --
------------------

require('user.keymap.completion').setup()
-- require('user.keymap.dap').setupStatic()
-- require('user.keymap.dap').setupOnAttach()

require('user.keymap.editing').setup()
require('user.keymap.editor-actions').setupWrite()
require('user.keymap.editor-actions').setupQuitImmediate()

require('user.keymap.folding').setup()
require('user.keymap.formatting').setup()
-- require('user.keymap.fterm').setup()

-- require('user.keymap.git').setup()
-- require('user.keymap.harpoon').setup()

-- require('user.keymap.lsp').setup()
require('user.keymap.movement').setup()

-- require('user.keymap.notes').setup()
require('user.keymap.quickfix').setup()
require('user.keymap.reload-config').setup()
require('user.keymap.search').setup()
-- require('user.keymap.telescope').setup()
-- require('user.keymap.trouble').setup()
require('user.keymap.ui-settings').setup()
require('user.keymap.undotree').setup()
require('user.keymap.windows').setup()

------------------
-- Autocommands --
------------------

-- require('user.autocommands.cwd').setup()
require('user.autocommands.folding').setup()
-- require('user.autocommands.macro-cmdheight').setup()

-------------------------------------------------------------------
-- Emanote Integration (WIP, move to plugins.lua when published) --
-------------------------------------------------------------------

-- require('user.emanote_integration')

