local M = {}

function M.includes(tbl, subject)
  for _, elem in ipairs(tbl) do
    if elem == subject then return true end
  end

  return false
end

function M.map(tbl, fn)
  local res = {}
  for i, v in ipairs(tbl) do
    table.insert(res, fn(v, i, tbl))
  end
  return res
end

function M.find(tbl, fn)
  for i, v in ipairs(tbl) do
    if fn(v, i, tbl) then return v end
  end
  return nil
end

function M.join(tbl, sep)
  local res = ''
  for i, str in ipairs(tbl) do
    if i == 1 then
      res = str
    else
      res = res .. sep .. str
    end
  end
  return res
end

function M.shallowCopy(tbl)
  local res = {}

  for i, v in ipairs(tbl) do
    res[i] = v
  end

  for k, v in pairs(tbl) do
    res[k] = v
  end

  return res
end

function M.concat(t1, t2)
  local res = {}
  for _, v in ipairs(t1) do
    table.insert(res, v)
  end
  for _, v in ipairs(t2) do
    table.insert(res, v)
  end
  return res
end

function M.slice(tbl, start, finish)
  finish = finish or #tbl
  local res = {}
  for i = start, finish do
    table.insert(res, tbl[i])
  end

  return res
end

function M.isDict(tbl)
  if type(tbl) ~= 'table' then
    return false
  end

  -- If we find any hash keys in the table, we consider it a dictionary
  for k, _ in pairs(tbl) do
    if type(k) == 'string' then
      return true
    end
  end

  return false
end

return M
