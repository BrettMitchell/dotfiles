local make_feature = require('pax-vim.util.feature').make_feature_v2
local hl = require('pax-vim.util.highlight')

local pax_colors = make_feature({
  name = 'Pax Colors',

  description = [[
  This feature will eventually replace the 'pywal' colorscheme,
  but for now, it serves only as a supplement to clean up some
  contrast issues.
  ]],

  reset = function(self)
    require("pywal").setup {}
    self:load()
  end,

  load = function ()
    local json = require('lunajson')

    -- TODO: Add proper guard statements here. This runs into issues when you
    -- don't have your environment configured as expected.

    local current_theme_name_file = vim.fn.getenv('THEMES_DIR') .. '/current'
    local current_theme_file = vim.fn.readfile(current_theme_name_file)[1]
    local current_theme_values_arr = vim.fn.readfile(current_theme_file .. '/value_store.json')
    local current_theme_values = ''
    for _, line in ipairs(current_theme_values_arr) do
      current_theme_values = current_theme_values .. line
    end

    local current_theme = json.decode(current_theme_values)
    local text = current_theme.color.foreground1
    local bg = current_theme.color.background2

    hl('GalaxyLineSeparatorHighlight', {
      fg = text,
      bg = bg,
    })

    hl('GalaxyLineHighlightReset', {
      fg = text,
      bg = bg,
    })

    hl('GalaxyLineFileName', {
      fg = current_theme.color.accent1,
      bg = bg,
    })

    hl('GalaxyLineFileType', {
      fg = current_theme.color.accent1,
      bg = bg,
    })

    hl('GalaxyLineViMode', {
      fg = bg,
      bg = current_theme.color.foreground2,
    })

    hl('GalaxyLineBranch', {
      fg = bg,
      bg = current_theme.color.foreground2,
    })

    hl('GalaxyLineScrollPercentage', {
      fg = bg,
      bg = current_theme.color.foreground2,
    })

    hl('StatusLine', {
      fg = text,
      bg = bg,
    })

    hl('WinSeparator', {
      fg = current_theme.color.foreground1,
      bg = current_theme.color.background1,
    })

    -- hl('TelescopePromptNormal', {
    --   fg = current_theme.color.foreground1,
    --   bg = current_theme.color.background2,
    -- })

    -- hl('TelescopePromptPrefix', {
    --   fg = current_theme.color.foreground1,
    --   bg = current_theme.color.background1,
    -- })

    -- hl('TelescopePromptBorder', {
    --   fg = current_theme.color.accent1,
    --   bg = current_theme.color.background2,
    -- })

    hl('TelescopePromptCounter', {
      fg = current_theme.color.accent3,
      bg = current_theme.color.background1,
    })

    hl('TelescopeSelection', {
      fg = current_theme.color.foreground1,
      bg = current_theme.color.background1,
    })

    hl('Visual', {
      fg = current_theme.color.text_selected,
      bg = current_theme.color.background_selected,
    })

    hl('IncSearch', {
      fg = current_theme.color.text_selected,
      bg = current_theme.color.background_selected,
    })

    hl('QuickFixLine', {
      fg = current_theme.color.background1,
      bg = current_theme.color.accent3,
    })

    -- TODO: Differentiate search and selection colors
    --       This will require a color audit in existing themes
    hl('Search', {
      fg = current_theme.color.text_selected,
      bg = current_theme.color.background_selected,
    })

    hl('FlashMatch', {
      fg = current_theme.color.text_selected,
      bg = current_theme.color.background_selected,
    })

    hl('FlashCurrent', {
      fg = current_theme.color.foreground1,
      bg = current_theme.color.background2,
    })

    hl('FlashLabel', {
      fg = current_theme.color.accent1,
      bg = current_theme.color.background2,
    })

    hl('FlashBackdrop', {
      fg = current_theme.color.accent3,
      bg = current_theme.color.background1,
    })
  end,

  apply = function (self)
    self:load()
  end
})

return pax_colors
