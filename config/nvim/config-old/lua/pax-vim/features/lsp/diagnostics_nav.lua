local make_feature = require('pax-vim.util.feature').make_feature
local keymap = require('pax-vim.util.keymap')

local lsp = require('pax-vim.features.lsp')
local lspsaga = require('pax-vim.features.lsp.lspsaga')

local methods = {
  goto_next_diagnostic = function()
    require('lspsaga.diagnostic'):goto_prev()
  end,

  goto_previous_diagnostic = function()
    require('lspsaga.diagnostic'):goto_prev()
  end,

  goto_next_error = function()
    require('lspsaga.diagnostic'):goto_next({ severity = vim.diagnostic.severity.ERROR })
  end,

  goto_previous_error = function()
    require('lspsaga.diagnostic'):goto_next({ severity = vim.diagnostic.severity.ERROR })
  end
}

local diagnosticsKeymap = keymap {
  previous_diagnostic = { 'n', '[e', methods.goto_previous_diagnostic, { desc = 'Jump to previous diagnostic' } },
  next_diagnostic = { 'n', ']e', methods.goto_next_diagnostic, { desc = 'Jump to next diagnostic' } },

  previous_error = { 'n', '[E', methods.goto_previous_error, { desc = 'Jump to previous error diagnostic' } },
  next_error = { 'n', ']E', methods.goto_next_error, { desc = 'Jump to next error diagnostic' } },
}

local M = make_feature({
  name = 'LSP :: Diagnostics Navigation',
  dependencies = { lsp, lspsaga },

  description = [[
  This feature adds keymaps for navigating between LSP diagnostics
  ]],

  keymaps = diagnosticsKeymap,

  setup_seq = { keymaps = false },

  on_activate = function()
    lsp.methods.add_hook({
      hook_name = 'on_attach',
      callback = function(_, bufnr)
        diagnosticsKeymap.apply(bufnr)
      end
    })
  end
})

M.methods = methods

return M
