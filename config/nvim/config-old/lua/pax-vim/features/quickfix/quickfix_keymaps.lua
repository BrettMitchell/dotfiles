local make_feature = require('pax-vim.util.feature').make_feature_v2
local keymap = require('pax-vim.util.keymap')
local make_augroup = require('pax-vim.util.autocmd').make_augroup

local qf = require('pax-vim.features.quickfix.qf')
local bqf = require('pax-vim.features.quickfix.bqf')

local M = make_feature({
  name = 'Quickfix',
  description = [[
  This feature adds what I believe are sensible default keybindings
  for the quickfix list.
  ]],

  dependencies = { qf, bqf },

  toggle_keymaps = keymap {
    { 'n', '<C-q>', function() require('qf').toggle('c', false) end, { desc = 'Toggles quickfix menu' }, },
  },

  quickfix_buffer_local_keymaps = keymap {
    -- { 'n', 'cc', function() vim.cmd 'call setqflist([], "f") | cclose' end, { desc = 'Clears the quickfix menu' }, },
    { 'n', '<', '<cmd>silent colder<cr>', { desc = 'Open previous quickfix list' } },
    { 'n', '>', '<cmd>silent cnewer<cr>', { desc = 'Open next quickfix list' } },
  },

  init = function(self)
    self.augroups = make_augroup {
      {
        name = 'NavigateQuickfixHistory',
        autocmds = {
          {
            events = 'FileType',
            opts = {
              callback = function()
                if vim.bo.filetype ~= 'qf' then return end

                if self.quickfix_buffer_local_keymaps then
                  self.quickfix_buffer_local_keymaps.apply(vim.fn.bufnr('%'))
                end
              end
            }
          }
        }
      }
    }
  end,

  apply = function (self)
    if self.toggle_keymaps then self.toggle_keymaps.apply() end
    if self.augroups then self.augroups:apply() end
  end
})

return M
