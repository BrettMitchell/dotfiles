local zk = require('zk.api')
local previewers = require('telescope.previewers')

local note_util = require('user.features.notes.util')
local make_lines = require('user.util.buffer.make_lines')

local function makeZkPreviewer(opts)
  opts = opts or {}
  local getNotesDir = opts.getNotesDir or note_util.getNotesDir
  local getQuery = opts.getQuery or function() return { select = { 'title', 'tags' } } end
  local getText = opts.getText or function(self, notes) return vim.inspect(notes) end

  return previewers.new_buffer_previewer({
    define_preview = function(self, entry)
      local query = getQuery(self, entry)

      zk.list(getNotesDir(), query, function(err, notes)
        local text = ''
        if (err) then
          text = 'An error occurred while running zk:\n\n' .. vim.inspect(err.message or err)
        else
          text = getText(self, notes)
        end

        local lines = make_lines(text)

        vim.api.nvim_buf_set_lines(self.state.bufnr, 0, -1, false, lines)
      end)
    end
  })
end

return {
  makeZkPreviewer = makeZkPreviewer
}
