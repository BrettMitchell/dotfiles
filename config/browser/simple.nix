{ nixpkgs, ... }:

{
  imports = [
    ./firefox/simple.nix
  ];

  home.packages = with nixpkgs.stable; [
    brave
    librewolf-wayland
  ];
}
