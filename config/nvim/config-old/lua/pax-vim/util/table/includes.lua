return function (item, tbl)
  for _, el in ipairs(tbl) do
    if el == item then
      return true
    end
  end

  return false
end
