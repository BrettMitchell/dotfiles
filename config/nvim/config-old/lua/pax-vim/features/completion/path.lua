-- https://github.com/tzachar/cmp-fuzzy-path
-- https://github.com/FelipeLema/cmp-async-path

local make_feature = require('pax-vim.util.feature').make_feature_v2

local cmp_path = make_feature({
  name = 'Completion :: File Paths',

  description = [[
  This feature adds a cmp completion source for the filesystem.
  Paths are relative to the current file's location.
  ]],

  source_definition = {
    name = 'async_path',
    max_item_count = 5,
  },

  plugins = {
    { 'https://github.com/FelipeLema/cmp-async-path' }
  },
})

return cmp_path
