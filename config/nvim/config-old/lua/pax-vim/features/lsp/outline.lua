local make_feature = require('pax-vim.util.feature').make_feature
local keymap = require('pax-vim.util.keymap')
local lsp = require('pax-vim.features.lsp')

local methods = {
  toggle_outline = function()
    vim.cmd [[SymbolsOutline]]
  end
}

local outline_keymap = keymap {
  toggle_outline = { { 'n', 'x' }, '<space>lo', '<CMD>SymbolsOutline<CR>', { desc = 'Show LSP symbol outline' } },
}

local M = make_feature({
  name = 'LSP :: Symbol outline',
  dependencies = { lsp },

  description = [[
  This feature adds a toggleable, LSP-driven symbol outline
  ]],

  keymaps = outline_keymap,

  plugins = {
    {
      'https://github.com/simrat39/symbols-outline.nvim',
      event = "LspAttach",
      config = function()
        require('symbols-outline').setup({
          highlight_hovered_item = true,
          show_guides = true,
          auto_preview = false,
          position = 'left',
          relative_width = true,
          width = 25,
          auto_close = false,
          show_numbers = false,
          show_relative_numbers = false,
          show_symbol_details = true,
          preview_bg_highlight = 'Pmenu',
          autofold_depth = nil,
          auto_unfold_hover = true,
          fold_markers = { '', '' },
          wrap = false,
          keymaps = { -- These keymaps can be a string or a table for multiple keys
            close = { "<Esc>", "q" },
            goto_location = "<Cr>",
            focus_location = "o",
            hover_symbol = "<C-space>",
            toggle_preview = "<space>li",
            rename_symbol = "<space>lr",
            code_actions = "<space>la",
            fold = "zf",
            unfold = "zu",
            fold_all = "W",
            unfold_all = "E",
            fold_reset = "R",
          },
          lsp_blacklist = {},
          symbol_blacklist = {},
          symbols = {
            File = { icon = "file", hl = "@text.uri" },
            Module = { icon = "module", hl = "@namespace" },
            Namespace = { icon = "namespace", hl = "@namespace" },
            Package = { icon = "pkg", hl = "@namespace" },
            Class = { icon = "class", hl = "@type" },
            Method = { icon = "method", hl = "@method" },
            Property = { icon = "prop", hl = "@method" },
            Field = { icon = "field", hl = "@field" },
            Constructor = { icon = "ctor", hl = "@constructor" },
            Enum = { icon = "enum", hl = "@type" },
            Interface = { icon = "iface", hl = "@type" },
            Function = { icon = "fn", hl = "@function" },
            Variable = { icon = "var", hl = "@constant" },
            Constant = { icon = "const", hl = "@constant" },
            String = { icon = "str", hl = "@string" },
            Number = { icon = "num", hl = "@number" },
            Boolean = { icon = "bool", hl = "@boolean" },
            Array = { icon = "[]", hl = "@constant" },
            Object = { icon = "{}", hl = "@type" },
            Key = { icon = "key", hl = "@type" },
            Null = { icon = "null", hl = "@type" },
            EnumMember = { icon = "member", hl = "@field" },
            Struct = { icon = "struct", hl = "@type" },
            Event = { icon = "event", hl = "@type" },
            Operator = { icon = "op", hl = "@operator" },
            TypeParameter = { icon = "parameter", hl = "@parameter" },
            Component = { icon = "ui", hl = "@function" },
            Fragment = { icon = "ui", hl = "@constant" },
          },
        })
      end
    }
  }
})

M.methods = methods

return M
