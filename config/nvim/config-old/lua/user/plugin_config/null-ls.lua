local M = {}

function M.setup()
  local null_ls = require('null-ls')

  local formatting = null_ls.builtins.formatting
  local diagnostics = null_ls.builtins.diagnostics

  null_ls.setup {
    debug = false,
    sources = {
      formatting.eslint_d,
      formatting.black,
      diagnostics.eslint_d,
    }
  }
end

return M
