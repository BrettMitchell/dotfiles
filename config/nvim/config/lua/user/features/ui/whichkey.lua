local make_feature = require('user.util.feature').make_feature

local config = {
  timeout = nil,
  silent_toggle = false,
  no_toggle = false,
  whichkey_config = {}
}

local enabled = true

-- Ugly hack to make which-key toggleable
local function makeToggleableDisable(originalDisableValue)
  local toggleableDisable = {}

  setmetatable(toggleableDisable, {
    __index = function(_, key)
      if not enabled then
        if key == 'filetypes' then
          local buf = vim.api.nvim_get_current_buf()
          return { vim.api.nvim_buf_get_option(buf, "filetype") }
        end

        return {}
      else
        return originalDisableValue[key] or {}
      end
    end
  })

  return toggleableDisable
end

local function enable()
  if config.no_toggle then return end
  enabled = true
  if not config.silent_toggle then
    vim.notify('Enabled which-key')
  end
end

local function disable()
  if config.no_toggle then return end
  enabled = false
  if not config.silent_toggle then
    vim.notify('Disabled which-key')
  end
end

local function toggle()
  if config.no_toggle then return end
  if enabled then
    disable()
  else
    enable()
  end
end

local function label_group(group_prefix, label)
  local ok, wk = pcall(require, 'which-key')
  if not ok then return end
  wk.register({ [group_prefix] = { name = label } })
end

local M = make_feature({
  name = 'Whichkey',
  description = [[
  This feature configures whichkey for keymap visualization
  ]],
  plugins = {
    {
      'https://github.com/folke/which-key.nvim',
      config = function()
        if type(config.timeout) == 'number' then
          vim.o.timeout = true
          vim.o.timeoutlen = config.timeout
        end

        require("which-key").setup(config.whichkey_config)

        if not config.no_toggle then
          require('which-key.config').options.disable = makeToggleableDisable(
            config.whichkey_config.disable or {}
          )
        end
      end
    }
  },

  config = config,

  keymaps = {
    { 'n', '<space>uw', toggle, { desc = 'Toggle whichkey' } },
  }
})

M.methods = {
  enable = enable,
  disable = disable,
  toggle = toggle,
  label_group = label_group,
}

return M
