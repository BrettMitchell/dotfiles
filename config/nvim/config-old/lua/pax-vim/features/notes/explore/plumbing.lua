local zk = require('zk.api')
local util = require('pax-vim.features.notes.util')

local M = {}

function M.getTags(opts, cb)
  opts = opts or {}
  local notesDir = opts.notesDir or util.getNotesDir()
  opts.notesDir = nil

  zk.tag.list(notesDir, opts, cb)
end

function M.getNotes(opts, cb)
  opts = opts or {}
  local notesDir = opts.notesDir or util.getNotesDir()
  opts.notesDir = nil
  opts.select = opts.select or { 'absPath', 'path', 'title' }

  zk.list(notesDir, opts, cb)
end

function M.getNotesByTitle(opts, cb)
  opts = opts or {}
  local notesDir = opts.notesDir or util.getNotesDir()
  local title = opts.title
  local select = opts.select or { 'absPath', 'path', 'title' }

  zk.list(notesDir, {
    matchStrategy = 'exact',
    select = select,
    match = { 'title: ' .. title },
  }, cb)
end

function M.getNotesByTags(opts, cb)
  opts = opts or {}
  local notesDir = opts.notesDir or util.getNotesDir()
  local tags = opts.tags
  local select = opts.select or { 'absPath', 'path', 'title', 'tags' }

  zk.list(notesDir, {
    matchStrategy = 'exact',
    select = select,
    tags = tags,
  }, cb)
end

return M
