# zmodload zsh/zprof

### Added by Zinit's installer
if [[ ! -f $HOME/.local/share/zinit/zinit.git/zinit.zsh ]]; then
    print -P "%F{33} %F{220}Installing %F{33}ZDHARMA-CONTINUUM%F{220} Initiative Plugin Manager (%F{33}zdharma-continuum/zinit%F{220})…%f"
    command mkdir -p "$HOME/.local/share/zinit" && command chmod g-rwX "$HOME/.local/share/zinit"
    command git clone https://github.com/zdharma-continuum/zinit "$HOME/.local/share/zinit/zinit.git" && \
        print -P "%F{33} %F{34}Installation successful.%f%b" || \
        print -P "%F{160} The clone has failed.%f%b"
fi

source "$HOME/.local/share/zinit/zinit.git/zinit.zsh"
autoload -Uz _zinit
(( ${+_comps} )) && _comps[zinit]=_zinit

# Load a few important annexes, without Turbo
# (this is currently required for annexes)
zinit light-mode for \
    zdharma-continuum/zinit-annex-as-monitor \
    zdharma-continuum/zinit-annex-bin-gem-node \
    zdharma-continuum/zinit-annex-patch-dl \
    zdharma-continuum/zinit-annex-rust

### End of Zinit's installer chunk

HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000

setopt \
  autocd \
  extendedglob \
  nomatch \
  notify \
  sharehistory \
  histsavenodups \
  histignorespace \
  histignorealldups

unsetopt beep

# Add dotfile completions to zsh fpath
fpath=("$HOME/.config/zsh/completions" $fpath)

# Load generated TTY color scheme
source "$HOME/.cache/wal/colors-tty.sh"

# Load broot utility into shell environment
zinit ice wait lucid
zinit snippet "$HOME/.config/broot/launcher/zsh/br"

eval "$(mise activate zsh)"

# Load user configuration
zinit ice depth=1
zinit light jeffreytse/zsh-vi-mode

source "$HOME/.config/zsh/opts.sh"

# zinit ice wait lucid
# zinit snippet "$HOME/.config/zsh/plugins.sh"
zinit snippet "$HOME/.config/zsh/prompt.sh"
zinit ice wait lucid
zinit snippet "$HOME/.config/zsh/functions.sh"
zinit ice wait lucid
zinit snippet "$HOME/.config/zsh/completions/direct.sh"
zinit ice wait lucid
zinit snippet "$HOME/.config/zsh/keys.sh"
zinit ice wait lucid
zinit snippet "$HOME/.config/zsh/remote_control_cmds.sh"
zinit ice wait lucid
zinit snippet "$HOME/.config/zsh/remote_control.sh"
zinit ice wait lucid
zinit snippet "$HOME/.config/zsh/remote_control_invoke.sh"

direnv_zshrc="${XDG_CONFIG_HOME:-$HOME/.config}/asdf-direnv/zshrc"
if [[ -f "$direnv_zshrc" ]] ; then 
  zinit ice wait lucid
  zinit snippet "$direnv_zshrc"
fi

# pnpm
export PNPM_HOME="$HOME/.local/share/pnpm"
case ":$PATH:" in
  *":$PNPM_HOME:"*) ;;
  *) export PATH="$PNPM_HOME:$PATH" ;;
esac
# pnpm end

# Note: For some reason, doing this after loading zinit.zsh is WAY faster.
autoload -U compinit
if [[ ! -f ~/.zcompdump ]] ; then
  compinit -d ~/.zcompdump
else
  compinit -C
fi

autoload -U bashcompinit
bashcompinit

zinit cdreplay -q

zinit ice depth=1 wait silent
zinit light zdharma-continuum/fast-syntax-highlighting

if [[ -f "$HOME/.zshrc.local" ]] ; then
  zinit ice wait lucid
  zinit snippet "$HOME/.zshrc.local"
fi

# zprof
