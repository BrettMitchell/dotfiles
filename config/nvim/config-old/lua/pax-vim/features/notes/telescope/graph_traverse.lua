local zk            = require('zk.api')
local table_utils   = require('pax-vim.util.table')
local deepMergeLeft = require('pax-vim.util.table.merge').deepMergeLeft
local note_util     = require('pax-vim.features.notes.util')
local frontmatter   = require('pax-vim.features.notes.frontmatter.v2')
local pickNote      = require('pax-vim.features.notes.telescope').pickNote
local TAGS          = require('pax-vim.features.notes.special_tags')

-- Graph traversal
--
-- Principles
--   Treat all discoverable relationships between notes as edges in a graph
--   Provide keybindings for moving over those edges
--   Provide a preview that shows those edges

local M             = {}

-- Special picker which displays entries with an edge type prefix, and then handles each
-- edge type specially
local function make_edge_list(edges, opts)
  --[[

  Backlink:
    :back > Note name goes here

  Link:
    :link > Note name goes here

  Tag:
    :tag(tag.name.goes.here) > Note name goes here

  --]]

  local link_entries = table_utils.map(edges.links, function(link)
    link.title = ':link > ' .. link.title
    return link
  end)

  local backlink_entries = table_utils.map(edges.backlinks, function(backlink)
    backlink.title = ':back > ' .. backlink.title
    return backlink
  end)

  local tag_entries = {}
  for tag_name, tagged_notes in pairs(edges.tags) do
    if opts.verbose or not table_utils.includes(tag_name, TAGS.NOISY_TAG_NAMES) then
      for _, tagged_note in ipairs(tagged_notes) do
        table.insert(tag_entries, {
          title = ':tag(' .. tag_name .. ') > ' .. tagged_note.title,
          absPath = tagged_note.absPath,
          rawContent = tagged_note.rawContent
        })
      end
    end
  end

  local results = {}
  if opts.show_links then
    results = table_utils.concat(results, link_entries)
  end

  if opts.show_backlinks then
    results = table_utils.concat(results, backlink_entries)
  end

  if opts.show_tags then
    results = table_utils.concat(results, tag_entries)
  end

  return results
end

local function get_edges(opts, note, note_tags, cb)
  -- Tags
  local tag_match = ''
  for i, tag in ipairs(note_tags or {}) do
    if i > 1 then
      tag_match = tag_match .. ' OR ' .. tag
    else
      tag_match = tag
    end
  end

  -- Backlinks
  zk.list(
    note_util.getNotesDir(opts),
    { linkTo = { note }, select = { 'absPath', 'title', 'rawContent' } },
    function(get_backlink_err, backlinkNotes)
      if get_backlink_err then return cb(nil, get_backlink_err) end

      -- Links
      zk.list(
        note_util.getNotesDir(opts),
        { linkedBy = { note }, select = { 'absPath', 'title', 'rawContent' } },
        function(get_link_err, linkNotes)
          if get_link_err then return cb(nil, get_link_err) end

          if #(note_tags or {}) == 0 then
            return cb({
              tags = {},
              backlinks = backlinkNotes,
              links = linkNotes,
            })
          end

          -- Notes related by tag
          zk.list(
            note_util.getNotesDir(opts),
            { tags = { tag_match }, select = { 'absPath', 'title', 'rawContent', 'tags' } },
            function(get_tagged_notes_error, tagged_notes)
              if get_tagged_notes_error then return cb(nil, get_tagged_notes_error) end

              local tags = {}

              for _, tag in ipairs(note_tags or {}) do
                tags[tag] = {}

                for _, tagged_note in ipairs(tagged_notes) do
                  if tagged_note.absPath ~= note and table_utils.includes(tag, tagged_note.tags) then
                    table.insert(tags[tag], tagged_note)
                  end
                end
              end

              return cb({
                tags = tags,
                backlinks = backlinkNotes,
                links = linkNotes,
              })
            end
          )
        end
      )
    end
  )
end

--[[

Interactions:
  Enter -> Navigate to note in graph traversal dialog
  C-o   -> Go to previous file in nav stack
  C-i   -> Go to next file in nav stack
  C-q   -> Send current list to quickfix list
  C-v   -> Toggle verbose mode
  C-y   -> Yank all file paths to clipboard
  C-t   -> Only show tag edges
  C-l   -> Only show link edges
  C-b   -> Only show backlink edges
  C-a   -> Show all types of edges
  Esc   -> Clear the current prompt, if any. Close graph view if none.

As the user navigates to other note files, synchronize the last active window with the current focused file in the graph traverser
This keeps the jump list in sync with the dialog history, and allows us to omit an 'open' keybinding. When you're done navigating, just close the dialog

--]]
function M.graph_traverse(opts)
  local current_note = opts and opts.note or vim.fn.expand('%:p')
  local fm = frontmatter.get_file_frontmatter({ filename = current_note })
  if not fm then
    vim.notify('No frontmatter found for note file ' .. current_note, 'error')
    return
  end

  opts = deepMergeLeft(
    { title = 'Traverse note graph' },
    opts or {
      note = current_note,
      jump_stack = { current_note },
      jump_index = 1,
      winnr = vim.fn.winnr(),
      verbose = false,
      show_tags = true,
      show_links = true,
      show_backlinks = true,
    }
  )

  local function go_back()
    if opts.jump_index < 2 then return end

    local new_jump_index = opts.jump_index - 1
    opts.note = opts.jump_stack[new_jump_index]
    opts.jump_index = new_jump_index

    vim.cmd(':' .. tostring(opts.winnr) .. "windo e " .. opts.note)

    M.graph_traverse(opts)
  end

  local function go_forward()
    if opts.jump_index >= #opts.jump_stack then return end

    local new_jump_index = opts.jump_index + 1
    opts.note = opts.jump_stack[new_jump_index]
    opts.jump_index = new_jump_index

    vim.cmd(':' .. tostring(opts.winnr) .. "windo e " .. opts.note .. "")

    M.graph_traverse(opts)
  end

  local function toggle_tags()
    opts.show_tags = not opts.show_tags
    M.graph_traverse(opts)
  end

  local function toggle_links()
    opts.show_links = not opts.show_links
    M.graph_traverse(opts)
  end

  local function toggle_backlinks()
    opts.show_backlinks = not opts.show_backlinks
    M.graph_traverse(opts)
  end

  local function toggle_verbose()
    opts.verbose = not opts.verbose
    M.graph_traverse(opts)
  end

  get_edges(opts, current_note, fm.tags, function(edges, err)
    if err then
      vim.notify('An error occurred while getting traversable edges: ' .. vim.inspect(err))
      return
    end

    local edge_note_items = make_edge_list(edges, opts)

    local mappings = {
      select_default = function(selected_notes)
        local selected_note = selected_notes[1]
        local truncated_stack = table_utils.slice(opts.jump_stack, 1, opts.jump_index)

        vim.cmd(':' .. tostring(opts.winnr) .. "windo e " .. selected_note.value.absPath .. "")

        opts.note = selected_note.value.absPath
        opts.jump_stack = table_utils.concat(truncated_stack, { selected_note.value.absPath })
        opts.jump_index = opts.jump_index + 1

        M.graph_traverse(opts)
      end,

      i = {
        -- ['<ESC>'] = function ()
        --
        -- end,

        ['<C-o>'] = go_back,
        ['<C-i>'] = go_forward,
        -- ['<C-q>'] = open_in_qf,
        -- ['<C-y>'] = yank_filenames,
        ['<C-t>'] = toggle_tags,
        ['<C-l>'] = toggle_links,
        ['<C-b>'] = toggle_backlinks,
        ['<C-v>'] = toggle_verbose,
      }
    }

    -- todo: make this be the current note's title, maybe include some info
    -- about active dialog flags etc.
    local title = 'Graph traverse ['
    if opts.verbose then title = title .. 'v' end
    if opts.show_tags then title = title .. 'T' end
    if opts.show_links then title = title .. 'L' end
    if opts.show_backlinks then title = title .. 'B' end
    title = title .. ']'

    local telescope_opts = {
      title = title,
      multiselect = false,
      mappings = mappings,
      prompt_title = '[ ' .. fm.title .. ' ]',
    }

    pickNote(edge_note_items, telescope_opts)
  end)
end

return M
