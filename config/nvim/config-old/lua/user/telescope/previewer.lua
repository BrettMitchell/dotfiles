local conf       = require('telescope.config')
local previewers = require('telescope.previewers')
local makeLines  = require('user.lib.makeLines').makeLines

local HL_GROUP = 'TelescopeResultsConstant'

local function highlight(bufnr, highlights)
  local ns_telescope_entry = vim.api.nvim_create_namespace('telescope_entry')

  for _, row in ipairs(row) do
    for _, highlight in ipairs(highlights) do
      vim.api.nvim_buf_add_highlight(
        bufnr,
        ns_telescope_entry,
        HL_GROUP,
        row,
        highlight.hl_start,
        highlight.hl_end
      )
    end
  end
end

local function makeTextPreviewer(opts)
  opts = opts or {}
  local getText = opts.getText or function(_, entry) return vim.inspect(entry) end
  -- local getTitle = opts.getTitle or function() return 'File' end
  local highlight = opts.highlight or function(_, _, _, _) end

  return previewers.new_buffer_previewer({
    define_preview = function(self, entry)
      local text, highlights = getText(self, entry) or ''
      local lines = makeLines(text)

      vim.api.nvim_buf_set_lines(self.state.bufnr, 0, -1, false, lines)

      -- highlight(self, entry, text, preCalculatedHighlights)
    end
  })
end

local function makeFilePreviewer(getFilePath, getTitle)
  getTitle = getTitle or function() return 'File' end

  return previewers.new_buffer_previewer({
    define_preview = function(self, entry)
      conf.values.buffer_previewer_maker(
        getFilePath(entry),
        self.state.bufnr,
        { bufname = getTitle(entry) }
      )
    end
  })
end

return {
  makeFilePreviewer = makeFilePreviewer,
  makeTextPreviewer = makeTextPreviewer,
}
