--[[

Feature design:

  Find by tag - Form 1:
    - Open telescope prompt with all tags
    - Each tag's preview contains a list of the files that contain it
    - Selecting a tag opens the tag's representative note, creating it if necessary
    - Pressing M-q populates the quickfix list with the referring files for all selected tags
    - Pressing M-y yanks a list of the referring files for all selected tags
    - Pressing M-CR opens a new telescope prompt with the referring files for all selected tags. This picker operates like a normal file picker.

  Find by tag - Form 2:
    - Open telescope prompt with all taggable files
    - The search term fuzzy finds over the available tags in the system, but each result item is an individual file
    - This provides a scrollable preview of each individual file from the primary view
    - Pressing M-q populates the quickfix list with all selected files
    - Pressing M-y yanks the filenames of all selected files
    - Selecting a file opens it at the location of the matched tag

  Explore backlinks:
    - Utilize emanote for this, not sure how yet
    - Populate a telescope prompt with the names of all files that contain links to the currently opened file
    - The entries in this list behave exactly like those in Find by tag - Form 2

--]]

local has_lunajson, lunajson = pcall(require, 'lunajson')
if not has_lunajson then
  error 'Emanote telescope integration requires lunajson from LuaRocks'
end

local helpers = require('user.emanote_integration.helpers')

local actions      = require 'telescope.actions'
local action_state = require 'telescope.actions.state'
-- local builtin      = require 'telescope.builtin'
local finders      = require 'telescope.finders'
local pickers      = require 'telescope.pickers'
local previewers   = require 'telescope.previewers'
local conf         = require 'telescope.config'.values

local M = {}

function M.getFilesFromPickedTagEntries(tagEntries)
  local promptTitle = 'Files tagged with: '
  local foundFiles = {}
  local allFiles = {}

  for i, entry in ipairs(tagEntries) do
    if i == 1 then
      promptTitle = promptTitle .. entry.value.tagName
    elseif i == #tagEntries then
      promptTitle = promptTitle .. ', or ' .. entry.value.tagName
    else
      promptTitle = promptTitle .. ', ' .. entry.value.tagName
    end

    for _, filename in ipairs(entry.value.files) do
      if not foundFiles[filename] then
        table.insert(allFiles, filename)
        foundFiles[filename] = true
      end
    end
  end

  return allFiles, promptTitle
end

function M.pickFromFileSubset(fileList, opts)
  opts = opts or {}

  pickers.new(opts, {
    prompt_title = opts.prompt_title or 'Select files',
    sorter = conf.generic_sorter(opts),
    finder = finders.new_table {
      results = fileList,
    },
  }):find()
end

function M.getTagsInCwd()
  local command = 'emanote export'
  local commandHandle = io.popen(command)
  if not commandHandle then
    error('Could not execute command: ' .. command)
  end

  local jsonOutput = commandHandle:read('*a')
  local parsed = lunajson.decode(jsonOutput)

  local filesWithFrontmatter = {}
  for _, fileWithFrontmatter in pairs(parsed.files) do
    local tags = {}
    if fileWithFrontmatter.meta and fileWithFrontmatter.meta.tags then
      tags = fileWithFrontmatter.meta.tags
    end

    table.insert(filesWithFrontmatter, {
      filename = fileWithFrontmatter.filePath,
      tags = tags,
    })
  end

  local tags = {}
  for _, file in ipairs(filesWithFrontmatter) do
    for _, tag in ipairs(file.tags) do
      if not tags[tag] then
        tags[tag] = { file.filename }
      else
        table.insert(tags[tag], file.filename)
      end
    end
  end

  local tagList = {}
  for tagName, files in pairs(tags) do
    table.insert(tagList, { tagName = tagName, files = files })
  end

  return tagList
end

-- Default function called when picking a tag from the list.
-- Split in two and exported from the module to make custom keybindings easier to manage.
function M.pickFromFilesForSelectedTags(prompt_bufnr)
  local tagEntries = helpers.getMultiSelection(prompt_bufnr)
  actions.close(prompt_bufnr)

  local files, promptTitle = M.getFilesFromPickedTagEntries(tagEntries)
  M.pickFromFileSubset(files, { prompt_title = promptTitle })
end

function M.getRepresentativeFileForTagName(tagName)
end

function M.openTagNoteFile(prompt_bufnr)
  local tagName = helpers.getSingleSelection(prompt_bufnr)
  actions.close(prompt_bufnr)

  local tagFiles = M.getRepresentativeFileForTagName(tagName)
  M.pickFromFileSubset(tagFiles, { prompt_title = 'Tag representative files' })
end

M.defaultMappings = {
  i = {
    ['<C-CR>'] = M.pickFromFilesForSelectedTags,
    -- ['<C-y>'] = M.copyFilesForSelectedTags,
    -- ['<C-q>'] = M.populateQuickFixWithFilesForSelectedTags,
  }
}

function M.findTags(opts)
  local tagList = M.getTagsInCwd()

  local tagDirectory = (opts or {}).tagDirectory

  pickers.new(opts, {
    prompt_title = 'Tags',
    sorter       = conf.generic_sorter(opts),
    previewer    = previewers.display_content.new(opts),

    finder = finders.new_table {
      results = tagList,
      entry_maker = function(resultItem)
        return {
          value   = resultItem,
          display = resultItem.tagName,
          ordinal = resultItem.tagName,

          preview_command = function(entry, bufnr)
            vim.api.nvim_buf_set_lines(bufnr, 0, -1, true, entry.value.files)
          end
        }
      end
    },

    attach_mappings = function(prompt_bufnr, map)
      local mappings = ((opts or {}).find_tags or {}).mappings or M.defaultMappings

      if type(mappings) ~= 'table' then
        error 'Mappings must be a table of the form "{ [mode] = { [key] = action } }"'
      end

      for mode, mapTargets in pairs(mappings) do
        if type(mapTargets) ~= 'table' then
          error 'Mappings must be a table of the form "{ [mode] = { [key] = action } }"'
        end

        for key, action in pairs(mapTargets or {}) do
          map(mode, key, action)
        end
      end

      actions.select_default:replace(M.openTagNoteFile)

      return true
    end
  }):find()
end

return M
