local feat_util = require('user.util.feature')
local lazy = require('user.util.plugins.lazy')
local keymap = require('user.util.keymap')

local luasnip = {
  name = 'LuaSnip',

  description = [[
  This feature adds the basic configuration required for snippets.

  It does not add any snippets itself. Snippets may be added by
  registering a directory with this feature using the `paths` config
  property, or by enabling a snippet preset feature.

  Set the `show_node_indicator` option to show a customizable virtual
  text icon indicating LuaSnip node type.
  ]],

  load_formats = {
    'snipmate',
    'vscode',
    'lua',
  },

  luasnip_choice_config = {
    auto_open = true
  },

  show_node_indicator = {
    enable = false,

    choice_node = {
      icon = '●',
      hl = 'LuaSnipChoiceNode',
    },

    insert_node = {
      icon = '●',
      hl = 'LuaSnipInsertNode',
    }
  },

  paths = {},

  keymap = keymap {
    {
      { 'i', 's' },
      '<C-n>',
      function() require('luasnip').jump(1) end,
      { silent = true, desc = 'Next snippet node' },
    },

    {
      { 'i', 's' },
      '<C-p>',
      function() require('luasnip').jump(-1) end,
      { silent = true, desc = 'Previous snippet node' },
    }
  },

  expand = function(args)
    require('luasnip').lsp_expand(args.body)
  end,

  source_definitions = {
    cmp_luasnip = { name = 'luasnip' },
    cmp_luasnip_choice = { name = 'luasnip_choice' },
  },

  snippet_builders = {},
}

function luasnip.add_snippet_builder(fn)
  table.insert(luasnip.snippet_builders, fn)
end

function luasnip.setup(user_config)
  feat_util.apply_config(user_config, luasnip)

  luasnip.keymap.apply()

  lazy.add_plugins({
    {
      'https://github.com/L3MON4D3/LuaSnip',
      -- install jsregexp
      build = "make install_jsregexp",
      event = 'VeryLazy',
      config = function()
        luasnip.luasnip_config = luasnip.luasnip_config or {}

        if luasnip.show_node_indicator.enable then
          local types = require('luasnip.util.types')
          local ext_opts = {
            ext_opts = {
              [types.choiceNode] = {
                active = {
                  virt_text = { {
                    luasnip.show_node_indicator.choice_node.icon,
                    luasnip.show_node_indicator.choice_node.hl,
                  } }
                }
              },

              [types.insertNode] = {
                active = {
                  virt_text = { {
                    luasnip.show_node_indicator.insert_node.icon,
                    luasnip.show_node_indicator.insert_node.hl,
                  } }
                }
              }
            }
          }

          luasnip.luasnip_config = vim.tbl_deep_extend('force', luasnip.luasnip_config, ext_opts)
        end

        local ls = require('luasnip')
        ls.config.setup(luasnip.luasnip_config)

        require('luasnip.loaders.from_vscode').lazy_load()
        require('luasnip.loaders.from_vscode').lazy_load({ paths = luasnip.paths })
        require('luasnip.loaders.from_snipmate').lazy_load()
        require('luasnip.loaders.from_snipmate').lazy_load({ paths = luasnip.paths })
        require('luasnip.loaders.from_lua').lazy_load()
        require('luasnip.loaders.from_lua').lazy_load({ paths = luasnip.paths })

        for _, snippet_builder in ipairs(luasnip.snippet_builders) do
          snippet_builder(ls)
        end
      end
    },

    {
      -- TODO: Take a look at the keybindings here and see if they need to be incorporated
      -- https://github.com/doxnit/cmp-luasnip-choice/pull/1
      'L3MON4D3/cmp-luasnip-choice',
      event = 'VeryLazy',
      config = function()
        require('cmp_luasnip_choice').setup(luasnip.luasnip_choice_config or {})
      end
    },

    {
      'https://github.com/rafamadriz/friendly-snippets',
      event = 'VeryLazy',
      dependencies = { 'https://github.com/L3MON4D3/LuaSnip' },
      config = function()
        local luasnip_mdl = require("luasnip")
        luasnip_mdl.filetype_extend("typescript", { "tsdoc" })
        luasnip_mdl.filetype_extend("javascript", { "jsdoc" })
        luasnip_mdl.filetype_extend("lua", { "luadoc" })
        luasnip_mdl.filetype_extend("python", { "pydoc" })
        luasnip_mdl.filetype_extend("rust", { "rustdoc" })
        luasnip_mdl.filetype_extend("cs", { "csharpdoc" })
        luasnip_mdl.filetype_extend("java", { "javadoc" })
        luasnip_mdl.filetype_extend("c", { "cdoc" })
        luasnip_mdl.filetype_extend("cpp", { "cppdoc" })
        luasnip_mdl.filetype_extend("php", { "phpdoc" })
        luasnip_mdl.filetype_extend("kotlin", { "kdoc" })
        luasnip_mdl.filetype_extend("ruby", { "rdoc" })
        luasnip_mdl.filetype_extend("sh", { "shelldoc" })
      end
    },

    {
      'https://github.com/saadparwaiz1/cmp_luasnip',
      event = 'VeryLazy',
    },
  })
end

return luasnip
