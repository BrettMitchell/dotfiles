local telescopeActions = require('telescope.actions')

local M = {}

M.insertMode = {
  -- Disable exit to normal mode. All controls should be available within a single mode
  -- Within this setup, telescope is intended to be used for quick searches that don't
  -- really need more editing power than insert mode can provide.

  ['<esc>'] = telescopeActions.close,
  ['<enter>'] = telescopeActions.select_default,

  ['<M-j>'] = telescopeActions.move_selection_next,
  ['<M-k>'] = telescopeActions.move_selection_previous,

  ['<M-S-j>'] = telescopeActions.preview_scrolling_down,
  ['<M-S-k>'] = telescopeActions.preview_scrolling_up,

  ['<C-j>'] = telescopeActions.cycle_history_next,
  ['<C-k>'] = telescopeActions.cycle_history_prev,

  ['<M-q>'] = telescopeActions.send_to_qflist,
}

return M
