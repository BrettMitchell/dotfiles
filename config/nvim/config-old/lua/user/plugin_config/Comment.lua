return {
  config = function ()
      require('Comment').setup {
        ---LHS of toggle mappings in NORMAL mode
        ---@type table
        toggler = {
          ---Line-comment toggle keymap
          line = '<space>fcc',
          ---Block-comment toggle keymap
          block = '<space>fbc',
        },

        ---LHS of operator-pending mappings in NORMAL mode
        ---LHS of mapping in VISUAL mode
        ---@type table
        opleader = {
          ---Line-comment keymap
          line = '<space>fc',
          ---Block-comment keymap
          block = '<space>fb',
        },

        ---LHS of extra mappings
        ---@type table
        extra = {
          ---Add comment on the line above
          above = '<space>fcO',
          ---Add comment on the line below
          below = '<space>fco',
          ---Add comment at the end of line
          eol = '<space>fcA',
        },

        ---Create basic (operator-pending) and extended mappings for NORMAL + VISUAL mode
        ---NOTE: If `mappings = false` then the plugin won't create any mappings
        mappings = {
          ---Operator-pending mapping
          ---Includes `gcc`, `gbc`, `gc[count]{motion}` and `gb[count]{motion}`
          ---NOTE: These mappings can be changed individually by `opleader` and `toggler` config
          basic = true,
          ---Extra mapping
          ---Includes `gco`, `gcO`, `gcA`
          extra = true,
        },
      }
  end
}
