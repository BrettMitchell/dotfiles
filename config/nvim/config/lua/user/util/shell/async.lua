local table_util = require('user.util.table')
local co_util = require('user.util.coroutine')

return co_util.async(function(...)
  local args = table_util.pack(...)
  -- local cmd = args[1]
  local cb = args[#args]

  local opts = {}
  if #args == 3 then
    opts = args[2]
  else
    table.insert(args, 2, opts)
  end

  local stdout = {}
  local stderr = {}

  local old_on_stderr = opts.on_stderr or function() end
  opts.on_stderr = function(job_id, data, pipe_t)
    if type(data) ~= 'nil' then
      stderr = table_util.concat(stderr, data)
    end
    old_on_stderr(job_id, data, pipe_t)
  end

  local old_on_stdout = opts.on_stdout or function() end
  opts.on_stdout = function(job_id, data, pipe_t)
    if type(data) ~= 'nil' then
      stdout = table_util.concat(stdout, data)
    end
    old_on_stdout(job_id, data, pipe_t)
  end

  local old_on_exit = opts.on_exit or function() end
  opts.on_exit = function(...)
    old_on_exit(...)
    cb({ stdout = stdout, stderr = stderr })
  end

  vim.fn.jobstart(args[1], args[2])
end)
