{ nixpkgs, ... }:

let
  lock = value: { Value = value; Status = "locked"; };

  rycee-addons = nixpkgs.nur.repos.rycee.firefox-addons;
  buildFirefoxXpiAddon = rycee-addons.buildFirefoxXpiAddon;

  lastpass = buildFirefoxXpiAddon {
    pname = "lastpass-password-manager";
    addonId = "support@lastpass.com";
    version = "4.133.0";
    url = "https://addons.mozilla.org/firefox/downloads/file/4343091/lastpass_password_manager-4.133.0.xpi";
    sha256 = "4512122651cef8d8597042a49464399099c2bf1fd79c4d4390a36de7dd87483b";
    meta = {};
  };

  tree-style-tab = buildFirefoxXpiAddon {
    pname = "tree-style-tab";
    addonId = "treestyletab@piro.sakura.ne.jp";
    version = "4.0.18";
    url = "https://addons.mozilla.org/firefox/downloads/file/4302180/tree_style_tab-4.0.18.xpi";
    sha256 = "4e619c4d7ff604a93db483ad66d4b0a9ef5832255541115c0e9d280608ab8f76";
    meta = {};
  };

  tst-more-tree-commands = buildFirefoxXpiAddon {
    pname = "tst-more-tree-commands";
    addonId = "tst-more-tree-commands@piro.sakura.ne.jp";
    version = "1.5.1";
    url = "https://addons.mozilla.org/firefox/downloads/file/4255633/tst_more_tree_commands-1.5.1.xpi";
    sha256 = "698bb52891454c18af3ebf4803546f18d990206d099f4f3721a1e65c91120fca";
    meta = {};
  };

  tst-lock-tree-collapsed = buildFirefoxXpiAddon {
    pname = "tst-lock-tree-collapsed";
    addonId = "tst-lock-tree-collapsed@piro.sakura.ne.jp";
    version = "1.4.3";
    url = "https://addons.mozilla.org/firefox/downloads/file/4259052/tst_lock_tree_collapsed-1.4.3.xpi";
    sha256 = "d7e25f5407ca1b9e57d3eca88fd9e7762805ed27c306377c5b8afc7f2570f796";
    meta = {};
  };

  tst-open-bookmarks-as-partial-tree = buildFirefoxXpiAddon {
    pname = "tst-open-bookmarks-as-partial-";
    addonId = "tst-open-bookmarks-as-partial-tree@piro.sakura.ne.jp";
    version = "1.0.10";
    url = "https://addons.mozilla.org/firefox/downloads/file/4300215/tst_open_bookmarks_as_partial-1.0.10.xpi";
    sha256 = "b66943d5f0b828cbbf01f85d21e9975483d1bb125d86d0017ba74ae49a092927";
    meta = {};
  };

  tab-unload-for-tree-style-tab = buildFirefoxXpiAddon {
    pname = "tab-unload-for-tree-style-tab";
    addonId = "{7aa0a466-58f8-427b-8cd2-e94645c4edc2}";
    version = "6.16";
    url = "https://addons.mozilla.org/firefox/downloads/file/4250340/tab_unload_for_tree_style_tab-6.16.xpi";
    sha256 = "39cff2d0811695b2c9ac16f4ded001bbb4e2e237becb64d23774548183786dde";
    meta = {};
  };

  clearurls = buildFirefoxXpiAddon {
    pname = "clearurls";
    addonId = "{74145f27-f039-47ce-a470-a662b129930a}";
    version = "1.26.1";
    url = "https://addons.mozilla.org/firefox/downloads/file/4064884/clearurls-1.26.1.xpi";
    sha256 = "e20168d63cb1b8ba3ad0de4cdb42c540d99fe00aa9679b59f49bccc36f106291";
    meta = {};
  };

  canvasblocker = buildFirefoxXpiAddon {
    pname = "canvasblocker";
    addonId = "CanvasBlocker@kkapsner.de";
    version = "1.10.1";
    url = "https://addons.mozilla.org/firefox/downloads/file/4262820/canvasblocker-1.10.1.xpi";
    sha256 = "dae3b648f0b559b8b08cdad8adaaba2fcde3aa7baf0ffe9b2cbca5a3373c98b7";
    meta = {};
  };

  decentraleyes = buildFirefoxXpiAddon {
    pname = "decentraleyes";
    addonId = "jid1-BoFifL9Vbdl2zQ@jetpack";
    version = "2.0.19";
    url = "https://addons.mozilla.org/firefox/downloads/file/4255788/decentraleyes-2.0.19.xpi";
    sha256 = "105d65bf8189d527251647d0452715c5725af6065fba67cd08187190aae4a98f";
    meta = {};
  };

  enhancer-for-youtube = buildFirefoxXpiAddon {
    pname = "enhancer-for-youtube";
    addonId = "enhancerforyoutube@maximerf.addons.mozilla.org";
    version = "2.0.125.1";
    url = "https://addons.mozilla.org/firefox/downloads/file/4304694/enhancer_for_youtube-2.0.125.1.xpi";
    sha256 = "bc3fdd06b1d701f2c6d5c07c5475a852c4703de62129e4cd7e999678b115df22";
    meta = {};
  };

  downthemall = buildFirefoxXpiAddon {
    pname = "downthemall";
    addonId = "{DDC359D1-844A-42a7-9AA1-88A850A938A8}";
    version = "4.12.1";
    url = "https://addons.mozilla.org/firefox/downloads/file/4228862/downthemall-4.12.1.xpi";
    sha256 = "4e652c23da2560d02246afa3eeeee442b6de3dddb1ee3ce10aaa214e57e676fd";
    meta = {};
  };

  view-page-archive = buildFirefoxXpiAddon {
    pname = "view-page-archive";
    addonId = "{d07ccf11-c0cd-4938-a265-2a4d6ad01189}";
    version = "6.0.0";
    url = "https://addons.mozilla.org/firefox/downloads/file/4296435/view_page_archive-6.0.0.xpi";
    sha256 = "b9787762aa45a905d93266b5db1203a1eb2b3aec9e6fc9938faee86e8d6e65ad";
    meta = {};
  };

  tridactyl = buildFirefoxXpiAddon {
    pname = "tridactyl-vim";
    addonId = "tridactyl.vim@cmcaine.co.uk";
    version = "1.24.1";
    url = "https://addons.mozilla.org/firefox/downloads/file/4261352/tridactyl_vim-1.24.1.xpi";
    sha256 = "ab63fe1554471c280f234409393172fc58e1bb2ca527f4329d983b028073e19c";
    meta = {};
  };

  new-tab-override = buildFirefoxXpiAddon {
    pname = "new-tab-override";
    addonId = "newtaboverride@agenedia.com";
    version = "16.0.1";
    url = "https://addons.mozilla.org/firefox/downloads/file/4303770/new_tab_override-16.0.1.xpi";
    sha256 = "d61605a80daefa1379881250e33f060b884adedb1a34c7961804003c938374b3";
    meta = {};
  };

  copy-tab-to-clipboard = buildFirefoxXpiAddon {
    pname = "copy-selected-tabs-to-clipboar";
    addonId = "copy-selected-tabs-to-clipboard@piro.sakura.ne.jp";
    version = "1.6.5";
    url = "https://addons.mozilla.org/firefox/downloads/file/4300219/copy_selected_tabs_to_clipboar-1.6.5.xpi";
    sha256 = "46bd79f0ca316c65b4c4339568dbced24e57c873efe2820ddfdf7030e7ab6b97";
    meta = {};
  };
in {
  home.file.".config/tridactyl/tridactylrc".source = ./tridactylrc;

  programs.firefox = {
    enable = true;

    package = nixpkgs.unstable.firefox.override {
      nativeMessagingHosts = [
        # Tridactyl native connector
        nixpkgs.stable.tridactyl-native
      ];
    };

    profiles.advanced = {
      isDefault = true;

      userChrome = builtins.readFile ./userChrome.css;

      search = {
        force = true;
        default = "DuckDuckGo";

        order = [
          "DuckDuckGo"
          "Google"
          "Amazon"
          "Nix Packages"
          "NixOS Wiki"
        ];

        engines = {
          "Nix Packages" = {
            urls = [{
              template = "https://search.nixos.org/packages";
              params = [
                { name = "type"; value = "packages"; }
                { name = "query"; value = "{searchTerms}"; }
              ];
            }];
            definedAliases = [ "!np" ];
          };

          "NixOS Wiki" = {
            urls = [{ template = "https://wiki.nixos.org/index.php?search={searchTerms}"; }];
            iconUpdateURL = "https://wiki.nixos.org/favicon.png";
            updateInterval = 24 * 60 * 60 * 1000; # every day
            definedAliases = [ "!nw" ];
          };

          "Bing".metaData.hidden = true;
          "Google".metaData.alias = "!g";
        };
      };

      extensions = [
        lastpass
        tridactyl

        rycee-addons.ublock-origin
        clearurls
        canvasblocker
        decentraleyes

        enhancer-for-youtube
        downthemall
        view-page-archive
        new-tab-override

        tree-style-tab
        tst-more-tree-commands
        tst-lock-tree-collapsed
        tst-open-bookmarks-as-partial-tree
        tab-unload-for-tree-style-tab
      ];
    };

    policies = {
      Preferences = { 
        "browser.contentblocking.category" = lock "strict";
        "extensions.pocket.enabled" = lock false;
        "extensions.screenshots.disabled" = lock true;

        "browser.search.suggest.enabled" = lock false;
        "browser.search.suggest.enabled.private" = lock false;
        "browser.urlbar.suggest.searches" = lock false;
        "browser.urlbar.showSearchSuggestionsFirst" = lock false;

        "signon.rememberSignons" = lock false;
        "browser.formfill.enable" = lock false;

        "privacy.resistFingerprinting" = lock true;
        "privacy.bounceTrackingProtection.enabled" = lock true;

        "toolkit.legacyUserProfileCustomizations.stylesheets" = lock true;

        # Enable extensions on install
        "extensions.autoDisableScopes" = lock true;

        # TODO: Figure out why this doesn't actually apply when generating firefox config
        "identity.fxaccounts.enabled" = lock false;
        "identity.fxaccounts.toolbar.enabled" = lock false;

        # Disable newtab content completely for new tabs and new windows
        # Update this manually once per install
        # "browser.startup.homepage" = "moz-extension://_/static/newtab.html";
        "browser.newtabpage.enabled" = lock false;
        "browser.newtabpage.activity-stream.feeds.newtabinit" = lock false;
      };
    };
  };
}
