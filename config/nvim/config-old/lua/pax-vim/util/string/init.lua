return {
  repeat_str = require('pax-vim.util.string.repeat_str'),
  split = require('pax-vim.util.string.split'),
  trim = require('pax-vim.util.string.trim'),
  starts_with = require('pax-vim.util.string.starts_with'),
  ends_with = require('pax-vim.util.string.ends_with'),
}
