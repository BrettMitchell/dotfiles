local TAGS = require('user.features.notes.special_tags')

local M = {}

M.webResource = {
  frontmatter = function(existingFrontmatter, _, cb)
    return cb(nil, {
      title = 'Web resource :: RESOURCE_NAME',
      tags = { TAGS.WEB_RESOURCE },
      url = existingFrontmatter.url or 'URL',
      bindings = {
        [';;o'] = {
          lua = "vim.fn['netrw#BrowseX'] (('user.notes.frontmatter').parseFrontmatter().url, 1)"
        },
      }
    })
  end,
  build = function(existingBody, _, cb) return cb(nil, existingBody) end
}

return M
