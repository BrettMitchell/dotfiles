local telescope = require('user.telescope')
local registerKeyMap = require('user.lib.map').registerKeyMap

local M = {}

function M.setup()
  -- Telescope
  registerKeyMap({ 'n', 'v' }, {
    ['<space>t'] = {
      -- f = { function() telescope.cwdFilesByName(true) end, 'Fuzzy search for project git files by name' },
      -- F = { function() telescope.cwdFilesByName(false) end, 'Fuzzy search for all project files by name' },
      -- r = { function() telescope.frecency() end, 'Fuzzy find recent files' },
      -- c = { function() telescope.commands() end, 'Fuzzy find commands' },
      -- j = { function() telescope.jump_list() end, 'Fuzzy find in jump list' },

      l = { function() telescope.currentFileLspSymbols() end, 'Search document symbols' },
      f = { function() telescope.currentFileContents() end, 'Fuzzy search in current file' },
      F = { function() telescope.cwdFilesByContent() end, 'Fuzzy search within project files' },
      B = { function() telescope.buffers() end, 'Fuzzy find open buffer' },
      h = { function() telescope.help() end, 'Fuzzy find help' },
      m = { function() telescope.keymaps() end, 'Fuzzy find keymaps and their descriptions' },

      c = { function() telescope.gitHistory() end, 'Search through commits in the current project' },
      C = { function() telescope.currentFileGitHistory() end, 'Search through commits made against the current file' },
      b = { function() telescope.gitBranches() end, 'Search through commits made against the current file' },
      s = { function() telescope.gitStatus() end, 'Search through active git changes' },

      [':'] = { function() telescope.command_history() end, 'Fuzzy find previously executed command' },
    },
    -- TODO: Move this somewhere else, because it's more a part of my git config than my search config
    ['<space>gC'] = { function() telescope.findGitConflicts() end, 'Fuzzy find unresolved merge conflicts' },
  })

  -- If not in notes directory, <C-space> should search over filenames
  if require("zk.util").notebook_root(vim.fn.expand('%:p')) == nil then
    registerKeyMap({ 'n', 'v' }, {
      -- Quicker alternative for accessing files by name
      ['<C-space>'] = { function() telescope.cwdFilesByName(false) end, 'Fuzzy search for project git files by name' },
      -- ['<C-space>'] = { function() telescope.cwdFilesByName(true) end, 'Fuzzy search for project git files by name' },
    })
  end
end

return M
