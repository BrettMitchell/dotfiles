local frontmatter = require('user.notes.frontmatter')
local explore     = require('user.notes.explore.plumbing')
local pickNote    = require('user.notes.telescope').pickNote
local mergeLeft   = require('user.lib.merge').mergeLeft
local fsOps       = require('user.notes.fsOps')

local M           = {}

function M.pickProject(opts)
  opts = mergeLeft({ title = 'Projects' }, opts or {})

  explore.getNotesByTags(
    {
      tags = { 'meta.project.root' },
      select = { 'absPath', 'path', 'title', 'metadata', 'tags', 'rawContent' }
    },

    function(err, notes)
      if (err) then
        vim.notify('Encountered an error while finding projects: ' .. vim.inspect(err), 'error')
        return
      end

      pickNote(notes, opts)
    end)
end

function M.gotoProjectRoot(opts)
  local currentFrontmatter = frontmatter.parseFrontmatter()
  if not currentFrontmatter then return end

  local tags = currentFrontmatter.tags
  local tagQuery = {}
  for _, tag in ipairs(tags) do
    if tag:find('^project%.') then
      table.insert(tagQuery, tag)
    end
  end

  if #tagQuery == 0 then
    vim.notify('Note is not a member of any projects')
    return
  end

  table.insert(tagQuery, 'meta.project.root')

  explore.getNotesByTags({ tags = tagQuery }, function(errByTitle, projectNotes)
    if (errByTitle) then
      vim.notify('Encountered an error while finding project root: ' .. vim.inspect(errByTitle), 'error')
      return
    end

    if #projectNotes == 1 then
      vim.cmd('e ' .. projectNotes[1].absPath)
      return
    end

    pickNote(projectNotes, opts)
  end)
end

function M.pickWithinProject(opts)
  opts = mergeLeft({ title = 'Project peer notes' }, opts or {})

  local currentFrontmatter = frontmatter.parseFrontmatter()
  if not currentFrontmatter then return end

  local tags = currentFrontmatter.tags

  local allContainingProjects = {}
  for _, tag in ipairs(tags) do
    if tag:find('^project%.') then
      table.insert(allContainingProjects, tag)
    end
  end

  if #allContainingProjects == 0 then
    vim.notify('Note is not a member of any projects')
    return
  end

  explore.getNotesByTags({ tags = allContainingProjects }, function(errByTitle, projectNotes)
    if (errByTitle) then
      vim.notify('Encountered an error while finding project root: ' .. vim.inspect(errByTitle), 'error')
      return
    end

    pickNote(projectNotes, opts)
  end)
end

local function createProjectNote(parentNote)
  fsOps.createOrOpen(
    {
      title = parentNote.title .. ' :: TITLE_SUFFIX',
      skipFrontmatterGeneration = false
    },

    function()
      local tags = {}
      for _, tag in ipairs(parentNote.tags) do
        if tag ~= 'meta.project.root' then
          table.insert(tags, tag)
        end
      end

      frontmatter.setAttributes(
        {
          title = parentNote.title .. ' :: TITLE_SUFFIX',
          tags = tags,
        },
        { merge = true }
      )
      vim.cmd('write')
    end
  )
end

local createNotePickerOpts = {
  mappings = {
    select_default = function(selectedParent)
      createProjectNote(selectedParent)
    end
  }
}

function M.newProjectNote()
  local noteFrontmatter = frontmatter.parseFrontmatter()
  if not noteFrontmatter then
    vim.notify('Could not parse note frontmatter', 'error')
    return
  end

  if type(noteFrontmatter.tags) ~= 'table' then
    vim.notify('Invalid frontmatter tags: ' .. vim.inspect(noteFrontmatter), 'error')
    return
  end

  local searchTags = {}
  for _, tag in ipairs(noteFrontmatter.tags) do
    if (tag:find('^project%.')) then
      table.insert(searchTags, tag)
    end
  end

  if #searchTags == 0 then
    vim.notify('Note is not part of a project, please pick a project to add a new note to')
    M.pickProject(createNotePickerOpts)
    return
  end

  table.insert(searchTags, 'meta.project.root')

  explore.getNotesByTags(
    {
      tags = searchTags,
      select = { 'absPath', 'path', 'title', 'metadata', 'tags', 'rawContent' }
    },

    function(err, notes)
      if (err) then
        vim.notify('Encountered an error while finding project root: ' .. vim.inspect(err), 'error')
        return
      end

      if #notes == 0 then
        vim.notify('Could not find project root for current file', 'warn')
        M.pickProject(createNotePickerOpts)
      elseif #notes > 1 then
        vim.notify('More than one containing project found for current note, please pick from among them')
        pickNote(notes, createNotePickerOpts)
      else
        createProjectNote(notes[1])
      end
    end)
end

return M
