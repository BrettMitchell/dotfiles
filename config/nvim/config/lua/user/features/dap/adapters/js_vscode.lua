-- https://github.com/mxsdev/nvim-dap-vscode-js

local feat_util = require('user.util.feature')
local table_util = require('user.util.table')
local plugin_util = require('user.util.plugins.lazy')
local dap = require('user.features.dap')
local adapter_util = require('user.features.dap.adapters.util')

local M = {
  name = "DAP :: Adapter 'vscode-js-debug'",

  description = [[
  This feature adds utilities to aid in the installation and startup of
  the 'vscode-js-debug' debug adapter.
  ]],

  adapter_name = 'pwa-node',
  install_dir = dap.adapter_install_dir .. '/vscode-js-debug',
  ref = 'main',
}

function M.get_dap_config(self)
  return {
    {
      name = 'Launch ' .. M.adapter_name,
      type = M.adapter_name,
      request = 'launch',
      program = '${file}',
      -- cwd = '${workspaceFolder}',
      sourceMaps = true,
      skipFiles = { '<node_internals>/**/*.js' },
    },

    {
      -- For this to work you need to make sure the node process is started with the `--inspect` or `--inspect-brk` flag.
      name = 'Attach ' .. M.adapter_name .. ' - Enter port',
      type = M.adapter_name,
      request = 'attach',
      sourceMaps = true,
      skipFiles = { '<node_internals>/**/*.js' },
      port = function()
        local port = vim.fn.input('Enter a port: ')
        if port:len() == 0 then
          return require('dap').ABORT
        end
        return port
      end
    },

    {
      -- For this to work you need to make sure the node process is started with the `--inspect` or `--inspect-brk` flag.
      name = 'Attach ' .. M.adapter_name .. ' - Pick port',
      type = M.adapter_name,
      request = 'attach',
      sourceMaps = true,
      port = function()
        return coroutine.create(function(dap_run_co)
          adapter_util.pick_listening_process('node', function(proc_record)
            local port = require('dap').ABORT
            if proc_record ~= nil then
              port = proc_record.port
            end
            coroutine.resume(dap_run_co, port)
          end)
        end)
      end,
      skipFiles = { '<node_internals>/**/*.js' },
    },
  }
end

function M.init()
  local _dap = require('dap')
  _dap.configurations.javascript = table_util.concat(
    _dap.configurations.javascript or {},
    M.get_dap_config()
  )
end

function M.setup(config)
  feat_util.assert_deps('DAP Adapter vscode-js-debug', { dap })
  feat_util.apply_config(config, M)
  if not M.install_dir then
    M.install_dir = dap.adapter_install_dir .. '/' .. M.adapter_name
  end
  dap.methods.add_installable(M.adapter_name, M.install)

  plugin_util.add_plugins({
    {
      'https://github.com/mxsdev/nvim-dap-vscode-js',
      event = 'VeryLazy',
      config = function()
        require('dap-vscode-js').setup({
          debugger_path = M.install_dir,
          adapters = { 'pwa-node' },
          -- adapters = { 'pwa-node', 'pwa-chrome', 'pwa-msedge', 'node-terminal', 'pwa-extensionHost' },
        })
      end
    }
  })

  plugin_util.add_post_load_cb(function()
    M.init()
  end)
end

function M.remove(opts)
  opts = opts or {}

  if not M.install_dir then
    vim.notify("Cannot remove directory '" .. M.install_dir .. "'", 'error')
    return
  end

  if not opts.no_confirm then
    local confirmation = vim.fn.input("Are you sure you want to permanantly delete the directory '" ..
      M.install_dir .. "'? (y/n) : ")
    if not table_util.includes(confirmation, { 'y', 'Y' }) then
      vim.notify('Aborted by user')
      return
    end
  end

  vim.notify("Removing directory '" .. M.install_dir .. "'")
  vim.system({ 'rm', '-rf', M.install_dir }):wait()
end

function M.install()
  local shell = os.getenv('SHELL')

  if (vim.fn.isdirectory(M.install_dir) == 1) then
    vim.notify('https://github.com/microsoft/vscode-js-debug.git already installed to ' .. M.install_dir, 'info')
    return
  end

  vim.notify('Installing vscode-js-debug adapter: ' .. vim.inspect({ shell = shell, installDir = M.install_dir }))
  dap.hooks.do_hook('pre_install', { name = M.adapter_name })

  -- Source: https://github.com/mxsdev/nvim-dap-vscode-js?tab=readme-ov-file#manually
  local install_script = ([[
      git clone https://github.com/microsoft/vscode-js-debug '%s';
      cd '%s';
      git checkout '%s';
      npm install --legacy-peer-deps \
        && npx gulp vsDebugServerBundle \
        && mv dist out;
    ]]):format(M.install_dir, M.install_dir, M.ref)
  local res = vim.system({ shell, '-c', install_script }):wait()
  if res.stderr and #res.stderr then
    vim.notify(res.stderr, 'error')
  end
  if res.stdout and #res.stdout then
    vim.notify(res.stdout, 'info')
  end

  vim.notify('Finished installing vscode-js-debug adapter')
  dap.hooks.do_hook('post_install', { name = M.adapter_name })
end

return M
