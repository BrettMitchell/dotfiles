{ config, lib, pkgs, nixpkgs, ... }:
{
  nix.settings = {
    experimental-features = [ "nix-command" "flakes" ];
    auto-optimise-store = true;
  };

  imports =
    [
      ./hardware-configuration.nix
      ../shared/audio.nix
      ../shared/bluetooth.nix
      ../shared/bootloader.nix
      ../shared/console.nix
      ../shared/wifi.nix
    ];

  # hint electron apps to use wayland:
  # TODO: Move this to hyprland/swayfx config
  environment.sessionVariables.NIXOS_OZONE_WL = "1";

  # This is not at all what I want. I'd much prefer to have this stuff configured at the user level
  virtualisation.containers.enable = true;
  virtualisation = {
    docker.enable = true;
    podman = {
      enable = true;

      # Required for containers under podman-compose to be able to talk to each other.
      defaultNetwork.settings.dns_enabled = true;
    };
  };

  # Set your time zone.
  time.timeZone = "America/New_York";

  documentation.man = {
    enable = true;
    generateCaches = true;
  };

  programs.zsh.enable = true;

  nixpkgs.config.allowUnfree = true;
  environment.systemPackages = with pkgs; [
    wget
    curl
    nvi # Original Vi

    git
    gnumake
    gcc
    killall
    brightnessctl
    coreutils-full
  ];

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.brettm = {
    isNormalUser = true;
    extraGroups = [
      "wheel"
      "networkmanager"
      "network"
      "audio"
      "docker"
      "podman"
      "libvirtd"
    ];
    shell = pkgs.zsh;
  };

  # Required by wlroots/sway
  hardware.graphics.enable = true;

  # Enable sound.
  # sound.enable = true;
  # hardware.pulseaudio.enable = true;

  # Select internationalisation properties.
  # i18n.defaultLocale = "en_US.UTF-8";
  # console = {
  #   font = "Lat2-Terminus16";
  #   keyMap = "us";
  #   useXkbConfig = true; # use xkb.options in tty.
  # };

  # Enable the X11 windowing system.
  # services.xserver.enable = true;

  # Configure keymap in X11
  # services.xserver.xkb.layout = "us";
  # services.xserver.xkb.options = "eurosign:e,caps:escape";

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # system.copySystemConfiguration = true;

  # This option defines the first version of NixOS you have installed on this particular machine,
  # and is used to maintain compatibility with application data (e.g. databases) created on older NixOS versions.
  #
  # Most users should NEVER change this value after the initial install, for any reason,
  # even if you've upgraded your system to a new NixOS release.
  #
  # This value does NOT affect the Nixpkgs version your packages and OS are pulled from,
  # so changing it will NOT upgrade your system.
  #
  # This value being lower than the current NixOS release does NOT mean your system is
  # out of date, out of support, or vulnerable.
  #
  # Do NOT change this value unless you have manually inspected all the changes it would make to your configuration,
  # and migrated your data accordingly.
  #
  # For more information, see `man configuration.nix` or https://nixos.org/manual/nixos/stable/options#opt-system.stateVersion .
  system.stateVersion = "23.11"; # Did you read the comment?
}

