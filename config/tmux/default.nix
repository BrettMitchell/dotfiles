{ externalPackages, ... }:
{
  home.file.".config/tmux/tmux.conf".source = ./tmux.conf;
  home.file.".config/tmux/background.conf".source = ./background.conf;
  home.file.".tmux/plugins/tpm".source = externalPackages.tmux-tpm;
}
