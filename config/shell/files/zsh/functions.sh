
source "$HOME/.config/skim/keybindings.sh"
source "$HOME/.config/skim/colors.sh"

# Directory list and tree

alias ls='eza'
alias la='eza -la'
alias ll='eza -l'

lfcd () {
  cd "$(command lf -print-last-dir "$@")"
}

up () {
  local num="$1"
  if [[ -z "$1" ]]; then
    num=1
  fi

  if ! is-integer "$num" ; then
    printf '%s\n' "up : Given parameter is not an integer" > /dev/stderr
    return 1
  fi

  local p='..'
  if [[ "$num" -gt 1 ]] ; then
    for i in {1..$num..1}; do
      p="../$p"
    done
  fi

  cd "$p"
}

# Skim

alias skim='sk --color="$SK_COLORS" --bind="$SKIM_KEYBINDINGS_MOTION"'

