#!/usr/bin/env bash

rm_safe "$HOME/.config/tridactyl/tridactylrc"

firefoxDir=`ls $HOME/.mozilla/firefox | grep default-release`
userChromeDir="$HOME/.mozilla/firefox/$firefoxDir/chrome"
rm_safe "$userChromeDir/userChrome.css"
