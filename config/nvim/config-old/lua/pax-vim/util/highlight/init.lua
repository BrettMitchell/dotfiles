-- Either pass a table containing highlight group attributes,
-- or pass a name of another highlight group to link to.
--
-- E.g.
-- hl('Visual', { fg = '#000000', bg = '#ffffff' })
-- hl('Search', 'IncSearch')
return function (group, hl_spec)
  hl_spec = hl_spec or {}
  if type(hl_spec) == 'table' then
    vim.cmd(('highlight %s gui=%s guifg=%s guibg=%s guisp=%s'):format(
      group,
      hl_spec.style or 'NONE',
      hl_spec.fg or 'NONE',
      hl_spec.bg or 'NONE',
      hl_spec.sp or 'NONE'
    ))
  elseif type(hl_spec) == 'string' then
    vim.cmd(('highlight! link %s %s'):format(
      group,
      hl_spec
    ))
  end
end
