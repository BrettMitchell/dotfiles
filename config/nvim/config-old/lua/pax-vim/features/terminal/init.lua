local make_feature = require('pax-vim.util.feature').make_feature

local terms = {}

local function makeTerminal(opts)
  local Terminal = require('FTerm.terminal')

  opts = opts or {}
  if not opts.name then
    vim.notify('Integrated Terminal :: makeTerminal :: Must supply a terminal name')
    return
  end

  local iface = { name = opts.name }

  if terms[iface.name] ~= nil then
    vim.notify('Integrated Terminal :: makeTerminal :: Terminal ' .. iface.name .. ' already exists')
    return
  end

  function iface.init(self)
    local ft = opts and opts.ft or 'term-' .. self.name
    self.term = Terminal:new()
    self.term:setup {
      ft = ft,
      cmd = opts.cmd
    }
    terms[#terms + 1] = self.term
  end

  function iface.isOpen(self)
    return self.term
  end

  function iface.toggle(self)
    if not self.term then
      vim.notify('Terminal not initialized')
      return
    end

    self.term:toggle()
  end

  function iface.open(self)
    if not self.term then
      iface:init()
    end

    self.term:open()
  end

  function iface.close(self)
    if not self.term then
      return
    end

    self.term:close()
  end

  function iface.sendKeys(self, keys)
    if not self.term then
      vim.notify('Terminal ' .. self.name .. ' not open')
      return
    end

    -- Use custom send to channel instead of term:run
    -- because term:run appends a <CR> termcode
    vim.api.nvim_chan_send(
      self.term.terminal,
      keys
    )
  end

  function iface.focus(self)
    local win = self.term.win
    if win and vim.api.nvim_win_is_valid(win) then
      vim.api.nvim_set_current_win(win)
      vim.cmd 'startinsert'
    end
  end

  terms[iface.name] = iface

  return iface
end

local function getTerminal(name)
  return terms[name]
end

local function getActiveTerminal()
  local Utils = require('FTerm.utils')

  for _, term in pairs(terms) do
    if Utils.is_win_valid(term.term.win) then
      return term
    end
  end

  return nil
end

local function closeAll()
  for _, term in pairs(terms) do
    return term:close()
  end
end

local config = {
  fterm_config = {}
}

local M = make_feature({
  name = 'Integrated Terminal',

  description = [[
  This feature exposes an interface for creating programmable floating
  terminal instances.
  ]],

  plugins = {
    {
      'https://github.com/numToStr/FTerm.nvim',
      config = function ()
        require('FTerm').setup(config.fterm_config or {})
      end
    }
  },

  config = config
})

M.methods = {
  makeTerminal = makeTerminal,
  getTerminal = getTerminal,
  getActiveTerminal = getActiveTerminal,
  closeAll = closeAll,
}

return M
