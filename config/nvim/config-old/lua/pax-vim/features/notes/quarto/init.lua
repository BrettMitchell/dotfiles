local feat_util = require('pax-vim.util.feature')
local tmux_util = require('pax-vim.util.tmux')
local table_util = require('pax-vim.util.table')
local add_plugins = require('pax-vim.util.plugins.lazy').add_plugins
local keymap = require('pax-vim.util.keymap')
local augroup = require('pax-vim.util.autocmd').make_augroup
local send_text = require('pax-vim.features.repl.send_text')

local M = {
  feature = {
    name = 'Quarto',

    description = [[
    This feature adds support for quarto syntax, compilation, and preview functionality
    while editing Markdown files in Neovim.
    ]],

    completion_source = { name = 'otter' },

    close_preview_on_exit = true,
    preview_port = 4200,
    preview_browser_cmd = nil,

    hover_opts = {},
    languages = { 'r', 'python', 'julia', 'bash', 'lua', 'html', 'css' },
    completion = true,
    diagnostics = true,
    tsquery = [[
      (fenced_code_block
      (info_string
        (language) @_lang
      ) @info
        (#match? @info "{")
      (code_fence_content) @content (#offset! @content)
      )
      ((html_block) @html @combined)

      ((minus_metadata) @yaml (#offset! @yaml 1 0 -1 0))
      ((plus_metadata) @toml (#offset! @toml 1 0 -1 0))
    ]],
  }
}

function M.setup(config)
  feat_util.apply_config(config or {}, M.feature)

  add_plugins({
    {
      'https://github.com/jmbuhr/otter.nvim',
      config = function()
        local otter_handlers = require('otter.tools.handlers')
        otter_handlers.hover = function(_, result)
          local util = require('vim.lsp.util')
          local hover = require('hover.actions')

          local lines = util.convert_input_to_markdown_lines(result.contents)
          lines = util.trim_empty_lines(lines)

          if not vim.tbl_isempty(lines) then
            hover.run_provider({
              name = 'Passthrough',
              execute = function()
                return { lines = lines, filetype = 'markdown' }
              end
            })
          end
        end
      end
    }
  })

  M.feature.quarto_keymap.apply()

  local otter_keymap = keymap {
    { 'n', 'gS',        function() require 'otter'.ask_document_symbols() end, { silent = true } },
    { 'n', 'gdd',       function() require 'otter'.ask_definition() end,       { silent = true } },
    { 'n', 'gDD',       function() require 'otter'.ask_type_definition() end,  { silent = true } },
    { 'n', 'gr',        function() require 'otter'.ask_references() end,       { silent = true } },
    { 'n', '<space>r',  function() require 'otter'.ask_rename() end,           { silent = true } },
    { 'n', '<space>ef', function() require 'otter'.ask_format() end,           { silent = true } },

    {
      'n',
      '<space>cf',
      function()
        local text = require('otter.keeper').get_language_lines(true)
        if text then
          send_text.send(table_util.join(text, '\n'))
        end
      end,
      { silent = true, desc = 'Send all code blocks for current language to REPL' }
    },

    {
      'n',
      '<space>li',
      function()
        require('otter').ask_hover()
      end,
      { silent = true },
    },
  }

  local assign_keybindings_autocmd = augroup {
    name = 'AssignOtterKeybindings',
    autocmds = { {
      events = { 'FileType', 'WinEnter' },
      opts = {
        callback = function()
          if vim.bo.filetype == 'quarto' then
            local ok = pcall(
              require('otter').activate,
              M.feature.languages,
              M.feature.completion,
              M.feature.diagnostics,
              M.feature.tsquery
            )

            if ok then
              otter_keymap.apply(vim.fn.bufnr('%'))
            end
          end
        end
      }
    } }
  }

  assign_keybindings_autocmd:apply()
end

function M.start_preview(opts)
  opts = opts or {}
  local args = opts.args or {}

  local yaml = require('lyaml')
  local util = require("lspconfig.util")

  -- find root directory / check if it is a project
  local buffer_path = vim.api.nvim_buf_get_name(0)
  local root_dir = util.root_pattern("_quarto.yml")(buffer_path)

  local port = M.feature.preview_port
  local config_file = (root_dir or '') .. '/_quarto.yml'
  if root_dir and vim.fn.filereadable(config_file) then
    local config_text = vim.fn.readfile(config_file)
    local joined = ''
    for _, line in ipairs(config_text) do
      joined = joined .. '\n' .. line
    end
    local config = yaml.load(joined)
    if config and config.project and config.project.preview and config.project.preview.port then
      port = config.project.preview.port
    end
  end

  local url = 'http://localhost:' .. port
  local cmd = { 'quarto', 'preview', '--no-browser', '--port', port }

  local mode = 'project'
  if not root_dir then
    mode = 'file'
    -- ??? This was in the original quarto code, but I don't use Windows, so I
    -- don't know if the extra escaped double quotes are even necessary with
    -- the command in table form.
    if vim.loop.os_uname().sysname == "Windows_NT" then
      cmd = table_util.concat(cmd, { '\\"' .. buffer_path .. '\\"' })
    else
      cmd = table_util.concat(cmd, { buffer_path })
    end
  end

  cmd = table_util.concat(cmd, args)

  local file_extension = buffer_path:match("^.+(%..+)$")
  if mode == "file" and not file_extension then
    vim.notify("Not in a file, exiting")
    return
  end

  if tmux_util.running_in_tmux() then
    M.preview_tmux = tmux_util.run_new_window(cmd, root_dir or vim.fn.getcwd())
  else
    M.preview_job_id = vim.fn.jobstart(cmd)
  end

  if url and M.feature.preview_browser_cmd then
    local replaced_browser_cmd = {}
    for _, segment in ipairs(M.feature.preview_browser_cmd) do
      local replaced_segment = segment:gsub('{}', url)
      table.insert(replaced_browser_cmd, replaced_segment)
    end
    M.browser_job_id = vim.fn.jobstart(replaced_browser_cmd)
  end

  if M.feature.close_preview_on_exit then
    vim.api.nvim_create_autocmd({ "QuitPre" }, {
      callback = function()
        M.stop_preview()
      end
    })
  end
end

function M.stop_preview()
  if tmux_util.running_in_tmux() and M.preview_tmux then
    tmux_util.kill_pane(M.preview_tmux.pane_id)
    M.preview_tmux = nil
  else
    if M.preview_job_id then
      vim.fn.jobstop(M.preview_job_id)
    end
    if M.browser_job_id then
      vim.fn.jobstop(M.browser_job_id)
    end

    M.preview_job_id = nil
    M.browser_job_id = nil
  end
end

function M.preview_running()
  return M.preview_job_id ~= nil or M.preview_tmux ~= nil
end

function M.toggle_preview()
  if M.preview_running() then
    M.stop_preview()
  else
    M.start_preview()
  end
end

function M.open_preview()
end

function M.render()
end

M.feature.quarto_keymap = keymap {
  toggle_preview = { 'n', '<space>upq', M.toggle_preview, { desc = 'Toggle Quarto preview' } },
  -- open_preview = {},
  -- render = {},
}

return M
