-- [[
--
-- This file exists to declare values which are shared among nvim configurations.
--
-- ]]

return {
  PLUGIN_DIR = '',
  DAP_INSTALL_DIR = '',
  BIN_INSTALL_DIR = '',
}

