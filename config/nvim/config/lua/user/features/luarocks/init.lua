local lazy = require('user.util.plugins.lazy')

local M = {
  name = 'LuaRocks Support',

  description = [[
  This feature enables LuaRocks support anywhere in your neovim config.

  This is necessary as a standalone feature if you wish to use LuaRocks
  because lazy.nvim does not support it.

  This requires that you install luarocks manually (I recommend 'mise' for
  this.)

  Requires that you set the 'luarocks_install_path' configuration property.
  This value can be found by running `luarocks config` and looking for
  the 'deploy_lua_dir' value in the command's output.

  Specific rocks can be required for your nvim configuration with the
  'ensure_installed' config property. Missing rocks will be shown in a
  notification message on startup.
  ]],

  luarocks_install_path = nil,
  ensure_installed = {}
}

function M.setup(config)
  if not config.luarocks_install_path then
    vim.notify("Config property 'luarocks_install_path' is required.")
  end

  M.luarocks_install_path = config.luarocks_install_path
  if type(M.luarocks_install_path) == 'function' then
    M.luarocks_install_path = M.luarocks_install_path()
  end
  if type(M.luarocks_install_path) ~= 'nil' and type(M.luarocks_install_path) ~= 'table' then
    M.luarocks_install_path = { M.luarocks_install_path }
  end

  M.luarocks_install_cpath = config.luarocks_install_cpath
  if type(M.luarocks_install_cpath) == 'function' then
    M.luarocks_install_cpath = M.luarocks_install_cpath()
  end
  if type(M.luarocks_install_cpath) ~= 'nil' and type(M.luarocks_install_cpath) ~= 'table' then
    M.luarocks_install_cpath = { M.luarocks_install_cpath }
  end

  M.ensure_installed = config.ensure_installed or M.ensure_installed
  M.quiet = config.quiet or M.quiet

  if M.luarocks_install_path then
    for _, path in pairs(M.luarocks_install_path) do
      package.path = package.path .. ';' .. path .. '/?/?.lua'
      package.path = package.path .. ';' .. path .. '/?.lua'
      package.path = package.path .. ';' .. path .. '/?/init.lua'
    end
  end

  if M.luarocks_install_cpath then
    for _, cpath in pairs(M.luarocks_install_cpath) do
      package.cpath = package.cpath .. ';' .. cpath .. '/?/?.so'
      package.cpath = package.cpath .. ';' .. cpath .. '/?.so'
    end
  end

  local function chunk_output(output)
    return vim.split(output, '\n')
  end

  local function get_installed_luarocks()
    local cmdOut = vim.fn.system({ 'luarocks', 'list', '--porcelain' })
    local outLines = chunk_output(cmdOut)

    local installed = {}
    for _, line in ipairs(outLines) do
      for pkgName in string.gmatch(line, '([^\t]+)\t([^\t]+)\t([^\t]+)\t([^\t]+)') do
        table.insert(installed, pkgName)
      end
    end

    return installed
  end

  lazy.add_post_load_cb(function()
    local installed = get_installed_luarocks()

    local function is_installed(rock)
      for _, installedRock in ipairs(installed or {}) do
        if installedRock == rock then
          return true
        end
      end

      return false
    end

    for _, rock in ipairs(config.ensure_installed) do
      if not is_installed(rock) then
        vim.notify('Required luarock "' .. rock .. '" is not installed, please use luarocks to install')
      end
    end
  end)
end

return M
