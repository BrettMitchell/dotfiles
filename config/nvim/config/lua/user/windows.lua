local keymap = require('user.util.keymap')

local M = {}

local function getTmuxDirection(dir)
  if dir == 'h' then return 'L' end
  if dir == 'j' then return 'D' end
  if dir == 'k' then return 'U' end
  if dir == 'l' then return 'R' end
end

-- This allows M-h/j/k/l to seamlessly move focus between
-- both vim windows and tmux panes.
local function navigateWindowOrTmux(dir)
  return function()
    local winnr = vim.fn.winnr()
    vim.cmd('wincmd ' .. dir)
    if winnr == vim.fn.winnr() then
      vim.fn.system('tmux select-pane -' .. getTmuxDirection(dir))
    end
  end
end

-- Note: This set of bindings is an exception to the general rule of: ctrl/shift for applications,
-- meta/alt for surrounding infrastructure like OS, shell, and multiplexer
M.keymap = keymap {
  { { 'n', 'v', 'i' }, '<A-h>', navigateWindowOrTmux('h'), { desc = 'Navigate window left' } },
  { { 'n', 'v', 'i' }, '<A-j>', navigateWindowOrTmux('j'), { desc = 'Navigate window down' } },
  { { 'n', 'v', 'i' }, '<A-k>', navigateWindowOrTmux('k'), { desc = 'Navigate window up' } },
  { { 'n', 'v', 'i' }, '<A-l>', navigateWindowOrTmux('l'), { desc = 'Navigate window right' } },
}

function M.setup()
  M.keymap.apply()
end

return M
