local concat = require('pax-vim.util.table.concat')
local string_util = require('pax-vim.util.string')
local TAGS = require('pax-vim.features.notes.special_tags')

local M = {}

M.hasDueDate = {
  frontmatter = function(prev, _, cb)
    local frontmatter = {
      due = '2023-00-00',
      tags = concat(prev.tags or {}, {
        'has-due-date',
      }),
    }
    return cb(nil, frontmatter)
  end,
  build = function(existingBody, _, cb) return cb(nil, existingBody) end
}

M.task = {
  frontmatter = function(_, _, cb)
    return cb(nil, {
      title = 'TASK',
      tags = { TAGS.TASK },
    })
  end,

  build = function(existingBody, _, cb)
    return cb(nil, [[
# Overview
## Background
## Task definition
# Todo
- [ ] Todo item
# Log
]])
  end
}

M.taskLog = {
  build = function(prev, _, cb)
    local log_item = vim.fn.input('Log: ')
    local log_line = '[' .. os.date('%Y-%m-%dT%H:%M:%S') .. '] ' .. log_item

    local new_body = {}
    local skip_next = false
    local added = false
    for i, line in ipairs(prev) do
      if not skip_next then
        table.insert(new_body, line)

        if string_util.ends_with(line, '# Log') then
          if i ~= #prev and prev[i + 1] == '' then
            table.insert(new_body, '')
            -- Insert a newline that already exists, so skip it when
            -- we come across it in the next iteration
            skip_next = true
          end

          added = true
          -- This will insert the log line at the top so the most
          -- recent items are always closest to the top of the file
          table.insert(new_body, log_line)
        end
      else
        skip_next = false
      end
    end

    -- Add a new log section at the end when there wasn't one before
    if not added then
      table.insert(new_body, {
        '# Log',
        '',
        log_line,
      })
    end

    return cb(nil, new_body)
  end
}

return M
