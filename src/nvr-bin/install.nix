{ pkgs, ... }:
  pkgs.buildGoModule {
    pname = "nvr-go";
    version = "v1.0.0";
    src = "./neovim-remote-go";
  }
