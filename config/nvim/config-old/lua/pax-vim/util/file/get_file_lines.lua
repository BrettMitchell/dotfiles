return function(opts)
  opts = opts or {}

  if vim.fn.filereadable(opts.filename) ~= 1 then
    if not opts.silent then
      vim.notify('Cannot read file: ' .. opts.filename, 'error')
    end
    return
  end

  return vim.fn.readfile(opts.filename)
end
