local lazy = require('user.util.plugins.lazy')

local M = {
  name = 'Completion Source - LaTeX Symbols',

  description = [[
  This feature adds support for LaTeX symbol completions
  ]],

  symbols_source_definition = {
    name = 'latex_symbols',
    option = {
      strategy = 2,
    }
  }
}

function M.setup()
  lazy.add_plugin({
    'https://github.com/kdheepak/cmp-latex-symbols',
    event = 'VeryLazy',
  })
end

return M
