local make_feature = require('pax-vim.util.feature').make_feature_v2
local keymap = require('pax-vim.util.keymap')

local write = make_feature({
  name = 'System Commands: Write Files',

  description = [[
  This feature adds easier keymaps for writing the files.
  ]],

  keymap = keymap {
    { 'n', '<space>w', '<cmd>w<cr>', { desc = 'Write current file' } },
    { 'n', '<space>W', '<cmd>wa<cr>', { desc = 'Write all files' } },
  },

  apply = function (self)
    self.keymap.apply()
  end
})

return write
