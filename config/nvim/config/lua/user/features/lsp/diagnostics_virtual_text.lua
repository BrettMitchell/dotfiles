local make_feature = require('user.util.feature').make_feature

local lsp = require('user.features.lsp')

local M = make_feature({
  name = 'Diagnostic Virtual Text',
  dependencies = { lsp },

  description = [[
  This feature adds support for toggleable virtual text displaying diagnostics from
  the LSP.
  ]],

  plugins = {
    {
      'https://git.sr.ht/~whynothugo/lsp_lines.nvim',
      config = function()
        require('lsp_lines').setup()
      end,
    }
  },

  keymaps = {
    {
      'n',
      '<space>lv',
      function()
        require('lsp_lines').toggle()
      end,
      { desc = 'Toggle LSP diagnostics virtual text' },
    }
  }
})

return M
