local make_feature = require('user.util.feature').make_feature_v2

-- local config = {
--   cmp_buffer_config = {
--     keyword_length = 3,
--     keyword_pattern = [[\%(-\?\d\+\%(\.\d\+\)\?\|\h\w*\%([\-.]\w*\)*\)]],
--     get_bufnrs = function () end,
--     indexing_interval = 100,
--     indexing_batch_size = 1000,
--     max_indexed_line_length = 1024 * 40
--   }
-- }

local cmp_buffer_text = make_feature({
  name = 'Completion :: Buffer Text',

  description = [[
  Completion source for words in the current buffer
  ]],

  source_definition = {
    name = 'buffer',
    max_item_count = 5,
  },

  plugins = {
    { 'https://github.com/hrsh7th/cmp-buffer' }
  },
})

return cmp_buffer_text
