local ends_with = require('user.util.string.ends_with')

return function(str, prefix, replacement)
  if not ends_with(str, prefix) then
    return str
  end

  return str:sub(1, #str - #prefix) .. replacement
end
