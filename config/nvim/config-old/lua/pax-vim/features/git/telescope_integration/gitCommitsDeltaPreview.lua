local previewers = require('telescope.previewers')
local builtin = require('telescope.builtin')

local M = {}

-- Adapted from: https://github.com/nvim-telescope/telescope.nvim/issues/605#issuecomment-1455199897
function M.git_bcommits_delta(opts)
  opts = opts or {}

  opts.previewer = previewers.new_termopen_previewer({
    get_command = function(entry)
      local fileStatePrev = table.concat({ entry.value, '~:', vim.fn.expand('#') })
      local fileStateCurrent = table.concat({ entry.value, ':', vim.fn.expand('#') })

      return {
        -- Show diff between selected commit and previous commit (only showing changes in current file).
        -- If there is no previous commit, throw away stderr
        -- and show the selected commit (only showing changes in current file).

        "zsh", "-c",
        table.concat({
          "git diff", fileStatePrev, fileStateCurrent, "2> /dev/null | delta",
          "|| git show", fileStateCurrent
        }, ' ')
      }
    end,
  })

  builtin.git_bcommits(opts)
end

function M.git_commits_delta(opts)
  opts = opts or {}

  opts.previewer = previewers.new_termopen_previewer({
    get_command = function(entry)
      return {
        -- Show diff between selected commit and previous commit (only showing changes in current file).
        -- If there is no previous commit, throw away stderr
        -- and show the selected commit (only showing changes in current file).

        "zsh", "-c",
        table.concat({
          "git diff", entry.value, "2> /dev/null | delta",
          "|| git show", entry.value
        }, ' ')
      }
    end,
  })

  builtin.git_commits(opts)
end

return M
