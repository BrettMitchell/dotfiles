local lazy = require('user.util.plugins.lazy')
local priority = require('user.util.priority')

local state = {
  severity = 0,
  message_filters = {},
}

local notifyPlugin = nil
local function notify(msg, severity)
  if notifyPlugin == nil then return print(msg) end

  for _, filter in pairs(state.message_filters) do
    if filter(msg) then return end
  end

  local severityIdx = severity
  if type(severity) == 'string' then
    severityIdx = vim.log.levels[severity]
  end

  if severityIdx == nil or severityIdx >= state.severity then
    notifyPlugin(msg, severity)
  end
end

local M = {
  name = 'Notifications',

  description = [[
  This plugin configures notifications and exposes a notification
  function which supports severities and severity filtering.
  ]],
}

function M.setup()
  lazy.add_plugin({
    lazy = false,
    priority = priority.notification,
    'https://github.com/rcarriga/nvim-notify',
    config = function()
      notifyPlugin = require('notify')
      notifyPlugin.setup()
      vim.notify =  notify
    end,
  })
end

M.methods = {
  notify = notify,

  get_severity = function () return state.severity end,
  set_severity = function (newSeverity) state.severity = newSeverity end,

  get_filters = function () return state.filters end,
  set_filters = function (newFilters) state.filters = newFilters end,
  add_filters = function (newFilters)
    for filter in ipairs(newFilters) do
      table.insert(state.filters, filter)
    end
  end
}

return M
