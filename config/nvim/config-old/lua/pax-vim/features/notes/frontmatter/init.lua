--[[

Feature design:

  Data points to track and support:
    Created date-time
    Updated date-time
    Title
    Description
    Tags

  Requirements:
    On save:
      If frontmatter exists for the current file
        Assign the current date to the 'updated' field. This must be in ISO 8601 format.
        This must not re-order the keys in the frontmatter
      If frontmatter does not exist
        Generate new values for all fields as follows
          created: <iso-8601-date-time>
          updated: <iso-8601-date-time>
          title: Transform filepath relative to root notes folder
          description: <empty-string>
          tags: <empty-list>

    On file moved:
      If the title corresponds to the file's path
        Regenerate the title
      If the title does not correspond to the file's path
        Leave the title as is
      In either case
        Update all relative links to other files within the moved note
        Update all relative links to the file in other notes

  Done:
    - [x] Parse and generate frontmatter programmatically
    - [x] Set up autocommand to keep frontmatter up-to-date
    - [x] Preserve cursor location when updating frontmatter

  Todo:
    - [ ] Support ignored files with a glob pattern
    - [ ] Transform file path into title
    - [ ] Provide consistent key ordering in YAML output: https://github.com/gvvaughan/lyaml/issues/45, https://github.com/Kong/kong/pull/7480
    - [ ] Provide keymap to fully re-generate frontmatter on demand

--]]
local tableUtil = require('user.lib.table')
local registerBufferKeyMap = require('user.lib.map').registerBufferKeyMap
local t = require('user.lib.termcodes').t

local function dumpYamlSortedKeys(t)
  -- TODO
  local lyaml = require('lyaml')
  return lyaml.dump({ t })
end

local M = {}

M.defaultConfig = {
  enabled = true,
  keymap = {
    ['<leader>nf'] = { M.generateFrontmatter },
  },
  autocmd = {
    enabled = true,
    ignore = {},
  },
}

function M.getFileLines(opts)
  opts = opts or { showNotifications = true }

  local showNotifications = opts.showNotifications
  if (showNotifications == nil) then
    showNotifications = true
  end

  local function notify(msg, severity)
    if showNotifications then
      vim.notify(msg, severity)
    end
  end

  local filename = opts.filename or vim.fn.expand('%:p')

  local contentLines = {}
  -- Load from the current buffer (pulling in unsaved changes) if possible
  -- If not (i.e. we are working with a different file), then pull frontmatter
  -- from the disk.
  if opts.filename then
    if vim.fn.filereadable(filename) ~= 1 then
      notify('Cannot read file: ' .. filename, 'error')
      return
    end

    contentLines = vim.fn.readfile(filename)
  else
    contentLines = vim.fn.getbufline('%', 0, '$')
  end

  return contentLines
end

function M.getFrontmatterLines(fileLines)
  if (fileLines[1] ~= '---') then
    -- if opts.showNotifications then
    -- notify('Invalid frontmatter: First line must be "---"')
    -- end
    return nil
  end

  local frontmatterLines = {}
  local i = 2
  while fileLines[i] ~= '---' do
    frontmatterLines[i - 1] = fileLines[i]
    i = i + 1
  end

  return frontmatterLines
end

local function getFrontmatterString(opts)
  opts = opts or {}

  local frontmatterLines = opts.frontmatterLines

  if not frontmatterLines then
    local fileLines = M.getFileLines(opts)
    frontmatterLines = M.getFrontmatterLines(fileLines)
    if not frontmatterLines then return nil end
  end

  local frontmatterString = ''
  for _, line in ipairs(frontmatterLines) do
    frontmatterString = frontmatterString .. '\n' .. line
  end

  return frontmatterString
end

function M.parseFrontmatter(opts)
  local lyaml = require('lyaml')
  local frontmatterString = getFrontmatterString(opts)
  if not frontmatterString then return nil end

  local parsed = lyaml.load(frontmatterString)
  return parsed
end

local function getTitleFromFilename(filename)
  return filename
end

local function serializeFrontmatter(frontmatter)
  return dumpYamlSortedKeys(frontmatter)
end

-- TODO: Make this work for any file in the filesystem, not just the current
--       open buffer
local function removeExistingFrontmatter()
  local fileLines = M.getFileLines()
  local rawFrontmatter = M.getFrontmatterLines(fileLines)
  if not rawFrontmatter then return end

  vim.cmd('0,' .. #rawFrontmatter + 2 .. 'd')
end

local function removeFrontmatterLines(fileLines)
  local frontmatterLines = M.getFrontmatterLines(fileLines)
  local withoutFrontmatter = tableUtil.slice(fileLines, #frontmatterLines + 3)
  return withoutFrontmatter
end

local function writeFrontmatter(opts)
  opts = opts or {}
  local serializedFrontmatter = opts.serializedFrontmatter or ''

  local i = 1
  local lines = {}
  for line in serializedFrontmatter:gmatch '[^\n]+' do
    lines[i] = line
    i = i + 1
  end
  -- lyaml terminates blocks with '...', but we want '---'
  lines[#lines] = '---'

  if not opts.filename then
    -- Set frontmatter directly in buffer contents
    vim.api.nvim_buf_set_lines(0, 0, opts.prevSize or 0, false, lines)
  else
    -- Otherwise, modify the file directly in the filesystem

    local fileLines = vim.fn.readfile(opts.filename)
    local withoutFrontmatter = removeFrontmatterLines(fileLines)

    local withNewFrontmatter = {}

    for _, line in ipairs(lines) do
      table.insert(withNewFrontmatter, line)
    end
    for _, line in ipairs(withoutFrontmatter) do
      table.insert(withNewFrontmatter, line)
    end

    vim.fn.writefile(withNewFrontmatter, opts.filename)
    vim.cmd('e')
  end
end

-- Can use a table key trick to get unique values when only working with strings
local function uniqueStrings(tbl)
  local container = {}
  for _, value in ipairs(tbl) do
    container[value] = value
  end

  local res = {}
  for _, v in pairs(container) do
    table.insert(res, v)
  end

  return res
end

function M.setAttributes(attributes, opts)
  if not attributes then
    return
  end

  opts = opts or { merge = false }

  local fileLines = M.getFileLines(opts)
  local frontmatterLines = M.getFrontmatterLines(fileLines)
  local existingFrontmatter = M.parseFrontmatter({ frontmatterLines = frontmatterLines })
  existingFrontmatter = existingFrontmatter or {}
  local prevSize = #frontmatterLines + 2

  for key, value in pairs(attributes) do
    if opts.merge then
      -- If value is a table, push new values
      if type(existingFrontmatter[key]) == 'table' then
        if tableUtil.isDict(existingFrontmatter[key]) then
          -- Dictionary
          -- TODO: Support recursion here for deep merge support
          for innerKey, innerValue in pairs(value) do
            existingFrontmatter[key][innerKey] = innerValue
          end
        else
          -- Array
          for _, innerValue in ipairs(value) do
            table.insert(existingFrontmatter[key], innerValue)
          end
        end
      else
        -- Not a table of any kind
        existingFrontmatter[key] = value
      end
    else
      existingFrontmatter[key] = value
    end
  end

  -- Tags need to be kept unique and sorted
  if existingFrontmatter.tags then
    existingFrontmatter.tags = uniqueStrings(existingFrontmatter.tags)
    table.sort(existingFrontmatter.tags)
  end

  local serialized = serializeFrontmatter(existingFrontmatter)
  writeFrontmatter({ serializedFrontmatter = serialized, filename = opts.filename, prevSize = prevSize })
end

-- Update existing frontmatter, and provide defaults when frontmatter doesn't exist
-- TODO: This whole module has gotten really stupid, this function in particular.
--       Refactor this function so that it just accepts opts.
--       Refactor the module so that each function has clear responsibility, and composes well with others.
--       Why does this support 'shouldIgnoreFile'? Just don't call the function if you want to ignore a file...
function M.generateFrontmatter(shouldIgnoreFile, opts)
  opts = opts or {}

  shouldIgnoreFile = shouldIgnoreFile or function() return false end

  local filename = opts.filename or vim.fn.expand('%:p')
  if (shouldIgnoreFile(filename)) then
    return
  end

  local formattedDate = os.date('%Y-%m-%dT%H:%M:%S')

  local fileLines = M.getFileLines({ filename = filename })
  local frontmatterLines = M.getFrontmatterLines(fileLines)
  local existingFrontmatter = M.parseFrontmatter({ frontmatterLines = frontmatterLines })
  local prevSize = 0
  if existingFrontmatter then
    prevSize = #frontmatterLines + 2
  end
  existingFrontmatter = existingFrontmatter or {}

  existingFrontmatter.tags = existingFrontmatter.tags or {}
  existingFrontmatter.description = existingFrontmatter.description or ''
  existingFrontmatter.created = existingFrontmatter.created or formattedDate
  existingFrontmatter.updated = formattedDate

  existingFrontmatter.title = existingFrontmatter.title or getTitleFromFilename(filename)

  local serialized = serializeFrontmatter(existingFrontmatter)
  writeFrontmatter({ serializedFrontmatter = serialized, prevSize = prevSize, filename = filename })
end

-- Support note-local bindings with `bindings` frontmatter property
function M.loadBindings(opts)
  opts = opts or {}

  local frontmatter = M.parseFrontmatter(opts)
  if not frontmatter then return end

  local bindings = frontmatter.bindings or {}

  for key, cmd in pairs(bindings) do
    local normalizedCmd = cmd

    if type(cmd) == 'table' then
      if cmd.keys then
        normalizedCmd = ':normal! ' .. t(cmd.keys)
      end

      if cmd.lua then
        normalizedCmd = 'lua ' .. cmd.lua
      end

      -- Redundant, but this makes the intended structure clearer
      if cmd.cmd then
        normalizedCmd = cmd
      end
    end

    if type(normalizedCmd) ~= 'string' then
      vim.notify('Unsupported command config: ' .. vim.inspect(cmd))
      return
    end

    if normalizedCmd:sub(1, 1) == ':' then
      normalizedCmd = normalizedCmd:sub(2)
    end

    -- Remove any newlines, as vim's command mode doesn't play nicely with them
    normalizedCmd = string.gsub(normalizedCmd, '[\n]+', '')

    registerBufferKeyMap({ 'n' }, {
      [key] = { function()
        vim.cmd(normalizedCmd)
      end }
    })
  end
end

return M
