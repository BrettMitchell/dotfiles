--[[

Feature description:

  This module allows the user to mark a directory as a notes directory.

  Todo:
    - [x] Frame out functions
    - [x] Store list of notes directories in the users cache directory
    - [x] Provide a way for other modules to require this and ask if a directory is a notes directory
    - [ ] Provide a way for other modules to register actions to occur when a directory's 'is notes' status changes

Note: A notes directory metadata entry looks like the following:

{
  directory = "/absolute/path/to/the/directory",
  isNotesDirectory = true,
}

--]]

local json = require('user.lib.json')

local cacheFile = vim.env.HOME .. '/.cache/nvim/plugins/emanote/note_directories.json'
if vim.fn.has('win32') then
  cacheFile = vim.env.HOME .. '/AppData/Local/nvim/plugins/emanote/note_directories.json'
end

local M = {
  cacheFile = cacheFile
}

function M.isNotesDir(dir)
  local cacheFileContents = json.loadJson(M.cacheFile)

  if cacheFileContents and cacheFileContents[dir] then
    return true
  end

  return false
end

function M.setIsNotesDir(dir, isNotesDir)
  local cacheFileContents = json.loadJson(M.cacheFile) or {}

  if not cacheFileContents[dir] then
    cacheFileContents[dir] = {
      directory = dir,
      isNotesDir = isNotesDir,
    }
  else
    cacheFileContents[dir].isNotesDir = isNotesDir
  end

  json.saveJson(M.cacheFile, cacheFileContents)
end

function M.toggleIsNotesDir(dir)
  local cacheFileContents = json.loadJson(M.cacheFile) or {}

  if not cacheFileContents[dir] then
    cacheFileContents[dir] = {
      directory = dir,
      isNotesDir = true,
    }
  else
    cacheFileContents[dir].isNotesDir = not cacheFileContents[dir].isNotesDir
  end

  json.saveJson(M.cacheFile, cacheFileContents)
end

function M.getNotesDirs()
  local cacheFileContents = json.loadJson(M.cacheFile) or {}

  local dirs = {}
  for k in pairs(cacheFileContents) do
    table.insert(dirs, k)
  end

  return dirs
end

return M
