local templates = require('user.notes.templates')
local tags = require('user.notes.tags')
local projects = require('user.notes.projects')
local fsOps = require('user.notes.fsOps')
local autocmd = require('user.notes.autocmd')
local telescope = require('user.notes.telescope')
local links = require('user.notes.links')

math.randomseed(os.time())

local M = {}

-- Proxied exports
M.open = fsOps.open
M.newNote = fsOps.newNote
M.createOrOpen = fsOps.createOrOpen
M.addTags = tags.addTags
M.refactorTagUnderCursor = tags.refactorTagUnderCursor
M.pickTemplate = templates.pickTemplate
M.newProjectNote = projects.newProjectNote
M.gotoProjectRoot = projects.gotoProjectRoot
M.multiSearch = telescope.multiSearch
M.insertLinkFromClipboard = links.insertLinkFromClipboard
M.insertLink = links.insertLink
M.telescope = telescope

-- Create or open journal note for a particular date
function M.journal(date)
  fsOps.createOrOpen({ title = date }, function(res)
    if res.created then
      templates.applyAutoTemplate('journal', { date = date })
      vim.cmd('write')
    end
  end)
end

-- Create or open today's journal note
function M.todaysJournal()
  local today = os.date('%Y-%m-%d')
  M.journal(today)
end

-- Create or open tomorrow's journal note
-- Note: Add time to a date by representing the time in milliseconds and adding an integer.
-- This doesn't handle irregular intervals of time like months and years, but for now, I
-- don't need to work with those.
function M.tomorrowsJournal()
  local tomorrowDate = os.date('!*t')
  if type(tomorrowDate) == 'string' then return end

  tomorrowDate.day = tomorrowDate.day + 1
  local tomorrowEpoch = os.time(tomorrowDate)

  local tomorrow = os.date('%Y-%m-%d', tomorrowEpoch)
  M.journal(tomorrow)
end

function M.setup()
  if require('zk.util').notebook_root(vim.fn.expand('%:p')) ~= nil then
    autocmd.setup()
  end
end

return M
