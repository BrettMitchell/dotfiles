-- Source: https://github.com/parz3val/pywal-complete.nvim/blob/main/lua/pywal16/config.lua

local hl = require('user.util.highlight')
local TRANSPARENT = 'NONE'

return function(theme)
  -- hl('TelescopePromptNormal', {
  --   fg = current_theme.color.foreground1,
  --   bg = current_theme.color.background2,
  -- })

  -- hl('TelescopePromptPrefix', {
  --   fg = current_theme.color.foreground1,
  --   bg = current_theme.color.background1,
  -- })

  -- hl('TelescopePromptBorder', {
  --   fg = current_theme.color.accent1,
  --   bg = current_theme.color.background2,
  -- })

  hl('TelescopePromptCounter', {
    fg = theme.color.accent3,
    bg = theme.color.background1,
  })

  hl('TelescopeSelection', {
    fg = theme.color.foreground1,
    bg = theme.color.background1,
  })

  hl('TelescopePreviewMatch', {
    fg = theme.color.text_selected,
    bg = theme.color.background_selected,
  })

  ---

  hl('TelescopeBorder', { fg = theme.color.accent5, bg = TRANSPARENT })
  hl('TelescopeNormal', { fg = theme.color.foreground, bg = TRANSPARENT })
  -- hl('TelescopeSelection', { fg = TRANSPARENT, bg = theme.color.accent2 })
end
