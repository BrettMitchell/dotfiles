local feat_util = require('user.util.feature')
local keymap = require('user.util.keymap')
local augroup = require('user.util.autocmd').make_augroup

local system_clipboard = {
  name = 'Edit Commands: System Clipboard',

  description = [[
  This feature adds keymaps for copying to and pasting from the
  system clipboard.
  ]],

  augroup = augroup {
    {
      name = 'SynchronizeSystemClipboard',
      autocmds = { {
        events = 'TextYankPost',
        opts = {
          callback = function ()
            if vim.v.operator ~= 'y' then return end
            local ok, yank_data = pcall(vim.fn.getreg, '"')
            if ok and #yank_data > 0 then
              vim.fn.setreg('+', yank_data)
            end
          end
        }
      } }
    }
  },

  keymap = keymap {
    { { 'n', 'v' }, '<space>p', '"+p', { desc = 'Paste from the system clipboard' } },
    { { 'n', 'v' }, '<space>P', '"+P', { desc = 'Paste from the system clipboard' } },

    -- TODO:
    --  - ;yaf -> yank absolute filepath
    --  - ;yad -> yank absolute containing directory
    --  - ;yrf -> yank relative filepath
    --  - ;yrd -> yank relative containing directory
    { 'n', ';yp', "<cmd>let @+ = expand('%:p')<cr>", { desc = 'Yank full file path' } },
    { 'n', ';yP', "<cmd>let @+ = expand('%:p:h')<cr>", { desc = 'Yank containing directory' } },
  },
}

function system_clipboard.setup(config)
  feat_util.apply_config(config or {}, system_clipboard)
  system_clipboard.keymap.apply()
  system_clipboard.augroup:apply()
end

return system_clipboard
