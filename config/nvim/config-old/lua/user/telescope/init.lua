local mergeLeft = require('user.lib.merge').mergeLeft
local theme = require('user.telescope.theme')
local utils = require('user.utils');

local M = {}

M.notes = require('user.telescope.notes').searchNotes
M.findGitConflicts = require('user.telescope.findGitConflicts').findGitConflicts
M.git_bcommits_delta = require('user.telescope.gitCommitsDeltaPreview').git_bcommits_delta

-----------------------------------------------------------

function M.cwdFilesByName(useGit)
  local _, ret, stderr = utils.get_os_command_output({ 'git', 'rev-parse', '--is-inside-worktree' })
  if useGit and ret == 0 then
    require('telescope.builtin').git_files(
      mergeLeft(
        theme.horizontal,
        { prompt_title = 'Git files' }
      )
    )
  else
    require('telescope.builtin').find_files(theme.horizontal)
  end
end

-----------------------------------------------------------

function M.currentFileContents()
  require("telescope.builtin").current_buffer_fuzzy_find(
    mergeLeft(
      theme.horizontal,
      { prompt_title = 'Current file contents', search_dirs = { vim.fn.expand("%:p") } }
    )
  )
end

-----------------------------------------------------------

function M.currentFileLspSymbols()
  require("telescope.builtin").lsp_document_symbols(
    mergeLeft(
      theme.horizontal,
      { prompt_title = 'LSP Document Symbols' }
    )
  )
end

-----------------------------------------------------------

function M.cwdFilesByContent()
  require('telescope.builtin').live_grep(
    mergeLeft(
      theme.horizontal,
      { prompt_title = 'Project-wide file contents' }
    )
  )
end

-----------------------------------------------------------

-- TODO: Upgrade keybindings here to allow deleting buffers
function M.buffers()
  require('telescope.builtin').buffers(theme.horizontal)
end

-----------------------------------------------------------

function M.tokens()
  require('telescope.builtin').buffers(theme.horizontal)
end

-----------------------------------------------------------

function M.command_history()
  require('telescope.builtin').command_history(theme.horizontal)
end

-----------------------------------------------------------

function M.help()
  require('telescope.builtin').help_tags()
end

-----------------------------------------------------------

function M.commands()
  require('telescope.builtin').commands()
end

-----------------------------------------------------------

function M.frecency()
  require('telescope').extensions.frecency.frecency(theme.horizontal)
end

-----------------------------------------------------------

function M.search_note_names()
  require('telescope.builtin').find_files(
    mergeLeft(
      theme.horizontal,
      { cwd = os.getenv('NOTES_DIR') }
    )
  )
end

-----------------------------------------------------------

function M.search_notes_text()
  require('telescope.builtin').live_grep(
    mergeLeft(
      theme.horizontal,
      { cwd = os.getenv('NOTES_DIR') }
    )
  )
end

-----------------------------------------------------------

function M.jump_list()
  require('telescope.builtin').jumplist(
    mergeLeft(
      theme.horizontal,
      { cwd = os.getenv('NOTES_DIR') }
    )
  )
end

-----------------------------------------------------------

function M.keymaps()
  require('telescope.builtin').keymaps(
    mergeLeft(
      theme.horizontal,
      { cwd = os.getenv('NOTES_DIR') }
    )
  )
end

-----------------------------------------------------------

local gitTheme = mergeLeft(
  theme.horizontal,
  { sorting_strategy = 'descending' }
)

function M.currentFileGitHistory()
  require('user.telescope.gitCommitsDeltaPreview').git_bcommits_delta()
end

-----------------------------------------------------------

function M.gitHistory()
  require('user.telescope.gitCommitsDeltaPreview').git_commits_delta()
end

-----------------------------------------------------------

function M.gitStatus()
  require('telescope.builtin').git_status(theme.horizontal)
end

-----------------------------------------------------------

function M.gitBranches()
  require('telescope.builtin').git_branches(theme.horizontal)
end

-----------------------------------------------------------

return M
