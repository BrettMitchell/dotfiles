
setopt PROMPT_SUBST

function __nix_shell {
  if [ -n "$IN_NIX_SHELL" ] ; then
    printf ' nix'
  fi
}

export PROMPT=' %F{red}%1~ %F{blue}[$SHLVL$(__nix_shell)]%f 
 %F{blue}λ%f '

