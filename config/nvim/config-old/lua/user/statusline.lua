local M = {}

function M.toggle()
  if vim.opt.laststatus:get() == 0 then
    vim.opt.laststatus = 3
    vim.opt.cmdheight = 1
  else
    vim.opt.laststatus = 0
    vim.opt.cmdheight = 0
  end
end

return M
