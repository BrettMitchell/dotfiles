#!/usr/bin/env bash

function main {
  local window_corner_radius=`theme-get window.corner_radius`
  local window_border=`theme-get window.border`

  cat <<CONF > "$HOME/.config/picom/picom.conf"

  #-- Backend

  # xrender or glx
  # glx is usually faster
  backend = "glx";

  # glx background config
  glx-copy-from-front = false;



  #-- Animations

  # transition-length = 300
  # transition-pow-x = 0.5
  # transition-pow-y = 0.5
  # transition-pow-w = 0.5
  # transition-pow-h = 0.5
  # size-transition = true



  #-- Shadows

  shadow = false;



  #-- Opacity

  inactive-opacity = 1;
  active-opacity = 1;
  frame-opacity = 1;
  inactive-override-opacity = false;

  opacity-rule = [
    "90:class_g = 'code-oss'",
    # "90:class_g = 'kitty'",
    # "90:class_g = 'kitty-float'",
    "90:class_g = 'firefox'",
  ]



  #-- Dimming

  # inactive-dim = 0.2;
  # inactive-dim-fixed = true;



  #-- Rounded corners

  corner-radius = $window_corner_radius;
  rounded-corners-exclude = [
    "class_g = 'Polybar'",
    "class_g = 'Dunst'"
    # "class_g = 'kitty'",
    # "class_g = 'firefox'"
  ]
  round-borders = $(( $window_corner_radius + $window_border ));
  round-borders-exclude = [
  ]



  #-- Blur

  experimental-backends = true;

  blur: {
    method = "kawase";
    strength = 10;
    background = false;
    background-frame = false;
    background-fixed = false;
  }

  # Blur background of transparent windows. Bad performance with X Render backend. GLX backend is preferred.
  # blur-background = false;
  # Blur background of opaque windows with transparent frames as well.
  # blur-background-frame = true;
  # Do not let blur radius adjust based on window opacity.

  #blur-kern = "3x3box"
  #blur-kern = "7x7gaussian"
  #blur-kern = "7x7gaussian";
  # blur-background-fixed = false;

  blur-background-exclude = [
      "class_g = 'Dunst'",
      "class_g = 'slop'",
      "window_type = 'dock'",
      "window_type = 'desktop'",
  ];



  #-- Fade

  # Fade windows during opacity changes.
  fading = true;
  # The time between steps in a fade in milliseconds. (default 10).
  fade-delta = 4;
  # Opacity change between steps while fading in. (default 0.028).
  fade-in-step = 0.03;
  # Opacity change between steps while fading out. (default 0.03).
  fade-out-step = 0.03;
  # Fade windows in/out when opening/closing
  # no-fading-openclose = true;

  # Specify a list of conditions of windows that should not be faded.
  fade-exclude = [ ];



  #-- Focus

  # Try to detect WM windows and mark them as active.
  mark-wmwin-focused = true;
  # Mark all non-WM but override-redirect windows active (e.g. menus).
  mark-ovredir-focused = true;
  # Use EWMH _NET_WM_ACTIVE_WINDOW to determine which window is focused instead of using FocusIn/Out events.
  # Usually more reliable but depends on a EWMH-compliant WM.
  use-ewmh-active-win = true;

  # focus-exclude = [ 
  #     "name *= 'Chromium'",
  #     "name *= 'Chrome'",
  #     "name *= 'Freeplane'",
  #     "name *= 'Hive'",
  #     "class_g = 'R_x11'",
  #     "class_g = 'Firefox'",
  #     "class_g = 'firefox'",
  #     "class_g = 'feh'",
  #     "class_g = 'zoom'",
  #     "class_g = 'Zathura'",
  # ];



  #-- Misc

  # Detect rounded corners and treat them as rectangular when --shadow-ignore-shaped is on.
  detect-rounded-corners = true;

  # Detect _NET_WM_OPACITY on client windows, useful for window managers not passing _NET_WM_OPACITY of client windows to frame windows.
  # This prevents opacity being ignored for some apps.
  # For example without this enabled my xfce4-notifyd is 100% opacity no matter what.
  detect-client-opacity = true;

  # Specify refresh rate of the screen.
  # If not specified or 0, picom will try detecting this with X RandR extension.
  refresh-rate = 0;

  # Vertical synchronization: match the refresh rate of the monitor
  vsync = true;

  # Enable DBE painting mode, intended to use with VSync to (hopefully) eliminate tearing.
  # Reported to have no effect, though.
  dbe = false;

  # Limit picom to repaint at most once every 1 / refresh_rate second to boost performance.
  # This should not be used with --vsync drm/opengl/opengl-oml as they essentially does --sw-opti's job already,
  # unless you wish to specify a lower refresh rate than the actual value.
  #sw-opti = true;

  # Unredirect all windows if a full-screen opaque window is detected, to maximize performance for full-screen windows, like games.
  # Known to cause flickering when redirecting/unredirecting windows.
  unredir-if-possible = false;

  # Use WM_TRANSIENT_FOR to group windows, and consider windows in the same group focused at the same time.
  detect-transient = true;
  # Use WM_CLIENT_LEADER to group windows, and consider windows in the same group focused at the same time.
  # WM_TRANSIENT_FOR has higher priority if --detect-transient is enabled, too.
  detect-client-leader = true;

  wintypes: {
      tooltip = {
          # fade: Fade the particular type of windows.
          fade = true;
          # shadow: Give those windows shadow
          shadow = false;
          # opacity: Default opacity for the type of windows.
          opacity = 0.85;
          # focus: Whether to always consider windows of this type focused.
          focus = true;
      };
      dropdown_menu = { shadow = false; };
      popup_menu    = { shadow = false; };
      utility       = { shadow = false; };
  };

  xrender-sync-fence = true;

CONF

  # picom watches its config and will automatically apply changes

  # killall picom;
  # sleep 0.1;
  # picom &! disown;
}

main "$@"
