local make_feature = require('user.util.feature').make_feature
local keymap = require('user.util.keymap')
local lsp = require('user.features.lsp')

local config = {
  toggle_key = '<C-s>',
  select_signature_key = '<C-n>'
}

local methods = {
  -- toggle = function ()
  --   require('lsp_signature').toggle_float_win()
  --   vim.notify('Toggling LSP signature help')
  -- end,
}

local lsp_signature_keymaps = keymap {
  toggle = { { 'i', 'n', 'v', 's' }, '<space>uls', methods.toggle, { desc = 'Toggle LSP signature floating window' } },
}

local lsp_signature = make_feature({
  name = 'LSP :: Signature Documentation Floating Window',

  dependencies = { lsp },

  description = [[
  This feature adds a floating signature help window powered by LSP information.
  ]],

  config = config,
  -- keymaps = lsp_signature_keymaps,

  plugins = {
    {
      'https://github.com/ray-x/lsp_signature.nvim',
      config = function ()
        require('lsp_signature').setup(config)
      end
    },
  }
})

lsp_signature.methods = methods

return lsp_signature
