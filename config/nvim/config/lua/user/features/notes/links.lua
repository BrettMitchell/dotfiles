local stringUtil = require('user.util.string')
local tableUtil = require('user.util.table')
local frontmatter = require('user.features.notes.frontmatter')

local M = {}

function M.makeLink(opts)
  opts = opts or {}
  local text = opts.text or ''
  local target = opts.target or ''
  local link = ('[%s](%s)'):format(text, target)
  return link
end

local function insertTextAtCursor(text)
  local row, column = unpack(vim.api.nvim_win_get_cursor(0))
  if type(text) == 'string' then text = { text } end
  vim.api.nvim_buf_set_text(0, row - 1, column, row - 1, column, text)
end

local function replaceTextRange(newText, line, startCol, endCol)
  if type(newText) == 'string' then newText = { newText } end
  if startCol > 0 then startCol = startCol - 1 end

  vim.api.nvim_buf_set_text(
    0,
    line - 1,
    startCol,
    line - 1,
    endCol,
    newText
  )
end

local function appendLines(newLines, lineIndex)
  if type(newLines) == 'string' then newLines = { newLines } end

  vim.api.nvim_buf_set_lines(
    0,
    lineIndex,
    lineIndex,
    false,
    newLines
  )
end

function M.insertLink(opts)
  local link = M.makeLink(opts)
  insertTextAtCursor(link)
end

function M.getRelativeFilename(filename, parentDir)
  -- If file starts with a './' or a '../', assume it's a relative path.
  -- Note that we can't just check for a leading '.', because that will incorrectly identify hidden files as relative paths
  if filename:sub(1, 2) == './' or filename:sub(1, 3) == '../' then
    return filename
  end

  -- Trim trailing parentDir slash if it exists for normalized path building later on
  if parentDir:sub(parentDir:len(), parentDir:len()) == '/' then
    parentDir = parentDir:sub(1, parentDir:len() - 1)
  end

  -- If the file is not an absolute path, assume it is an unqualified path (i.e. no leading relative path segments) relative to the given parent folder
  if filename:sub(1, 1) ~= '/' then
    filename = parentDir .. '/' .. filename
  end

  --[[

  Assumptions:
  - Both parentDir and filename are both absolute paths at this point
  - Paths are separated by forward slash '/'. This won't work on Windows.

  Algorithm:
  - Split paths by path separator into a list of terms
  - Discard any common leading terms
  - Convert any remaining common terms into parent directory segments ('..')
  - If there are no remaining common terms, insert a current directory segment ('.')
  - Join resulting terms by path separator

  --]]
  local splitParent = stringUtil.split(parentDir, '/')
  local splitFilename = stringUtil.split(filename, '/')

  local parentTraversals = {}
  local relativeFileSegments = {}

  local differentTermEncountered = false
  local pathStartIdx = 0
  for i = 1, #splitParent do
    -- Do nothing until a different term is found (i.e. discard until different term is found)
    if not differentTermEncountered and splitParent[i] ~= splitFilename[i] then
      differentTermEncountered = true
      pathStartIdx = i
    end

    -- After different term is found, convert remaining parent path segments into parent traversal segments
    if differentTermEncountered then
      table.insert(parentTraversals, '..')
    end
  end

  -- If there are no directories to traverse upwards, insert a current directory traversal segment
  if #parentTraversals == 0 then
    parentTraversals = { '.' }
    pathStartIdx = #splitParent + 1
  end

  -- Insert file path segments
  for i = pathStartIdx, #splitFilename do
    table.insert(relativeFileSegments, splitFilename[i])
  end

  local parentTraversalsAndFileSegments = tableUtil.concat(parentTraversals, relativeFileSegments)
  local relativeFilepath = tableUtil.join(parentTraversalsAndFileSegments, '/')

  return relativeFilepath
end

local function isVisualMode(mode)
  return (
      mode == 'v'
      or mode == 'V'
      or mode == '\22'
      )
end

function M.insertLinkFromClipboard()
  -- Do not join visual selection lines
  local selectionPositions = getVisualPositions()
  local inVisualMode = isVisualMode(vim.fn.mode())

  local i = 1
  for line in vim.fn.getreg('+'):gmatch('([^\n]+)') do
    local target = line
    local title = ''

    -- If this is a local file, try to treat it like a note
    -- If line is not a local file, it may be a URL or some other linkable resource, so link to it as-is in that case.
    if vim.fn.filereadable(line) == 1 then
      local parentDir = vim.fn.expand('%:p:h')
      target = M.getRelativeFilename(line, parentDir)
      local fileFrontmatter = frontmatter.parseFrontmatter({ filename = line })
      if fileFrontmatter and fileFrontmatter.title then
        title = fileFrontmatter.title
      end
    end

    if i <= #selectionPositions then
      -- For any filename that corresponds to a selected line,
      -- use the selection in the corresponding line to construct
      -- a link

      local selectionForLine = selectionPositions[i]
      local text = selectionForLine.selection
      if title and not inVisualMode then
        text = title
      end

      local link = M.makeLink({ target = target, text = text })
      replaceTextRange(
        link,
        selectionForLine.lineIndex,
        selectionForLine.startCol,
        selectionForLine.endCol
      )
    else
      -- For any remaining filenames to link, duplicate the last
      -- selection and insert a copy of the line after the selected
      -- block.

      local selectionForLine = selectionPositions[#selectionPositions]
      local beforeSelection = string.sub(selectionForLine.line, 1, selectionForLine.startCol - 1)
      local afterSelection = string.sub(selectionForLine.line, selectionForLine.endCol + 1)

      local text = selectionForLine.selection
      if title and not inVisualMode then
        text = title
      end

      local link = M.makeLink({ target = target, text = text })
      appendLines(
        beforeSelection .. link .. afterSelection,
        selectionForLine.lineIndex
      )
    end
    i = i + 1
  end
end

return M
