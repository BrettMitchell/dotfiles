local pick_static = require('user.features.telescope').methods.pick_static

local journal = require('user.features.notes.templates.journal')
local taxon = require('user.features.notes.templates.taxon')
local taxonomy = require('user.features.notes.templates.taxonomy')
local savedSearch = require('user.features.notes.templates.savedSearch')
local templateNote = require('user.features.notes.templates.templateNote')
local fileRep = require('user.features.notes.templates.fileRep')
local webResource = require('user.features.notes.templates.webResource')
local task = require('user.features.notes.templates.task')
local query = require('user.features.notes.templates.query')
local media = require('user.features.notes.templates.media')

local frontmatter = require('user.features.notes.frontmatter')
local frontmatterV2 = require('user.features.notes.frontmatter.v2')

local M = {
  autoTemplates = {
    journal = journal.auto,
  },
  contextFreeTemplates = {
    ['Journal (weekday)'] = journal.weekday,
    ['Journal (weekend)'] = journal.weekend,
    ['Taxonomy'] = taxonomy.taxonomy,
    ['Taxon'] = taxon.taxon,
    ['File representative'] = fileRep.fileRep,
    ['Web resource'] = webResource.webResource,
    ['Saved search'] = savedSearch.savedSearch,
    ['New template note'] = templateNote.newTemplateNote,
    ['From template note'] = templateNote.fromTemplateNote,
    ['Task'] = task.task,
    ['Task log'] = task.taskLog,
    ['Due date'] = task.hasDueDate,
    ['Query'] = query.query,

    ['Media - Book'] = media.book,
    ['Media - Online Video'] = media.onlineVideo,
    ['Media - Film'] = media.film,
    ['Media - Article'] = media.article,
    ['Media - Pick File'] = media.fromFileFzf,
  }
}

-- Insert template contents under cursor and set template frontmatter attributes
function M.applyTemplate(template, ctx)
  local noteBody = frontmatterV2.get_buf_body({ bufnr = 0 })

  template.build(noteBody, ctx, function(textErr, body)
    if textErr then
      vim.notify('Encountered an error while applying template build function: ' .. vim.inspect(textErr))
      return
    end

    frontmatterV2.write_buf_body({ bufnr = 0, body = body })

    if template.frontmatter then
      local fm = frontmatterV2.get_buf_frontmatter({ bufnr = 0 })
      template.frontmatter(fm, ctx, function(frontmatterErr, builtFrontmatter)
        if frontmatterErr then
          vim.notify('Encountered an error while applying template build function: ' .. vim.inspect(textErr))
          return
        end

        frontmatter.setAttributes(builtFrontmatter, { merge = true })
      end)
    end
  end)
end

function M.applyAutoTemplate(templateName, ctx)
  local template = M.autoTemplates[templateName]
  if not template then
    vim.notify('No template named ' .. templateName, 'error')
    return
  end

  return M.applyTemplate(template, ctx)
end

local function handleSelectedTemplate(selectedTemplates)
  if #selectedTemplates == 0 then return end

  if #selectedTemplates > 1 then
    vim.notify('Selecting more than 1 template at a time is not supported', 'error')
    return
  end

  local templateName = selectedTemplates[1][1]
  local template = M.contextFreeTemplates[templateName]

  if not template then
    vim.notify('No such template ' .. templateName, 'error')
    return
  end

  M.applyTemplate(template)
end

function M.pickTemplate()
  local templateOptions = {}
  for k, _ in pairs(M.contextFreeTemplates) do
    table.insert(templateOptions, k)
  end

  table.sort(templateOptions)

  pick_static(templateOptions, {
    prompt_title = 'Pick a template to apply',
    mappings = { select_default = handleSelectedTemplate },
    layout_type = 'minimal_reversed',
    layout_config = {
      width = 0.3,
      height = 0.5,
    },
  })
end

return M
