#!/usr/bin/env bash

ln_safe "$module/files/.profile" "$HOME/.profile"

ln_safe "$module/files/.bash_profile" "$HOME/.bash_profile"
ln_safe "$module/files/.bashrc" "$HOME/.bashrc"

ln_safe "$module/files/.zprofile" "$HOME/.zprofile"
ln_safe "$module/files/.zshenv" "$HOME/.zshenv"
ln_safe "$module/files/.zshrc" "$HOME/.zshrc"
ln_safe "$module/files/zsh" "$HOME/.config/zsh"
