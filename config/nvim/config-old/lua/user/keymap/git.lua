local map = vim.keymap.set

local M = {}

function M.setup()
  map('n', '<space>gg', '<cmd>Git<cr>') -- Open Fugitive equivalent of git status
  map('n', '<space>gp', '<cmd>Git pull<cr>') -- Pull from remote
  map('n', '<space>gP', '<cmd>Git push<cr>') -- Push to remote
  map('n', '<space>gc', '<cmd>Git commit<cr>') -- Push to remote
  map('n', '<space>gB', '<cmd>Git blame<cr>') -- Run git blame on current file
  map('n', '<space>gl', '<cmd>Git log<cr>') -- Open git log
end

return M
