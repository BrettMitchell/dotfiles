local feat_util = require('user.util.feature')
local plugin_util = require('user.util.plugins.lazy')
local dap = require('user.features.dap')

local M = {
  name = 'DAP :: Variable Values in Virtual Text',
  dependencies = { dap },

  description = [[
  This feature shows variable values in virtual text at the end of the
  line where the variable is defined or referenced.
  ]],
}

function M.setup()
  feat_util.assert_deps('DAP Virtual Text', { dap })
  plugin_util.add_plugins({
    {
      'https://github.com/theHamsta/nvim-dap-virtual-text',
      config = function()
        require('nvim-dap-virtual-text').setup()
      end
    }
  })
end

return M
