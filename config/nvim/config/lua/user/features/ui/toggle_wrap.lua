local keymap = require('user.util.keymap')

local toggle_wrap = {
  name = 'UI Commands: Toggle Word Wrap',

  description = [[
  This feature adds a keybinding to toggle word wrap.

  Wrapping is configured to break on whitespace rather
  than in the middle of words.
  ]],

  keymap = keymap {
    { 'n', ';w', '<cmd>set wrap!<cr>', { desc = 'Toggle word wrap' } },
  },
}

function toggle_wrap.setup()
  toggle_wrap.keymap.apply()
  vim.opt.linebreak = true
  vim.opt.list = false
end

return toggle_wrap
