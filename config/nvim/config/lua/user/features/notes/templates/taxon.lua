local TAGS = require('user.features.notes.special_tags')

local M = {}

M.taxon = {
  frontmatter = function(_, _, cb)
    return cb(nil, {
      title = 'Taxon :: TAXON_TAG_NAME',
      tags = { TAGS.TAXON },
      queries = { { tags = {} } }
    })
  end,
  build = function(existingBody, _, cb) return cb(nil, existingBody) end
}

return M
