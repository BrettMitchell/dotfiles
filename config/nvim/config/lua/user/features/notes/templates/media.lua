local theme       = require('user.features.telescope.theme')
local actions     = require('telescope.actions')
local helpers     = require('user.features.telescope.helpers')

local concat      = require('user.util.table.concat')
local merge_left  = require('user.util.table.merge').mergeLeft
local starts_with = require('user.util.string.starts_with')
local split       = require('user.util.string.split')


local M = {}


local SEP = package.config:sub(1, 1)


M.book = {
  frontmatter = function(prev, _, cb)
    local frontmatter = {
      title = 'Book - title',
      tags = concat(prev.tags or {}, {
        't.media.text.book',
        't.media.text.have-not-read'
      }),
    }

    local link = vim.fn.getreg('+')
    if starts_with(link, 'http://') or starts_with(link, 'https://') then
      frontmatter.link = link
    end

    return cb(nil, frontmatter)
  end,
  build = function(existingBody, _, cb) return cb(nil, existingBody) end
}


M.article     = {
  frontmatter = function(prev, _, cb)
    local frontmatter = {
      title = 'Article - title',
      tags = concat(prev.tags or {}, {
        't.media.text.article',
        't.media.text.have-not-read'
      }),
    }

    local link = vim.fn.getreg('+')
    if starts_with(link, 'http://') or starts_with(link, 'https://') then
      frontmatter.link = link
    end

    return cb(nil, frontmatter)
  end,
  build = function(existingBody, _, cb) return cb(nil, existingBody) end
}

M.onlineVideo = {
  frontmatter = function(prev, _, cb)
    local frontmatter = {
      title = 'Video - title',
      tags = concat(prev.tags or {}, {
        't.media.video.online-video',
        't.media.video.have-not-seen'
      }),
    }

    local link = vim.fn.getreg('+')
    if starts_with(link, 'http://') or starts_with(link, 'https://') then
      frontmatter.link = link
    end

    return cb(nil, frontmatter)
  end,
  build = function(existingBody, _, cb) return cb(nil, existingBody) end
}


M.film = {
  frontmatter = function(prev, _, cb)
    local frontmatter = {
      title = 'Film - title',
      tags = concat(prev.tags or {}, {
        't.media.video.film',
        't.media.video.have-not-seen'
      }),
    }

    local link = vim.fn.getreg('+')
    if starts_with(link, 'http://') or starts_with(link, 'https://') then
      frontmatter.link = link
    end

    return cb(nil, frontmatter)
  end,
  build = function(existingBody, _, cb) return cb(nil, existingBody) end
}


local function handleSelection(cb)
  return function(prompt_bufnr)
    local selectedItems = helpers.getMultiSelection(prompt_bufnr)
    actions.close(prompt_bufnr)

    return cb(selectedItems, prompt_bufnr)
  end
end

M.fromFileFzf = {
  frontmatter = function(prev, _, cb)
    local media_dir = vim.fn.getenv('MEDIA_DIR')
    if vim.fn.isdirectory(media_dir) == 0 then
      vim.notify("Environment variable 'MEDIA_DIR' does not point to a directory", 'error')
      return
    end

    require('telescope.builtin').find_files(
      merge_left(
        theme.horizontal,
        {
          cwd = media_dir,
          attach_mappings = function(prompt_bufnr, map)
            actions.select_default:replace(handleSelection(function(selected_files)
              if #selected_files < 1 then
                vim.notify('Nothing selected')
                return
              end

              local filename = selected_files[1][1]
              local split_file = split(filename, SEP)
              local basename = split_file[#split_file]

              local title = prev.title or ''
              local new_title = basename:match('^([^-]+ %- [^-]+)%.%w+$')
              if new_title then
                -- TEMPORARY: Generalize this to other media types, or find a way to
                --            preserve the existing prefix.
                --            Alternatively, cue off of existing tags to read the intended
                --            media type and assign appropriate prefix.
                title = 'Book - ' .. new_title
              else
                new_title = basename:match('^([^-]+ %- [^-]+) - .*%.%w+$')
                if new_title then
                  title = 'Book - ' .. new_title
                end
              end

              local file_path = '$MEDIA_DIR/' .. filename
              return cb(nil, {
                -- TODO: Add appropriate tags and title based on directory (video, book, etc.)
                file = file_path,
                title = title,
              })
            end))
            return true
          end
        }
      )
    )
  end,
  build = function(existingBody, _, cb) return cb(nil, existingBody) end
}


return M
