local make_feature = require('pax-vim.util.feature').make_feature
local startup = require('pax-vim.features.startup.events')

local M = make_feature({
  name = 'Static Session Working Directory',

  description = [[
  This feature sets neovim's working directory at startup based
  on the first parameter supplied from the command line.

  If no argument is provided, this feature does not activate.

  If a directory is provided as the first parameter, it is used
  as the working directory. This will work if the path is absolute
  or if it is given relative to the shell's working directory.
  ]],

  augroups = {
    {
      name = 'SetCwd',
      autocmds = {
        {
          events = 'User',
          opts = {
            pattern = startup.events.Loaded,
            callback = function(event)
              if not event.data
                  or not event.data.pathFromArgs
                  or vim.fn.isdirectory(event.data.pathFromArgs) ~= 1 then
                return
              end

              vim.cmd('cd ' .. event.data.pathFromArgs)
              vim.g.__user__cwd__ = event.data.pathFromArgs
            end
          }
        }
      }
    },

    {
      name = 'PreserveCwd',
      autocmds = {
        {
          events = 'BufEnter',
          opts = {
            callback = function()
              if not vim.g.__user__cwd__ then return end
              vim.cmd('cd ' .. vim.g.__user__cwd__)
            end
          }
        }
      }
    }
  }
})

return M
