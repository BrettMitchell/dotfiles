return {
  pickNote = require('user.features.notes.telescope.pickNote').pickNote,
  multi_search = require('user.features.notes.telescope.multiSearch').multiSearch,
  pickNoteByTag = require('user.features.notes.telescope.zkBuiltins').pickNoteByTag,
  pick_note_by_tag_raw = require('user.features.notes.telescope.zkBuiltins').pick_note_by_tag_raw,
  pickNoteByTitle = require('user.features.notes.telescope.zkBuiltins').pickNoteByTitle,
  pickTagOrphans = require('user.features.notes.telescope.pickNote').pickTagOrphans,
  pickLinkOrphans = require('user.features.notes.telescope.pickNote').pickLinkOrphans,
  -- pickLinkGraph = require('user.features.notes.telescope.pickNote').pickLinkGraph,
}
