return {
  config = function()
    require('mkdnflow').setup {
      filetypes = { md = true, rmd = true, markdown = true },
      create_dirs = true,
      wrap = false,
      silent = false,

      modules = {
        bib = true,
        buffers = true,
        conceal = true,
        cursor = true,
        folds = true,
        links = true,
        lists = true,
        maps = true,
        paths = true,
        tables = true
      },

      perspective = {
        priority = 'first',
        fallback = 'current',
        root_tell = false,
        nvim_wd_heel = false
      },

      bib = {
        default_path = nil,
        find_in_root = true
      },

      links = {
        style = 'markdown',
        conceal = false,
        implicit_extension = nil,
        transform_implicit = false,
        transform_explicit = function(text)
          text = text:gsub(" ", "-")
          text = text:lower()
          text = os.date('%Y-%m-%d_') .. text
          return (text)
        end
      },

      to_do = {
        symbols = { ' ', '-', 'X' },
        update_parents = true,
        not_started = ' ',
        in_progress = '-',
        complete = 'X'
      },

      tables = {
        trim_whitespace = true,
        format_on_move = true
      },

      mappings = {
        MkdnEnter = false,
        MkdnTab = false,
        MkdnSTab = false,
        MkdnNextLink = false, -- { 'n', ';l' },
        MkdnPrevLink = false, -- { 'n', ';L' },
        MkdnNextHeading = { 'n', ';h' },
        MkdnPrevHeading = { 'n', ';H' },
        MkdnGoBack = false,
        MkdnGoForward = false,

        MkdnFollowLink = { 'n', ';g' },
        MkdnDestroyLink = { 'n', '<M-CR>' },
        MkdnMoveSource = false, -- { 'n', '<space>r' },
        MkdnYankAnchorLink = { 'n', ';ya' },
        MkdnYankFileAnchorLink = { 'n', ';yfa' },

        MkdnIncreaseHeading = { 'n', '+' },
        MkdnDecreaseHeading = { 'n', '-' },

        MkdnToggleToDo = { 'n', ';it' },
        MkdnNewListItem = { 'i', '<CR>' },
        MkdnNewListItemBelowInsert = { 'n', 'o' },
        MkdnNewListItemAboveInsert = { 'n', 'O' },
        MkdnExtendList = false,
        MkdnUpdateNumbering = { 'n', ';in' },

        MkdnTableFormat = { 'n', ';if' },
        MkdnTableNextCell = { 'i', '<Tab>' },
        MkdnTablePrevCell = { 'i', '<S-Tab>' },
        MkdnTableNextRow = { 'i', '<S-M-CR>' },
        MkdnTablePrevRow = { 'i', '<M-CR>' },
        MkdnTableNewRowBelow = { 'n', ';ir' },
        MkdnTableNewRowAbove = { 'n', ';iR' },
        MkdnTableNewColAfter = { 'n', ';ic' },
        MkdnTableNewColBefore = { 'n', ';iC' },

        -- MkdnFoldSection = { 'n', ';f' },
        -- MkdnUnfoldSection = { 'n', ';F' }
        MkdnFoldSection = false,
        MkdnUnfoldSection = false
      }
    }
  end
}
