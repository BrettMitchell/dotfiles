return function(opts)
  if type(opts.newLines) == 'string' then opts.newLines = { opts.newLines } end

  vim.api.nvim_buf_set_lines(
    opts.bufnr or 0,
    opts.lineIndex,
    opts.lineIndex,
    false,
    opts.newLines
  )
end
