local make_feature = require('pax-vim.util.feature').make_feature

local config = {
  ignore_file = function() return false end,
  start_open = true
}

local M = make_feature({
  name = 'Treesitter Folding',

  description = [[
  This feature creates folds using treesitter and optionally
  unfolds them.
  ]],

  config = config,

  augroups = {
    {
      name = 'TreeSitterExprFolding',
      autocmds = {
        {
          events = {
            'BufAdd',
            'BufEnter',
            'BufNew',
            'BufNewFile',
            'BufWinEnter',
            'BufReadPost'
          },

          opts = {
            callback = function()
              if config.ignore_file() then return end

              vim.schedule(function ()
                vim.cmd [[ setlocal foldmethod=expr ]]
                vim.cmd [[ setlocal foldexpr=nvim_treesitter#foldexpr() ]]

                if config.start_open then
                  vim.cmd [[ silent! :%foldopen! ]]
                end
              end)
            end
          }
        }
      }
    }
  }
})

return M
