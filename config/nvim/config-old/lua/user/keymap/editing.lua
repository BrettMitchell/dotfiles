-- local map = require('user.lib.map').map

local M = {}

function M.setup()
  -- local n = { 'n' }
  -- local v = { 'v' }
  -- local i = { 'i' }
  -- local no = { 'n', 'o' }
  -- local nov = { 'n', 'o', 'v' }
  -- local nvi = { 'n', 'v', 'i' }
  -- local nv = { 'n', 'v' }
  --
  -- Indentation
  -- map(n, '>', '>>') -- Indent
  -- map(n, '<', '<<') -- Dedent
  -- map(v, '>', '>gv') -- Indent
  -- map(v, '<', '<gv') -- Dedent
  -- map(n, '<M->>', '<C-o>>><right><right>i') -- Indent
  -- map(n, '<M-<>', '<C-o><<i') -- Dedent

  -- Uppercase and lowercase
  -- map(n, '<space>U', 'mzviwU`z') -- Uppercase word under cursor
  -- map(n, '<space>u', 'mzviwu`z') -- Lowercase word under cursor

  -- Toggle word wrap in current buffer
  -- map(n, ';w', '<cmd>set wrap!<cr>')

  -- -- tmux does something weird to C-/
  -- local function getCtrlSlash()
  --   local tmuxEnvVar = vim.fn.getenv('TMUX')
  --   if tmuxEnvVar ~= vim.NIL and tmuxEnvVar:len() > 0 then
  --     return '<C-_>'
  --   end
  --   return '<C-/>'
  -- end
  --
  -- -- Comment lines
  -- map(
  --   'no',
  --   getCtrlSlash(),
  --   '<plug>(comment_toggle_linewise_current)',
  --   { noremap = false }
  -- ) -- Comment single line from normal mode
  --
  -- map(
  --   'i',
  --   getCtrlSlash(),
  --   '<C-o><plug>(comment_toggle_linewise_current)',
  --   { noremap = false }
  -- ) -- Comment single line from normal mode
  --
  -- map(
  --   'v',
  --   getCtrlSlash(),
  --   '<plug>(comment_toggle_linewise_visual)gv',
  --   { noremap = false }
  -- ) -- Comment block and return to visual mode

  -- This, combined with no setting for vim.opt.clipboard
  -- mean that yanks are promoted to the system clipboard
  -- but deletes are not, which is how I like it.
  -- map(nov, 'y', '"+y')
  -- map(no, 'yy', '"+yy')
  -- map(no, 'Y', '"+y$') -- 'Y' should behave like 'D'
  -- map(v, 'Y', '"+Y') -- 'Y' should behave like 'D'
  -- map(nv, '<space>p', '"+p') -- Paste from the system clipboard
  -- map(nv, '<space>P', '"+P') -- Paste from the system clipboard
  --
  -- map(n, ';yp', "<cmd>let @+ = expand('%:p')<cr>") -- Yank full file path into system clipboard
  -- map(n, ';yP', "<cmd>let @+ = expand('%:p:h')<cr>") -- Yank current file's containing directory into system clipboard

  -- local function focusCurrentFileInLf()
  --   local filename = vim.fn.expand('%:p')
  --   if string.len(filename) == 0 then
  --     vim.notify('Buffer is not saved to a file', 'error')
  --     return
  --   end
  --
  --   require('pax-vim.features.file-browser.lf').methods.open()
  --   vim.fn.system(('lf -remote "send $id cd-dir-or-file \'%s\'"'):format(filename))
  -- end
  --
  -- map(nvi, '<M-o>', focusCurrentFileInLf) -- Focus the currently focused buffer in LF if it exists in the filesystem.
end

return M
