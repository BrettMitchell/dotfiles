{ hostname, ... }:
{
  networking.hostName = hostname;
  networking.wireless.iwd = {
    enable = true;
    settings = {
      IPv6 = { Enabled = true; };
      Settings = { AutoConnect = true; };
    };
  };
  networking.networkmanager = {
    enable = true;
    wifi.backend = "iwd";
  };
}
