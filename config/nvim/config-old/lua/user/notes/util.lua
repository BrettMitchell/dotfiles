local M = {}

function M.getNotesDir(opts)
  opts = opts or {}
  if opts.notesDir then return opts.notesDir end
  local notesDir = vim.fn.getenv('NOTES_DIR')
  return notesDir
end

return M
