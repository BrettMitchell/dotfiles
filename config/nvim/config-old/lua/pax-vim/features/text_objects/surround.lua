local make_feature = require('pax-vim.util.feature').make_feature_v2

local surround = make_feature({
  name = 'Text Objects: Surrounding Character Pairs',

  description = [[
  This feature adds support for several more text objects which are defined
  by pairs of matching characters and the text they enclose.
  ]],

  plugins = { {
    'https://github.com/kylechui/nvim-surround',
    version = '*',
  } },

  init = function(self)
    self.config = {}
  end,

  apply = function(self)
      require('nvim-surround').setup(self.config)
  end
})

return surround
