#!/usr/bin/env bash

rm_safe "$HOME/.config/hypr/hyprland.conf"
rm_safe "$HOME/.config/hypr/keybindings.conf"
rm_safe "$HOME/.config/waybar"
