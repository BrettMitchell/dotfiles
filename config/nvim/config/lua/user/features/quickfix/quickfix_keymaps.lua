local feat_util = require('user.util.feature')

local keymap = require('user.util.keymap')
local make_augroup = require('user.util.autocmd').make_augroup

local qf = require('user.features.quickfix.qf')
local bqf = require('user.features.quickfix.bqf')

local M = {}

local toggle_keymaps = keymap {
  { 'n', '<C-q>', function() require('qf').toggle('c', false) end, { desc = 'Toggles quickfix menu' }, },
}

local quickfix_buffer_local_keymaps = keymap {
  -- { 'n', 'cc', function() vim.cmd 'call setqflist([], "f") | cclose' end, { desc = 'Clears the quickfix menu' }, },
  { 'n', '<', '<cmd>silent colder<cr>', { desc = 'Open previous quickfix list' } },
  { 'n', '>', '<cmd>silent cnewer<cr>', { desc = 'Open next quickfix list' } },
}

local augroups = make_augroup {
  {
    name = 'NavigateQuickfixHistory',
    autocmds = {
      {
        events = 'FileType',
        opts = {
          callback = function()
            if vim.bo.filetype ~= 'qf' then return end

            if M.quickfix_buffer_local_keymaps then
              M.quickfix_buffer_local_keymaps.apply(vim.fn.bufnr('%'))
            end
          end
        }
      }
    }
  }
}

M = {
  name = 'Quickfix',
  description = [[
  This feature adds what I believe are sensible default keybindings
  for the quickfix list.
  ]],

  dependencies = { qf, bqf },

  toggle_keymaps = toggle_keymaps,
  quickfix_buffer_local_keymaps = quickfix_buffer_local_keymaps,
  augroups = augroups,
}

function M.setup(config)
  feat_util.apply_config(config or {}, M)
  if M.toggle_keymaps then M.toggle_keymaps.apply() end
  if M.augroups then M.augroups:apply() end
end

return M
