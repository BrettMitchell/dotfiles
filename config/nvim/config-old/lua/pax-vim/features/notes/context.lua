--[[
--
--   This module defines a dialog for constructing a tag-based working set of notes.
--   This is done by building up a ZK tag query and saving it at the module level.
--
--   The dialog is comprised of a few elements:
--   1. A telescope fuzzy finder for choosing tags:
--   - List is always in insert mode
--   - <ESC> clears the prompt if there is one, and closes the dialog if the prompt is empty
--   - <C-j/k> navigate the list
--   - <C-h/l> add/remove the currently selected tag to the context
--   - <C-x> clears the current context
--
--   2. A buffer listing out the tag expressions in the context definition.
--   - Editing the text in this buffer updates the context. After editing, the context
--     just set to the current lines in the buffer.
--   - <C-x> still works here
--
--   3. Context match preview
--   - Up to 100 matching notes are shown here. Toggle the limit with C-s
--   - Buffer cannot be modified
--   - Search is debounced by 500ms, so if you update the context many times repeatedly, you'll
--     still only get one search once you stop for 500ms.
--   - Go to one of the notes by pressing enter on its name
--
--   Navigate between the panels in the dialog with <M-h/j/k/l>. As with all telescope
--   dialogs, you can't focus the fuzzy finder results list, only the prompt and the two
--   custom buffers mentioned above.
--
--   The value returned by 'get_tag_expression' exported from this module may be used
--   elsewhere to limit the scope of the notes that you interact with, e.g. in the main
--   finder.
--
--   Additionally, 'get_auto_tags' may be used to automatically tag notes when creating
--   them so that they go immediately into the current context.
--
--   The UI for this is built with NUI through a custom telescope layout. This requires the
--   'create_layout' parameter to be given to the picker.
--
--]]

local table_util  = require('pax-vim.util.table')
local string_util = require('pax-vim.util.string')
local keymap      = require('pax-vim.util.keymap')
local noteUtil    = require('pax-vim.features.notes.util')

-- Context value is stored at the top level of the module
local M           = {
  context = {},
  context_mode = 'all-of',

  search_limit = 100,

  get_tag_expression = function(self)
    local tags_query = self.context
    if self.context_mode == 'any-of' then
      tags_query = { table_util.join(self.context, ' OR ') }
    end
    return tags_query
  end,

  get_auto_tags = function(self)
    -- Assume that a tag with whitespace is probably either invalid or
    -- is a compound search expression.
    --
    -- Remove any that contain an asterisk, as that is also special search
    -- syntax.
    return table_util.filter(self.context, function(tag)
      return not tag:find('%s') and not tag:find('%*')
    end)
  end,
}

-- This function marries Telescope's layout concept with NUI popups
-- Popups are stored in the 'ref_container' so we can access them
-- later while interacting with the dialog.
local function create_context_layout(ref_container)
  return function(picker)
    local Layout = require('nui.layout')
    local Popup = require('nui.popup')
    local TSLayout = require('telescope.pickers.layout')

    local function make_popup(options)
      local popup = Popup(options)
      function popup.border:change_title(title)
        popup.border.set_text(popup.border, "top", title)
      end

      return TSLayout.Window(popup)
    end

    local function set_popup_text(popup, new_text)
      vim.api.nvim_buf_set_lines(popup.bufnr, 0, -1, true, new_text)
    end

    local prompt_border = {
      top_left = "┌",
      top = "─",
      top_right = "┬",
      right = "│",
      bottom_right = "",
      bottom = "",
      bottom_left = "",
      left = "│",
    }

    -- Contains the telescope prompt text
    ref_container.prompt = make_popup({
      enter = true,
      border = {
        style = prompt_border,
        text = {
          top = ' Tags ',
          top_align = "center",
        },
      },
      win_options = {
        winhighlight = "Normal:Normal",
      },
    })

    local tag_results_border = {
      top_left = "├",
      top = "─",
      top_right = "┤",
      right = "│",
      bottom_right = "┴",
      bottom = "─",
      bottom_left = "└",
      left = "│",
    }

    -- Contains the telescope search results
    ref_container.tag_results = make_popup({
      focusable = true,
      border = {
        style = tag_results_border,
      },
      win_options = {
        winhighlight = "Normal:Normal",
      },
    })

    local context_query_border = {
      top_left = "",
      top = "─",
      top_right = "┐",
      right = "│",
      bottom_right = "",
      bottom = "",
      bottom_left = "",
      left = "",
    }

    -- Contains the elements in the current context value
    ref_container.context_query = make_popup({
      focusable = true,
      border = {
        style = context_query_border,
        text = {
          top = ' Context ',
          top_align = "center",
        },
      },
    })
    ref_container.context_query.set_text = set_popup_text

    local query_results_border = {
      top_left = "",
      top = "─",
      top_right = "┤",
      right = "│",
      bottom_right = "┘",
      bottom = "─",
      bottom_left = "",
      left = "",
    }

    -- Contains the names of notes matching the current context
    ref_container.query_results = make_popup({
      focusable = true,
      editable = false,
      border = {
        style = query_results_border,
        text = {
          top = ' Context Members ',
          top_align = "center",
        },
      },
    })
    ref_container.query_results.set_text = set_popup_text

    --[[
    -- _________________
    -- |prompt |context|
    -- |-------|-------|
    -- |tags   |notes  |
    -- -----------------
    --]]
    local box = Layout.Box({
      Layout.Box({
        Layout.Box(ref_container.prompt, { size = 2 }),
        Layout.Box(ref_container.tag_results, { grow = 1 }),
      }, { dir = 'col', size = '40%' }),

      Layout.Box({
        Layout.Box(ref_container.context_query, { size = '40%' }),
        Layout.Box(ref_container.query_results, { grow = 1 }),
      }, { dir = 'col', grow = 1 })
    }, { dir = 'row' })

    local size = {
      width = '90%',
      height = '90%',
    }

    local layout = Layout({
      relative = 'editor',
      position = '50%',
      size = size,
    }, box)

    -- Associate NUI components with properties that telescope is expecting
    layout.picker = picker
    layout.results = ref_container.tag_results
    layout.prompt = ref_container.prompt
    layout.preview = nil

    -- This is required to get the layout to adjust when the window is resized
    local layout_update = layout.update
    function layout:update()
      layout_update(self, { size = size }, box)
    end

    return layout
  end
end

function M.show_context_dialog(opts)
  opts                 = opts or {}

  local zk             = require('zk.api')
  local pickers        = require('telescope.pickers')
  local sorters        = require('telescope.sorters')
  local finders        = require('telescope.finders')
  local actions        = require('telescope.actions')
  local state_actions  = require('telescope.actions.state')

  local frontmatter    = require('pax-vim.features.notes.frontmatter.v2')
  local fs_ops         = require('pax-vim.features.notes.fsOps')
  local selection      = require('pax-vim.features.notes.telescope.util.selection')
  local explore        = require('pax-vim.features.notes.explore.plumbing')
  local attachMappings = require('pax-vim.features.telescope.attachMappings')

  explore.getTags({}, function(get_tag_err, tags)
    if get_tag_err then
      vim.notify('Could not search for tags ', vim.inspect(get_tag_err))
      return
    end

    local tag_names = table_util.map(tags, function(t) return t.name end)
    local finder_config = { results = tag_names }

    local popups = {
      prompt = nil,
      tag_results = nil,
      context_query = nil,
      query_results = nil,
    }

    local search_cache = {}

    -- Cache this so that it can be used even when the prompt is not focused
    local prompt_bufnr = nil

    local function get_prompt()
      local current_picker = state_actions.get_current_picker(prompt_bufnr)
      return current_picker:_get_prompt()
    end

    local function set_prompt(new_prompt)
      local current_picker = state_actions.get_current_picker(prompt_bufnr)
      return current_picker:set_prompt(new_prompt)
    end

    local function populate_query_preview()
      local tags_query = M:get_tag_expression()
      zk.list(noteUtil.getNotesDir(opts), { limit = M.search_limit, tags = tags_query, select = { 'title', 'absPath' } },
        function(err, notes)
          if vim.fn.bufexists(popups.query_results.bufnr) == 0 then
            return
          end

          local lines = {}
          if err then
            lines = {
              'Could not fetch matching notes:',
              string_util.split(vim.inspect(err), '\n'),
            }
          else
            lines = table_util.map(notes, function(note)
              return note.title
            end)
            search_cache = notes
          end

          vim.bo[popups.query_results.bufnr].modifiable = true
          popups.query_results:set_text(lines)
          vim.bo[popups.query_results.bufnr].modifiable = false
        end)
    end

    local timer = nil
    local function populate_query_preview_debounced()
      if timer then timer:stop() end
      timer = vim.defer_fn(populate_query_preview, 500)
    end

    -- Only close dialog when prompt is empty.
    -- This means if you have a prompt, you'll press <ESC> twice to exit the dialog.
    local function clear_prompt_or_close()
      if get_prompt():len() == 0 then
        actions.close(prompt_bufnr)
      else
        set_prompt('')
      end
    end

    -- Set new context value, ensure it doesn't have empty lines or outer whitespace,
    -- update the query preview, and update the context panel.
    local function set_context(new_context)
      local without_empty = table_util.filter(new_context, function(line)
        return not line:find('^%s*$')
      end)
      local trimmed = table_util.map(without_empty, string_util.trim)
      M.context = trimmed
      popups.context_query:set_text(M.context)
      populate_query_preview_debounced()
    end

    local function remove_from_context()
      local selected_tags = selection.getMultiSelection(prompt_bufnr)
      local selected_tag_names = table_util.map(selected_tags, function(tag) return tag[1] end)
      set_context(table_util.filter(
        M.context,
        function(existing_tag)
          return not table_util.includes(existing_tag, selected_tag_names)
        end
      ))
    end

    local function add_to_context()
      local selected_tags = selection.getMultiSelection(prompt_bufnr)
      local add_to_context = {}
      for _, tag in ipairs(selected_tags) do
        if not table_util.includes(tag[1], M.context) then
          table.insert(add_to_context, tag[1])
        end
      end
      set_context(table_util.concat(M.context, add_to_context))
    end

    local function clear_context()
      set_context({})
    end

    -- Navigation between panes is pretty hacky and manual, but it works.
    -- It just associates each pane with its neighbors by direction.
    --
    -- 'get_current_focus' returns the popup reference for the current active
    -- window.
    --
    -- 'get_focus_target' returns the next popup to focus on given a particular
    -- direction.
    --
    -- 'nav' curries in a direction to produce a function to bind a key to.

    local function get_current_focus()
      local focused_winid = vim.api.nvim_get_current_win()
      for _, popup in pairs(popups) do
        if popup.winid == focused_winid then
          return popup
        end
      end
      return nil
    end

    local function get_focus_target(dir)
      local current_focus = get_current_focus()
      if not current_focus then return nil end

      local NAV_TARGETS = {
        [popups.prompt] = {
          right = popups.context_query,
          left = nil,
          up = nil,
          down = nil,
        },

        [popups.context_query] = {
          right = nil,
          left = popups.prompt,
          up = nil,
          down = popups.query_results,
        },

        [popups.query_results] = {
          right = nil,
          left = popups.prompt,
          up = popups.context_query,
          down = nil,
        }
      }

      local targets = NAV_TARGETS[current_focus]
      if not targets then return nil end

      return targets[dir]
    end

    local function nav(dir)
      return function()
        local next_target = get_focus_target(dir)
        if not next_target then return end

        vim.api.nvim_set_current_win(next_target.winid)
      end
    end

    -- Mode tells the context how to combine its members into a query.
    local function toggle_mode()
      if M.context_mode == 'all-of' then
        M.context_mode = 'any-of'
      else
        M.context_mode = 'all-of'
      end
      -- refresh prompt text
      set_prompt(get_prompt())
      populate_query_preview_debounced()
    end

    local function toggle_search_limit()
      if M.search_limit == nil then
        M.search_limit = 100
      else
        M.search_limit = nil
      end
      set_prompt(get_prompt())
      populate_query_preview_debounced()
    end

    local function save_to_note()
      show('saving')
      actions.close(prompt_bufnr)
      fs_ops.newNote()
      frontmatter.map_buf_frontmatter({
        bufnr = 0,
        fn = function(prev)
          prev.context = {}
          for _, tag in ipairs(M.context) do
            table.insert(prev.context, tag)
          end
          prev.context_mode = M.context_mode

          prev.title = 'Saved Context - CONTEXT_NAME'
          prev.tags = { 'saved-context' }

          return prev
        end
      })
    end

    local mappings = {
      i = {
        -- Disable default selection
        select_default = function() end,
        ['<C-h>']      = remove_from_context,
        ['<C-l>']      = add_to_context,
        ['<C-x>']      = clear_context,
        ['<C-m>']      = toggle_mode,
        ['<C-s>']      = toggle_search_limit,
        ['<C-o>']      = save_to_note,
        ['<ESC>']      = clear_prompt_or_close,
        ['<M-l>']      = nav('right'),
        ['<M-h>']      = nav('left'),
        ['<M-j>']      = nav('down'),
        ['<M-k>']      = nav('up'),
      }
    }

    local picker = pickers.new(opts, {
      close_on_buf_leave = false,
      mappings = mappings,
      finder = finders.new_table(finder_config),
      sorter = sorters.get_fuzzy_file(),
      previewer = nil,
      attach_mappings = attachMappings.make_attach_mappings(mappings),
      create_layout = create_context_layout(popups),

      sorting_strategy = 'ascending',
    })

    -- Show dialog modes in prompt status text
    local OLD_get_status_text = picker.get_status_text
    picker.get_status_text = function(...)
      local status_text = OLD_get_status_text(...)
      return (
        status_text
        .. ' | '
        .. (M.context_mode == 'any-of' and '∪' or '∩')
        .. ':'
        .. (M.search_limit == nil and '∞' or tostring(M.search_limit))
        .. ' '
      )
    end

    picker:find()
    prompt_bufnr = picker.prompt_bufnr

    -- Initialize the dialog with existing context object
    popups.context_query:set_text(M.context)
    populate_query_preview()

    -- Telescope only knows to attach mappings to the prompt buffer.
    -- We're left to attach them to the others on our own.
    local pax_keymap = {
      { { 'n', 'i', 'x' }, '<C-x>', clear_context, },
      { { 'n', 'i', 'x' }, '<C-m>', toggle_mode, },
      { { 'n', 'i', 'x' }, '<C-s>', toggle_search_limit, },
      { { 'n', 'i', 'x' }, '<C-o>', save_to_note, },
      { { 'n', 'i', 'x' }, '<ESC>', clear_prompt_or_close, },
      { { 'n', 'i', 'x' }, '<M-l>', nav('right'), },
      { { 'n', 'i', 'x' }, '<M-h>', nav('left'), },
      { { 'n', 'i', 'x' }, '<M-j>', nav('down'), },
      { { 'n', 'i', 'x' }, '<M-k>', nav('up'), },
      { 'n',               '<ESC>', function() actions.close(prompt_bufnr) end }
    }

    local function goto_note()
      local winid = vim.api.nvim_get_current_win()
      local row, _ = unpack(vim.api.nvim_win_get_cursor(winid))
      local note = search_cache[row]
      if not note or not note.absPath then
        vim.notify('Could not get note from search cache')
        return
      end

      actions.close(prompt_bufnr)
      vim.cmd('e ' .. note.absPath)
    end

    local context_query_keymap = keymap(pax_keymap)
    local query_results_keymap = keymap(
      table_util.concat(pax_keymap, {
        { 'n', '<CR>', goto_note }
      })
    )

    context_query_keymap.apply(popups.context_query.bufnr)
    query_results_keymap.apply(popups.query_results.bufnr)

    -- By default, telescope exits when the prompt buffer loses focus
    -- Clearing this autocmd allows us to navigate between the different popups
    vim.api.nvim_clear_autocmds({
      group = 'PickerInsert',
      buffer = picker.prompt_bufnr,
      event = 'BufLeave',
    })

    -- Keep M.context up to date with text updates to the query popup
    vim.api.nvim_create_autocmd('TextChanged', {
      buffer = popups.context_query.bufnr,
      group = 'PickerInsert',
      callback = function()
        local lines = vim.api.nvim_buf_get_lines(popups.context_query.bufnr, 0, -1, false)
        set_context(lines)
      end
    })
  end)
end

function M.pick_saved_context()
  local actions      = require('telescope.actions')

  local pick_note    = require('pax-vim.features.notes.telescope.pickNote')
  local SPECIAL_TAGS = require('pax-vim.features.notes.special_tags')
  local get_notes    = require('pax-vim.features.notes.explore.plumbing')
  local frontmatter  = require('pax-vim.features.notes.frontmatter.v2')
  local helpers      = require('pax-vim.features.telescope.helpers')

  get_notes.getNotesByTags({
    tags = { SPECIAL_TAGS.SAVED_CONTEXT },
  }, function(err, notes)
    if err then return end

    pick_note.pickNote(notes, {
      prompt_title = 'Set context',
      mappings = {
        select_default = function(picked_notes)
          local first_note = picked_notes[1]
          local fm = frontmatter.get_file_frontmatter({ filename = first_note.value.absPath })
          if fm == nil then return end
          M.context = fm.context
          M.context_mode = fm.context_mode
        end,

        i = {
          ['<C-o>'] = function(prompt_bufnr)
            local selected_notes = helpers.getMultiSelection(prompt_bufnr)
            actions.close(prompt_bufnr)
            local first_note = selected_notes[1]
            local path = first_note.value.absPath
            vim.cmd('silent edit ' .. path)
          end,
        },
      }
    })
  end)
end

return M
