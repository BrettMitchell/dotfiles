local M = {}

function M.system_binary(binaryName)
  return {
    name = 'Required system binary: ' .. binaryName,
    is_statisfied = function()
      return vim.fn.executable(binaryName)
    end
  }
end

return M
