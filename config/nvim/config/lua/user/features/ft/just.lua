local make_feature = require('user.util.feature').make_feature_v2

local syntax_highlighting_hjson = make_feature({
  name = 'Syntax Highlighting: just',

  description = [[
  This feature adds syntax higlighing for just files.
  ]],

  plugins = { {
    'https://github.com/NoahTheDuke/vim-just',
  } },
})

return syntax_highlighting_hjson
