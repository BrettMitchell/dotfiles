local string_util = require('user.util.string')

return function(dirname, pattern)
  local result = vim.system({
    'fd',
    '--base-directory',
    dirname,
    '--strip-cwd-prefix',
    '--relative-path',
    '--print0',
    '--max-depth',
    '1',
    pattern or '.',
  }):wait()

  local woTrailingNewline = string_util.trim(result.stdout)

  return string_util.split(woTrailingNewline, '%z')
end
