local table_util = require('pax-vim.util.table')
local plumbing = require('pax-vim.features.notes.explore.plumbing')
local pick = require('pax-vim.features.notes.telescope.pickNote')
local deepMergeLeft = require('pax-vim.util.table.merge').deepMergeLeft
local context = require('pax-vim.features.notes.context')

local M = {}

-- All these pickers are implemented by zk-nvim.
-- Unfortunately they do no support custom mappings, so we'll force the issue here

function M.pickNoteByTitle(opts)
  opts = opts or {}
  opts.search = opts.search or {}
  opts.search.tags = table_util.concat(opts.search.tags or {}, context:get_tag_expression())
  opts.telescope = opts.telescope or {}

  plumbing.getNotes(opts.search, function(err, notes)
    if err then
      vim.notify('Error encountered while running zk: ' .. vim.inspect(err), 'error')
      return
    end

    pick.pickNote(notes, opts.telescope)
  end)
end

function M.pickNoteByTag(opts)
  opts = opts or {}
  opts.tagSearch = opts.tagSearch or {}

  -- Support combined telescope config
  opts.tagTelescope = opts.tagTelescope or opts.telescope or {}
  opts.noteTelescope = opts.noteTelescope or opts.telescope or {}

  local function onTags(pickedTags)
    local tagNames = {}
    for _, tag in ipairs(pickedTags) do
      table.insert(tagNames, tag.value.name)
    end

    plumbing.getNotesByTags({ tags = tagNames }, function(getNoteErr, notes)
      if getNoteErr then
        vim.notify('Error encountered while running zk: ' .. vim.inspect(getNoteErr), 'error')
        return
      end

      pick.pickNote(notes, opts.noteTelescope)
    end)
  end

  plumbing.getTags(opts.tagSearch, function(getTagErr, tags)
    if getTagErr then
      vim.notify('Error encountered while running zk: ' .. vim.inspect(getTagErr), 'error')
      return
    end

    pick.pickTags(
      tags,
      deepMergeLeft(
        { mappings = { select_default = onTags } },
        opts.tagTelescope
      )
    )
  end)
end

return M
