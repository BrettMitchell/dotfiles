local make_feature = require('pax-vim.util.feature').make_feature

local lsp = require('pax-vim.features.lsp')

local M = make_feature({
  name = 'Diagnostic Virtual Text',
  dependencies = { lsp },

  description = [[
  This feature adds support for toggleable virtual text displaying diagnostics from
  the LSP.
  ]],

  plugins = {
    {
      'https://git.sr.ht/~whynothugo/lsp_lines.nvim',
      config = function()
        require('lsp_lines').setup()
      end,
    }
  },

  keymaps = {
    {
      'n',
      '<space>lv',
      function()
        local new_value = require('lsp_lines').toggle()
        if new_value then
          vim.notify('Enabled diagnostic virtual lines')
        else
          vim.notify('Disabled diagnostic virtual lines')
        end
      end,
      { desc = 'Toggle LSP diagnostics virtual text' },
    }
  }
})

return M
