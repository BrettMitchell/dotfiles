
# Overview

## Dependencies

- `jq` or `yq`

This module depends on `jq` (or `yq`) for nested property access.

## Binaries

All parameters which are limited to a discrete set of choices show a wizard-style sequence of fuzzy-finders for those options where they are omitted.

### `theme-new-from-image`

Generates a new theme using pywal as starting point. The image specified will become the main wallpaper.

### `theme-use`

Sets the current theme.

### `theme-set`

Sets an attribute within a theme file. This can be use to customize any part of the theme. Operates over a `jq`-style property path for nested property access.

### `theme-get`

Gets an attribute within a theme file. Operates over a `jq`-style property path in the same way as `theme-set`.

### `theme-apply-*`

Each of these scripts is responsible for applying the current theme to a particular part of the system. `theme-apply-all` is an aggregate script which applies each application-specific config in a parallel thread.

