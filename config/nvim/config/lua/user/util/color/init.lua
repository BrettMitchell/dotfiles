return {
  to_hex = require('user.util.color.to_hex'),
  to_rgb = require('user.util.color.to_rgb'),
  lerp = require('user.util.color.lerp'),
  lighten = require('user.util.color.lighten'),
  darken = require('user.util.color.darken'),
}
