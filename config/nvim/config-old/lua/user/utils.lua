-- File source: https://github.com/whatsthatsmell/dots/blob/master/public%20dots/vim-nvim/lua/joel/utils.lua

local Job = require "plenary.job"

local M = {}

function M.is_buffer_empty()
  -- Check whether the current buffer is empty
  return vim.fn.empty(vim.fn.expand "%:t") == 1
end

function M.has_width_gt(cols)
  -- Check if the windows width is greater than a given number of columns
  return vim.fn.winwidth(0) / 2 > cols
end

-- Source: 🔭 utils: https://git.io/JK3ht
function M.get_os_command_output(cmd, cwd)
  if type(cmd) ~= "table" then
    print "Utils: [get_os_command_output]: cmd has to be a table"
    return {}
  end
  local command = table.remove(cmd, 1)
  local stderr = {}
  local stdout, ret = Job
      :new({
        command = command,
        args = cmd,
        cwd = cwd,
        on_stderr = function(_, data)
          table.insert(stderr, data)
        end,
      })
      :sync()
  return stdout, ret, stderr
end

function M.showDashboard()
  if require('zk.util').notebook_root(vim.fn.expand('%:p')) ~= nil then
    require('user.notes.explore.plumbing').getNotesByTags({ tags = { 'meta.type.dashboard' } }, function(err, notes)
      if (err) then
        vim.notify('Could not open dashboard note: ' .. vim.inspect(err), 'error')
        vim.cmd('Alpha')
        return
      end

      if #notes == 0 then
        vim.notify('No dashboard note found', 'error')
        vim.cmd('Alpha')
        return
      end

      local note = notes[1]

      vim.cmd('edit' .. note.absPath)
    end)
    return
  else
    vim.cmd('Alpha')
  end
end

function M.escapePattern(str)
  return str:gsub('([-$()?+^%%.*%[%]])', '%%%1')
end

function M.closeWindow(winnr)
  local _, res = pcall(vim.api.nvim_win_close, winnr, false)
  if res and res:find('Cannot close last window') then
    M.showDashboard()
  end
end

return M
