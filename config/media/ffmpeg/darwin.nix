{ nixpkgs, ... }:
{
  home.packages = with nixpkgs.stable; [
    ffmpeg
    ffmpegthumbnailer
  ];
}
