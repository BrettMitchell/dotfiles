{ nixpkgs, ... }:

{
  home.file.".config/mpv".source = ./config;

  home.packages = with nixpkgs.stable; [
    ffmpeg_7-full
    imagemagick
    yt-dlp
    playerctl
    mpv-custom
  ];
}
