
# Define foreground function for controlling any interactive zsh
# process which uses this module.
#
# TODO: This is currently a fire-and-forget API.
#       Figure out a low-latency mechanism for signaling the end
#       of output (possibly just an obscure sequence of control chars)
#       to add support for round-trip output.
#
#       Note that this will require 2 variants:
#       1. No stdout redirection (required to use interactive commands like nvim)
#       2. stdout redirection (required to return output to the calling process)
function zle_remote {
  local remote="$1"
  shift

  local out=/dev/null
  # local out=`mktemp`

  local msg="$out"
  for arg in "$@" ; do
    msg+=$'\t'"$arg"
  done

  # Wait for the other process to close its
  # file descriptor to the out file
  # inotifywait -e close "$out" & # 1>/dev/null 2>/dev/null
  # sleep 1

  printf '%s\n' "$msg" >$remote

  # fg

  # IFS= read -r output <"$out"
  # print -r - "$output"

  # rm "$out"
}
