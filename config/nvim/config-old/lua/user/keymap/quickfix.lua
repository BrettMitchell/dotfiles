-- local qf             = require('qf')
-- local replacer       = require('replacer')
-- local autocmd        = require('user.lib.autocmd').autocmd

local registerKeyMap = require('user.lib.map').registerKeyMap

local M              = {}

function M.setup()
  -- Quickfix
  registerKeyMap({ 'n' }, {
    ['<M-q>'] = {
      function()
        require('qf').toggle('c', false)
      end,
      'Toggles quickfix menu',
    },
    ['<M-S-q>'] = {
      function()
        vim.cmd 'cexpr []'
      end,
      'Clears the quickfix menu',
    },
    -- ['<C-r>'] = {
    --   function()
    --     if (vim.bo.filetype == 'qf') then
    --       replacer.run()
    --     end
    --   end,
    --   'Enable replacer',
    -- },
  });

  -- TODO: Create a buffer local keybinding for this only for quickfix file types.
  --       This should NOT be C-r. Pick something else
  -- autocmd(
  --   'Replacer',
  --   {}
  --  )
end

return M
