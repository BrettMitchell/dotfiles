local TAGS = require('pax-vim.features.notes.special_tags')

local M = {}

M.taxonomy = {
  frontmatter = function(_, _, cb)
    return cb(nil, {
      title = 'Taxonomy :: TAXONOMY_ROOT_TAG_NAME',
      tags = { TAGS.TAXONOMY },
      -- tags = { TAGS.QUERY, TAGS.TAXONOMY },
      -- queries = { { tags = {}, format = 'taxonomy' } }
    })
  end,
  build = function(existingBody, _, cb) return cb(nil, existingBody) end
}

return M
