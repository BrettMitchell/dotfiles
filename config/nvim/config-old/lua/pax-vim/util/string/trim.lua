return function(str)
  local without_leading = string.gsub(str, '^%s+', '')
  local without_trailing = string.gsub(without_leading, '%s+$', '')
  return without_trailing
end
