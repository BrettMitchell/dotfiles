source ~/.config/skim/keybindings.sh
source ~/.config/skim/colors.sh

bindkey "^[^?" backward-kill-line
bindkey "^H" backward-kill-word

# Open or create a project session for a project directory
bindkey -s "^[o" ' open-project-session `choose-project`^M'
# Open or create a project session for a branch in any bare project repo clone
bindkey -s "^[G" ' open-project-session `choose-project-worktree-branch`^M'
# Open or create a project session for any directory in projects root
bindkey -s "^[D" ' open-project-session `search-filenames --dir "$PROJECTS_DIR" --type dir`^M'

# Attach to the last active tmux session
bindkey -s "^[p" ' tmux attach^M'

# Fuzzy find active tmux sessions and attach to one
bindkey -s "^[i" ' open-project-session `pick-tmux-session "Open a tmux session"`^M'
# Fuzzy find active tmux sessions and kill one or more
bindkey -s "^[x" ' kill-project-session `pick-tmux-session "Open a tmux session"`^M'

# Attach to notes session
if [[ -d "$NOTES_DIR" ]] ; then
  bindkey -s "^[n" " open-project-session '$NOTES_DIR'^M"
else
  bindkey -s "^[n" " printf \"Environment variable \$NOTES_DIR does not point to a directory: '$NOTES_DIR'\"^M"
fi

# Bind scripts as line editor widgets before binding the key
function zle_eval {
  echo -en "\e[2K\r"
  eval "$@"
  zle reset-prompt
}

function zle_btm { zle_eval btm; }
zle -N zle_btm; bindkey '^[m' zle_btm

function zle_clipboard {
  choose-clipboard-history < /dev/tty > /dev/null
}
zle -N zle_clipboard; bindkey "^[y" zle_clipboard

# function zle_lf {
#   lfcd --config ~/.config/lf/shell.lfrc < /dev/tty
# }
# zle -N zle_lf; bindkey "^[b" zle_lf
bindkey -s "^[b" ' lfcd --config ~/.config/lf/shell.lfrc^M'

function zle_ncmpcpp {
  ncmpcpp < /dev/tty
}
zle -N zle_ncmpcpp; bindkey "^[M" zle_ncmpcpp

function zle_fzf_visible {
  dir="$(search-filenames --dir $(pwd) --type dir < /dev/tty)";
  if [[ -d "$dir" ]] ; then
    zle_eval cd "$dir"
  fi
}
zle -N zle_fzf_visible; bindkey "^[s" zle_fzf_visible

function zle_fzf_hidden {
  dir="$(search-filenames --dir $(pwd) --type dir --show-hidden true < /dev/tty)";
  if [[ -d "$dir" ]] ; then
    zle_eval cd "$dir"
  fi
}
zle -N zle_fzf_hidden; bindkey "^[S" zle_fzf_hidden

function zle_maybe_kill {
  pids=`choose-process-id < /dev/tty`

  if [[ -z "$pids" ]]; then
    printf "No processes selected, exiting without killing anything...\n"
    return 0
  fi

  printf "Killing process IDs: $pids\n"

  kill $pids
}
# bindkey -s "^[K" ' maybe_kill `ps aux --no-headers | sk --multi --reverse --color="$SK_COLORS" --bind="ctrl-r:toggle-sort,$SKIM_KEYBINDINGS_MOTION,$SKIM_KEYBINDINGS_MULTISELECT" | awk '"'"'{print $2}'"'"'`^M'
zle -N zle_maybe_kill; bindkey "^[K" zle_maybe_kill

# Adapted from the skim repo: https://github.com/lotabout/skim/blob/master/shell/key-bindings.zsh
function zle_fzf_rev_history {
  local selected num
  setopt localoptions noglobsubst noposixbuiltins pipefail no_aliases 2> /dev/null

  selected=( $(fc -rl 1 | perl -ne 'print if !$seen{(/^\s*[0-9]+\**\s+(.*)/, $1)}++' |
    sk -n2..,.. --tiebreak=index --color="$SK_COLORS" --bind="ctrl-r:toggle-sort,$SKIM_KEYBINDINGS_MOTION" --query=${LBUFFER} --no-multi) )

  local ret=$?
  if [ -n "$selected" ]; then
    num=$selected[1]
    if [ -n "$num" ]; then
      zle vi-fetch-history -n $num
    fi
  fi

  zle reset-prompt
  return $ret
}
zle -N zle_fzf_rev_history; bindkey '^[u' zle_fzf_rev_history

function zle_show_not_tmux {
  local message="tmux is running"
  if [ -z "$TMUX" ]; then
    message="tmux is not running"
  fi
  zle_eval "echo 'ZSH got M-space, and $message'";
}
zle -N zle_show_not_tmux; bindkey '^[' zle_show_not_tmux

