#!/usr/bin/env bash

# ln_safe "$module/systemd" "$HOME/.config/systemd"
mkdir -p "$HOME/.config/systemd/user"

default_cmd_script=`command -v __mux-default-cmd-list`
external_cmd_dir="$HOME/.local/share/tsessd/session_data"
mkdir -p "$external_cmd_dir"

cat <<SERVICE > "$HOME/.config/systemd/user/tsessd.service"
[Unit]
Description=Session coordinator daemon for tmux

[Service]
Type=simple
Environment='TSESSD_DEFAULT_CMD_SCRIPT=$default_cmd_script'
Environment='TSESSD_EXTERNAL_CMD_DIR=$external_cmd_dir'
ExecStart=$HOME/.local/bin/tsessd

[Install]
WantedBy=default.target
SERVICE
