local feat_util = require('user.util.feature')
local plugin_util = require('user.util.plugins.lazy')
local dap = require('user.features.dap')
local telescope = require('user.features.telescope')

-- Reference for future use when creating more advanced, ergonomic attach interaction
-- https://github.com/nvim-telescope/telescope-dap.nvim/blob/master/lua/telescope/_extensions/dap.lua#L62

local M = {
  name = 'DAP :: Variable Values in Virtual Text',

  description = [[
  This feature shows variable values in virtual text at the end of the
  line where the variable is defined or referenced.
  ]],
}

function M.setup()
  feat_util.assert_deps('DAP Telescope Integration', { dap, telescope })
  plugin_util.add_plugins({
    {
      'https://github.com/nvim-telescope/telescope-dap.nvim',
      event = 'VeryLazy',
      dependencies = { 'https://github.com/nvim-telescope/telescope.nvim' },
      config = function()
        require('telescope').load_extension('dap')
      end
    }
  })
end

return M
