local filter = require('pax-vim.util.table.filter')
local concat = require('pax-vim.util.table.concat')

local M = {}

M.plugins = {}

M.opts = {}

M.post_load_callbacks = {}

-- Source: https://github.com/folke/lazy.nvim#-installation
function M.bootstrap()
  local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"

  if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
      "git",
      "clone",
      "--filter=blob:none",
      "https://github.com/folke/lazy.nvim.git",
      "--branch=stable",
      lazypath,
    })
  end

  vim.opt.rtp:prepend(lazypath)
end

function M.setup()
  require('lazy').setup(M.plugins, M.opts)

  for _, cb in ipairs(M.post_load_callbacks) do
    cb()
  end
end

function M.add_plugins(plugins)
  M.plugins = concat(M.plugins, plugins)
end

function M.add_plugin(plugin)
  table.insert(M.plugins, plugin)
end

function M.add_post_load_cb(callback)
  table.insert(M.post_load_callbacks, callback)
end

function M.remove_post_load_cb(callback)
  M.post_load_callbacks = filter(
    M.post_load_callbacks,
    function(cb) return cb ~= callback end
  )
end

function M.find_lazy_plugin(plugin_def)
  local lazy_config = require('lazy.core.config')

  for _, plugin in pairs(lazy_config.plugins) do
    if type(plugin_def[1]) == 'string' then
      if plugin.url == lazy_config.git.url_format(plugin_def[1]) then
        return plugin
      end
    end

    if type(plugin_def.url) == 'string' then
      if plugin.url == plugin_def.url then
        return plugin
      end
    end
  end

  return nil
end

return M
