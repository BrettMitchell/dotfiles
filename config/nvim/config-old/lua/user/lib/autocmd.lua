local function autocmd(group, groupSpec)
  local groupOpts = {}
  local cmds = {}
  for k, v in pairs(groupSpec) do
    if type(k) == 'number' then
      cmds[k] = v
    else
      groupOpts[k] = v
    end
  end

  local groupId = vim.api.nvim_create_augroup(group, groupOpts)

  for _, cmd in ipairs(cmds) do
    local triggers = {}
    for i = 1, #cmd - 1 do
      triggers[i] = cmd[i]
    end

    local cmdSpec = cmd[#cmd]
    local fullSpec = vim.tbl_extend("force", { group = groupId }, cmdSpec)

    vim.api.nvim_create_autocmd(triggers, fullSpec)
  end

  return groupId
end

return { autocmd = autocmd }
