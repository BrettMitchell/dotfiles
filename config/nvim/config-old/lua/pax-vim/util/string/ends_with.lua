return function (str, suffix)
  return str:sub(-#suffix, -1) == suffix
end
