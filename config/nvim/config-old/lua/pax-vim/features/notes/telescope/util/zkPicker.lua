local zkApi = require('zk.api')

local pickers = require('telescope.pickers')
local finders = require('telescope.finders')
local sorters = require('telescope.sorters')

local table_util = require('pax-vim.util.table')

local attachMappings = require('pax-vim.features.telescope.attachMappings')
local theme = require('pax-vim.features.telescope.theme')

local zkFinder = require('pax-vim.features.notes.telescope.util.zkFinder')
local noteUtil = require('pax-vim.features.notes.util')

local function zkPick(opts)
  local default_theme = theme.horizontal
  if not opts.previewer then
    default_theme = theme.centered
  end

  opts = table_util.merge.deepMergeLeft(opts or {}, default_theme)

  local mappings = opts.mappings or {}

  local finderConfig = {
    getQuery = opts.getQuery
  }

  if opts.entry_maker then
    finderConfig.entry_maker = opts.entry_maker
  end

  local function getSorter()
    if (opts.highlighter) then
      return sorters.Sorter:new({
        scoring_function = function () return 1 end,
        highlighter = opts.highlighter,
      })
    end

    return sorters.highlighter_only()
  end

  local sorter = opts.sorter or getSorter()

  local picker = pickers.new(opts, {
    default_text = opts.default_text,
    results_title = opts.title or '',
    prompt_title = opts.prompt_title or '',
    finder = zkFinder.new_zk_finder(finderConfig),
    sorter = sorter,
    previewer = opts.previewer or nil,
    attach_mappings = attachMappings.make_attach_mappings(mappings),
  })

  picker:find(theme.horizontal)
end

local function zkPickTags(opts)
  local default_theme = theme.horizontal
  if not opts.previewer then
    default_theme = theme.centered
  end

  opts = table_util.merge.deepMergeLeft(opts or {}, default_theme)

  local mappings = opts.mappings or {}

  zkApi.tag.list(noteUtil.getNotesDir(), opts.query or {}, function(err, tags)
    if (err) then
      vim.notify('Encountered an error while running zk: ' .. vim.inspect(err), 'error')
      return
    end

    local picker = pickers.new(opts, {
      default_text = opts.default_text,
      results_title = opts.results_title or '',
      prompt_title = opts.prompt_title or '',

      attach_mappings = attachMappings.make_attach_mappings(mappings),
      previewer = opts.previewer or nil,
      sorter = sorters.get_generic_fuzzy_sorter(),

      finder = finders.new_table {
        results = tags,
        entry_maker = function(tag)
          return {
            value = tag,
            display = tag.name,
            ordinal = tag.name,

            preview_command = function(entry, bufnr)
              vim.api.nvim_buf_set_lines(bufnr, 0, -1, true, entry.value.files)
            end
          }
        end
      }
    })

    picker:find()
  end)
end

return {
  zkPick = zkPick,
  zkPickTags = zkPickTags,
}
