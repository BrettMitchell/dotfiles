local make_feature = require('user.util.feature').make_feature_v2
local system_binary = require('user.util.feature.system_binary_dependency').system_binary
local make_augroup = require('user.util.autocmd').make_augroup
local make_keymap = require('user.util.keymap')
local table_utils = require('user.util.table')

local markdown_preview = make_feature({
  name = 'Markdown Preview',

  description = [[
  This feature uses 'pandoc' to convert Markdown files to PDF and then
  previews the result in 'zathura'.

  This avoids closing and reopening zathura by sending the converted
  file to a temporary file on save, and watching that rather than 
  the currently focused buffer file directly.
  ]],

  dependencies = {
    system_binary('zathura'),
    system_binary('pandoc'),
    system_binary('pdflatex'), -- NOTE: This comes from extra/texlive group on Arch Linux
  },

  tmp_filename = vim.fn.tempname() .. '.pdf',
  preview_enabled = false,
  zathura_job = nil,
  initial_render_done = false,

  preview_zathura_file = function (self, filename)
    if self.zathura_job then
      return
    end

    local cmd = 'zathura ' .. filename
    local ok, res = pcall(vim.fn.jobstart, cmd, {
      on_exit = function ()
        self.zathura_job = nil
        self.preview_enabled = false
      end,
    })

    if not ok then
      vim.notify('Error while starting zathura: ' .. vim.inspect(res))
    else
      self.zathura_job = res
    end
  end,

  stop_zathura_preview = function (self)
    if self.zathura_job then
      vim.fn.jobstop(self.zathura_job)
      self.zathura_job = nil
    end
  end,

  set_preview = function (self, preview_enabled)
    self.preview_enabled = preview_enabled
    if self.preview_enabled then
      self:preview_zathura_file(self.tmp_filename)
    else
      self:stop_zathura_preview()
    end
  end,

  toggle_preview = function (self)
    self.preview_enabled = not self.preview_enabled
    if self.preview_enabled then
      self:preview_zathura_file(self.tmp_filename)
    else
      self:stop_zathura_preview()
    end
  end,

  init = function(self)
    self.keymaps = make_keymap {
      { 'n', '<space>upm', function ()
        self:toggle_preview()
      end, { desc = 'Toggle markdown auto-preview' } }
    }

    self.augroups = make_augroup {
      {
        name = 'MarkdownPreviewTempFileWrite',
        autocmds = { {
          events = { 'BufWritePost', 'BufEnter' },
          opts = {
            callback = function ()
              local ft = vim.bo.filetype
              if ft == 'markdown' or ft == 'quarto' then
                local filename = vim.fn.expand('%:p')
                local cmd = 'pandoc -o ' .. self.tmp_filename .. ' ' .. filename
                local stdout = {}
                local stderr = {}

                -- It is important that we wait until the initial render is done before 
                local start_ok = pcall(vim.fn.jobstart, cmd, {
                  on_stdout = function(job_id, data)
                    table.insert(stdout, table_utils.join(data, ''))
                  end,

                  on_stderr = function(job_id, data)
                    table.insert(stderr, table_utils.join(data, ''))
                  end,

                  on_exit = function(job_id, exit_code, exit_reason)
                    if exit_code ~= 0 then
                      vim.notify('Convert job exited with code ' .. tostring(exit_code), 'error')
                      vim.notify('Stdout: \n' .. table_utils.join(stdout, '\n'))
                      vim.notify('Stderr: \n' .. table_utils.join(stderr, '\n'))
                      return
                    end

                    if not self.initial_render_done and self.preview_enabled then
                      self.initial_render_done = true
                      self:preview_zathura_file(self.tmp_filename)
                    end
                  end
                })

                if not start_ok then
                  vim.notify('Could not start convert job', 'error')
                end
              end
            end
          }
        } }
      },

      {
        name = 'MarkdownAutoPreview',
        autocmds = { {
          events = 'BufEnter',
          opts = {
            callback = function ()
              local ft = vim.bo.filetype
              if self.preview_enabled and (ft == 'markdown' or ft == 'quarto') then
                if self.initial_render_done then
                  self:preview_zathura_file(self.tmp_filename)
                end
              end
            end
          }
        } }
      }
    }
  end,

  apply = function(self)
    self.keymaps.apply()
    self.augroups:apply()
  end
})

return markdown_preview
