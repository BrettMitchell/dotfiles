local make_augroup = require('user.util.autocmd').make_augroup
local keymap = require('user.util.keymap')
local pick_static = require('user.features.telescope').methods.pick_static
local plugin_util = require('user.util.plugins.lazy')
local tmux_util = require('user.util.tmux')
local sys_util = require('user.util.sys')
local file_util = require('user.util.file')
local string_util = require('user.util.string')

local open_external = require('user.features.mux.open_external')

local M = {}

--[[

Unmap:
https://github.com/kevinhwang91/nvim-bqf/blob/main/lua/bqf/keymap.lua

this feature should:
- Open the most recent quickfix list
- Make the buffer readonly
- Bind:
  - o -> Open
  - O -> Open, pick by number
  - p -> Toggle preview

--]]

M = {
  name = 'Use nvim as an interface to session local reference lists',

  description = [[
  This feature converts nvim into a reference list client.

  Reference lists are automatically loaded, and normal editing is disabled.
  ]],

  --- @type string
  current = nil,
  open_target = nil,
  -- filename -> parsed ref list
  ref_lists = {},
  -- bufnr -> filename
  buf_map = {},
  should_hook_telescope = true,

  augroup = make_augroup {
    {
      name = 'AutoOpenQuickFix',
      autocmds = {
        {
          events = 'VimEnter',
          opts = {
            callback = function()
              local dir = M.dir()
              if not dir then
                vim.notify('No quickfix directory found for current TMUX session', 'error')
                return
              end

              if M.current ~= nil then
                M.open(M.current)
                return
              end

              local current_index = M.get_highest_index()
              if current_index then
                M.open(current_index)
              end
            end
          }
        }
      }
    }
  }
}

function M.dir()
  local data_dir = tmux_util.get_tsessd_state_dir()
  if not data_dir then return end
  local qf_dir = data_dir .. '/ref-lists'
  return qf_dir
end

-- NOTE: Depends on 'fd'
function M.list(pattern)
  local qf_dir = M.dir()
  if not qf_dir then return {} end
  local qf_files = sys_util.scandir(qf_dir, pattern)
  for i, qf_file in ipairs(qf_files) do
    qf_files[i] = string_util.replace_end(qf_file, '.json', '')
  end
  return qf_files
end

function M.load_from_disk(id)
  local json = require('lunajson')

  if not id then
    id = M.get_highest_index()
  end

  if not id then return end

  local filename = M.dir() .. '/' .. id .. '.json'
  if vim.fn.filereadable(filename) ~= 1 then
    vim.notify("No such file: " .. filename, 'error')
    return nil
  end

  local filelines = file_util.get_file_str({
    filename = filename
  })

  local parsed = json.decode(filelines)
  M.ref_lists[filename] = parsed

  return parsed
end

function M.render_ref_list(ref_list)
  local lines = {}

  local tmux_root_dir = tmux_util.get_tmux_value('session_path')
  -- Ensure trailing slash for consistent formatting
  if tmux_root_dir:sub(#tmux_root_dir, #tmux_root_dir) ~= '/' then
    tmux_root_dir = tmux_root_dir .. '/'
  end

  for i, ref in ipairs(ref_list.refs) do
    -- TODO: Potentially support alternative rendering strategies here
    --       There may be some additional metadata to show for things like LSP references
    lines[i] = M.format_display(ref, tmux_root_dir)
  end
  return lines
end

local function open_from_telescope(selected_lists)
  if #selected_lists < 1 then return end
  local list_id = selected_lists[1][1]
  M.open(list_id)
end

function M.pick_list(cb)
  local opts = M.list()
  pick_static(opts, {
    prompt_title = 'Ref lists',
    -- Required for single character searching
    sorter = require('telescope.sorters').get_fzy_sorter(),
    mappings = {
      select_default = cb,
    }
  })
end

function M.get_highest_index()
  local sorted_numeric_qf_files = M.list('^[0-9]+\\.json$')
  local highest_index = sorted_numeric_qf_files[#sorted_numeric_qf_files]
  return highest_index
end

function M.next()
  if not M.current or not M.current:match('^%d+$') then
    return
  end

  local qf_dir = M.dir()
  if not qf_dir then return end

  local next_num = tonumber(M.current) + 1
  local next_str = tostring(next_num)
  if vim.fn.filereadable(qf_dir .. '/' .. next_str .. '.json') ~= 1 then
    next_str = '0'
  end

  M.current = next_str
  M.open(M.current)
end

function M.prev()
  if not M.current or not M.current:match('^%d+$') then
    return
  end

  local qf_dir = M.dir()
  if not qf_dir then return end

  local prev_num = tonumber(M.current) - 1
  local prev_str = tostring(prev_num)
  if vim.fn.filereadable(qf_dir .. '/' .. prev_str .. '.json') ~= 1 then
    prev_str = M.get_highest_index()
  end

  M.current = prev_str
  M.open(M.current)
end

function M.open(id)
  local ref_list = M.load_from_disk(id)
  if not ref_list then
    return
  end

  local rendered_lines = M.render_ref_list(ref_list)

  local bufnr = nil
  for existing_bufnr, ref_list_filename in pairs(M.buf_map) do
    if ref_list_filename == ref_list.filename then
      bufnr = tonumber(existing_bufnr)
    end
  end

  if bufnr == nil then
    vim.cmd('enew')
    bufnr = vim.api.nvim_get_current_buf()
    local name = ref_list.title
    if name == nil or name == "" then
      name = ref_list.id
    end
    if name == nil or name == "" then
      name = 'Untitled ref list'
    end
    vim.api.nvim_buf_set_option(bufnr, "buftype", "nofile")
    vim.api.nvim_buf_set_option(bufnr, 'statusline', '%#Normal# ' .. name .. ' ')
    M.buf_map[bufnr] = ref_list.filename
  else
    vim.cmd('buffer ' .. tostring(bufnr))
  end

  vim.api.nvim_buf_set_option(bufnr, "modifiable", true)
  vim.api.nvim_buf_set_lines(bufnr, 0, -1, true, rendered_lines)
  vim.api.nvim_buf_set_option(bufnr, "modifiable", false)
  vim.api.nvim_buf_set_option(bufnr, "filetype", "quickfix")

  M.current = id

  M.keymaps.apply(bufnr)
end

function M.get_ref_under_cursor()
  local bufnr = vim.api.nvim_get_current_buf()
  local current_file = M.buf_map[bufnr]
  local current_ref_list = M.ref_lists[current_file]
  if not current_ref_list then
    return nil
  end

  local cursor_pos = vim.fn.getpos('.')
  local line_nr = cursor_pos[2]

  local ref = current_ref_list.refs[line_nr]
  return ref
end

local function truncate_file(file, tmux_root_dir)
  if not string_util.starts_with(file, tmux_root_dir) then
    return file
  end

  return string_util.replace_start(file, tmux_root_dir, '')
end

function M.format_display(ref, tmux_root_dir)
  if ref.type == open_external.REF_TYPE.FILE then
    return truncate_file(ref.target.filename, tmux_root_dir)
  end

  if ref.type == open_external.REF_TYPE.FILE_LINE then
    return truncate_file(ref.target.filename, tmux_root_dir) ..
    ':' .. tostring(ref.target.line) .. ':' .. ref.target.text
  end

  if ref.type == open_external.REF_TYPE.FILE_LINE_COLUMN then
    return truncate_file(ref.target.filename, tmux_root_dir) ..
        ':' .. tostring(ref.target.line) .. ':' .. tostring(ref.target.col) .. ':' .. ref.target.text
  end

  if type(ref.display) == 'string' then
    return ref.display
  end

  return require('lunajson').encode(ref)
end

function M.open_under_cursor()
  local ref = M.get_ref_under_cursor()
  if not ref then
    vim.notify('No ref found under cursor', 'error')
    return
  end
  open_external.open_ref_external({ ref = ref })
end

function M.open_under_cursor_pick()
  local ref = M.get_ref_under_cursor()
  open_external.open_ref_external_pick({ ref = ref })
end

function M.rm_current_list()
  if not M.current then return end
  local session_name = tmux_util.get_tmux_value('session_name')
  local bufnr = vim.api.nvim_get_current_buf()
  vim.system({ 'tsess', 'ref-list-rm', session_name, M.current })
  vim.cmd('bdelete')
  M.buf_map[bufnr] = nil

  -- ref-list-rm re-indexes numbered lists, so M.current will now
  -- point to a different list. For any case other than deleting
  -- the last list, M.current should stay where it is
  local filename = M.dir() .. '/' .. M.current .. '.json'
  if vim.fn.filereadable(filename) == 0 then
    M.current = M.get_highest_index()
  end

  M.open(M.current)
end

local function entry_to_qf(entry)
  local text = entry.text

  if not text then
    if type(entry.value) == "table" then
      text = entry.value.text
    else
      text = entry.value
    end
  end

  local filename = entry.filename
  if filename == nil then
    filename = entry.value
  end
  if filename == nil then
    return nil
  end

  local ref_type = open_external.REF_TYPE.FILE
  local target = {
    filename = filename,
  }

  if entry.lnum ~= nil then
    target.line = tonumber(entry.lnum)
    ref_type = open_external.REF_TYPE.FILE_LINE
  elseif entry.col ~= nil then
    target.line = 0
  end

  if entry.col ~= nil then
    target.col = tonumber(entry.col)
    ref_type = open_external.REF_TYPE.FILE_LINE_COLUMN
  end

  if ref_type == open_external.REF_TYPE.FILE_LINE or ref_type == open_external.REF_TYPE.FILE_LINE_COLUMN then
    target.text = entry.text
  end

  return {
    type = ref_type,
    target = target,
    metadata = {
      source = 'nvim.telescope',
    },
  }
end

function M.telescope_create_reflist(prompt_bufnr, mode, target)
  local json = require('lunajson')
  local action_state = require('telescope.actions.state')
  local actions = require('telescope.actions')

  local picker = action_state.get_current_picker(prompt_bufnr)
  local manager = picker.manager

  local refs = {}
  for entry in manager:iter() do
    table.insert(refs, entry_to_qf(entry))
  end

  actions.close(prompt_bufnr)

  local ref_list = {
    refs = refs
  }

  local serialized = json.encode(ref_list)
  local session_name = tmux_util.get_tmux_value('session_name')
  vim.system({ 'tsess', 'ref-list-new', session_name, '--value', serialized })
end

function M.hook_telescope()
  plugin_util.add_post_load_cb(function()
    local ok, telescope_keys = pcall(require, 'user.features.telescope.keys')
    if not ok then
      show(telescope_keys)
    else
      local oldGetInsertMode = telescope_keys.getInsertMode
      telescope_keys.getInsertMode = function()
        local insertMode = oldGetInsertMode()
        insertMode['<C-q>'] = M.telescope_create_reflist
        return insertMode
      end
    end
  end)
end

function M.setup()
  if M.augroup ~= nil then
    M.augroup:apply()
  end

  if M.global_keymap ~= nil then
    M.global_keymap.apply()
  end

  if M.should_hook_telescope then
    M.hook_telescope()
  end
end

M.global_keymap = keymap {
  { 'n', '<space>f', function() M.pick_list(open_from_telescope) end, { desc = 'Pick list with telescope' } },
}

M.keymaps = keymap {
  { 'n', '>',        M.next,                   { desc = 'Next ref list' } },
  { 'n', '<',        M.prev,                   { desc = 'Previous ref list' } },
  { 'n', '<enter>',  M.open_under_cursor,      { desc = 'Open location item under cursor' } },
  { 'n', 'o',        M.open_under_cursor,      { desc = 'Open location item under cursor' } },
  { 'n', 'O',        M.open_under_cursor_pick, { desc = 'Open location item under cursor, pick pane by number' } },
  { 'n', '<space>D', M.rm_current_list,        { desc = 'Remove the currently focused list' } },
}

return M
