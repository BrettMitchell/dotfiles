local M = {}

function M.split(str, sep)
  if sep == nil or string.len(sep) == 0 then sep = "%s" end

  local parts = {}
  for match in string.gmatch(str, "([^" .. sep .. "]+)") do
    table.insert(parts, match)
  end
  return parts
end

function M.repeatStr(n, str)
  local res = ''
  for i = 1, n do
    res = res .. str
  end
  return res
end

return M
