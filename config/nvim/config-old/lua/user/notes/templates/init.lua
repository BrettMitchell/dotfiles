local pickOption = require('user.telescope.pickOption').pickOption
local tableUtil = require('user.lib.table')

local journal = require('user.notes.templates.journal')
local savedSearch = require('user.notes.templates.savedSearch')
local templateNote = require('user.notes.templates.templateNote')
local fileRep = require('user.notes.templates.fileRep')
local webResource = require('user.notes.templates.webResource')
local frontmatter = require('user.notes.frontmatter')

local M = {
  autoTemplates = {
    journal = journal.auto,
  },
  contextFreeTemplates = {
    ['Journal (weekday)'] = journal.weekday,
    ['Journal (weekend)'] = journal.weekend,
    ['File representative'] = fileRep.fileRep,
    ['Web resource'] = webResource.webResource,
    ['Saved search'] = savedSearch.savedSearch,
    ['New template note'] = templateNote.newTemplateNote,
    ['From template note'] = templateNote.fromTemplateNote,
  }
}

-- Insert template contents under cursor and set template frontmatter attributes
function M.applyTemplate(template, ctx)
  local fileLines = frontmatter.getFileLines({ showNotifications = false })
  local frontmatterLines = frontmatter.getFrontmatterLines(fileLines)
  local insertPoint = vim.fn.line('.')
  if frontmatterLines ~= nil then
    if #frontmatterLines >= insertPoint then
      insertPoint = #frontmatterLines + 2
    end
  end

  local noteBody = tableUtil.slice(fileLines, #frontmatterLines + 3)

  template.build(noteBody, ctx, function (textErr, text)
    if textErr then
      vim.notify('Encountered an error while applying template build function: ' .. vim.inspect(textErr))
      return
    end

    local lines = text
    if type(lines) == 'string' then
      local i = 1
      lines = {}
      for line in text:gmatch '[^\n]+' do
        lines[i] = line
        i = i + 1
      end
    end

    vim.fn.append(insertPoint, lines)

    if template.frontmatter then
      local parsedFrontmatter = frontmatter.parseFrontmatter({ frontmatterLines = frontmatterLines })
      template.frontmatter(parsedFrontmatter, ctx, function (frontmatterErr, builtFrontmatter)
        if frontmatterErr then
          vim.notify('Encountered an error while applying template build function: ' .. vim.inspect(textErr))
          return
        end

        frontmatter.setAttributes(builtFrontmatter, { merge = true })
      end)
    end
  end)
end

function M.applyAutoTemplate(templateName, ctx)
  local template = M.autoTemplates[templateName]
  if not template then
    vim.notify('No template named ' .. templateName, 'error')
    return
  end

  return M.applyTemplate(template, ctx)
end

local function handleSelectedTemplate(selectedTemplates)
  if #selectedTemplates == 0 then return end

  if #selectedTemplates > 1 then
    vim.notify('Selecting more than 1 template at a time is not supported', 'error')
    return
  end

  local templateName = selectedTemplates[1][1]
  local template = M.contextFreeTemplates[templateName]

  if not template then
    vim.notify('No such template ' .. templateName, 'error')
    return
  end

  M.applyTemplate(template)
end

function M.pickTemplate()
  local templateOptions = {}
  for k, _ in pairs(M.contextFreeTemplates) do
    table.insert(templateOptions, k)
  end

  pickOption(templateOptions, {
    title = 'Pick a template to apply',
    mappings = {
      select_default = handleSelectedTemplate,
    },
  })
end

return M
