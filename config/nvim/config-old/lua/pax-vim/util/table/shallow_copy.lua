return function(tbl)
  local res = {}

  for i, v in ipairs(tbl) do
    res[i] = v
  end

  for k, v in pairs(tbl) do
    res[k] = v
  end

  return res
end
