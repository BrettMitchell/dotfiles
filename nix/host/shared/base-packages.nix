{ pkgs, ... }:
{
  basePackages = [
    wget
    curl
    nvi # Original Vi

    git
    gnumake
    gcc

    binutils
  ]
}
