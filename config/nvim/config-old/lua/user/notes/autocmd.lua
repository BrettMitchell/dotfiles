local autocmd = require('user.lib.autocmd').autocmd

local frontmatter = require('user.notes.frontmatter')

local M = {}

function M.setup()
  autocmd(
    'FrontmatterOnSave',
    {
      clear = true,
      { 'BufWritePre', {
        pattern = { '*.md', '*.qmd' },
        callback = function()
          frontmatter.generateFrontmatter()
        end
      } }
    }
  )

  autocmd(
    'BindingsOnBufEnter',
    {
      clear = true,
      { 'BufEnter', {
        pattern = { '*.md', '*.qmd' },
        callback = function()
          frontmatter.loadBindings()
        end
      } }
    }
  )

  autocmd(
    'IndexZkNotes',

    {
      clear = true,

      {
        'BufEnter', {
          callback = function()
            -- Note: Empty function required to suppress notification
            require('zk').index({}, function() end)
          end
        }
      }
    }
  )
end

return M
