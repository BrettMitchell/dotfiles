local table_util = require('user.util.table')
local co_util = require('user.util.coroutine')

return function(ms, fn)
  local timer = nil
  return co_util.async(function(...)
    local args = { ... }
    local cb = args[#args]
    table.remove(args, #args)

    if type(timer) ~= 'nil' then
      timer:stop()
      timer:close()
    end

    timer = vim.loop.new_timer()
    timer:start(ms, 0, function ()
      timer:stop()
      timer:close()
      timer = nil
      cb(fn(table_util.unpack(args)))
    end)
  end)
end

