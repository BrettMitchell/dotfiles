local ok, _ = pcall(require, 'lspconfig')
if not ok then
  return
end

-- local mapLspBuffLocalKeys = require('user.keymap.lspBufferLocal').map

local M = {}

local function on_attach(client, bufnr)
  -- require('lsp-format').on_attach(...)
  -- mapLspBuffLocalKeys()
  if client.name == 'tsserver' then
    client.server_capabilities.documentFormattingProvider = false
  end
end

function M.configureLsp()
  -- require('mason').setup {
  --   ui = {
  --     border = 'single'
  --   }
  -- }

  -- require('mason-lspconfig').setup {
  --   automaticInstallation = false,
  --   ensure_installed = { 'sumneko_lua', 'gopls' },
  -- }

  local cmp = require('cmp')

  cmp.setup {
    snippet = {
      expand = function(args)
        require('luasnip').lsp_expand(args.body)
      end
    },

    window = {
      completion = cmp.config.window.bordered(),
      documentation = cmp.config.window.bordered(),
    },

    completion = {
      autocomplete = false,
      -- Always preselect first value
      completeopt = 'menu,menuone,noinsert'
    },

    mapping = cmp.mapping.preset.insert {
      ['<Tab>'] = function(fallback)
        if cmp.visible() then
          cmp.select_next_item {}
        else
          fallback()
        end
      end,

      ['<S-Tab>'] = function(fallback)
        if cmp.visible() then
          cmp.select_prev_item {}
        else
          fallback()
        end
      end,

      ['<A-j>'] = function(fallback)
        if cmp.visible() then
          cmp.select_next_item {}
        else
          fallback()
        end
      end,

      ['<A-k>'] = function(fallback)
        if cmp.visible() then
          cmp.select_prev_item {}
        else
          fallback()
        end
      end,

      ['<A-S-j>'] = function(fallback)
        if cmp.visible() then
          cmp.scroll_docs(5)
        else
          fallback()
        end
      end,

      ['<A-S-k>'] = function(fallback)
        if cmp.visible() then
          cmp.scroll_docs( -5)
        else
          fallback()
        end
      end,

      ['<CR>'] = function(fallback)
        if cmp.visible() then
          cmp.confirm { select = true }
        else
          fallback()
        end
      end,
    },

    sources = cmp.config.sources(
      { { name = 'luasnip' } },
      { { name = 'nvim_lsp' } },
      { { name = 'buffer' } },
      { { name = 'path' } }
    )
  }

  cmp.setup.cmdline('/', {
    completion = {
      -- autocomplete = true,
      -- Always preselect first value
      completeopt = 'menu,menuone,noinsert'
    },
    -- mapping = cmp.mapping.preset.cmdline(),
    sources = {
      { name = 'buffer' }
    }
  })

  cmp.setup.cmdline(':', {
    completion = {
      -- autocomplete = true,
      -- Always preselect first value
      completeopt = 'menu,menuone,noinsert'
    },
    -- mapping = cmp.mapping.preset.cmdline(),
    sources = cmp.config.sources(
      { { name = 'path' } },
      { { name = 'cmdline' } },
      { { name = 'path' } }
    )
  })

  local capabilities = require('cmp_nvim_lsp').default_capabilities()

  require('lsp-format').setup {}

  require("mason-lspconfig").setup_handlers {
    -- The first entry (without a key) will be the default handler
    -- and will be called for each installed server that doesn't have
    -- a dedicated handler.
    function(server_name) -- default handler (optional)
      require("lspconfig")[server_name].setup {
        capabilities = capabilities,
        on_attach = on_attach,
      }
    end,

    -- Next, you can provide a dedicated handler for specific servers.
    -- For example, a handler override for the `rust_analyzer`:
    -- ["rust_analyzer"] = function ()
    --     require("rust-tools").setup {}
    -- end
  }
end

return M
