local registerKeyMap = require('user.lib.map').registerKeyMap

local M = {}

function M.setup()
  -- Undotree
  registerKeyMap({ 'n' }, {
    -- Note: Sacrificing page up is not a big deal for me; S-j/k are 15 lines up/down,
    --       which suits my needs. IMO, this jump size is a good balance of readable and fast.
    ['<M-u>'] = { '<cmd>UndotreeToggle<enter>', 'Toggle undotree' }
  });
end

return M
