local M = {}

M.savedSearch = {
  frontmatter = function(existingFrontmatter, _, cb)
    -- Saved searches originate in the clipboard
    -- If you accidentally create a saved search note for something that isn't a valid search
    -- just copy the search again and run the template a second time to overwrite
    local search = vim.fn.getreg('"')

    return cb(nil, {
      title = 'Saved search :: SEARCH_NAME',
      tags = { 'meta.type.saved-search' },
      bindings = {
        [';;s'] = {
          lua = "require('user.notes.telescope').multiSearch { search = " .. search .. ", mode = 'title' }"
        },

        [';;y'] = {
          lua = "require('user.notes.telescope.multiSearch').copySearchResultsAsList(" .. search .. ")"
        },
      }
    })
  end,
  build = function(existingBody, _, cb) return cb(nil, '') end
}

return M
