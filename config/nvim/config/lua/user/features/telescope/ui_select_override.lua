local feat_util = require('user.util.feature')
local plugin_util = require('user.util.plugins.lazy')
local telescope = require('user.features.telescope')

local M = {
  name = 'Telescope :: Override vim.ui.select',

  description = [[
  This feature uses telescope as the default handler for 'vim.ui.select'.
  ]],
}

function M.setup()
  feat_util.assert_deps(M.name, { telescope })
  plugin_util.add_plugins({
    {
      'https://github.com/nvim-telescope/telescope-ui-select.nvim',
      event = 'VeryLazy',
      dependencies = { 'https://github.com/nvim-telescope/telescope.nvim' },
      config = function()
        require("telescope").load_extension("ui-select")
        require("telescope/_extensions")._config["ui-select"] = {
          require("telescope.themes").get_dropdown()
        }
      end
    }
  })
end

return M
