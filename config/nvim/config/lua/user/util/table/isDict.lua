return function (tbl)
  if type(tbl) ~= 'table' then
    return false
  end

  -- If we find any hash keys in the table, we consider it a dictionary
  for k, _ in pairs(tbl) do
    if type(k) == 'string' then
      return true
    end
  end

  return false
end
