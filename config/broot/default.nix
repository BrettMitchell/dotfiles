{
  home.file.".config/broot/conf.hjson".source = ./conf.hjson;
  home.file.".config/broot/conf.keys.hjson".source = ./conf.keys.hjson;
  home.file.".config/broot/conf.keys-mux.hjson".source = ./conf.keys-mux.hjson;
  home.file.".config/broot/conf.lf.hjson".source = ./conf.lf.hjson;
  home.file.".config/broot/conf.shell.hjson".source = ./conf.shell.hjson;
  home.file.".config/broot/conf.mux.hjson".source = ./conf.mux.hjson;
  home.file.".config/broot/conf.template.hjson".source = ./conf.template.hjson;
}
