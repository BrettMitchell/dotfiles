local feat_util = require('user.util.feature')
local add_plugins = require('user.util.plugins.lazy').add_plugins
local keymap = require('user.util.keymap')

local M = {
  feature = {
    name = 'Context',

    description = [[
    Shows where you are in a file by making lines that define parent scopes
    stick at the top of the window.
    ]],

    enabled = true,
  },
}

function M.toggle()
  if M.feature.enabled then
    M.feature.enabled = false
    vim.cmd [[TSContextDisable]]
    vim.notify('Disabled context')
  else
    M.feature.enabled = true
    vim.cmd [[TSContextEnable]]
    vim.notify('Enabled context')
  end
end

M.feature.keymap = keymap {
  toggle = { { 'n' }, '<space>uc', M.toggle, { desc = 'Toggle context' } }
}

function M.setup(config)
  feat_util.apply_config(config or {}, M.feature)

  M.feature.keymap.apply()

  add_plugins({ {
    'https://github.com/nvim-treesitter/nvim-treesitter-context',
    config = function()
      require('treesitter-context').setup({
        enable = M.feature.enabled,
        max_lines = 0,
        min_window_height = 7,
      })
    end
  } })
end

return M
