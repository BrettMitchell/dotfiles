local feat_util = require('pax-vim.util.feature')
local add_plugins = require('pax-vim.util.plugins.lazy').add_plugins
local make_keymap = require('pax-vim.util.keymap')

local M = {
  feature = {
    name = 'Zen Mode',
    description = [[
    This feature adds a distraction-free writing mode.
    ]],

    keymaps = make_keymap {
      toggle_zen_mode = { 'n', '<space>z', '<cmd>ZenMode<cr>', { desc = 'Toggle zen mode' } }
    }
  }
}

function M.setup(config)
  M.feature = feat_util.apply_config(config, M.feature)

  add_plugins({
    {
      'https://github.com/folke/zen-mode.nvim',

      opts = {
        window = {
          backdrop = 0.85,
          -- height and width can be:
          -- * an absolute number of cells when > 1
          -- * a percentage of the width / height of the editor when <= 1
          -- * a function that returns the width or the height
          width = 80, -- width of the Zen window
          height = 1, -- height of the Zen window

          -- by default, no options are changed for the Zen window
          -- uncomment any of the options below, or add other vim.wo options you want to apply
          options = {
            signcolumn = "no",  -- disable signcolumn
            number = false,     -- disable number column
            relativenumber = false, -- disable relative numbers
            cursorline = false, -- disable cursorline
            foldcolumn = "0",   -- disable fold column
          },
        },

        plugins = {
          options = {
            enabled = true,
            showcmd = false, -- disables the command in the last line of the screen
          },

          twilight = { enabled = true },
          gitsigns = { enabled = false },
        },
      },

      config = function()
        M.feature.keymaps.apply()
      end
    }
  })
end

return M


--[[

  Eventually this will define a Zen-mode feature using Twighlight and True-zen

---------------------------------------------------------------------------------------------

  Plugin specs for package manager

{
  'https://github.com/Pocco81/true-zen.nvim',
  commit = '98740c76254c65576ec294551028b65081053588',
  config = require('user.plugin_config.true-zen').config,
},

{
  'https://github.com/folke/twilight.nvim',
  commit = '8bb7fa7b918baab1ca81b977102ddb54afa63512',
  config = require('user.plugin_config.twilight').config,
}

---------------------------------------------------------------------------------------------

  Previous plugin config for Twilight

return {
  config = function ()
    require('twilight').setup {
      dimming = {
        alpha = 0.25, -- amount of dimming
        -- we try to get the foreground from the highlight groups or fallback color
        color = { "Normal", "#ffffff" },
        inactive = false, -- when true, other windows will be fully dimmed (unless they contain the same buffer)
      },

      context = 10, -- amount of lines we will try to show around the current line
      treesitter = true, -- use treesitter when available for the filetype
      -- treesitter is used to automatically expand the visible text,

      -- but you can further control the types of nodes that should always be fully expanded
      expand = { -- for treesitter, we we always try to expand to the top-most ancestor with these types
        "function",
        "method",
        "table",
        "if_statement",
      },

      exclude = {}, -- exclude these filetypes
    }
  end
}

---------------------------------------------------------------------------------------------

  Previous definition of user.gitsigns

local closeWindow = require('user.utils').closeWindow

local M = {}

M.gutterEnabled = true

function M.toggleGutterSigns()
  vim.cmd('Gitsigns toggle_signs')
  vim.notify('Toggled git gutter signs')
end

function M.toggleCurrentLineBlame()
  vim.cmd('Gitsigns toggle_current_line_blame')
  vim.notify('Toggled git blame')
end

function M.toggle()
  M.gutterEnabled = not M.gutterEnabled
  if M.gutterEnabled then
    require('gitsigns').attach()
  else
    require('gitsigns').detach()
  end
  vim.notify('Toggled gitsigns plugin')
end

function M.enable()
  M.gutterEnabled = true
  require('gitsigns').attach()
  vim.notify('Enabled gitsigns plugin')
end

function M.disable()
  M.gutterEnabled = false
  require('gitsigns').detach()
  vim.notify('Disabled gitsigns plugin')
end

-- local function getVisibleBuffers()
--   local wins = vim.api.nvim_list_wins()
--   local bufs = {}
--
--   for _, win in ipairs(wins) do
--     table.insert(bufs, vim.api.nvim_win_get_buf(win))
--   end
--
--   return bufs
-- end

local function getWinForFilenamePattern(pattern)
  for _, win in ipairs(vim.api.nvim_list_wins()) do
    local buf = vim.api.nvim_win_get_buf(win)
    local filename = vim.api.nvim_buf_get_name(buf)
    if filename:find(pattern) then
      return win, buf, filename
    end
  end

  return nil, nil, nil
end

M.config = {
  on_attach = function(bufnr)
    local function map(mode, l, r, opts)
      opts = opts or {}
      opts.buffer = bufnr
      vim.keymap.set(mode, l, r, opts)
    end

    local n = { 'n' }
    local nv = { 'n', 'v' }
    local ox = { 'o', 'x' }

    map(ox, 'ih', ':<C-U>Gitsigns select_hunk<CR>')

    map(n, '<space>gm', '<cmd>Gvdiffsplit!<cr>') -- Open a 3-way merge editor
    map(n, '<space>ghp', '<cmd>Gitsigns preview_hunk<cr>') -- Preview previous version of hunk in a floating window

    map(nv, '<space>gs', '<cmd>Gitsigns stage_hunk<cr>') -- Stage hunk under cursor
    map(n, '<space>gS', '<cmd>Gitsigns stage_buffer<cr>') -- Stage current buffer
    map(nv, '<space>gr', '<cmd>Gitsigns reset_hunk<cr>') -- Reset hunk under cursor
    map(n, '<space>gR', '<cmd>Gitsigns reset_buffer<cr>') -- Reset current buffer

    map(n, '[h', '<cmd>Gitsigns next_hunk<cr>') -- Next hunk in the current file
    map(n, ']h', '<cmd>Gitsigns prev_hunk<cr>') -- Previous hunk in the current file

    -- Merge-specific bindings

    local filename = vim.fn.expand('%')
    local isFugitive = filename:match('^fugitive://')
    -- local isWorkingCopy = filename:match('^fugitive://.*//1')
    local isTarget = filename:match('^fugitive://.*//2')
    local isMerge = filename:match('^fugitive://.*//3')

    local function quitAll()
      local targetWin = getWinForFilenamePattern('^fugitive://.*//2')
      local mergeWin = getWinForFilenamePattern('^fugitive://.*//3')

      -- If in merge mode, we want to quit merge mode before closing the entire file
      if targetWin then vim.api.nvim_win_close(targetWin, false) end
      if mergeWin then vim.api.nvim_win_close(mergeWin, false) end

      -- If not in merge mode, we want to have normal close file behavior
      if not targetWin and not mergeWin then
        closeWindow(0)
      end
    end

    map(n, '<space>q', quitAll) -- Quit all members of the 3-way merge editor

    -- Saving inside a Fugitive merge window should update the actual file using the working copy
    -- if isFugitive then
    --   map(n, 'KK')
    -- end

    if isFugitive and (isTarget or isMerge) then
      map(n, '<space>ga', '<cmd>diffput<cr>') -- Put changes under cursor in working copy
    end

    map(n, '<space>gah', '<cmd>diffget //2<cr>') -- Accept //2 conflict contents
    map(n, '<space>gal', '<cmd>diffget //3<cr>') -- Accept //3 conflict contents

    -- Maybe incorporate stage hunk into process here, but maybe just use the existing binding

    return M.gutterEnabled
  end
}

return M

---------------------------------------------------------------------------------------------

  Previous plugin config for true-zen

return {
  config = function ()
      local gitsigns = require('user.gitsigns')
      local galaxyline = require('galaxyline')

      require('true-zen').setup {
        modes = {
          narrow = {
            callbacks = {
              open_pre = function()
                gitsigns.disable()
                galaxyline.disable_galaxyline()
              end,
              close_pos = function()
                gitsigns.enable()
                galaxyline.load_galaxyline()
              end,
            }
          },

          ataraxis = {
            callbacks = {
              open_pre = function()
                gitsigns.disable()
                galaxyline.disable_galaxyline()
              end,
              close_pos = function()
                gitsigns.enable()
                galaxyline.load_galaxyline()
              end,
            }
          }
        },

        integrations = {
          twilight = true,
        }
      }
  end
}

--]]
