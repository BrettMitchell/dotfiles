return function(tbl, fn)
  for i, v in ipairs(tbl) do
    if fn(v, i, tbl) then return v end
  end
  return nil
end
