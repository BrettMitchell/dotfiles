local keymap = require('user.util.keymap')
local tmux_util = require('user.util.tmux')
local feat_util = require('user.util.feature')

local M = {
  name = 'Use nvim as an interface to session local reference lists',

  description = [[
  This feature adds keybindings when nvim is run inside of tmux, which allow the
  user to open the currently focused file in another pane.

  This assists in situations where you've navigated to another file in nvim,
  and now you want to focus your file browser to that location.
  ]],

  target_pane = nil,

  REF_TYPE = {
    FILE = 'file',
    FILE_LINE = 'file-line',
    FILE_LINE_COLUMN = 'file-line-column',
  },
}

local function ensure_abs(filename)
  if filename:sub(1, 1) == '/' then
    return filename
  end

  local path = require('path')
  local session_path = tmux_util.get_tmux_value('session_path')
  local abs = path.join(session_path, filename)
  return abs
end

function M.format_target(ref)
  if type(ref) == 'string' then
    return ensure_abs(ref)
  end

  local abs = ensure_abs(ref.target.filename)
  if ref.type == M.REF_TYPE.FILE then
    return abs
  elseif ref.type == M.REF_TYPE.FILE_LINE then
    return abs .. ':' .. ref.target.line
  elseif ref.type == M.REF_TYPE.FILE_LINE_COLUMN then
    return abs .. ':' .. ref.target.line .. ':' .. ref.target.col
  end

  return nil
end

function M.open_external_in_pane(opts)
  opts = opts or {}

  local tmux_session_name = tmux_util.get_tmux_value('session_name')
  local target = M.format_target(opts.ref)
  if not target then return end

  vim.system({
    'tsess',
    'open-item',
    tmux_session_name,
    opts.pane,
    target,
    '--ref-type',
    opts.ref.type or M.REF_TYPE.FILE,
  }, {}, function()
    if opts.focus then
      vim.system({ 'tmux', 'select-pane', '-t', opts.pane })
    end
  end)
end

function M.open_ref_external(opts)
  local pane = opts.pane

  if pane == nil and M.target_pane ~= nil then
    pane = M.target_pane
  end

  if pane == nil and #tmux_util.list_panes() <= 2 then
    pane = '{last}'
  end

  if pane == nil then
    pane = tmux_util.pick_pane()
  end

  M.open_external_in_pane({
    ref = opts.ref,
    pane = pane,
    focus = true,
  })
end

function M.open_ref_external_pick(opts)
  local pane = tmux_util.pick_pane()
  M.open_external_in_pane({
    ref = opts.ref,
    pane = pane,
    focus = true,
  })
end

function M.open_current_file_external()
  local ref = vim.fn.expand('%:p')
  M.open_ref_external({ ref = ref })
end

function M.open_current_file_external_pick()
  local ref = vim.fn.expand('%:p')
  M.open_ref_external_pick({ ref = ref })
end

function M.set_target_pane()
  M.target_pane = tmux_util.pick_pane()
end

M.open_keymap = keymap {
  { 'n', '<space>o', M.open_current_file_external,      { desc = 'Open currently focused file in another tmux pane' } },
  { 'n', '<space>O', M.open_current_file_external_pick, { desc = 'Open currently focused file in another tmux pane, picking the pane even if a focus has been selected' } },
}

M.set_target_keymap = keymap {
  { 'n', 'To', M.set_target_pane, { desc = 'Set a particular pane as the target' } },
}

function M.setup(config)
  if not tmux_util.running_in_tmux() then return end
  feat_util.apply_config(config or {}, M.feature)
  if M.open_keymap then
    M.open_keymap.apply()
  end

  if M.set_target_keymap then
    M.set_target_keymap.apply()
  end
end

return M
