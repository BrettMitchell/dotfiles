-- Global so that this can be called as follows
--  :lua show({ a = 1, b = 2, c = 'something else' })
-- Technically this can also be done with:
--  :lua =<expr1>, <expr2>, ...
-- However, I like the formatting this produces better
-- and this is usable from other lua files.
function _G.show(...)
  local objects = {}
  for i = 1, select('#', ...) do
    local v = select(i, ...)
    table.insert(objects, vim.inspect(v))
  end

  vim.notify(table.concat(objects, '\n'))
  return ...
end

function _G.literalSearch(term)
  local escapedTerm = term:gsub('%\\', '\\\\'):gsub("'", "\\'"):gsub('"', '\\"')
  vim.cmd([[silent let @/='\V'."]] .. escapedTerm .. '"')
end

function _G.getVisualPositions()
  local startPos = { line = 0, column = 0 }
  local endPos = { line = 0, column = 0 }

  local startPosFromVim = vim.fn.getpos('v')
  local endPosFromVim = vim.fn.getpos('.')

  startPos.line = startPosFromVim[2]
  startPos.column = startPosFromVim[3]

  endPos.line = endPosFromVim[2]
  endPos.column = endPosFromVim[3]

  -- If the selection was made backwards, reverse the order of startPos and endPos
  local startPosSerial = (vim.fn.line2byte(startPos.line) + startPos.column)
  local endPosSerial = (vim.fn.line2byte(endPos.line) + endPos.column)
  if startPosSerial > endPosSerial then
    local tmp = endPos
    endPos = startPos
    startPos = tmp
  end

  -- If in visual block mode, swap columns to ensure positive progression from startPos to endPos
  if vim.fn.mode() == '\22' and startPos.column > endPos.column then
    local tmp = startPos.column
    startPos.column = endPos.column
    endPos.column = tmp
  end

  local lines = vim.fn.getline(startPos.line, endPos.line)

  -- If nothing is selected, return the current cursor position
  if startPos.line == endPos.line and startPos.column == endPos.column then
    return { {
      lineIndex = startPos.line,
      line = lines[1],
      selection = '',
      startCol = startPos.column,
      endCol = startPos.column - 1,
    } }
  end

  -- Remove the last character to make it match the visual selection
  if vim.opt.selection:get() == 'exclusive' then
    endPos.column = endPos.column - 1
  end

  local positions = {}
  local visualMode = vim.fn.mode()

  -- Block visual mode
  if visualMode == '\22' then
    for i, line in ipairs(lines) do
      local startCol = startPos.column
      if startCol > string.len(line) then
        startCol = string.len(line)
      end

      local endCol = endPos.column
      if endCol > string.len(line) then
        endCol = string.len(line)
      end

      positions[i] = {
        lineIndex = startPos.line + i - 1,
        line = line,
        startCol = startCol,
        endCol = endCol
      }
    end

    -- Regular visual mode
  elseif visualMode == 'v' then
    if startPos.line == endPos.line then
      positions[1] = {
        lineIndex = startPos.line,
        line = lines[1],
        startCol = startPos.column,
        endCol = endPos.column
      }
    else
      for i, line in ipairs(lines) do
        if i == 1 then
          positions[i] = {
            lineIndex = startPos.line + i - 1,
            line = line,
            startCol = startPos.column,
            endCol = #line
          }
        elseif i == #lines then
          positions[i] = {
            lineIndex = startPos.line + i - 1,
            line = line,
            startCol = 1,
            endCol = endPos.column
          }
        else
          positions[i] = {
            lineIndex = startPos.line + i - 1,
            line = line,
            startCol = 1,
            endCol = #line
          }
        end
      end
    end

    -- Line-wise visual mode
  elseif visualMode == 'V' then
    for i, line in ipairs(lines) do
      positions[i] = {
        lineIndex = startPos.line + i - 1,
        line = line,
        startCol = 1,
        endCol = #line
      }
    end
  end

  for _, position in ipairs(positions) do
    position.selection = string.sub(
      position.line,
      position.startCol,
      position.endCol
    )
  end

  return positions
end

-- Adapted from VimScript solution found here: https://stackoverflow.com/a/64639752
function _G.getVisualSelection(joinBlock)
  local startPos = { line = 0, column = 0 }
  local endPos = { line = 0, column = 0 }

  local startPosFromVim = vim.fn.getpos('v')
  local endPosFromVim = vim.fn.getpos('.')

  startPos.line = startPosFromVim[2]
  startPos.column = startPosFromVim[3]

  endPos.line = endPosFromVim[2]
  endPos.column = endPosFromVim[3]

  -- If the selection was made backwards, reverse the order of startPos and endPos
  local startPosSerial = (vim.fn.line2byte(startPos.line) + startPos.column)
  local endPosSerial = (vim.fn.line2byte(endPos.line) + endPos.column)
  if startPosSerial > endPosSerial then
    local tmp = endPos
    endPos = startPos
    startPos = tmp
  end

  -- If nothing is selected, return an empty result
  local lines = vim.fn.getline(startPos.line, endPos.line)
  if #lines == 0 then
    return { '' }
  end

  -- Remove the last character to make it match the visual selection
  if vim.opt.selection:get() == 'exclusive' then
    endPos.column = endPos.column - 1
  end

  -- Note: Original source material uses 'visualmode' here.
  --       This is incorrect, as 'visualmode' only returns the most recently used mode, not the current one.
  local visualMode = vim.fn.mode()
  -- Block visual mode
  if visualMode == '\22' then
    for i = 1, #lines do
      lines[i] = lines[i]:sub(startPos.column, endPos.column)
    end
    -- Regular visual mode
  elseif visualMode == 'v' then
    if startPos.line == endPos.line then
      lines[1] = lines[1]:sub(startPos.column, endPos.column)
    else
      lines[1] = lines[1]:sub(startPos.column)
      lines[#lines] = lines[#lines]:sub(1, endPos.column)
    end
    -- Line-wise visual mode
  elseif visualMode == 'V' then
    -- Do nothing. Line-wise selection doesn't need to be trimmed
    -- Empty case included for documentation purposes.
  end

  if joinBlock then
    return table.concat(lines, '\n')
  end

  return lines
end
