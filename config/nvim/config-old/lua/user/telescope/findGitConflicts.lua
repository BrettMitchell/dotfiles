local conf = require("telescope.config").values

local pickOption = require('user.telescope.pickOption').pickOption
local utils = require('user.utils');

local M = {}

function M.findGitConflicts()
  local res, ret, stderr = utils.get_os_command_output({ 'git', 'diff', '--name-only', '--diff-filter=U' })
  pickOption(res, {
    previewer = conf.file_previewer({}),
  })
end

return M
