local zkApi = require('zk.api')

local makeCallableObj = require('user.features.notes.telescope.util.callableObj').makeCallableObj
local noteUtil = require('user.features.notes.util')

local ZkFinder = makeCallableObj()

function ZkFinder:new(opts)
  opts = opts or {}

  local instance = setmetatable({
    entry_maker = opts.entry_maker,
    getQuery = opts.getQuery or function() return {} end,
  }, self)

  return instance
end

function ZkFinder:_find(prompt, process_result, process_complete)
  local query = self.getQuery(prompt)

  if not query.select then
    query.select = { 'absPath', 'title', 'tags' }
  end

  zkApi.list(noteUtil.getNotesDir(), query, function(err, notes)
    if (err) then
      vim.notify('Encountered an error while running zk: ' .. vim.inspect(err))
      return process_complete()
    end

    for _, note in ipairs(notes) do
      if process_result(self.entry_maker(note)) then
        return
      end
    end

    process_complete()
  end)
end

return {
  new_zk_finder = function(opts) return ZkFinder:new(opts) end,
}
