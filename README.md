
# Dotfiles

These dotfiles are intended to be widely applicable.

I prefer Nix, and manage my systems using Nix, but my application configurations should remain installable on a system without it.

For this reason, wherever possible, configuration is statically defined in plain configuration files, and the Nix flakes that compose the system refer to those files.

To install with Nix, use; `sudo nix-rebuild --flake gitlab:personal-system/dotfiles-v2/nix#t490`

To install without Nix, clone this repo and use the `install.sh` and `remove.sh` scripts to add and remove config modules.

