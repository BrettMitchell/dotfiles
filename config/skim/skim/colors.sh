
# 0 -> background
# 7 -> foreground
# other -> color#

fg='7'
bg='0'

export SK_COLORS="\
query:$fg,header:$fg,info:$fg,fg:$fg,\
matched_bg:$bg,query_bg:$bg,border:$bg,bg:$bg,\
matched:3,\
current:3,current_match:$bg,\
current_bg:$bg,current_match_bg:$fg,\
prompt:2,pointer:2,marker:2,spinner:2"

