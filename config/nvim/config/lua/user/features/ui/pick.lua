local add_plugins = require('user.util.plugins.lazy').add_plugins

local M = {
  name = 'Picker utility',

  description = [[
  A utility for picking things from a fuzzy finder.
  Built on top of NUI.
  Much simpler than Telescope.
  Not as powerful as Telescope.
  ]],
}

function M.setup()
  add_plugins({ {
    dir = '~/projects/software/nvim_plugins/pick'
  } })
end

return M
