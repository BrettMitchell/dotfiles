
local add_plugins = require('user.util.plugins.lazy').add_plugins

local M = {
  name = 'Automatic shift width',

  description = [[
  This module sets the shiftwidth based on the contents of the current file
  ]],
}

function M.setup()
  add_plugins({ {
    'https://github.com/tpope/vim-sleuth'
  } })
end

return M

