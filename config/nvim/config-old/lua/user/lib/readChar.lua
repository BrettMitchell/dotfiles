local function readChar()
  local ok, key = pcall(vim.fn.getchar)
  if not ok then -- Interrupted by <C-c>
    return nil
  end

  if type(key) == 'number' then
    return vim.fn.nr2char(key)
  elseif key:byte() == 128 then
    -- It's a special key in string
    return nil
  end

  if key == K_Esc then
    return nil
  end
end

return { readChar = readChar }
