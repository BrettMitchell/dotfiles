return function(opts)
  opts = opts or {}
  return vim.fn.getbufline('%', opts.bufnr or 0, '$')
end
