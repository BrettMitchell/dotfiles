-- Reference: https://github.com/nvim-telescope/telescope.nvim/blob/master/lua/telescope/pickers/layout_strategies.lua

local mergeLeft = require('user.lib.merge').mergeLeft

-- local borderCharsVertical = {
--   prompt = { "─", "│", "─", "│", "┌", "┐", "┘", "└" },
--   results = { "─", "│", "─", "│", "┌", "┐", "┤", "├" },
--   preview = { "─", "│", "─", "│", "┌", "┐", "┘", "└" },
-- }

local borderCharsHorizontal = {
  prompt = { "─", "│", "─", "│", "┌", "┐", "┘", "└" },
  results = { "─", "│", "─", "│", "┌", "┐", "┘", "└" },
  preview = { "─", "│", "─", "│", "┌", "┐", "┘", "└" },
}

local borderCharsCentered = {
  prompt = { "─", "│", " ", "│", "┌", "┐", "│", "│" },
  results = { "─", "│", "─", "│", "├", "┤", "┘", "└" },
  preview = { "─", "│", "─", "│", "┌", "┐", "┘", "└" },
}

local common = {
  -- winblend = 20
}

local centered = mergeLeft(
  common,
  {
    sorting_strategy = 'ascending',
    layout_strategy = 'center',
    results_title = false,
    preview_title = false,
    layout_config = {
      width = 0.9,
      height = 0.7,
    },
    show_line = false,
    borderchars = borderCharsCentered
  }
)

local horizontal = mergeLeft(
  common,
  {
    sorting_strategy = 'ascending',
    layout_strategy = 'horizontal',
    results_title = false,
    preview_title = false,
    layout_config = {
      width = 0.9,
      height = 0.9,
    },
    borderchars = borderCharsHorizontal,
  }
)

-- local vertical = {}

local cursor = mergeLeft(
  common,
  require('telescope.themes').get_cursor {
    borderchars = {
      prompt = { "─", "│", " ", "│", "┌", "┐", "│", "│" },
      results = { "─", "│", "─", "│", "├", "┤", "┘", "└" },
      preview = { "─", "│", "─", "│", "┌", "┐", "┘", "└" },
    }
  }
)

return { centered = centered, horizontal = horizontal, cursor = cursor }
