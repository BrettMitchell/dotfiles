return {
  config = function ()
      -- TODO: Do this in a VimResize (?) autocommand to keep harpoon in sync with window size
      require('harpoon').setup {
        menu = {
          width = math.min(120, vim.api.nvim_win_get_width(0) - 8),
          height = 10,
        }
      }
  end
}
