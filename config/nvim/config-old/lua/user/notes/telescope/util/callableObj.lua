return {
  makeCallableObj = function()
    local obj = {}

    obj.__index = obj
    obj.__call = function(t, ...)
      return t:_find(...)
    end

    obj.close = function() end

    return obj
  end
}
