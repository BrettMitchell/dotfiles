#!/usr/bin/env bash

ln_safe "$module/hypr/hyprland.conf" "$HOME/.config/hypr/hyprland.conf"
ln_safe "$module/hypr/keybindings.conf" "$HOME/.config/hypr/keybindings.conf"
ln_safe "$module/waybar" "$HOME/.config/waybar"
