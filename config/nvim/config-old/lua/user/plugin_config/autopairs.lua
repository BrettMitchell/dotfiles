return {
  config = function ()
      local npairs = require("nvim-autopairs")

      npairs.setup {
        enable_check_bracket_line = false
      }

      npairs.add_rules(require('nvim-autopairs.rules.endwise-elixir'))
      npairs.add_rules(require('nvim-autopairs.rules.endwise-lua'))
      npairs.add_rules(require('nvim-autopairs.rules.endwise-ruby'))
  end
}

-- local function x()
