#!/usr/bin/env bash

ln_safe "$module/tmux.conf" "$HOME/.config/tmux/tmux.conf"
ln_safe "$module/background.conf" "$HOME/.config/tmux/background.conf"

test ! -d $HOME/.tmux/plugins/tpm \
   && git clone https://github.com/tmux-plugins/tpm "$HOME/.tmux/plugins/tpm" && "$HOME/.tmux/plugins/tpm/bin/install_plugins"
