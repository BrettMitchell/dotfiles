{ nixpkgs, ... }:
{
  home.packages = with nixpkgs.stable; [
    docker
    docker-compose
  ];
}
