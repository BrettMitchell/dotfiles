--[[

TODO:
- Define hooks for setup process:
  - post_load (will be used for cmp most likely)
- Move mapLspBuffLocalKeys to a hook in a separate feature

--]]

local make_feature = require('user.util.feature').make_feature
local includes = require('user.util.table.includes')
local join = require('user.util.table.join')
local table_util = require('user.util.table')

local config = {
  plugin_config = {
    mason = {},
    lspconfig = {},
    mason_lspconfig = {},
  },

  capabilities = {
    all_servers = nil,
    servers = {},
  },

  hooks = {
    all_servers = {
      on_attach = {},
    },

    servers = {},

    -- TODO: Add support for an 'after' callback
    -- after = {},
  }
}

local function on_attach(server_name)
  return function(client, bufnr)
    local hooks = config.hooks or {}

    local all_servers_hooks = hooks.all_servers or {}
    local all_servers_on_attach = all_servers_hooks.on_attach or {}
    for _, fn in ipairs(all_servers_on_attach) do
      fn(client, bufnr)
    end

    local local_server_hooks = hooks[server_name] or {}
    local local_server_on_attach = local_server_hooks.on_attach or {}
    for _, fn in ipairs(local_server_on_attach) do
      fn(client, bufnr)
    end
  end
end

local M = make_feature({
  name = 'LSP',

  description = [[
  This feature configures basic editor features related
  to the language server protocol. It does not add
  specific capabilities, such as formatting or code actions,
  but it does add Mason as a package manager for LSP.
  ]],

  config = config,

  plugins = {
    {
      'https://github.com/neovim/nvim-lspconfig',
      -- TODO: Re-enable VeryLazy event, and figure out why it breaks the rename feature from lspsaga
      -- event = "VeryLazy",
      config = function()
        require('mason-lspconfig').setup(config.plugin_config.lspconfig)

        -- local hooks = config.hooks or {}
        local handlers = {}

        -- for server, hook in pairs(hooks.servers or {}) do
        --   handlers[server] = hook
        -- end

        handlers[1] = function(server_name) -- default handler (optional)
          local server_config = {
            on_attach = on_attach(server_name),
          }

          local extra_global_config = config.plugin_config.lspconfig.all_servers or {}
          server_config = table_util.merge.deepMergeLeft(server_config, extra_global_config)

          local extra_local_config = config.plugin_config.lspconfig[server_name] or {}
          server_config = table_util.merge.deepMergeLeft(server_config, extra_local_config)

          if not config.capabilities.all_servers and not config.capabilities.servers[server_name] then
            require("lspconfig")[server_name].setup(server_config)
          else
            local all_server_capabilities = config.capabilities.all_servers or {}
            if type(all_server_capabilities) == 'function' then
              all_server_capabilities = all_server_capabilities() or {}
            end

            local local_server_capabilities = config.capabilities[server_name] or {}
            if type(local_server_capabilities) == 'function' then
              local_server_capabilities = local_server_capabilities() or {}
            end

            local capabilities = vim.tbl_deep_extend(
              'force',
              all_server_capabilities,
              local_server_capabilities
            )

            server_config.capabilities = capabilities

            require("lspconfig")[server_name].setup(server_config)
          end
        end

        require('mason-lspconfig').setup_handlers(handlers)
      end
    },

    {
      'https://github.com/williamboman/mason.nvim',
      event = "VeryLazy",
      config = function()
        require('mason').setup(config.plugin_config.mason)
      end
    },

    {
      'https://github.com/williamboman/mason-lspconfig.nvim',
      event = "VeryLazy",
      config = function()
        require('mason-lspconfig').setup(config.plugin_config.mason_lspconfig)
      end
    },

    -- TODO: Move these to their own features

    {
      'https://github.com/lukas-reineke/lsp-format.nvim',
    },

    {
      'https://github.com/nvimtools/none-ls.nvim',
      event = "VeryLazy",
      dependencies = {
        'https://github.com/nvimtools/none-ls-extras.nvim'
      },
      config = function()
        local null_ls = require('null-ls')
        local formatting = null_ls.builtins.formatting

        null_ls.setup {
          debug = false,
          sources = {
            formatting.black,
          }
        }
      end
    },
  },

  keymaps = {
    { 'n', '<space>vli', '<cmd>LspInfo<enter>',    { desc = 'Show LSP info' } },
    { 'n', '<space>vlI', '<cmd>LspInstall<enter>', { desc = 'Install new LSP server' } },
    { 'n', '<space>vlL', '<cmd>LspLog<enter>',     { desc = 'Show LSP logs' } },
    { 'n', '<space>vlr', '<cmd>LspRestart<enter>', { desc = 'Restart current server' } },
    { 'n', '<space>vm',  '<cmd>Mason<enter>',      { desc = 'Show Mason dialog' } },
  },
})

M.methods = {
  buf_has_client = function(bufnr)
    bufnr = bufnr or vim.fn.bufnr('%')

    local attached_clients = vim.lsp.get_active_clients({ bufnr = bufnr })
    if #attached_clients > 0 then
      return true
    end

    return false
  end,

  add_hook = function(opts)
    opts = opts or {}

    if not opts.hook_name then
      vim.notify("lsp.methods.add_hook :: Option key 'hook_name' is required", 'error')
      return
    end

    local valid_hook_names = { 'on_attach' }

    if not includes(opts.hook_name, valid_hook_names) then
      local valid_hook_name_display = join(valid_hook_names, ', ')
      vim.notify("lsp.methods.add_hook :: Option key 'hook_name' must be one of " .. valid_hook_name_display, 'error')
      return
    end

    local hook_def = config.hooks.all_servers
    if opts.server then
      hook_def = config.hooks.servers[opts.server]
    end

    if type(hook_def) ~= 'table' then
      hook_def = {
        on_attach = {},
      }

      config.hooks.servers[opts.server] = hook_def
    end

    table.insert(hook_def[opts.hook_name], opts.callback)
  end
}

return M
