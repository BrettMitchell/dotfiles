-- local registerKeyMap = require('user.lib.map').registerKeyMap

local M = {}

-- function M.setup()
--   -- Trouble
--   registerKeyMap({ 'n' }, {
--     -- Read 'p' as 'problems'
--     ['<M-p>'] = { ':TroubleToggle document_diagnostics<enter>', 'Trouble (document)' },
--     ['<M-S-p>'] = { ':TroubleToggle workspace_diagnostics<enter>', 'Trouble (workspace)' },
--   })
-- end

return M
