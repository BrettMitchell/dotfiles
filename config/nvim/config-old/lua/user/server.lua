-- local paxShim = require('pax-vim.features.startup.server')
-- local server_name = paxShim.methods.get_server()

local server_name = vim.fn.serverstart()

local function run(command)
  local commandHandle = io.popen(command)
  if not commandHandle then
    error('Could not execute command: ' .. command)
  end
end

local function setServerVariables()
  run('u-set-session-variable NVIM_SESSION_SERVER_ID ' .. server_name)
  run('u-set-session-variable NVIM_SESSION_CWD ' .. vim.fn.getcwd())
end

setServerVariables()

return {
  server_name = server_name
}
