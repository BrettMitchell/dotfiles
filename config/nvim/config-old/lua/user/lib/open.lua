local M = {}

function M.open(item)
  local osSysname = vim.loop.os_uname().sysname

  if osSysname == "Darwin" then -- MacOS
    vim.fn.system({ "open", item })
  elseif osSysname == "Linux" or osSysname == "FreeBSD" then -- Linux and FreeBSD
    vim.fn.system({ "xdg-open", item })
  elseif vim.loop.os_uname().version:match("Windows") then -- Windows
    vim.fn.system({ "xdg-open", item })
  else
    vim.notify("Unsupported operating system for 'open'")
  end
end

return M
