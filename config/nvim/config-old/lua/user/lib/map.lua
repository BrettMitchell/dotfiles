local tableUtil = require('user.lib.table')

-- Add a key map, with noremap by default
-- Explicitly pass noremap = false to use recursion
local function _map(setKeyMap)
  return function(modes, lhs, rhs, opts)
    -- local options = { noremap = true, silent = false }
    local options = { noremap = true, silent = true }
    if opts then
      options = vim.tbl_extend("force", options, opts)
    end

    -- Allow all modes with empty string
    if #modes == 0 or type(modes) == 'table' then
      setKeyMap(modes, lhs, rhs, options)
      return
    end

    local modeTable = {}
    -- Allow multiple modes at once
    for i = 1, #modes do
      local mode = modes:sub(i, i)
      modeTable[i] = mode
    end

    setKeyMap(modeTable, lhs, rhs, options)
  end
end

local function _unmap(unsetKeyMap)
  return function(modes, lhs, opts)
    if #modes == 0 or type(modes) == 'table' then
      unsetKeyMap(modes, lhs, opts)
      return
    end

    local modeTable = {}
    -- Allow multiple modes at once
    for i = 1, #modes do
      local mode = modes:sub(i, i)
      modeTable[i] = mode
    end

    unsetKeyMap(modeTable, lhs, opts)
  end
end

local map = _map(vim.keymap.set)
local unmap = _unmap(vim.keymap.del)

local mapBufferLocal = _map(
  function(mode, lhs, rhs, opts)
    opts = opts or {}
    opts = vim.tbl_extend('keep', opts, { buffer = 0 })
    vim.keymap.set(mode, lhs, rhs, opts)
  end
)

local function naiveIsTable(tbl)
  return type(tbl) == 'table' and tbl[1] ~= nil
end

local function _registerKeyMap(mapKey)
  return function(modes, keymap, opts, _prefix)
    local prefix = _prefix and _prefix or ''

    -- Map which-key compatible mappings without which-key because I got tired
    -- of the way which-key worked, but didn't want to switch all my map statements
    for key, mapValue in pairs(keymap) do
      if key ~= 'name' then
        if not naiveIsTable(mapValue) then
          _registerKeyMap(mapKey)(modes, mapValue, opts, prefix .. key)
        else
          if opts and mapValue[2] then
            opts.desc = mapValue[2]
          end
          mapKey(modes, prefix .. key, mapValue[1], opts)
        end
      end
    end
  end
end

local function _unregisterKeyMap(unmapKey)
  return function(modes, keymap, opts, _prefix)
    local prefix = _prefix and _prefix or ''

    -- Map which-key compatible mappings without which-key because I got tired
    -- of the way which-key worked, but didn't want to switch all my map statements
    for key, mapValue in pairs(keymap) do
      if key ~= 'name' then
        if not naiveIsTable(mapValue) then
          _unregisterKeyMap(unmapKey)(modes, mapValue, opts, prefix .. key)
        else
          unmapKey(modes, prefix .. key, opts)
        end
      end
    end
  end
end

local registerKeyMap = _registerKeyMap(map)
local unregisterKeyMap = _unregisterKeyMap(unmap)

local registerBufferKeyMap = _registerKeyMap(mapBufferLocal)

local bufferLocalReplacementMap = {}
local function defineLocalOverwriteMap(id, modes, keymap, opts)
  for _, v in pairs(keymap) do
    if tableUtil.isDict(v) then
      vim.notify('defineLocalOverwriteMap does not permit nested keymaps', 'error')
      return
    end
  end

  bufferLocalReplacementMap[id] = {
    modes = modes,
    keymap = keymap,
    opts = opts,
    backup = {},
    hasMappingFor = function(lhs)
      for k, _ in pairs(keymap) do
        if k == lhs then return true end
      end
      return false
    end
  }
end

local function localOverwrite(id)
  for _, mode in ipairs(bufferLocalReplacementMap[id].modes) do
    bufferLocalReplacementMap[id].backup[mode] = {}

    for _, buf in pairs(vim.api.nvim_list_bufs()) do
      local localKeymaps = vim.api.nvim_buf_get_keymap(buf, mode)

      for _, keymap in pairs(localKeymaps) do
        if bufferLocalReplacementMap[id].hasMappingFor(keymap.lhs) then
          -- Backup existing mapping
          table.insert(bufferLocalReplacementMap[id].backup[mode], keymap)

          -- Unmap temporarily
          vim.api.nvim_buf_del_keymap(buf, mode, keymap.lhs)
        end
      end
    end

    -- Apply mappings defined in overwrite dict
    registerBufferKeyMap(
      bufferLocalReplacementMap[id].modes,
      bufferLocalReplacementMap[id].keymap
    )
  end
end

local function localRestore(id)
  for _, mode in ipairs(bufferLocalReplacementMap[id].modes) do
    for _, keymap in pairs(bufferLocalReplacementMap[id].backup[mode]) do
      vim.api.nvim_buf_set_keymap(
        keymap.buffer,
        keymap.mode,
        keymap.lhs,
        keymap.rhs,
        { silent = keymap.silent == 1 }
      )
    end
  end

  bufferLocalReplacementMap[id].backup = {}
end

local globalReplacementMap = {}
local function defineGlobalOverwriteMap(id, modes, keymap, opts)
  for _, v in pairs(keymap) do
    if tableUtil.isDict(v) then
      vim.notify('defineLocalOverwriteMap does not permit nested keymaps', 'error')
      return
    end
  end

  globalReplacementMap[id] = {
    modes = modes,
    keymap = keymap,
    opts = opts,
    backup = {},
    hasMappingFor = function(lhs)
      for k, _ in pairs(keymap) do
        if k == lhs then return true end
      end
      return false
    end
  }
end

local function globalOverwrite(id)
  for _, mode in ipairs(globalReplacementMap[id].modes) do
    globalReplacementMap[id].backup[mode] = {}

    local globalKeymaps = vim.api.nvim_get_keymap(mode)

    for _, keymap in pairs(globalKeymaps) do
      if globalReplacementMap[id].hasMappingFor(keymap.lhs) then
        -- Backup existing mapping
        table.insert(globalReplacementMap[id].backup[mode], keymap)

        -- Unmap temporarily
        vim.api.nvim_del_keymap(mode, keymap.lhs)
      end
    end

    -- Apply mappings defined in overwrite dict
    registerKeyMap(
      globalReplacementMap[id].modes,
      globalReplacementMap[id].keymap
    )
  end
end

local function globalRestore(id)
  for _, mode in ipairs(globalReplacementMap[id].modes) do
    for _, keymap in pairs(globalReplacementMap[id].backup[mode]) do
      vim.api.nvim_set_keymap(
        keymap.mode,
        keymap.lhs,
        keymap.rhs,
        { silent = keymap.silent == 1 }
      )
    end
  end

  globalReplacementMap[id].backup = {}
end

return {
  _map = _map,
  map = map,
  unmap = unmap,
  registerKeyMap = registerKeyMap,
  unregisterKeyMap = unregisterKeyMap,
  registerBufferKeyMap = registerBufferKeyMap,
  -- Temporarily override buffer local keymaps
  defineLocalOverwriteMap = defineLocalOverwriteMap,
  localOverwrite = localOverwrite,
  localRestore = localRestore,
  -- Temporarily override global keymap
  defineGlobalOverwriteMap = defineGlobalOverwriteMap,
  globalOverwrite = globalOverwrite,
  globalRestore = globalRestore,
}
