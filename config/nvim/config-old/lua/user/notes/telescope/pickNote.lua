local zk                = require('zk.api')

local entry_display     = require('telescope.pickers.entry_display')
local pickOption        = require('user.telescope.pickOption').pickOption
local makeFilePreviewer = require('user.telescope.previewer').makeFilePreviewer
local deepMergeRight    = require('user.lib.merge').deepMergeRight
local selection         = require('user.notes.telescope.util.selection')
local noteUtil          = require('user.notes.util')

local M                 = {}

local function openNote(notes)
  local firstNote = notes[1]
  local path = firstNote.value.absPath
  vim.cmd('silent edit ' .. path)
end

-- Pick tags

function M.pickTags(tags, opts)
  opts = deepMergeRight({
    title = 'Tags',
    mappings = {
      -- No sensible default action for this picker, so show result
      select_default = show,
      i = {
        ['<C-y>'] = selection.yankAttrOfSelected('name'),
      }
    },
    entry_maker = function(tag)
      local displayer = entry_display.create({
        separator = " ",
        items = {
          { width = opts.note_count_width or 4 },
          { remaining = true },
        },
      })

      local make_display = function(e)
        return displayer({
          { e.value.note_count, "TelescopeResultsNumber" },
          e.value.name,
        })
      end

      return {
        value = tag,
        display = make_display,
        ordinal = tag.name,
      }
    end,
  }, opts or {})

  pickOption(tags, opts)
end

-- Pick note by title

function M.pickNote(notes, opts)
  opts = deepMergeRight({
    title = 'Notes',
    mappings = {
      select_default = openNote,
      i = {
        ['<M-y>'] = selection.yankAttrOfSelected('absPath'),
      }
    },
    previewer = makeFilePreviewer(
      function(entry) return entry.value.absPath end,
      function(entry) return entry.value.title or entry.value.path end
    ),
    entry_maker = function(note)
      local title = note.title or note.path
      return {
        value   = note,
        path    = note.absPath,
        display = title,
        ordinal = title,
      }
    end,
  }, opts or {})

  pickOption(notes, opts)
end

-- Pick orphaned notes

function M.pickTagOrphans(opts)
  zk.list(noteUtil.getNotesDir(opts), { tags = { 'NOT *' }, select = { 'title', 'absPath' } }, function(err, notes)
    if err then
      vim.notify('Error encountered while running zk: ' .. vim.inspect(err))
      return
    end

    M.pickNote(notes, opts)
  end)
end

function M.pickLinkOrphans(opts)
  zk.list(noteUtil.getNotesDir(opts), { orphan = true, select = { 'title', 'absPath' } }, function(err, notes)
    if err then
      vim.notify('Error encountered while running zk: ' .. vim.inspect(err))
      return
    end

    M.pickNote(notes, opts)
  end)
end

return M
