
local theme = require('user.telescope.theme')

local pickers = require('telescope.pickers')
local finders = require('telescope.finders')
local sorters = require('telescope.sorters')

local opts = {
  finder = finders.new_oneshot_job({ 'rg', '--line-number', '--column' }),
  sorter = sorters.get_generic_fuzzy_sorter(),
}

local picker = pickers.new(opts)

return {
  find = function ()
    picker:find()
  end
}

