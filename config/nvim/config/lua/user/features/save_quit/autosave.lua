local make_augroup = require('user.util.autocmd').make_augroup

local autosave = {
  name = 'Autosave',

  description = [[
  This feature adds an autocommand which automatically writes to
  disk any time a change occurs.

  This is useful for interstitial edit prompts, such as the
  harpoon edit dialog.
  ]],

  augroup = make_augroup {
    {
      name = 'Autosave',
      autocmds = { {
        events = { 'InsertLeave', 'TextChanged' },
        opts = {
          pattern = '*',
          callback = function()
            vim.cmd('silent w')
          end
        }
      } }
    }
  }
}

function autosave.setup()
  autosave.augroup:apply()
end

return autosave
