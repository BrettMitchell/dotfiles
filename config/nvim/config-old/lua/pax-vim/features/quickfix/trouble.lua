local make_feature = require('pax-vim.util.feature').make_feature

local config = {
  trouble_config = {}
}

local M = make_feature({
  name = 'Trouble',

  description = [[
  This feature adds a quickfix style tray containing LSP diagnostics
  ]],

  plugins = {
    {
      'https://github.com/folke/trouble.nvim',
      requires = 'kyazdani42/nvim-web-devicons',
      config = function()
        require('trouble').setup(config.trouble_config)
      end
    }
  },

  config = config,

  keymaps = {
    { 'n', '<C-p>',   ':TroubleToggle document_diagnostics<enter>',  { desc = 'Trouble (document)' }, },
    { 'n', '<C-S-p>', ':TroubleToggle workspace_diagnostics<enter>', { desc = 'Trouble (workspace)' }, }
  }
})

return M
