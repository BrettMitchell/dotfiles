return {
  append_lines = require('user.util.file.append_lines'),
  get_file_lines = require('user.util.file.get_file_lines'),
  get_file_str = require('user.util.file.get_file_str'),
  replace_text_range = require('user.util.file.replace_text_range'),
  escape_filepath = require('user.util.file.escape_filepath'),
  unescape_filepath = require('user.util.file.unescape_filepath'),
}
