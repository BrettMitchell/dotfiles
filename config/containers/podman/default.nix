{ nixpkgs, ... }:
{
  home.file.".config/containers/registries.conf".text = "
[registries.search]
registries = ['docker.io']
  ";

  # TODO: Eventually, when home-manager implements the feature, this
  # file should use 'virtualization.podman' here.

  home.packages = with nixpkgs.stable; [
    podman
    podman-compose
    podman-tui
  ];
}
