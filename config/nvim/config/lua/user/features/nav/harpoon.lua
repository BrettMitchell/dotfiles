local protect = require('user.util.function.protect').protect
local escape_filepath = require('user.util.file').escape_filepath
local keymap = require('user.util.keymap')
local make_augroup = require('user.util.autocmd').make_augroup
local lazy = require('user.util.plugins.lazy')
local shell = require('user.util.shell')
local string_util = require('user.util.string')
local fn_util = require('user.util.function')
local tmux_util = require('user.util.tmux')

local harpoon = {
  name = 'Harpoon',

  description = [[
  This feature sets up harpoon, a mechanism for marking files
  and navigating between them very quickly.

  When run outside of a tmux session, this feature uses the
  standard nvim plugin.

  When run inside a tmux session, this feature integrates with
  the session-level harpoon feature implemented there.
  ]],
}

function harpoon.add_file(file)
  local path = require('path')
  if not harpoon.mark_file then return end

  local dirname = path.dirname(harpoon.mark_file)
  if vim.fn.isdirectory(dirname) then
    local res = vim.system({ 'mkdir', '-p', dirname }):wait()
    if res.code ~= 0 then
      vim.notify('Could not create state directory to store harpoon marks', 'error')
      vim.notify('STDOUT:\n' .. res.stdout .. '\nSTDERR:\n' .. res.stderr, 'error')
    end
  end

  vim.fn.writefile(
    { file .. ':1:0' },
    harpoon.mark_file,
    'a'
  )
end

function harpoon.setup()
  local tmux = vim.fn.getenv('TMUX')

  if tmux ~= vim.NIL then
    coroutine.wrap(function ()
      local data_dir = tmux_util.get_tsessd_state_dir()
      if not data_dir then return end
      harpoon.mark_file = data_dir .. '/harpoon-marks'

      harpoon.update_cursor = fn_util.debounce(250, function()
        vim.schedule(function()
          if vim.fn.filereadable(harpoon.mark_file) == 0 then return end
          local cur_file = vim.fn.expand('%:p')
          if #cur_file == 0 then return end
          if vim.fn.isdirectory(cur_file) == 1 then return end

          local row, column = unpack(vim.api.nvim_win_get_cursor(0))

          local new_marks = {}
          for i, line in ipairs(vim.fn.readfile(harpoon.mark_file, 'r')) do
            if string_util.starts_with(line, cur_file) then
              line = cur_file .. ':' .. row .. ':' .. column
            end
            table.insert(new_marks, line)
          end

          vim.fn.writefile(new_marks, harpoon.mark_file)
        end)
      end)

      harpoon.augroup = make_augroup {
        {
          name = 'HarpoonCursorUpdate',
          autocmds = { {
            events = { 'CursorMoved' },
            opts = {
              pattern = '*',
              callback = function ()
                coroutine.wrap(harpoon.update_cursor)()
              end,
            }
          } }
        }
      }

      harpoon.keymaps = keymap {
        { { 'n', 'i', 'v' }, '<M-m>', function () harpoon.add_file(vim.fn.expand('%:p')) end, { desc = 'Mark file (harpoon)' } },
      }

      harpoon.augroup:apply()
      harpoon.keymaps.apply()
    end)()
  else
    lazy.add_plugin({
      'https://github.com/ThePrimeagen/harpoon',
      config = function ()
        -- TODO: Do this in a VimResize (?) autocommand to keep harpoon in sync with window size
        require('harpoon').setup({
          menu = {
            width = math.min(120, vim.api.nvim_win_get_width(0) - 8),
            height = 10,
          }
        })

        harpoon.keymaps = keymap {
          { { 'n', 'i', 'v' }, '<M-S-m>',  protect(function() require('harpoon.ui').toggle_quick_menu() end), { desc = 'Open harpoon quick menu' } },
          { { 'n', 'i', 'v' }, '<M-m>',    protect(function() require('harpoon.mark').add_file() end),        { desc = 'Mark file (harpoon)' } },
          { { 'n', 'i', 'v' }, '<M-1>',    protect(function() require('harpoon.ui').nav_file(1) end),         { desc = 'Go to file 1 (harpoon)' } },
          { { 'n', 'i', 'v' }, '<M-2>',    protect(function() require('harpoon.ui').nav_file(2) end),         { desc = 'Go to file 2 (harpoon)' } },
          { { 'n', 'i', 'v' }, '<M-3>',    protect(function() require('harpoon.ui').nav_file(3) end),         { desc = 'Go to file 3 (harpoon)' } },
          { { 'n', 'i', 'v' }, '<M-4>',    protect(function() require('harpoon.ui').nav_file(4) end),         { desc = 'Go to file 4 (harpoon)' } },
          { { 'n', 'i', 'v' }, '<M-5>',    protect(function() require('harpoon.ui').nav_file(5) end),         { desc = 'Go to file 5 (harpoon)' } },
          { { 'n', 'i', 'v' }, '<M-6>',    protect(function() require('harpoon.ui').nav_file(6) end),         { desc = 'Go to file 6 (harpoon)' } },
          { { 'n', 'i', 'v' }, '<M-7>',    protect(function() require('harpoon.ui').nav_file(7) end),         { desc = 'Go to file 7 (harpoon)' } },
          { { 'n', 'i', 'v' }, '<M-8>',    protect(function() require('harpoon.ui').nav_file(8) end),         { desc = 'Go to file 8 (harpoon)' } },
          { { 'n', 'i', 'v' }, '<M-9>',    protect(function() require('harpoon.ui').nav_file(9) end),         { desc = 'Go to file 9 (harpoon)' } },
          { { 'n', 'i', 'v' }, '<M-0>',    protect(function() require('harpoon.ui').nav_file(10) end),        { desc = 'Go to file 10 (harpoon)' } },
          { { 'n', 'v' },      '<space>M', protect(function() require('harpoon.ui').toggle_quick_menu() end), { desc = 'Open harpoon quick menu' } },
        }

        harpoon.keymaps.apply()
      end
    })
  end
end

return harpoon
