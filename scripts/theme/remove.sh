#!/usr/bin/env bash

for script in "$module"/scripts/* ; do
  rm_safe "$HOME/.local/bin/"`basename "$script"`
done

