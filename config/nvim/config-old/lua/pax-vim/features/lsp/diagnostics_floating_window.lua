local make_feature = require('pax-vim.util.feature').make_feature_v2
local keymap = require('pax-vim.util.keymap')

local lsp = require('pax-vim.features.lsp')

local M = make_feature({
  name = 'Diagnostic Virtual Text',
  dependencies = { lsp },

  description = [[
  This feature adds bindings to open LSP diagnostics in a floating window
  ]],

  keymaps = keymap {
    { 'n', '<space>ld', '<cmd>Lspsaga show_line_diagnostics<cr>', { desc = 'Show line diagnostics' } }
  },

  apply = function (self)
    self.keymaps.apply()
  end
})

return M
