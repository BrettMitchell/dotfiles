local map = require('user.lib.map').map

local M = {}

function M.setup()
  map('ic', '<M-s>', function () require('cmp').complete() end) -- Open completions menu
end

return M
