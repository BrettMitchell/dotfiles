local TREESITTER_INSTALL_DIR = "~/.local/share/nvim/treesitter_packages";

vim.opt.runtimepath:append(TREESITTER_INSTALL_DIR)

return {
  config = function()
    require('nvim-treesitter.install').update { with_sync = true }
    require('nvim-treesitter.configs').setup {
      parser_install_dir = TREESITTER_INSTALL_DIR,
      highlight = {
        enable = true,
      },
      autotag = {
        enable = true
      }
    }
  end
}
