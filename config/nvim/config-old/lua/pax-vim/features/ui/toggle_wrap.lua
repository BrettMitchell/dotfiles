local make_feature = require('user.util.feature').make_feature_v2
local keymap = require('user.util.keymap')

local toggle_wrap = make_feature({
  name = 'UI Commands: Toggle Word Wrap',

  description = [[
  This feature adds a keybinding to toggle word wrap.

  Wrapping is configured to break on whitespace rather
  than in the middle of words.
  ]],

  keymap = keymap {
    { 'n', ';w', '<cmd>set wrap!<cr>', { desc = 'Toggle word wrap' } },
  },

  apply = function (self)
    self.keymap.apply()
    vim.opt.linebreak = true
    vim.opt.list = false
  end
})

return toggle_wrap
