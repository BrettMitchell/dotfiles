local map = require('user.lib.map').map

local hop = require('user.hop')

local n = { 'n' }
local nv = { 'n', 'v' }
local nxo = { 'n', 'x', 'o' }
local nvo = { 'n', 'v', 'o' }
local i = { 'i' }
local c = { 'c' }

local M = {}

function M.setup()
  -- TODO: Evaluate whether or not other movement modes are worth implementing
  --       More than 2 is probably not going to be worth it.
  local function registerMovementMode1()
    -- Home row navigation
    map(nv, 'H', '^') -- Start of line
    map(nv, 'L', '$') -- End of line
    map(nv, 'J', '15j') -- Quick down
    map(nv, 'K', '15k') -- Quick up

    map(nv, 's', function() require('leap').leap { target_windows = { vim.fn.win_getid() } } end) -- Go to digraph
    map(nv, 'S', hop.nChar) -- N-char hop search
    map(n, '<C-s>', '<cmd>HopChar1<enter>') -- Hop to single specific character

    map(nv, '<space>h', hop.wordBegin) -- Words to the left of the cursor
    map(nv, '<space>l', hop.wordEnd) -- Words to the right of the cursor
    -- Note: Linewise movement anchored to the beginning of the line
    -- is much easier to visually scan. Vertical jump scatters jump
    -- marks across the page, which is harder to process visually.
    map(nv, '<space>j', hop.lineBegin) -- Line down at current column
    map(nv, '<space>k', hop.lineVertical) -- Line up at current column

    map(nvo, 'f', '<plug>(clever-f-f)') -- Find onto character forward
    map(nvo, 'F', '<plug>(clever-f-F)') -- Find onto character backward
    map(nvo, 't', '<plug>(clever-f-t)') -- Find up to character forward
    map(nvo, 'T', '<plug>(clever-f-T)') -- Find up to character backward
  end

  registerMovementMode1()
  -- registerKeyMap({ 'n' }, {
  --   ['<space>;'] = { toggleMovementMode, 'Toggle movement mode' }
  -- })

  map(i, '<C-h>', '<left>') -- Left in insert mode
  map(i, '<C-j>', '<down>') -- Down in insert mode
  map(i, '<C-k>', '<up>') -- Up in insert mode
  map(i, '<C-l>', '<right>') -- Right in insert mode

  -- Basic text nav in command mode
  map(c, '<C-h>', '<left>', { silent = false }) -- Move left by one character
  map(c, '<C-l>', '<right>', { silent = false }) -- Move right by one character
  map(c, '<C-j>', '<down>', { silent = false }) -- Go back in command history by one entry
  map(c, '<C-k>', '<up>', { silent = false }) -- Go forward in command history by one entry
  map(c, '<C-e>', '<C-right>', { silent = false }) -- Move forward by one word
  map(c, '<C-b>', '<C-left>', { silent = false }) -- Move back by one word },
  map(c, '<C-S-h>', '<home>', { silent = false }) -- Go to the beginning of the line
  map(c, '<C-S-l>', '<end>', { silent = false }) -- Go to the end of the line

  -- vim-wordmotion
  map(nxo, 'b', '<Plug>WordMotion_b') -- Beginning of wordmotion word
  map(nxo, 'e', '<Plug>WordMotion_e') -- End of wordmotion word
end

return M
