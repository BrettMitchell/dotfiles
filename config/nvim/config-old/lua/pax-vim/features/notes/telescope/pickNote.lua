local zk                = require('zk.api')

local entry_display     = require('telescope.pickers.entry_display')
local pick_static       = require('pax-vim.features.telescope').methods.pick_static
local makeFilePreviewer = require('pax-vim.features.telescope.previewer').makeFilePreviewer
local deepMergeRight    = require('pax-vim.util.table.merge').deepMergeRight
local selection         = require('pax-vim.features.notes.telescope.util.selection')
local noteUtil          = require('pax-vim.features.notes.util')

local M                 = {}

local function openNote(notes)
  local firstNote = notes[1]
  local path = firstNote.value.absPath
  vim.cmd('silent edit ' .. path)
end

-- Pick tags

function M.pickTags(tags, opts)
  opts = deepMergeRight({
    prompt_title = 'Tags',
    mappings = {
      -- No sensible default action for this picker, so show result
      select_default = show,
      i = {
        ['<C-y>'] = selection.yankAttrOfSelected('name'),
      }
    },
    entry_maker = function(tag)
      local displayer = entry_display.create({
        separator = " ",
        items = {
          { width = opts.note_count_width or 4 },
          { remaining = true },
        },
      })

      local make_display = function(e)
        return displayer({
          { e.value.note_count, "TelescopeResultsNumber" },
          e.value.name,
        })
      end

      return {
        value = tag,
        display = make_display,
        ordinal = tag.name,
      }
    end,
  }, opts or {})

  pick_static(tags, opts)
end

-- Pick note by title

function M.pickNote(notes, opts)
  opts = deepMergeRight({
    prompt_title = 'Notes',
    mappings = {
      select_default = openNote,
      i = {
        ['<C-y>'] = selection.yankAttrOfSelected('absPath'),
      }
    },
    previewer = makeFilePreviewer(
      function(entry) return entry.value.absPath end,
      function(entry) return entry.value.title or entry.value.path end
    ),
    entry_maker = function(note)
      local title = note.title or note.path
      return {
        value   = note,
        path    = note.absPath,
        display = title,
        ordinal = title,
      }
    end,
  }, opts or {})

  pick_static(notes, opts)
end

-- Pick orphaned notes

function M.pickTagOrphans(opts)
  zk.list(noteUtil.getNotesDir(opts), { tags = { 'NOT *' }, select = { 'title', 'absPath' } }, function(err, notes)
    if err then
      vim.notify('Error encountered while running zk: ' .. vim.inspect(err))
      return
    end

    M.pickNote(notes, opts)
  end)
end

function M.pickLinkOrphans(opts)
  zk.list(noteUtil.getNotesDir(opts), { orphan = true, select = { 'title', 'absPath' } }, function(err, notes)
    if err then
      vim.notify('Error encountered while running zk: ' .. vim.inspect(err))
      return
    end

    M.pickNote(notes, opts)
  end)
end

return M
