
local json = require('user.lib.json')

local M = {}

function M.getTheme()
  local WAL_COLORS_JSON = vim.fn.getenv('HOME') .. '/.cache/wal/colors.json'
  local walColors = json.loadJson(WAL_COLORS_JSON)
  return walColors
end

return M

