local table_util = require('pax-vim.util.table')

local function pick_static(items, opts)
  local pickers = require('telescope.pickers')
  local sorters = require('telescope.sorters')
  local finders = require('telescope.finders')

  local theme = require('pax-vim.features.telescope.theme')
  local attachMappings = require('pax-vim.features.telescope.attachMappings')

  local default_theme = theme.horizontal
  if not opts.previewer then
    default_theme = theme.centered
  end

  opts = table_util.merge.deepMergeLeft(opts or {}, default_theme)

  local mappings = opts.mappings or {}

  local finderConfig = { results = items }
  if opts.entry_maker then
    finderConfig.entry_maker = opts.entry_maker
  end

  local picker = pickers.new(opts, {
    results_title = '',
    prompt_title = '',
    preview_title = '',
    finder = finders.new_table(finderConfig),
    sorter = sorters.get_fuzzy_file(),
    previewer = opts.previewer or nil,
    attach_mappings = attachMappings.make_attach_mappings(mappings),
  })

  picker:find()
end

return pick_static
