local make_feature = require('pax-vim.util.feature').make_feature
local includes = require('pax-vim.util.table.includes')
local telescope = require('pax-vim.features.telescope')

local KNOWN_DRIVERS = {
  'native',
  'telescope',
}

local config = {
  driver = 'native'
}

local pick_option = make_feature({
  name = 'Pick Option',

  description = [[
  This feature is used internally as a simple picker in other features.

  The driver may be set in the feature configuration.
  ]],

  config = config,

  set_config = function (user_config)
    user_config = user_config or {}

    if user_config.driver then
      if not includes(user_config.driver, KNOWN_DRIVERS) then
        vim.notify("Unknown driver: '" .. user_config.driver .. "'", 'error')
        return
      end

      config.driver = user_config.driver
    end
  end
})

local function pick_native(opts, cb)
  opts = opts or {}

  local prompt = opts.header or 'Pick one of the following options'

  vim.ui.select(opts.items or {}, { prompt = prompt }, function (picked, index)
    if not picked then return cb({}) end
    cb({ { item = picked, index = index } })
  end)
end

local function pick_telescope(opts, cb)
  if not telescope.status.setup_active then
    vim.notify("Telescope feature is not active. Call 'setup' on 'pax-vim.features.telescope' to use the telescope picker driver")
  end

  opts = opts or {}

  local telescope_opts = {
    mappings = {
      select_default = function(selections)
        local picked = {}
        for _, selection in ipairs(selections) do
          table.insert(picked, { item = selection[1], index = selection.index })
        end
        cb(picked)
      end
    },
    title = opts.header or  'Pick one of the following options'
  }

  telescope.methods.pick_static(opts.items or {}, telescope_opts)
end

local function get_picker(driver)
  if driver == 'native' then return pick_native end
  if driver == 'telescope' then return pick_telescope end
end

pick_option.methods = {
  pick_option = function(opts, cb)
    opts = opts or {}

    if opts.driver then
      if not includes(opts.driver, KNOWN_DRIVERS) then
        vim.notify("Unknown driver: '" .. opts.driver .. "'", 'error')
        return
      end
    end

    local picker = get_picker(opts.driver or config.driver)
    return picker({ items = opts.items, header = opts.header }, cb)
  end,

  pick_by_label = function(opts, cb)
    opts = opts or {}

    local labels = {}
    for index, item in ipairs(opts.items or {}) do
      table.insert(labels, item.label or ('NO-LABEL-' .. index))
    end

    local base_picker_options = vim.tbl_extend('force', opts, { items = labels })
    pick_option.methods.pick_option(base_picker_options, function(selected_labels)
      local selected_items = {}
      for _, selected_label in ipairs(selected_labels) do
        table.insert(selected_items, {
          label = selected_label.item,
          index = selected_label.index,
          item = opts.items[selected_label.index].value,
        })
      end
      cb(selected_items)
    end)
  end,

  pick_from_map = function(opts, cb)
    opts = opts or {}

    local items = {}
    for key, value in pairs(opts.items or {}) do
      table.insert(items, { label = key, value = value })
    end

    local base_picker_options = vim.tbl_extend('force', opts, { items = items })
    pick_option.methods.pick_by_label(base_picker_options, function(selected_items)
      local selected_map_members = {}
      for _, selected_item in ipairs(selected_items) do
        table.insert(selected_map_members, {
          key = selected_item.label,
          item = selected_item.item,
        })
      end
      cb(selected_map_members)
    end)
  end
}

return pick_option
