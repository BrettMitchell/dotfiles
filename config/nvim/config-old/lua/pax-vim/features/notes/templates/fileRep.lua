local TAGS = require('pax-vim.features.notes.special_tags')

local M = {}

M.fileRep = {
  frontmatter = function(existingFrontmatter, _, cb)
    return cb(nil, {
      title = 'File representative :: FILE',
      tags = { TAGS.FILE_RESOURCE },
      file = existingFrontmatter.file or 'FILE_GOES_HERE',
      bindings = {
        [';;o'] = {
          lua = "vim.fn.system(('u-open-item %s'):format(require('user.notes.frontmatter').parseFrontmatter().file))"
        },
      }
    })
  end,
  build = function(existingBody, _, cb) return cb(nil, existingBody) end
}

return M
