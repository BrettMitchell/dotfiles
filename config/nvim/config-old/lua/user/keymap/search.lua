local t = require('user.lib.termcodes').t
local registerKeyMap = require('user.lib.map').registerKeyMap

local M = {}

function M.setup()
  -- Search
  registerKeyMap({ 'n' }, {
    ['<esc>'] = {
      function()
        vim.cmd [[nohl]]
        require('notify').dismiss()
        -- vim.cmd [[NotifierClear]]
        vim.cmd [[echo '']]
        require('flash.util').exit()
      end,
      'Clear search highlighting',
    },
  })

  local function literalVisualSearch()
    -- Get visual selection joined by newlines into a single block of text
    local visualSelection = getVisualSelection(true)
    literalSearch(visualSelection)

    -- Return to normal mode
    vim.fn.feedkeys(t '<esc>')
  end

  registerKeyMap({ 'v' }, {
    ['<M-s>'] = { literalVisualSearch, 'Literal search on current selection' },
  }, { silent = true })

  registerKeyMap({ 'n' }, {
    ['<space>s'] = { ':%s//gI<left><left><left>', 'Global find and replace' },
    ['/'] = { '/\\c<left><left>', 'Case insensitive search' },
    ['?'] = { '/\\C<left><left>', 'Case sensitive search' },
  }, { silent = false })

  registerKeyMap({ 'n', 'o', 'v' }, {
    ['<space>n'] = { '*',
      'Next occurrence of word (hint: use n/N after <space>n, as the word will be set as the current search term)' },
  })
end

return M
