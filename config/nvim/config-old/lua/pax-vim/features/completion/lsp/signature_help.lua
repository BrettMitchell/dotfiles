local make_feature = require('pax-vim.util.feature').make_feature_v2

local cmp_buffer_text = make_feature({
  name = 'Completion :: LSP Signature',

  description = [[
  This feature provides LSP signature completions and help through cmp
  ]],

  source_definition = { name = 'nvim_lsp_signature_help' },

  plugins = { { 'https://github.com/hrsh7th/cmp-nvim-lsp-signature-help' } },
})

return cmp_buffer_text
