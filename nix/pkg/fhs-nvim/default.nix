{ pkgs, nixpkgs, ... }:
let
  base = pkgs.appimageTools.defaultFhsEnvArgs;
in
  pkgs.buildFHSUserEnv (base // {
    name = "fhs-nvim";
    targetPkgs = pkgs: (
      # pkgs.buildFHSUserEnv provides only a minimal FHS environment,
      # lacking many basic packages needed by most software.
      # Therefore, we need to add them manually.
      #
      # pkgs.appimageTools provides basic packages required by most software.
      (base.targetPkgs nixpkgs.stable) ++ [
        nixpkgs.stable.dash
        nixpkgs.unstable.neovim
        nixpkgs.stable.git
        nixpkgs.stable.curl
      ]
    );
    profile = "export FHS=1";
    runScript = ./run.sh;
    extraOutputsToInstall = ["dev"];
  })
