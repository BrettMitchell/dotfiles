return function (factor, c1, c2)
  if type(factor) ~= 'number' or factor < 0 or factor > 1 then
    error('lerp requires a factor between 0 and 1')
  end

  if type(c1) ~= 'table' or type(c2) ~= 'table' then
    error('lerp requires two RGB color tables')
  end

  local out = {}
  for i = 1, 3 do
    out[i] = math.floor(c1[i] + factor * (c2[i] - c1[i]))
  end

  return out
end
