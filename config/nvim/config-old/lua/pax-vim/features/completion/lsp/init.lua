local make_feature = require('pax-vim.util.feature').make_feature_v2
local lsp = require('pax-vim.features.lsp')

local cmp_lsp = make_feature({
  name = 'Completion :: LSP',
  dependencies = { lsp },

  description = [[
  Provides LSP autocompletion
  ]],

  plugins = { { 'https://github.com/hrsh7th/cmp-nvim-lsp' } },

  source_definition = {
    name = 'nvim_lsp',
  },

  apply = function ()
    lsp.config.capabilities.all_servers = function()
      return require('cmp_nvim_lsp').default_capabilities()
    end
  end
})

return cmp_lsp
