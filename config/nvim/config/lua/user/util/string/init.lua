return {
  repeat_str = require('user.util.string.repeat_str'),
  split = require('user.util.string.split'),
  trim = require('user.util.string.trim'),
  trim_start = require('user.util.string.trim_start'),
  trim_end = require('user.util.string.trim_end'),
  starts_with = require('user.util.string.starts_with'),
  ends_with = require('user.util.string.ends_with'),
  replace_start = require('user.util.string.replace_start'),
  replace_end = require('user.util.string.replace_end'),
  pad_start = require('user.util.string.pad_start'),
  pad_end = require('user.util.string.pad_end'),
}
