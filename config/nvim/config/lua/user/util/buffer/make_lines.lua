local split = require('user.util.string.split')

local function make_lines(text)
  local lines = {}

  if (vim.tbl_islist(text)) then
    for _, elem in ipairs(text) do
      local subLines = make_lines(elem)
      for _, line in ipairs(subLines) do
        table.insert(lines, line)
      end
    end
  else
    if type(text) ~= 'string' then
      text = vim.inspect(text)
    end
    lines = split(text, '\n')
  end

  return lines
end

return make_lines
