local make_feature = require('pax-vim.util.feature').make_feature_v2
local keymap = require('pax-vim.util.keymap')

local word_case_toggle = make_feature({
  name = 'Edit Commands: Word Case',

  description = [[
  This feature adds keymaps for quickly changing the case of a word
  from normal mode.
  ]],

  keymap = keymap {
    -- TODO: Switch this to a single toggle keybinding
    { 'n', '<space>eU', 'mzviwU`z', { desc = 'Uppercase word under cursor' } },
    { 'n', '<space>eu', 'mzviwu`z', { desc = 'Lowercase word under cursor' } },
  },

  apply = function (self)
    self.keymap.apply()
  end
})

return word_case_toggle
