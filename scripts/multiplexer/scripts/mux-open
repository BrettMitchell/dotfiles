#!/usr/bin/env sh

errlog() {
  printf '%s\n' "$1" >&2
}
err() {
  errlog "$1"
  exit 1
}

if [[ -z "$TMUX" ]] ; then
  printf '%s\n' "mux-open must be run inside a tmux session" > /dev/stderr
  exit 1
fi

if [[ "$#" -lt 2 ]] ; then
  err 'mux-open: At least 2 arguments are required'
fi

pane="$1"
shift

if [ "$pane" == {pick} ] ; then
  if [ `tmux list-panes | wc -l` -le 2 ] ; then
    pane='{last}'
  else
    pane=`mux-pick-pane`
    if [ -z "$pane" ] ; then
      errlog 'No pane picked, aborting...'
      exit 0
    fi
  fi
fi

session_name=`tmux display-message -p '#{session_name}'`
tsess open-item "$session_name" "$pane" "$@" >/dev/null
printf '%s\n' "$pane"
