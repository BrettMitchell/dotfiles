{ config, pkgs, nixpkgs, externalPackages, lib, ... }:

{
  home.username = "brettm";
  home.homeDirectory = "/home/brettm";
  home.stateVersion = "23.11";

  imports = [
    # Systemd services
    ../../shared/services/tsessd.nix

    # Font
    ../../shared/fonts

    # System components
    ../../../../config/browser/advanced.nix
    ../../../../config/browser/tor-browser
    ../../../../config/media

    # Configuration
    ../../../../config/broot
    ../../../../config/containers
    ../../../../config/de-sway
    ../../../../config/git
    ../../../../config/kitty
    ../../../../config/lf
    ../../../../config/mise
    ../../../../config/nvim
    ../../../../config/shell
    ../../../../config/skim
    ../../../../config/tmux
    ../../../../config/vm
    ../../../../config/wezterm
    ../../../../config/zathura

    # Scripts
    ../../../../scripts/desktop
    ../../../../scripts/lf
    ../../../../scripts/multiplexer
    ../../../../scripts/TEMP
    ../../../../scripts/theme
    ../../../../scripts/util
    ../../../../scripts/lazygit
  ];

  programs.man = {
    enable = true;
    generateCaches = true;
  };

  home.packages = with nixpkgs.stable; [
    externalPackages.tsessd.bin
    # externalPackages.nvr.bin

    # Shims
    (import ../../../pkg/fhs { inherit pkgs nixpkgs; })
    (import ../../../pkg/fhs-nvim { inherit pkgs nixpkgs; })

    # Shells
    dash

    # Terminal emulator
    wezterm
    kitty

    # Messaging
    discord

    # Runtimes, languages, and dev tools
    python313
    nixpkgs.unstable.nodejs_22
    luajit
    luajitPackages.luarocks
    luajitPackages.lyaml
    rustup
    gdb
    lldb
    valgrind
    lazygit
    unixtools.xxd
    pkg-config
    conan
    meson
    cmake
    bear
    ninja

    # Documentation
    manix
    man    
    man-pages
    man-pages-posix

    # Archives
    zip
    unzip
    rar
    pigz

    # General shell utilities
    bc
    skim
    ripgrep
    fd
    fzf
    broot
    eza
    bat
    tmux
    lf
    jq
    yq
    nixpkgs.unstable.mise
    delta
    trash-cli
    lazygit
    tokei
    xsv
    restic
    glow
    moreutils
    lsof
    libcdio
    file
    xdg-utils
    shared-mime-info

    # Libraries
    tk # Required to build Python
    zlib

    # Notes
    zk
    pandoc_3_5
    texliveFull

    zathura
    libreoffice-qt
    hunspell
    exiftool

    # System monitoring
    powertop
    bottom

    # Games
    nixpkgs.unstable.retroarch-full
  ];
}
