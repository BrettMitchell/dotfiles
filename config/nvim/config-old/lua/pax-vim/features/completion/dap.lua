local make_feature = require('pax-vim.util.feature').make_feature_v2

local cmp_dap = make_feature({
  name = 'Completion :: DAP REPL',

  description = [[
  Completion source for DAP expressions in the REPL buffer
  ]],

  source_definition = { name = 'dap' },

  plugins = {
    { 'https://github.com/rcarriga/cmp-dap' }
  },

  apply = function ()
    require('cmp').setup.filetype({ 'dap-repl', 'dapui_watches', 'dapui_hover' }, {
      sources = {
        { name = 'dap' }
      }
    })
  end
})

return cmp_dap
