# { pkgs, ... }:
{
  # Enable custom font in TTY
  console = {
    earlySetup = true;
    # TODO: Set up kmscon and set Victor Mono as font
    # https://discourse.nixos.org/t/setting-tty-fonts/14799
    # font = "${pkgs.}"
  };
}
