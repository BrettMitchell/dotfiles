return function(opts)
  opts = opts or {}

  if type(opts.newText) == 'string' then opts.newText = { opts.newText } end
  if opts.startCol > 0 then opts.startCol = opts.startCol - 1 end

  vim.api.nvim_buf_set_text(
    opts.bufnr or 0,
    opts.line - 1,
    opts.startCol,
    opts.line - 1,
    opts.endCol,
    opts.newText
  )
end
