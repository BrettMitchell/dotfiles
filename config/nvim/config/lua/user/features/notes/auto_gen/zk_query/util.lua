local zk             = require('zk.api')
local util           = require('user.features.notes.util')
local coroutine_util = require('user.util.coroutine')


-- @return table Notes matching the given query
local async_zk = coroutine_util.async(function(opts, query, cb)
  zk.list(
    util.getNotesDir(opts),
    query,
    function(err, notes)
      if err then
        vim.notify('Error encountered while running zk: ' .. vim.inspect(err))
        return cb(err, nil)
      end

      return cb(notes)
    end
  )
end)

-- @return table Notes matching the given query
local async_zk_tags = coroutine_util.async(function(opts, query, cb)
  zk.tag.list(
    util.getNotesDir(opts),
    query,
    function(err, tags)
      if err then
        vim.notify('Error encountered while running zk: ' .. vim.inspect(err))
        return cb(err, nil)
      end

      return cb(tags)
    end
  )
end)

return {
  async_zk = async_zk,
  async_zk_tags = async_zk_tags,
}
