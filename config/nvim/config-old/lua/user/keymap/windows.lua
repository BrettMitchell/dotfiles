local registerKeyMap = require('user.lib.map').registerKeyMap

local M = {}

function M.setup()
  local function getTmuxDirection(dir)
    if dir == 'h' then return 'L' end
    if dir == 'j' then return 'D' end
    if dir == 'k' then return 'U' end
    if dir == 'l' then return 'R' end
  end

  -- This allows M-h/j/k/l to seamlessly move focus between
  -- both vim windows and tmux panes.
  local function navigateWindowOrTmux(dir)
    return function()
      local winnr = vim.fn.winnr()
      vim.cmd('wincmd ' .. dir)
      if winnr == vim.fn.winnr() then
        vim.fn.system('tmux select-pane -' .. getTmuxDirection(dir))
      end
    end
  end

  -- Note: This set of bindings is an exception to the general rule of: ctrl/shift for applications, meta/alt for surrounding infrastructure like OS, shell, and tmux

  -- Windows
  registerKeyMap({ 'n', 'v' }, {
    ['<A-h>'] = { navigateWindowOrTmux('h'), 'Navigate window left' },
    ['<A-j>'] = { navigateWindowOrTmux('j'), 'Navigate window down' },
    ['<A-k>'] = { navigateWindowOrTmux('k'), 'Navigate window up' },
    ['<A-l>'] = { navigateWindowOrTmux('l'), 'Navigate window right' },
    ['<A-\\>'] = { ':rightbelow new<enter>', 'Split down to empty' },
    ['<A-/>'] = { ':rightbelow vnew<enter>', 'Split right to empty' },
    ['<A-S-\\>'] = { ':rightbelow split<enter>', 'Split down' },
    ['<A-S-/>'] = { ':rightbelow vsplit<enter>', 'Split right' },
    ['<A-=>'] = { ':wincmd =<enter>', 'Equalize window sizes' },
    ['<A-S-j>'] = { ':resize -2<enter>', 'Decrease window height' },
    ['<A-S-k>'] = { ':resize +2<enter>', 'Increase window height' },
    ['<A-S-h>'] = { ':vertical resize -2<enter>', 'Decrease window width' },
    ['<A-S-l>'] = { ':vertical resize +2<enter>', 'Increase window width' },
  })

  registerKeyMap({ 'i' }, {
    ['<A-h>'] = { navigateWindowOrTmux('h'), 'Navigate window left' },
    ['<A-j>'] = { navigateWindowOrTmux('j'), 'Navigate window down' },
    ['<A-k>'] = { navigateWindowOrTmux('k'), 'Navigate window up' },
    ['<A-l>'] = { navigateWindowOrTmux('l'), 'Navigate window right' },
    ['<A-\\>'] = { '<C-o>:rightbelow new<enter>', 'Split down to empty' },
    ['<A-/>'] = { '<C-o>:rightbelow vnew<enter>', 'Split right to empty' },
    ['<A-S-\\>'] = { '<C-o>:rightbelow split<enter>', 'Split down' },
    ['<A-S-/>'] = { '<C-o>:rightbelow vsplit<enter>', 'Split right' },
    ['<A-=>'] = { '<C-o>:wincmd =<enter>', 'Equalize window sizes' },
    ['<A-S-j>'] = { '<C-o>:resize -2<enter>', 'Decrease window height' },
    ['<A-S-k>'] = { '<C-o>:resize +2<enter>', 'Increase window height' },
    ['<A-S-h>'] = { '<C-o>:vertical resize -2<enter>', 'Decrease window width' },
    ['<A-S-l>'] = { '<C-o>:vertical resize +2<enter>', 'Increase window width' },
  })
end

return M
