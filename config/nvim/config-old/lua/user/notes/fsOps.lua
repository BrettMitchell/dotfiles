local explore = require('user.notes.explore.plumbing')
local frontmatter = require('user.notes.frontmatter')
local util = require('user.notes.util')

local M = {}

-- Internal
local function makeNote(filename, skipFrontmatterGeneration)
  M.open(filename)
  -- If this is a new file, make sure it exists before generating frontmatter
  if not skipFrontmatterGeneration then
    frontmatter.generateFrontmatter()
    vim.cmd('write')
  end
end

local function uuid()
  local template = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'
  return string.gsub(template, '[xy]', function(c)
    local v = (c == 'x') and math.random(0, 0xf) or math.random(8, 0xb)
    return string.format('%x', v)
  end)
end

-- Open a note
function M.open(filename)
  local notesDir = util.getNotesDir()
  if not notesDir then return end

  local fullPath = notesDir .. '/' .. filename

  -- vim.cmd('! mkdir -p ' .. directory)
  vim.cmd('silent edit ' .. fullPath)
end

-- Create a new blank note
function M.newNote(skipFrontmatterGeneration)
  local noteName = uuid()
  local filename = 'zettelkasten/' .. noteName .. '.qmd'
  makeNote(filename, skipFrontmatterGeneration)
  return { filename = filename }
end

-- Open a note, creating it if necessary
function M.createOrOpen(opts, cb)
  opts = opts or {}
  local title = opts.title
  local skipFrontmatterGeneration = opts.skipFrontmatterGeneration
  explore.getNotesByTitle({ title = title }, function(err, notes)
    if err then
      vim.notify('An error occurred: ' .. vim.inspect(err), 'error')
      return
    end

    if not notes or #notes == 0 then
      local res = M.newNote(skipFrontmatterGeneration)
      return cb({ created = true, filename = util.getNotesDir() .. res.filename })
    end

    if #notes > 1 then
      vim.notify('More than one file has the title: ' .. title .. '!', 'warn')
    end

    local note = notes[1]
    M.open(note.path)
    return cb({ created = false, filename = note.path })
  end)
end

return M
