local make_feature = require('user.util.feature').make_feature

return make_feature({
  name = 'Plugin Manager',
  on_activate = function()
    -- local lazy = require('user.util.plugins.lazy')
    -- lazy.bootstrap()
    -- lazy.setup()
  end,
  keymaps = {
    { 'n', '<space>vp', '<cmd>Lazy home<cr>' },
  }
})
