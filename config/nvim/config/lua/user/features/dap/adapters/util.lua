local string_util = require('user.util.string')
local table_util = require('user.util.table')
local tmux_util = require('user.util.tmux')
local pick_static = require('user.features.telescope.pick_static')

local M = {}

function M.get_project_root()
  if vim.fn.getenv('TMUX') ~= vim.NIL then
    return tmux_util.get_tmux_value('session_path')
  end
  return vim.fn.getcwd()
end

local function get_stdout_lines(stdout)
  local trimmed = string_util.trim(stdout)
  local split = string_util.split(trimmed, '\n')
  return split
end

function M.get_pids_pgrep(pgrep_pattern)
  local res = vim.system({ 'pgrep', pgrep_pattern }):wait()
  local pids = get_stdout_lines(res.stdout)
  return pids
end

function M.get_pids_two_phase(pgrep_pattern, ps_pattern)
  local shell = vim.fn.getenv('SHELL')
  if shell == vim.NIL then return {} end
  local script = ([[
    pgrep '%s' \
      | xargs ps -p \
      | grep '%s'
  ]]):format(pgrep_pattern, ps_pattern)

  local res = vim.system({ shell, '-c', script }):wait()

  local pids = get_stdout_lines(res.stdout)
  return pids
end

function M.get_ports_from_pids(pids)
  local pidsCsv = table_util.join(pids, ',')

  local shell = vim.fn.getenv('SHELL')
  if shell == vim.NIL then return {} end

  --[[

  lsof:
    -i -> Network related file descriptors
    -n -> Skip DNS resolution (means speed, with the side effect of 127.0.0.1 instead of localhost)
    -P -> Do not resolve ports to service names (127.0.0.1:2000 instead of 127.0.0.1:cisco-sccp)
    -a -> Logical AND
    -p -> Only show data for the given comma-seperated list of ports

  awk:
    /TCP/           -> On any line containing the text 'TCP'
    && /LISTEN/     -> AND containing the text '127.0.0.1'
    { printf(...) } -> Print a JSON object containing the address and pid extracted
                       from the 9th and 2nd fields respectively

  jq:
    -s -> Slurp input, i.e. all lines from stdin are nested in an array

  --]]

  -- TODO: Take this out once tsessd supports standalone managed processes
  local tmux_session_name = tmux_util.get_tmux_value('session_name')

  local wrapper = ''
  if vim.fn.executable('nix') == 1 then
    -- When on NixOS, nvim is placed in a simulated FHS environment with bubblewrap
    -- bubblewrap uses user namespaces, which revoke the permissions that lsof requires
    -- to inspect file descriptors of the processes we want to inspect in this call.
    -- To get around this, we use 'tsess' to make an IPC call that executes 'lsof'
    -- outside of the FHS context.
    wrapper = "tsess proc-run '" .. tmux_session_name .. "' -i -metadata '" .. '{"tags": ["remove-output"]}' .. "' -- "
  end

  local script = ([[
    %s lsof -i -n -P -a -p '%s' \
      | awk '
        /TCP/ && /LISTEN/ {
          printf("{\"addr\": \"%%s\", \"pid\": %%s}", $9, $2)
        }
      ' \
      | sort \
      | jq -s
  ]]):format(wrapper, pidsCsv)
  local res = vim.system({ shell, '-c', script }):wait()

  local port_records = vim.fn.json_decode(string_util.trim(res.stdout))
  for _, record in ipairs(port_records) do
    local split = string_util.split(record.addr, ':')
    local port = split[#split]
    record.port = port
  end

  return port_records
end

function M.list_pids()
  local shell = vim.fn.getenv('SHELL')
  if shell == vim.NIL then return {} end

  --[[

  ps:
    a       -> All processes 
    ww      -> Do not truncate output
    -o ...  -> Only show the following fields, and do not output a header row

  awk:
    cmd = $substr($0, index($0, $4)) -> Assign variable 'cmd' to rest of the line beyond the third field
    cmd = gsub("\\\\", "\\\\", cmd)  -> Replace a single backslash with two backslashes. `\\\\` matches
                                        a single backslash in the input, and `\\` inserts a single backslash
                                        in the output due to awks escaping and regex rules.
    printf(...)                      -> Print a JSON record with the fields inside it

  jq:
    -s -> Slurp input, i.e. all lines from stdin are nested in an array

  --]]

  local script = [[
    ps aww -o pid=,ppid=,tty=,command= \
      | awk '{
        printf("{\"pid\": %s, \"ppid\": %s, \"tty\": \"%s\"", $1, $2, $3);
        $1 = $2 = $3 = ""
        cmd = $0
        gsub("\\\\", "\\\\", cmd);
        printf(", \"cmd\": \"%s\"}\n", cmd);
      }' \
      | jq -s
  ]]
  local res = vim.system({ shell, '-c', script }):wait()

  local proc_records = vim.fn.json_decode(res.stdout)
  for _, proc_record in ipairs(proc_records) do
    proc_record.cmd = string_util.trim(proc_record.cmd)
  end
  return proc_records
end

function M.describe_pids(pids)
  local pidsCsv = table_util.join(pids, ',')

  local shell = vim.fn.getenv('SHELL')
  if shell == vim.NIL then return {} end

  --[[

  ps:
    ww      -> Do not truncate output
    -p '%s' -> Only return values for the given csv list of pids
    -o ...  -> Only show the following fields, and do not output a header row

  awk:
    cmd = $substr($0, index($0, $4)) -> Assign variable 'cmd' to rest of the line beyond the third field
    cmd = gsub("\\\\", "\\\\", cmd)  -> Replace a single backslash with two backslashes. `\\\\` matches
                                        a single backslash in the input, and `\\` inserts a single backslash
                                        in the output due to awks escaping and regex rules.
    printf(...)                      -> Print a JSON record with the fields inside it

  jq:
    -s -> Slurp input, i.e. all lines from stdin are nested in an array

  --]]

  local script = ([[
    ps ww -p '%s' -o pid=,ppid=,tty=,command= \
      | awk '{
          printf("{\"pid\": %%s, \"ppid\": %%s, \"tty\": \"%%s\"", $1, $2, $3);
          $1 = $2 = $3 = ""
          cmd = $0
          gsub("\\\\", "\\\\", cmd);
          printf(", \"cmd\": \"%%s\"}\n", cmd);
        }' \
      | jq -s
  ]]):format(pidsCsv)
  local res = vim.system({ shell, '-c', script }):wait()

  local proc_records = vim.fn.json_decode(res.stdout)
  for _, proc_record in ipairs(proc_records) do
    proc_record.cmd = string_util.trim(proc_record.cmd)
  end
  return proc_records
end

function M.get_listened_addrs()
  local shell = vim.fn.getenv('SHELL')
  if shell == vim.NIL then return {} end

  local script = [[
  lsof -i -n -P \
    | awk '
      /LISTEN/ {
        printf("{\"pid\": %s, \"addr\":\"%s\"}\n", $2, $9)
      }
    ' \
    | jq -s
  ]]
  local res = vim.system({ shell, '-c', script }):wait()
  if #res.stdout == 0 then return {} end

  local pid_addr_records = vim.fn.json_decode(res.stdout)
  for _, record in ipairs(pid_addr_records) do
    local split = string_util.split(record.addr, ':')
    local port = split[#split]
    record.port = port
  end

  return pid_addr_records
end

function M.get_proc_picker_source(pgrep_pattern)
  local addrs = M.get_listened_addrs()
  local proc_records = M.list_pids()


  local max_field_lengths = {
    addr = 0,
    pid = 0,
  }
  local picker_source = {}
  for _, record in ipairs(proc_records) do
    local addr_record = table_util.find(addrs, function(addr_record)
      return addr_record.pid == record.pid
    end)

    local picker_record = {
      addr = 'NO ADDR',
      port = 'NO PORT',
      pid = record.pid,
      cmd = record.cmd,
    }

    if addr_record then
      picker_record.addr = addr_record.addr
      picker_record.port = addr_record.port
    end

    max_field_lengths.addr = math.max(max_field_lengths.addr, #tostring(picker_record.addr))
    max_field_lengths.pid = math.max(max_field_lengths.pid, #tostring(picker_record.pid))
    table.insert(picker_source, picker_record)
  end

  for _, picker_record in ipairs(picker_source) do
    picker_record.display = (
      string_util.pad_end(tostring(picker_record.addr), max_field_lengths.addr + 1, ' ')
      .. string_util.pad_end(tostring(picker_record.pid), max_field_lengths.pid + 1, ' ')
      .. picker_record.cmd
    )
  end

  return picker_source
end

function M.pick_process(pgrep_pattern, cb)
  local picker_source = M.get_proc_picker_source(pgrep_pattern)

  if #picker_source == 0 then
    vim.notify('Nothing found', 'info')
    return
  end

  pick_static(picker_source, {
    layout_type = 'minimal_reversed',

    mappings = {
      select_default = function(entries)
        cb(entries[1].value)
      end,
    },

    entry_maker = function(entry)
      local display = entry.display
      entry.display = nil
      return {
        value = entry,
        display = display,
        ordinal = display,
      }
    end
  })
end

function M.get_port_picker_source(pgrep_pattern)
  local addrs = M.get_listened_addrs()
  local pids = table_util.map(addrs, function(addr) return addr.pid end)
  local proc_records = M.describe_pids(pids)

  local max_field_lengths = {
    addr = 0,
    pid = 0,
  }
  local picker_source = {}
  for _, record in ipairs(addrs) do
    local proc = table_util.find(proc_records, function(proc_record)
      return proc_record.pid == record.pid
    end)
    local cmd = 'UNKNOWN COMMAND'
    if proc then
      cmd = proc.cmd
    end

    local picker_record = {
      addr = record.addr,
      port = record.port,
      pid = record.pid,
      cmd = cmd,
    }

    max_field_lengths.addr = math.max(max_field_lengths.addr, #tostring(picker_record.addr))
    max_field_lengths.pid = math.max(max_field_lengths.pid, #tostring(picker_record.pid))
    table.insert(picker_source, picker_record)
  end

  for _, picker_record in ipairs(picker_source) do
    picker_record.display = (
      string_util.pad_end(tostring(picker_record.addr), max_field_lengths.addr + 1, ' ')
      .. string_util.pad_end(tostring(picker_record.pid), max_field_lengths.pid + 1, ' ')
      .. picker_record.cmd
    )
  end

  return picker_source
end


function M.pick_listening_process(pgrep_pattern, cb)
  local picker_source = M.get_port_picker_source(pgrep_pattern)

  if #picker_source == 0 then
    vim.notify('Nothing found', 'info')
    return
  end

  pick_static(picker_source, {
    layout_type = 'minimal_reversed',

    mappings = {
      select_default = function(entries)
        cb(entries[1].value)
      end,
    },

    entry_maker = function(entry)
      local display = entry.display
      entry.display = nil
      return {
        value = entry,
        display = display,
        ordinal = display,
      }
    end
  })
end

return M
