local lazy = require('user.util.plugins.lazy')

local surround = {
  name = 'Text Objects: Surrounding Character Pairs',

  description = [[
  This feature adds support for several more text objects which are defined
  by pairs of matching characters and the text they enclose.
  ]],
}

function surround.setup()
  lazy.add_plugin({
    'https://github.com/kylechui/nvim-surround',
    event = 'VeryLazy',
    version = '*',
    config = function ()
      require('nvim-surround').setup({})
    end
  })
end

return surround
