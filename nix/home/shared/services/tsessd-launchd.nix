{ lib, pkgs, externalPackages, ... }:
{
  launchd.user.agents.tsessd = {
    command = "${pkgs.zsh}/bin/zsh -l -c 'exec ${externalPackages.tsessd.bin}/bin/tsessd'";
    serviceConfig = {
      KeepAlive = true;
      RunAtLoad = true;
      StandardOutPath = "/tmp/tsessd.brettm.out.log";
      StandardErrorPath = "/tmp/tsessd.brettm.err.log";
      EnvironmentVariables = {
        PATH = "${lib.makeBinPath [
          pkgs.tmux
          pkgs.lf
          externalPackages.tsessd.bin
        ]}";
      };
    };
  };
}
