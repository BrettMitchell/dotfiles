return {
  config = function ()
      local easypick = require('easypick')
      local base_branch = 'master'

      easypick.setup {
        pickers = {
          { name = 'changed_files', command = "git diff --name-only $(git merge-base HEAD " .. base_branch .. " )" }
        }
      }
  end
}
