#!/usr/bin/env bash

ln_safe "$module/kitty.base.conf" "$HOME/.config/kitty/kitty.base.conf"
ln_safe "$module/kitty.conf" "$HOME/.config/kitty/kitty.conf"
