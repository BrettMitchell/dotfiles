#!/usr/bin/env bash

rm_safe "$HOME/.config/tmux/tmux.conf"
rm_safe "$HOME/.config/tmux/background.conf"

test -d $HOME/.tmux/plugins/tpm && \
   rm -rf "$HOME/.tmux/plugins/tpm"
