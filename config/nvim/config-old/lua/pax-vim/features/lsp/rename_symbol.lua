local make_feature = require('pax-vim.util.feature').make_feature
local keymap = require('pax-vim.util.keymap')
local lsp = require('pax-vim.features.lsp')
local lspsaga = require('pax-vim.features.lsp.lspsaga')

local methods = {
  rename_symbol = function ()
    require('lspsaga.rename'):lsp_rename()
  end
}

local rename_keymap = keymap {
  code_action = { 'n', '<space>r', methods.rename_symbol, { desc = 'Rename symbol' } }
}

local M = make_feature({
  name = 'LSP :: Rename symbol',

  dependencies = { lsp, lspsaga },

  keymaps = rename_keymap,
  setup_seq = { keymaps = false },

  description = [[
  This feature adds an LSP-powered rename action
  ]],

  on_activate = function()
    lsp.methods.add_hook({
      hook_name = 'on_attach',
      callback = function(_, bufnr)
        rename_keymap.apply(bufnr)
      end,
    })
  end
})

M.methods = methods

return M
