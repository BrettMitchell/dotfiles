local feat_util = require('user.util.feature')
local add_plugins = require('user.util.plugins.lazy').add_plugins

local M = {
  name = 'Quickfix',

  description = [[
  This feature adds to the quickfix list experience.
  https://github.com/ten3roberts/qf.nvim
  ]],

  qf_config = {},
}

function M.setup(config)
  feat_util.apply_config(config or {}, M)
  add_plugins({
    {
      'https://github.com/ten3roberts/qf.nvim',
      config = function ()
        require('qf').setup(M.qf_config)
      end
    }
  })
end

return M
