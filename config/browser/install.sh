#!/usr/bin/env bash

ln_safe "$module/firefox/tridactylrc" "$HOME/.config/tridactyl/tridactylrc"

firefoxDir=`ls $HOME/.mozilla/firefox | grep default-release`
userChromeDir="$HOME/.mozilla/firefox/$firefoxDir/chrome"
ln_safe "$module/firefox/userChrome.css" "$userChromeDir/userChrome.css"
