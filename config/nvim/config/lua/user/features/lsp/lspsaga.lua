local make_feature = require('user.util.feature').make_feature
local lsp = require('user.features.lsp')

local config = {}

local M = make_feature({
  name = 'LSP :: lspsaga',

  dependencies = { lsp },

  description = [[
  This feature adds and configures the 'lspsaga' plugin from glepnir.
  ]],

  config = config,

  plugins = {
    {
      'https://github.com/nvimdev/lspsaga.nvim',
      dependencies = { {'https://github.com/nvim-tree/nvim-web-devicons'} },
      event = "LspAttach",
      config = function ()
        require('lspsaga').setup(config)
      end
    },
  }
})

return M
