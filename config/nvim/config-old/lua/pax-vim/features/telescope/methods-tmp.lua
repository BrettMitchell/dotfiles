-- Note: This file contains finder definitions as they existed in the
-- previous iteration of my config. After all features are defined as
-- feature modules, come back here and improve this a bit.

local mergeLeft = require('pax-vim.util.table.merge').mergeLeft
local theme = require('pax-vim.features.telescope.theme')

local M = {}

-- M.notes = require('user.telescope.notes').searchNotes
-- M.findGitConflicts = require('user.telescope.findGitConflicts').findGitConflicts
-- M.git_bcommits_delta = require('user.telescope.gitCommitsDeltaPreview').git_bcommits_delta

-----------------------------------------------------------

function M.cwdFilesByName()
  require('telescope.builtin').find_files(
    mergeLeft(
      theme.horizontal,
      { hidden = true }
    )
  )
end

-----------------------------------------------------------

function M.currentFileContents()
  require("telescope.builtin").current_buffer_fuzzy_find(
    mergeLeft(
      theme.horizontal,
      { prompt_title = 'Current file contents' }
    )
  )
end

-----------------------------------------------------------

function M.currentFileLspSymbols()
  require("telescope.builtin").lsp_document_symbols(
    mergeLeft(
      theme.horizontal,
      { prompt_title = 'LSP Document Symbols' }
    )
  )
end

-----------------------------------------------------------

function M.cwdFilesByContent()
  require('telescope.builtin').live_grep(
    mergeLeft(
      theme.horizontal,
      {
        prompt_title = 'Project-wide file contents',
        additional_args = function()
          return { "--hidden" }
        end,
        glob_pattern = { "!.git" },
      }
    )
  )
end

-----------------------------------------------------------

-- TODO: Upgrade keybindings here to allow deleting buffers
function M.buffers()
  require('telescope.builtin').buffers(theme.horizontal)
end

-----------------------------------------------------------

function M.tokens()
  require('telescope.builtin').buffers(theme.horizontal)
end

-----------------------------------------------------------

function M.command_history()
  require('telescope.builtin').command_history({
    layout_strategy = 'minimal_reversed',
    sorting_strategy = 'ascending',
    results_title = '',
    layout_config = {
      width = '90%',
      height = '50%',
    },
  })
end

-----------------------------------------------------------

function M.help()
  require('telescope.builtin').help_tags(theme.horizontal)
end

-----------------------------------------------------------

function M.commands()
  require('telescope.builtin').commands(theme.horizontal)
end

-----------------------------------------------------------

function M.search_note_names()
  require('telescope.builtin').find_files(
    mergeLeft(
      theme.horizontal,
      { cwd = os.getenv('NOTES_DIR') }
    )
  )
end

-----------------------------------------------------------

function M.search_notes_text()
  require('telescope.builtin').live_grep(
    mergeLeft(
      theme.horizontal,
      { cwd = os.getenv('NOTES_DIR') }
    )
  )
end

-----------------------------------------------------------

function M.jump_list()
  require('telescope.builtin').jumplist()
end

-----------------------------------------------------------

function M.keymaps()
  require('telescope.builtin').keymaps({
    layout_strategy = 'minimal',
  })
end

-----------------------------------------------------------

-- local gitTheme = mergeLeft(
--   theme.horizontal,
--   { sorting_strategy = 'descending' }
-- )
--
-- function M.currentFileGitHistory()
--   require('user.telescope.gitCommitsDeltaPreview').git_bcommits_delta()
-- end

-----------------------------------------------------------

-- function M.gitHistory()
--   require('user.telescope.gitCommitsDeltaPreview').git_commits_delta()
-- end

-----------------------------------------------------------

-- function M.gitStatus()
--   require('telescope.builtin').git_status(theme.horizontal)
-- end

-----------------------------------------------------------

-- function M.gitBranches()
--   require('telescope.builtin').git_branches(theme.horizontal)
-- end

-----------------------------------------------------------

return M
