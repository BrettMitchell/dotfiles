return function(str, sep)
  if sep == nil or string.len(sep) == 0 then
    sep = "%s"
  end

  local parts = {}
  for match in string.gmatch(str .. sep, "([^" .. sep .. "]*)" .. sep) do
    table.insert(parts, match)
  end

  return parts
end
