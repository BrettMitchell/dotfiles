-- Source: https://github.com/parz3val/pywal-complete.nvim/blob/main/lua/pywal16/config.lua

local hl = require('user.util.highlight')
local color_util = require('user.util.color')

local function lerp_hex(factor, c1, c2)
  return color_util.to_hex(
    color_util.lerp(
      factor,
      color_util.to_rgb(c1),
      color_util.to_rgb(c2)
    )
  )
end

local function darken_hex(factor, color)
  return color_util.to_hex(
    color_util.darken(
      factor,
      color_util.to_rgb(color)
    )
  )
end

local function lighten_hex(factor, color)
  return color_util.to_hex(
    color_util.lighten(
      factor,
      color_util.to_rgb(color)
    )
  )
end

return function(theme)
  hl('GitSignsAddPreview', {
    bg = lerp_hex(0.085, theme.color.background1, theme.color.success),
  })
  hl('GitSignsAddInline', {
    fg = lighten_hex(0.7, theme.color.success),
    bg = lerp_hex(0.2, theme.color.background1, theme.color.success),
  })

  hl('GitSignsChangePreview', {
    bg = lerp_hex(0.085, theme.color.background1, theme.color.info),
  })
  hl('GitSignsChangeInline', {
    fg = lighten_hex(0.7, theme.color.info),
    bg = lerp_hex(0.2, theme.color.background1, theme.color.info),
  })

  hl('GitSignsDeletePreview', {
    bg = lerp_hex(0.085, theme.color.background1, theme.color.error),
  })
  hl('GitSignsDeleteInline', {
    fg = lighten_hex(0.7, theme.color.error),
    bg = lerp_hex(0.2, theme.color.background1, theme.color.error),
  })
end
