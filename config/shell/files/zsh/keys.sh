bindkey -v

copy=wl-copy
paste=wl-paste
if command -v pbcopy >/dev/null ; then
  copy=pbcopy
  paste=pbpaste
fi

# This is required to use space as a leader key
bindkey -rM vicmd ' '

function zle_clipboard {
  choose-clipboard-history < /dev/tty > /dev/null
}
zle -N zle_clipboard;

# Adapted from the skim repo: https://github.com/lotabout/skim/blob/master/shell/key-bindings.zsh
function zle_fzf_rev_history {
  local selected num
  setopt localoptions noglobsubst noposixbuiltins pipefail no_aliases 2> /dev/null

  selected=( $(fc -rl 1 | perl -ne 'print if !$seen{(/^\s*[0-9]+\**\s+(.*)/, $1)}++' |
    sk -n2..,.. --tiebreak=index --color="$SK_COLORS" --query=${BUFFER} --no-multi) )

  local ret=$?
  if [ -n "$selected" ]; then
    num=$selected[1]
    if [ -n "$num" ]; then
      zle vi-fetch-history -n $num
    fi
  fi

  zle reset-prompt
  return $ret
}
zle -N zle_fzf_rev_history

# TODO:
# - Add a preview (might require separate script)
# ? Add bookmarks of some kind
# - Add fuzzy finding over contents (new binding)
function zle_fzf_man_pages {
  (
    exec </dev/tty
    exec <&1
    local choice=`man -k . | sk --no-multi`
    if [[ -z "$choice" ]] ; then
      return
    fi

    local entry_name=`cut -d'(' -f1 <<<"$choice" | sed 's/[[:space:]]*$//'`
    local section=`cut -d'(' -f2 <<<"$choice" | cut -d')' -f1`

    man "$section" "$entry_name"
  )
}
zle -N zle_fzf_man_pages

function zle_fzf_man_pages_n {
  (
    exec </dev/tty
    exec <&1
    local choice=`apropos --long -s "$1" . | sk --no-multi`
    if [[ -z "$choice" ]] ; then
      return
    fi

    local entry_name=`sed -E 's/^(.*)[[:space:]]+\(.*$/\1/' <<<"$choice"`
    local section=`sed -E 's/^.*\((.*)\).*$/\1/' <<<"$choice"`

    man "$section" "$entry_name"
  )
}
function zle_fzf_man_pages_1 { zle_fzf_man_pages_n 1 }
function zle_fzf_man_pages_2 { zle_fzf_man_pages_n 2 }
function zle_fzf_man_pages_3 { zle_fzf_man_pages_n 3 }
function zle_fzf_man_pages_4 { zle_fzf_man_pages_n 4 }
function zle_fzf_man_pages_5 { zle_fzf_man_pages_n 5 }
function zle_fzf_man_pages_6 { zle_fzf_man_pages_n 6 }
function zle_fzf_man_pages_7 { zle_fzf_man_pages_n 7 }
function zle_fzf_man_pages_8 { zle_fzf_man_pages_n 8 }
function zle_fzf_man_pages_9 { zle_fzf_man_pages_n 9 }
zle -N zle_fzf_man_pages_1
zle -N zle_fzf_man_pages_2
zle -N zle_fzf_man_pages_3
zle -N zle_fzf_man_pages_4
zle -N zle_fzf_man_pages_5
zle -N zle_fzf_man_pages_6
zle -N zle_fzf_man_pages_7
zle -N zle_fzf_man_pages_8
zle -N zle_fzf_man_pages_9

function zle_find_session {
  (
    exec </dev/tty
    exec <&1
    __mux-bind-find-session > /dev/null
  )
}
zle -N zle_find_session

function zle_create_session {
  (
    exec </dev/tty
    exec <&1
    __mux-bind-create-session > /dev/null
  )
}
zle -N zle_create_session

function zle_nvim_notes {
  nvim -u "$HOME/.config/nvim/init-notes.lua" <"$TTY"
  zle reset-prompt
}
zle -N zle_nvim_notes

function zle_nvim {
  nvim <"$TTY"
  zle reset-prompt
}
zle -N zle_nvim

function zle_lf {
  lfcd </dev/tty 2>&1 1>/dev/null
  zle reset-prompt
}
zle -N zle_lf

function zle_br_root {
  if [[ -z "$TMUX" ]] ; then
    return
  fi

  local conf="$HOME/.config/broot/conf.shell.hjson"
  if [[ -n "$TMUX" ]] ; then
    conf="$HOME/.config/broot/conf.mux.hjson"
  fi

  local dir=`tmux display -p '#{session_path}'`
  local target=`broot --conf "$conf" "$dir" <"$TTY"`
  if [[ -d "$target" ]] ; then
    cd "$target"
  elif [[ -f "$target" ]] ; then
    cd `dirname "$target"`
  fi

  zle reset-prompt
}
zle -N zle_br_root

function zle_br {
  local conf="$HOME/.config/broot/conf.shell.hjson"
  if [[ -n "$TMUX" ]] ; then
    conf="$HOME/.config/broot/conf.mux.hjson"
  fi

  local target=`broot --conf "$conf" <"$TTY"`
  if [[ -d "$target" ]] ; then
    cd "$target"
  elif [[ -f "$target" ]] ; then
    cd `dirname "$target"`
  fi

  zle reset-prompt
}
zle -N zle_br

function zle_go_session_root {
  if [[ -z "$TMUX" ]] ; then
    return
  fi

  cd `tmux display-message -p '#{session_path}'`
  zle reset-prompt
}
zle -N zle_go_session_root

function zle_go_home {
  cd "$HOME"
  zle reset-prompt
}
zle -N zle_go_home

function zle_lazygit {
  (
    exec </dev/tty
    exec <&1
    lazygit > /dev/null
  )
}
zle -N zle_lazygit

function zle_prev_session {
  if [[ -z "$TMUX" ]] ; then
    (
      exec </dev/tty
      exec <&1
      tmux attach > /dev/null
    )
  fi
}
zle -N zle_prev_session

function zle_quit {
  exit
}
zle -N zle_quit

function zle_paste_clipboard {
  local text=`$paste`
  LBUFFER="$LBUFFER${RBUFFER:0:1}$text"
  RBUFFER="${RBUFFER:1}"
}
zle -N zle_paste_clipboard

function zle_paste_clipboard_minus_one {
  local text=`$paste`
  LBUFFER="$LBUFFER$text"
}
zle -N zle_paste_clipboard_minus_one

function zle_lazydocker_podman {
  DOCKER_HOST="unix:///run/user/$UID/podman/podman.sock" lazydocker <"$TTY"
}
zle -N zle_lazydocker_podman

function zle_lazydocker_docker {
  lazydocker
}
zle -N zle_lazydocker_docker

function zle_podman_tui {
  podman-tui <"$TTY"
}
zle -N zle_podman_tui

function zle_reflist_pick_named {
  local session_name=`tmux display-message -p '#{session_name}'`
  local selection=`
    tsess ref-list-show "$session_name" --format '{{.Id}}' \
      | rg -v '^[0-9]+$' \
      | sk --no-multi
  `

  if [[ -z "$selection" ]] ; then
    return
  fi

  nvim \
    -u "$HOME/.config/nvim/init-reflist.lua" \
    -c 'lua require("user.features.mux.reflist").current = "'"$selection"'"' \
    <"$TTY"

  zle reset-prompt
}
zle -N zle_reflist_pick_named

function zle_reflist {
  nvim -u "$HOME/.config/nvim/init-reflist.lua" <"$TTY"
  zle reset-prompt
}
zle -N zle_reflist

function zle_fzf_files {
  local file=`mux-pick-file <"$TTY"`
  if [[ -z "$file" ]] ; then
    return
  fi

  if [[ -z "$TMUX" ]] ; then
    "$copy" <<<"$file"
    return
  fi

  local session_name=`tmux display-message -p '#{session_name}'`
  ( tsess open-item "$session_name" :. "$file" --ref-type file >/dev/null & )
}
zle -N zle_fzf_files

function zle_fzf_directories {
  local dir=`mux-pick-dir <"$TTY"`
  if [[ -z "$dir" ]] ; then
    return
  fi

  if [[ -z "$TMUX" ]] ; then
    "$copy" <<<"$dir"
    return
  fi

  local session_name=`tmux display-message -p '#{session_name}'`
  ( tsess open-item "$session_name" :. "$dir" --ref-type file >/dev/null & )
}
zle -N zle_fzf_directories

function zle_fzf_file_contents {
  local file=`mux-search <"$TTY"`
  if [[ -z "$file" ]] ; then
    return
  fi

  if [[ -z "$TMUX" ]] ; then
    "$copy" <<<"$dir"
    return
  fi

  local session_name=`tmux display-message -p '#{session_name}'`
  ( tsess open-item "$session_name" :. "$file" --ref-type file-line-column >/dev/null & )
}
zle -N zle_fzf_file_contents

function zle_fzf_oci_images {
  local repo='{{.Repository}}'
  local tag='{{.Tag}}'
  local truncated_id='{{.ID}}'
  local human_size='{{.Size}}'
  local num_containers='{{.Containers}}'
  local created_at='{{.CreatedAt}}'
  local labels='{{range .Labels}}{{.}} {{end}}'
  local columns='RUNTIME,IMAGE,ID,SIZE,NUM-CONTAINERS,CREATED-AT,LABELS'

  local podman_images=''
  if command -v podman >/dev/null ; then
    podman_images=`podman images --format "podman\t$repo\t$tag\t$truncated_id\t$human_size\t$num_containers\t$created_at\t$labels" 2>/dev/null`
  fi

  local docker_images=''
  if command -v docker >/dev/null ; then
    docker_images=`docker images --format "docker\t$repo\t$tag\t$truncated_id\t$human_size\tN/A\t$created_at\tN/A" 2>/dev/null`
  fi

  local selection=`
    column -t -s $'\t' \
      --table-columns "$columns" \
      <<<"$podman_images"$'\n'"$docker_images" \
      | sk --no-multi --header-lines 1 --reverse
  `

  if [[ -z "$selection" ]] ; then
    return
  fi

  read -r runtime repo tag <<<`
    printf '%s' "$selection" \
      | tr -s ' ' \
      | cut -d' ' -f1-3
  `

  local value="$repo:$tag"

  if [[ "$BUFFER" =~ ^\s*dive ]] ; then
    value="$runtime://$value"
  fi

  "$copy" <<<"$value"
}
zle -N zle_fzf_oci_images

function zle_fzf_oci_containers {
  local state='{{.State}}'
  local name='{{.Names}}'
  local image='{{.Image}}'
  local ports='{{.Ports}}'
  local mounts='{{.Mounts}}'
  local networks='{{.Networks}}'
  local created_at='{{.CreatedAt}}'
  local id='{{.ID}}'
  local labels='{{range .Labels}}{{.}} {{end}}'

  local columns='RUNTIME,STATE,NAME,IMAGE,PORTS,MOUNTS,NETWORKS,CREATED-AT,ID,LABELS'

  local podman_containers=''
  if command -v podman >/dev/null ; then
    podman_containers=`podman ps -a --format "podman\t$state\t$name\t$image\t$ports\t$mounts\t$networks\t$created_at\t$id\t$labels" 2>/dev/null`
  fi

  local docker_containers=''
  if command -v docker >/dev/null ; then
    docker_containers=`docker ps -a --format "docker\t$state\t$name\t$image\t$ports\t$mounts\t$networks\t$created_at\t$id\tN/A" 2>/dev/null`
  fi

  local selection=`
    column -t -s $'\t' \
      --table-columns "$columns" \
      <<<"$podman_containers"$'\n'"$docker_containers" \
      | sk --no-multi --header-lines 1 --reverse
  `

  if [[ -z "$selection" ]] ; then
    return
  fi

  read -r container_name <<<`
    printf '%s' "$selection" \
      | tr -s ' ' \
      | cut -d' ' -f3
  `

  "$copy" <<<"$container_name"
}
zle -N zle_fzf_oci_containers

function zle_action_run {
  local pid=`__mux-bind-action-run --not-tag autostart`
  if [[ -z "$pid" ]] ; then ; return ; fi

  printf '\n'
  tsess proc-attach "$pid" \
    <"$TTY"
  zle reset-prompt
}
zle -N zle_action_run

function zle_service_run {
  local pid=`__mux-bind-action-run --tag autostart`
  if [[ -z "$pid" ]] ; then ; return ; fi

  printf '\n'
  tsess proc-attach "$pid" \
    <"$TTY"
  zle reset-prompt
}
zle -N zle_service_run

function zle_historical_output {
  local session_name=`tmux display-message -p '#{session_name}'`
  local state_dir=`tsess dump-state | jq -r ".sessions[] | select(.name == "'"'"$session_name"'"'") | .stateDir"`

  # State directory length
  local trim_len=$(( ${#state_dir} + 1 ))
  # /proc/ length
  trim_len=$(( $trim_len + 6 ))

  local selection=`
    fd --type f '(stdout|stderr)$' "$state_dir/proc" \
      | cut -c "$trim_len"- \
      | sort -r \
      | sk
  `

  if [[ -z "$selection" ]] ; then ; return ; fi
  ${PAGER:-less} +g "$state_dir/proc/$selection"
}
zle -N zle_historical_output

function zle_action_attach_output {
  local pid=`mux-pick-proc`
  if [[ -z "$pid" ]] ; then ; return ; fi
  printf '\n'
  tsess proc-attach "$pid" \
    <"$TTY"
  zle reset-prompt
}
zle -N zle_action_attach_output

function zle_action_kill {
  pid=`mux-pick-proc`
  if [[ -z "$pid" ]] ; then ; return ; fi
  kill "$pid"
}
zle -N zle_action_kill

function zle_action_clean {
  local session_name=`tmux display-message -p '#{session_name}'`
  local state_dir=`tsess dump-state | jq -r ".sessions[] | select(.name == "'"'"$session_name"'"'") | .stateDir"`
  rm -rf "$state_dir"/proc/*
}
zle -N zle_action_clean


bindkey -M vicmd ' fh' zle_fzf_rev_history
bindkey -M vicmd ' fy' zle_clipboard
bindkey -M vicmd ' ff' zle_fzf_files
bindkey -M vicmd ' fd' zle_fzf_directories
bindkey -M vicmd ' fs' zle_fzf_file_contents

bindkey -M vicmd ' sf' zle_find_session
bindkey -M vicmd ' sn' zle_create_session
bindkey -M vicmd ' l' zle_lf
bindkey -M vicmd ' e' zle_nvim
bindkey -M vicmd ' n' zle_nvim_notes
bindkey -M vicmd ' b' zle_br
bindkey -M vicmd ' B' zle_br_root
bindkey -M vicmd 'gr' zle_go_session_root
bindkey -M vicmd 'gh' zle_go_home
bindkey -M vicmd ' g' zle_lazygit
bindkey -M vicmd ' p' zle_paste_clipboard
bindkey -M vicmd ' P' zle_paste_clipboard_minus_one
bindkey -M vicmd ' q' zle_quit
bindkey -M vicmd ' k' zle_reflist
bindkey -M vicmd ' K' zle_reflist_pick_named

bindkey -M vicmd ' fm' zle_fzf_man_pages
bindkey -M vicmd ' f1' zle_fzf_man_pages_1
bindkey -M vicmd ' f2' zle_fzf_man_pages_2
bindkey -M vicmd ' f3' zle_fzf_man_pages_3
bindkey -M vicmd ' f4' zle_fzf_man_pages_4
bindkey -M vicmd ' f5' zle_fzf_man_pages_5
bindkey -M vicmd ' f6' zle_fzf_man_pages_6
bindkey -M vicmd ' f7' zle_fzf_man_pages_7
bindkey -M vicmd ' f8' zle_fzf_man_pages_8
bindkey -M vicmd ' f9' zle_fzf_man_pages_9

bindkey -M vicmd ' fci' zle_fzf_oci_images # Mixed podman and docker
bindkey -M vicmd ' fcc' zle_fzf_oci_containers # Mixed podman and docker

bindkey -M vicmd ' cp' zle_lazydocker_podman
bindkey -M vicmd ' cd' zle_lazydocker_docker
bindkey -M vicmd ' cP' zle_podman_tui

bindkey -M vicmd ' aa' zle_action_run
bindkey -M vicmd ' as' zle_service_run
bindkey -M vicmd ' ah' zle_historical_output
bindkey -M vicmd ' ao' zle_action_attach_output
bindkey -M vicmd ' ak' zle_action_kill
bindkey -M vicmd ' ac' zle_action_clean

function zle_yank {
  zle zvm_readkeys_handler vicmd "$KEYS"
  printf '%s' "$CUTBUFFER" | "$copy"
}
zle -N zle_yank

function zle_yank_to_end {
  zle vi-yank-eol
  printf '%s' "$CUTBUFFER" | "$copy"
}
zle -N zle_yank_to_end

bindkey -M vicmd 'y' zle_yank
# bindkey -M visual 'y' zle_yank
bindkey -M vicmd 'Y' zle_yank_to_end

