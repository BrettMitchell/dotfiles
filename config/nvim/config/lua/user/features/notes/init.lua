--[[
-- IMPORTANT:
--
-- This module is intended to turn neovim into a notes editor.
-- It should be applied from a neovim config which is dedicated to interacting
-- with a zk notebook, not from a top-level configuration used for general editing.
--]]


local make_feature = require('user.util.feature').make_feature_v2
local keymap = require('user.util.keymap')
local make_augroup = require('user.util.autocmd').make_augroup
local zk = require('user.features.notes.zk')
local telescope = require('user.features.telescope')
local frontmatter = require('user.features.notes.frontmatter.v2')

local function openNote(err, notes)
  if err then
    vim.notify('Could not open note: ' .. vim.inspect(err))
    return
  end

  local firstNote = notes[1]
  if not firstNote then
    return
  end

  local path = firstNote.value.absPath
  vim.cmd('silent edit ' .. path)
end

local notes = make_feature({
  name = 'Notes',

  deps = { zk, telescope },

  description = [[
  ZK powered note-taking utilities.
  ]],

  init = function(self)
    local templates = require('user.features.notes.templates')
    local tags = require('user.features.notes.tags')
    local projects = require('user.features.notes.projects')
    local fsOps = require('user.features.notes.fsOps')
    local notes_telescope = require('user.features.notes.telescope')
    local links = require('user.features.notes.links')
    local graph_traverse = require('user.features.notes.telescope.graph_traverse').graph_traverse
    local context = require('user.features.notes.context')

    local journal = function(date)
      fsOps.createOrOpen({ title = date }, function(res)
        if res.created then
          templates.applyAutoTemplate('journal', { date = date })
          vim.cmd('write')
        end
      end)
    end

    self.methods = {
      -- Proxied exports
      open = fsOps.open,
      newNote = fsOps.newNote,
      createOrOpen = fsOps.createOrOpen,
      addTags = tags.addTags,
      refactorTagUnderCursor = tags.refactorTagUnderCursor,
      pickTemplate = templates.pickTemplate,
      insertLinkFromClipboard = links.insertLinkFromClipboard,
      insertLink = links.insertLink,
      telescope = notes_telescope,
      projects = projects,

      open_taxonomy = tags.open_taxonomy,

      -- TODO: Load frontmatter and look at creation date.
      -- If the file was created in the last 5 minutes, just delete it right away
      -- If the file is more than 5 minutes old, prompt to confirm delete for safety
      -- At this point the keybinding can probably be changed to ';d' for easier access
      delete_file = function()
        local filename = vim.fn.expand('%:p')
        if not filename then return end
        vim.fn.delete(filename)
        vim.cmd 'bp! | bdelete! #'
      end,

      -- Create or open journal note for a particular date
      journal = journal,

      -- Create or open today's journal note
      todaysJournal = function()
        local today = os.date('%Y-%m-%d')
        journal(today)
      end,

      -- Create or open tomorrow's journal note
      -- Note: Add time to a date by representing the time in milliseconds and adding an integer.
      -- This doesn't handle irregular intervals of time like months and years, but for now, I
      -- don't need to work with those.
      tomorrowsJournal = function()
        local tomorrowDate = os.date('!*t')
        if type(tomorrowDate) == 'string' then return end

        tomorrowDate.day = tomorrowDate.day + 1
        local tomorrowEpoch = os.time(tomorrowDate)

        local tomorrow = os.date('%Y-%m-%d', tomorrowEpoch)
        journal(tomorrow)
      end,

      -- TODO
      -- planning = planning,
    }

    self.keymaps = keymap {
      graph_traverse = { 'n', ';;', graph_traverse, { desc = 'Note graph traversal' } },

      pick_note_by_title = {
        'n',
        '<C-space>',
        function()
          notes_telescope.pickNoteByTitle({
            search = {
              tags = { 'NOT taxon', 'NOT taxonomy', 'NOT journal' },
            },
          })
        end,
        { desc = 'Search for notes by title' },
      },
      pick_note_by_title_alt = { 'n', '<space>ff', notes_telescope.pickNoteByTitle,
        { desc = 'Search for notes by title' } },

      pick_journal = {
        'n',
        ';fj',
        function()
          notes_telescope.pickNoteByTitle({
            telescope = { prompt_title = 'Journal notes' },
            search = { tags = { 'journal' } }
          })
        end,
        { desc = 'Search for journal notes' },
      },

      pick_tag_orphans = { 'n', ';fot', notes_telescope.pickTagOrphans,
        { desc = 'Search notes with no tags by title' } },
      pick_link_orphans = { 'n', ';fol', notes_telescope.pickLinkOrphans,
        { desc = 'Search orphaned notes by title' } },
      focus_project_as_context = { 'n', ';p', projects.focus_project_as_context, { desc = 'Limit the current context to a single project' } },
      -- pick_project_member = { 'n', ';fp', projects.pickWithinProject, { desc = 'Pick note in current project' } },
      -- pick_project = { 'n', ';fP', projects.pickProject, { desc = 'Pick a project' } },
      open_taxonomy = { 'n', ';ft', self.methods.open_taxonomy, { desc = 'Open tag representative' } },

      -- ;fc -> pick a saved context
      open_context = { 'n', ';c', context.show_context_dialog, { desc = 'Edit the current context' } },
      pick_context = { 'n', ';C', context.pick_saved_context, { desc = 'Pick a saved context' } },

      open_file_in_frontmatter = { 'n', ';gf', function()
        local fm = frontmatter.get_buf_frontmatter({ bufnr = 0 })
        if not fm or not fm.file then return end
        vim.fn.jobstart(('u-open-item default "%s" &! disown'):format(fm.file))
      end, { desc = 'Open file linked in frontmatter' } },

      multi_search_tags_raw = { 'n', ';q',
        function() self.methods.telescope.multi_search({ mode = 'title' }, openNote) end,
        { desc = 'Query notes' } },

      todays_journal = { 'n', ';gj', self.methods.todaysJournal, { desc = 'Open todays daily journal note' } },
      tomorrows_journal = { 'n', ';gJ', self.methods.tomorrowsJournal, { desc = 'Open tomorrow daily journal note' } },
      -- goto_project_root = { 'n', ';gp', self.methods.projects.gotoProjectRoot, { desc = 'Go to root of current project' } },

      -- new_project_member = { 'n', ';p', self.methods.projects.newProjectNote, { desc = 'Create new in current project' } },
      new_note = { 'n', ';n', self.methods.newNote, { desc = 'Create a new blank note' } },

      delete_file = { 'n', ';D', self.methods.delete_file, { desc = 'Delete a file and remove its buffer' } },
      refactor_tag = { 'n', ';r', self.methods.refactorTagUnderCursor, { desc = 'Rename a tag throughout the zettel' } },

      -- TODO: Implement these bindings
      --       Additionally, remove ;j and ;J when daily methods are implemented
      --       When in a planning note, 'pick' alternatives should operate on the current time scope defined by the current note. Provide a telescope binding to expand out to all possible times
      --       When not in a planning note, 'pick' alternatives should operate over all possible times
      --       'current' alternatives should link directly to the note for the current time, creating the note if necessary
      --       All methods should maintain links between parent notes and their children (year -> month, month -> specific day, month -> week, week -> specific day)
      --
      -- planning_root = { 'n', ';gr', self.methods.planning.open_root, { desc = 'Open planning note: Root' } },
      -- planning_year = { 'n', ';gy', self.methods.planning.open_current_year, { desc = 'Open planning note: Current year' } },
      -- planning_year = { 'n', ';gY', self.methods.planning.pick_year, { desc = 'Open planning note: Pick year' } },
      -- planning_year = { 'n', ';gm', self.methods.planning.open_current_month, { desc = 'Open planning note: Current month' } },
      -- planning_year = { 'n', ';gM', self.methods.planning.pick_month, { desc = 'Open planning note: Pick month' } },
      -- planning_year = { 'n', ';gw', self.methods.planning.open_current_week, { desc = 'Open planning note: Current week' } },
      -- planning_year = { 'n', ';gW', self.methods.planning.pick_week, { desc = 'Open planning note: Pick week' } },
      -- planning_year = { 'n', ';gd', self.methods.planning.open_current_day, { desc = 'Open planning note: Current day' } },
      -- planning_year = { 'n', ';gD', self.methods.planning.pick_day, { desc = 'Open planning note: Pick day' } },
    }

    self.notes_keymaps_buffer_local = keymap {
      search_backlinks = { 'n', ';flb', '<cmd>ZkBacklinks<cr>', { desc = 'Search through backlinks in current note' } },
      search_links = { 'n', ';flo', '<cmd>ZkLinks<cr>', { desc = 'Search through links in current note' } },

      insert_link_from_clipboard_normal = { 'n', ';l', self.methods.insertLinkFromClipboard,
        { desc = 'Insert link under cursor using clipboard text. Supports multiple filenames if separated by newlines.' } },
      insert_link_from_clipboard_visual = { 'x', ';l', self.methods.insertLinkFromClipboard,
        {
          desc =
          'Wrap selected text in link using clipboard text. Supports multiple filenames if separated by newlines (will duplicate selected text for each link generated).'
        } },

      insert_empty_link_normal = { 'n', ';L', function() self.methods.insertLink() end,
        { desc = 'Insert empty link under cursor' } },
      insert_empty_link_visual = { 'x', ';L', function() self.methods.insertLink() end,
        { desc = 'Wrap selected text in empty link' } },

      add_tags = { 'n', ';t', self.methods.addTags, { desc = 'Add existing tags using a fuzzy finder' } },
      apply_template = { 'n', ';T', self.methods.pickTemplate,
        { desc = 'Pick a template from a fuzzy finder and apply it at the cursor' } },
    }

    local taxonomy = require('user.features.notes.tags.taxonomy')
    local zk_query = require('user.features.notes.auto_gen.zk_query')
    self.augroups = make_augroup {
      taxonomy.taxonomy_tree_augroup,
      taxonomy.taxonomy_member_augroup,
      zk_query.generate_body_augroup,

      {
        name = 'FrontmatterOnSave',
        autocmds = { {
          events = 'BufWritePre',
          opts = {
            pattern = { '*.md', '*.qmd' },
            callback = function()
              local existing_frontmatter = frontmatter.get_buf_frontmatter()
              local new_frontmatter = frontmatter.generate_frontmatter(existing_frontmatter)
              frontmatter.write_buf_frontmatter({ frontmatter = new_frontmatter })
            end
          }
        } }
      },

      {
        name = 'BindingsOnBufEnter',
        autocmds = { {
          events = 'BufEnter',
          opts = {
            pattern = { '*.md', '*.qmd' },
            callback = function()
              frontmatter.load_buf_keybindings()
            end
          }
        } }
      },

      {
        name = 'IndexZkNotes',
        autocmds = { {
          events = 'BufEnter',
          opts = {
            pattern = { '*.md', '*.qmd' },
            callback = function()
              -- Note: Empty function required to suppress notification
              require('zk').index({}, function() end)
            end
          }
        } }
      },

      {
        name = 'MarkdownKeybindings',
        autocmds = { {
          events = 'FileType',
          opts = {
            callback = function()
              local ft = vim.bo.filetype
              if ft ~= 'markdown' and ft ~= 'quarto' then
                return
              end

              local bufnr = vim.fn.bufnr('%')
              if not self.notes_keymaps_buffer_local.is_active(bufnr) then
                self.notes_keymaps_buffer_local.overlay(bufnr)
              end
            end
          }
        } }
      },
    }
  end,

  apply = function(self)
    math.randomseed(os.time())

    local notebook_dir = require('zk.util').notebook_root(vim.fn.getcwd())
    if notebook_dir == nil then
      notebook_dir = vim.fn.getenv('ZK_NOTEBOOK_DIR')
    end

    -- Early exit if no notebook dir available
    if require('zk.util').notebook_root(notebook_dir) == nil then
      return
    end

    vim.api.nvim_set_current_dir(notebook_dir)
    vim.opt.wrap = true

    require('user.features.notes.util').set_notes_dir(notebook_dir)

    -- TODO: Remove the init script-level dashboard note declaration and
    --       add an equivalent directory-level concept. Possibly a custom
    --       key in .zk/config.toml.
    if self.dashboard_note_name then
      self.methods.open_dashboard_note = function()
        require('user.features.notes.explore.plumbing').getNotesByTitle(
          { title = self.dashboard_note_name },
          function(err, notes)
            if err or #notes == 0 then
              vim.notify('Could not get dashboard note: ' .. vim.inspect(self.dashboard_note_name))
              return
            end
            self.methods.open(notes[1].absPath)
          end
        )
      end

      self.keymaps.spec.open_dashboard_note = { 'n', ';gd', self.methods.open_dashboard_note,
        { desc = 'Open dashboard note' } }

      self.methods.open_dashboard_note()
    end

    self.keymaps.apply()
    self.augroups:apply()
  end,
})

return notes
