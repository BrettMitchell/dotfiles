local make_feature = require('pax-vim.util.feature').make_feature_v2
local keymap = require('pax-vim.util.keymap')

local toggle_fold = make_feature({
  name = 'UI Commands: Toggle Fold',

  description = [[
  This feature adds keymaps to toggle folds.
  ]],

  keymap = keymap {
    { 'n', '<C-f>', 'za', { desc = 'Toggle fold under cursor' } },
  },

  apply = function (self)
    vim.o.foldlevel = 99
    vim.o.foldlevelstart = 99
    vim.o.foldenable = true

    self.keymap.apply()
  end
})

return toggle_fold
