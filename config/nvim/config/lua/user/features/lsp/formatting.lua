local make_feature = require('user.util.feature').make_feature
local keymap = require('user.util.keymap')
local lsp = require('user.features.lsp')

local methods = {
  format_buffer = vim.lsp.buf.format
}

local code_action_keymap = keymap {
  format_buffer = { { 'n' }, '<space>ef', methods.format_buffer, { desc = 'Format the current buffer' } },
  -- TODO: range formatting based on visual selection
}

local M = make_feature({
  name = 'LSP :: Formatting',

  dependencies = { lsp },

  keymaps = code_action_keymap,
  setup_seq = { keymaps = false },

  description = [[
  This feature adds a keybinding for LSP-driven formatting
  ]],

  on_activate = function()
    lsp.methods.add_hook({
      hook_name = 'on_attach',
      callback = function(_, bufnr)
        code_action_keymap.apply(bufnr)
      end,
    })
  end
})

M.methods = methods

return M
