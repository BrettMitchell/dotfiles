local make_feature = require('user.util.feature').make_feature_v2
local keymap = require('user.util.keymap')

local join_lines = make_feature({
  name = 'Edit Commands: Join lines',

  description = [[
  This feature adds a keybinding for joining lines.
  If you don't rebind 'J', this feature may not be useful.
  ]],

  keymap = keymap {
    { 'n', '<space>ej', 'J', { desc = 'Join lines' } },
  },

  apply = function (self)
    self.keymap.apply()
  end
})

return join_lines
