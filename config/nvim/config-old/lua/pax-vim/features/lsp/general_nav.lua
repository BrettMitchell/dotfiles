local make_feature = require('pax-vim.util.feature').make_feature
local keymap = require('pax-vim.util.keymap')
local split_and_do = require('pax-vim.util.keymap.split_and_do')

local lsp = require('pax-vim.features.lsp')

local methods = {
  goto_definition = vim.lsp.buf.definition,
  split_to_definition = {
    left = function() split_and_do('left', vim.lsp.buf.definition) end,
    right = function() split_and_do('right', vim.lsp.buf.definition) end,
    down = function() split_and_do('down', vim.lsp.buf.definition) end,
    up = function() split_and_do('up', vim.lsp.buf.definition) end,
  },

  goto_declaration = vim.lsp.buf.declaration,
  split_to_declaration = {
    left = function() split_and_do('left', vim.lsp.buf.declaration) end,
    right = function() split_and_do('right', vim.lsp.buf.declaration) end,
    down = function() split_and_do('down', vim.lsp.buf.declaration) end,
    up = function() split_and_do('up', vim.lsp.buf.declaration) end,
  },

  goto_references = vim.lsp.buf.references,
}

local navKeymap = keymap {
  goto_definition = { 'n', 'gdd', methods.goto_definition,           { desc = 'Go to definition of LSP item' } },

  goto_definition_left = { 'n', 'gdh', methods.split_to_definition.left,  { desc = 'Go to definition of LSP item in new split to the left' } },
  goto_definition_down = { 'n', 'gdj', methods.split_to_definition.down,  { desc = 'Go to definition of LSP item in new split down' } },
  goto_definition_up   = { 'n', 'gdk', methods.split_to_definition.up,    { desc = 'Go to definition of LSP item in new split up' } },
  goto_definition_right = { 'n', 'gdl', methods.split_to_definition.right, { desc = 'Go to definition of LSP item in new split to the right' } },

  goto_declaration = { 'n', 'gDD', methods.goto_declaration,          { desc = 'Go to definition of LSP item' } },

  goto_declaration_left  = { 'n', 'gDh', methods.split_to_definition.left,  { desc = 'Go to declaration of LSP item in new split to the left' } },
  goto_declaration_down  = { 'n', 'gDj', methods.split_to_definition.down,  { desc = 'Go to declaration of LSP item in new split down' } },
  goto_declaration_up    = { 'n', 'gDk', methods.split_to_definition.up,    { desc = 'Go to declaration of LSP item in new split up' } },
  goto_declaration_right = { 'n', 'gDl', methods.split_to_definition.right, { desc = 'Go to declaration of LSP item in new split to the right' } },

  goto_references = { 'n', 'gr',  methods.goto_references,           { desc = 'Open quickfix menu with references to LSP item' } },
}

local M = make_feature({
  name = 'LSP :: General Navigation',
  dependencies = { lsp },

  description = [[
  This feature adds buffer-local keymaps for navigating files using LSP features
  ]],

  keymap = navKeymap,
  setup_seq = { keymaps = false },

  on_activate = function()
    lsp.methods.add_hook({
      hook_name = 'on_attach',
      callback = function(_, bufnr) navKeymap.apply(bufnr) end,
    })
  end
})

M.methods = methods

return M
