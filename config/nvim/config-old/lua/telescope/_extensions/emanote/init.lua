local has_telescope, telescope = pcall(require, 'telescope')
if not has_telescope then
  error 'Emanote telescope integration requires nvim-telescope/telescope.nvim'
end

local emanoteTelescopeIntegration = require('user.emanote_integration.telescope')

local userConfig = {}

return telescope.register_extension {
  setup = function(extensionOptions)
    userConfig = extensionOptions or {}
  end,

  exports = {
    find_tags = function () emanoteTelescopeIntegration.findTags(userConfig) end
  }
}
