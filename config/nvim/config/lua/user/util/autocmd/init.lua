
local M = {}

--[[
--
--  Helper Functions
--
--]]

local function is_augroup_spec(tbl)
  return
      type(tbl) == 'table' and
      (not tbl.name or type(tbl.name) == 'string') and
      type(tbl.autocmds) == 'table'
end

local function apply_augroup(spec, bufnr)
  local groupName = spec.name
  local autocmd_specs = spec.autocmds
  local groupOpts = {}
  for k, v in pairs(spec) do
    if k ~= 'name' and k ~= 'autocmds' then
      groupOpts[k] = v
    end
  end

  local groupId = vim.api.nvim_create_augroup(groupName, groupOpts)

  for _, autocmd_spec in ipairs(autocmd_specs) do
    local events = autocmd_spec.events

    local autocmdOpts = autocmd_spec.opts
    autocmdOpts.group = groupId
    if type(bufnr) == 'number' then
      autocmdOpts.bufnr = bufnr
    end

    vim.api.nvim_create_autocmd(events, autocmdOpts)
  end

  return groupId
end

local function apply_augroups(specs, bufnr)
  local groupIds = {}
  for key, spec in pairs(specs) do
    if type(key) == 'string' and not spec.name then
      spec.name = key
    end

    local groupId = apply_augroup(spec, bufnr)
    table.insert(groupIds, groupId)
  end
  return groupIds
end

local function remove_augroups(group_ids)
  for _, group_id in ipairs(group_ids) do
    vim.api.nvim_del_augroup_by_id(group_id)
  end
end

--[[
--
--  Public Interface
--
--]]

function M.make_augroup(spec)
  if is_augroup_spec(spec) then
    spec = { spec }
  end

  local inst = {
    spec = spec,
    global_group_ids = nil,
    buffer_group_ids = {},
  }

  function inst:apply(bufnr)
    if bufnr == nil then
      if self.global_group_ids then return end
      self.global_group_ids = apply_augroups(self.spec, nil)
      return
    end

    if type(bufnr) == 'number' then
      if self.buffer_group_ids[bufnr] then return end
      self.buffer_group_ids[bufnr] = apply_augroups(self.spec, bufnr)
      return
    end

    vim.notify('augroup.apply :: Invalid buffer number: ' .. vim.inspect(bufnr), 'error')
  end

  function inst:remove(bufnr)
    if bufnr == nil then
      if not self.global_group_ids then return end
      remove_augroups(self.global_group_ids)
      self.global_group_ids = nil
      return
    end

    if type(bufnr) == 'number' then
      if not self.buffer_group_ids[bufnr] then return end
      remove_augroups(self.buffer_group_ids[bufnr])
      return
    end

    vim.notify('augroup.remove :: Invalid buffer number: ' .. vim.inspect(bufnr), 'error')
  end

  return inst
end

---------------------------------------- Legacy API ---------------------------------------------

M.augroup = function(spec)
  local groupName = spec.name
  local autocmdSpecs = spec.autocmds
  local groupOpts = {}
  for k, v in pairs(spec) do
    if k ~= 'name' and k ~= 'autocmds' then
      groupOpts[k] = v
    end
  end

  local groupId = vim.api.nvim_create_augroup(groupName, groupOpts)

  for _, autocmdSpec in ipairs(autocmdSpecs) do
    local events = autocmdSpec.events
    local autocmdOpts = autocmdSpec.opts
    autocmdOpts.group = groupId
    vim.api.nvim_create_autocmd(events, autocmdOpts)
  end

  return groupId
end

M.is_augroup_spec = function(tbl)
  if type(tbl) ~= 'table' then
    return false
  end
  return type(tbl.name) == 'string' and type(tbl.autocmds) == 'table'
end

return M
