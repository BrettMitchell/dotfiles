let
  ln = scriptName: type: {
    ".local/bin/${scriptName}".source = ./scripts/${scriptName};
  };

  flatten = attrs:
    (builtins.foldl'
      (s1: s2: s1 // s2)
      {}
      (builtins.attrValues attrs)
    );

  scripts = builtins.readDir ./scripts;
in {
  home.file = flatten (builtins.mapAttrs ln scripts);
}
