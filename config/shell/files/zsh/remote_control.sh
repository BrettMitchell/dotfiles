
# The following is an implementation of remote ZLE control
# using a FIFO as a simple message queue.
#
# Components:
# - mkfifo
# - zle -F -w <FD> <WIDGET-FUNCTION>
#   - Registers a widget function, which will intiate ZLE when the
#     given file descriptor FD triggers the POLLIN event.
#
# Technique:
#   - Use `mkfifo` to create a signal file with polling behavior 
#     that interfaces well with `zle -F -w`.
#   - After creating the signal fifo, assign a file descriptor
#     to it with `exec {zle_sig_fd}<>"$CONTROL_SIG_FILE"`
#   - The resulting file descriptor can be used with zle. When a message
#     is written to the signal file, it triggers our message handler
#     widget in a ZLE context.
#   - It is critical to read a line from the signal file after the
#     widget is initiated. `poll()` on a FIFO will return `POLLIN` as
#     long as:
#     1. The write end is open, OR
#     2. There is data in the file
#     Thus we have to empty the signal file in order to ensure that
#     the widget function is run only once for each message.
#
# IMPORTANT: This setup hooks into the interactive shell process itself,
#            not a sub-shell. Calling `zle_remote` inside the current
#            shell is BLOCKING and must be sent to the background with `&`.

function remote_handle {
  local fd="$1"
  read -u$fd line

  typeset -a args
  IFS=$'\t' read -r -A args <<< "${line%$'\n'}"
  local out="${args[1]}"
  local cmd="${args[2]}"
  args=(${args[@]:2})

  if ! command -v "$cmd" > /dev/null || \
     [[ ! -e "$out" ]] ; then
    return
  fi

  $cmd $args # >"$out"
}
zle -N remote_handle

export CONTROL_SIG_FILE="${XDG_RUNTIME_DIR:-${TMPDIR:-/tmp}}/zsh-zle-remote-$$.fifo"
if [[ -e "$CONTROL_SIG_FILE" ]] ; then
  rm -f "$CONTROL_SIG_FILE"
fi

mkfifo "$CONTROL_SIG_FILE"
exec {zle_sig_fd}<>"$CONTROL_SIG_FILE"
zle -F -w $zle_sig_fd remote_handle
