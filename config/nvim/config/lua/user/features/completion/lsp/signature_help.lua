local feat_util = require('user.util.feature')
local add_plugins = require('user.util.plugins.lazy').add_plugins

local M = {
  name = 'Completion :: LSP Signature',

  description = [[
  This feature provides LSP signature completions and help through cmp
  ]],

  source_definition = { name = 'nvim_lsp_signature_help' },
}

function M.setup(config)
  add_plugins({ { 'https://github.com/hrsh7th/cmp-nvim-lsp-signature-help' } })
  feat_util.apply_config(config or {}, M.feature)
end

return M
