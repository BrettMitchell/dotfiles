local make_feature = require('pax-vim.util.feature').make_feature_v2
local make_hook_registry = require('pax-vim.util.hooks').make_hook_registry
local make_keymap = require('pax-vim.util.keymap')

local which_key = require('pax-vim.features.ui.whichkey')

local pick_option = require('pax-vim.features.input.pick_option')

local state = {
  installable = {}
}

local hooks = make_hook_registry({
  'on_load',
  'pre_install',
  'post_install',
  'pre_attach',
  'post_attach'
})

local M = make_feature({
  name = 'DAP',
  description = [[
  This feature provides debugging facilities. It does not add
  support for any specific language; individual features exist
  for supported languages.
  ]],

  deps = { pick_option },

  adapter_install_dir = nil,
  signs = {
    breakpoint = nil,
    stopped = nil,
  },

  -- TODO: Add augroup to enable and disable keymaps when attaching and detaching from
  -- a debug session
  -- augroups = {},

  plugins = {
    {
      -- 'https://github.com/mfussenegger/nvim-dap',

      'https://github.com/gpanders/nvim-dap',
      -- branch = 'source-map',
      commit = '5120f6931b90526236bfdaf63b1ef9272c1be57f',
    },
  },

  init = function(self)
    self.methods = {
      ensure_install_dir = function ()
        vim.fn.system({ 'mkdir', '-p', self.adapter_install_dir })
      end,

      add_installable = function(label, install)
        state.installable[label] = install
      end,

      install_adapter = function()
        local labels = {}
        for k, _ in pairs (state.installable) do
          table.insert(labels, k)
        end

        pick_option.methods.pick_from_map({
          header = 'Select a debug adapter to install:',
          items = state.installable
        }, function(selected_adapters)
          if not selected_adapters or #selected_adapters == 0 then
            vim.notify('No adapter selected', 'info')
            return
          end

          for _, selected_adapter in ipairs(selected_adapters) do
            local install = selected_adapter.item
            install()
          end
        end)
      end,

      -- Breakpoints
      toggle_breakpoint = function () require('dap').toggle_breakpoint() end,
      clear_breakpoints = function () require('dap').clear_breakpoints() end,
      add_conditional_breakpoint = function ()
        require('dap').set_breakpoint(vim.fn.input('Breakpoint condition: '), nil, nil)
      end,
      add_log_point = function ()
        require('dap').set_breakpoint(nil, nil, vim.fn.input('Log point message: '))
      end,

      find_breakpoints = function (opts)
        require('telescope').extensions.dap.list_breakpoints(opts)
      end,

      -- Session commands
      attach = function ()
        require('telescope').extensions.dap.configurations()
      end,

      close = function ()
        require('dap').disconnect(
          { restart = false, terminateDebuggee = false },
          function () end
        )
      end,

      step_continue = function () require('dap').continue() end,
      step_over = function () require('dap').step_over() end,
      step_into = function () require('dap').step_into() end,
      step_out = function () require('dap').step_out() end,
      step_back = function () require('dap').step_back() end,
      step_up = function () require('dap').up() end,
      step_down = function () require('dap').down() end,
    }

    self.dap_global_keymaps = make_keymap {
      install_adapter = { 'n', '<space>dI', self.methods.install_adapter, { desc = 'Install a registered debug adapter' } },
      attach = { 'n', '<space>da', self.methods.attach, { desc = 'Attach to a debug session' } },

      -- Breakpoints
      toggle_breakpoint = { 'n', '<space>bb', self.methods.toggle_breakpoint, { desc = 'Toggle breakpoint' } },
      add_conditional_breakpoint = { 'n', '<space>bc', self.methods.add_conditional_breakpoint, { desc = 'Add conditional breakpoint' } },
      add_log_point = { 'n', '<space>bl', self.methods.add_log_point, { desc = 'Add log point' } },
      clear_breakpoints = { 'n', '<space>bx', self.methods.clear_breakpoints, { desc = 'Clear breakpoints' } },
      find_breakpoints = { 'n', '<space>bf', self.methods.find_breakpoints, { desc = 'Fuzzy find debugger breakpoints' } },
    }

    self.dap_session_keymaps = make_keymap {
      -- Session commands
      close = { 'n', '<space>dx', self.methods.close, { desc = 'Disconnect debug session' } },
      step_continue = { 'n', ';sc', self.methods.step_continue, { desc = 'Debugger: Step continue' } },
      step_back = { 'n', ';sb', self.methods.step_back, { desc = 'Debugger: Step back' } },
      step_over = { 'n', ';sj', self.methods.step_over, { desc = 'Debugger: Step over' } },
      step_into = { 'n', ';si', self.methods.step_into, { desc = 'Debugger: Step into' } },
      step_out = { 'n', ';so', self.methods.step_out, { desc = 'Debugger: Step out' } },
      up = { 'n', ';sl', self.methods.step_up, { desc = 'Debugger: Step up' } },
      down = { 'n', ';sh', self.methods.step_down, { desc = 'Debugger: Step down' } },
    }
  end,

  apply = function(self)
    self.dap_global_keymaps.apply()
    which_key.methods.label_group('<space>d', 'Debugging')

    if self.signs.breakpoint then
      vim.fn.sign_define('DapBreakpoint', {
        text = self.signs.breakpoint.icon,
        texthl = self.signs.breakpoint.texthl,
        linehl = self.signs.breakpoint.linehl,
        numhl = self.signs.breakpoint.numhl,
      })
    end

    if self.signs.stopped then
      vim.fn.sign_define('DapStopped', {
        text = self.signs.stopped.icon,
        texthl = self.signs.stopped.texthl,
        linehl = self.signs.stopped.linehl,
        numhl = self.signs.stopped.numhl,
      });
    end

    local dap = require('dap')
    dap.listeners.before.event_initialized.pax_vim_dap_session_keymaps = function()
      self.dap_session_keymaps.overlay()
    end
    dap.listeners.before.event_terminated.pax_vim_dap_session_keymaps = function()
      self.dap_session_keymaps.remove_overlay()
    end
    dap.listeners.before.event_exited.pax_vim_dap_session_keymaps = function()
      self.dap_session_keymaps.remove_overlay()
    end

    hooks.do_hook('on_load')
  end
})

M.hooks = hooks
M.state = state

return M
