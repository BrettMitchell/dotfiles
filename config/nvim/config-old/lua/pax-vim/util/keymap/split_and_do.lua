local SPLIT_COMMANDS = {
  left = 'leftabove vsplit',
  down = 'rightbelow split',
  up = 'leftabove split',
  right = 'rightbelow vsplit',
}

return function(direction, fn)
  if type(fn) == 'string' then
    fn = function() vim.cmd(fn) end
  end

  vim.cmd(SPLIT_COMMANDS[direction])
  fn()
end
