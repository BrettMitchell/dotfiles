local map = require('user.lib.map').map
local closeWindow = require('user.utils').closeWindow

local M = {}

function M.setupWrite()
  map('n', '<space>w', '<cmd>w<cr>') -- Write file
end

-- This is intended to be used in project session neovim instances
function M.setupQuit()
  map('n', '<space>q', function() closeWindow(0) end) -- Close window
  map('n', '<C-M-q>', '<cmd>qa<cr>') -- Quit
end

-- This is intended to be used in temporary, one-off neovim instances
-- which are integrated with other processes
function M.setupQuitImmediate()
  -- Cancel
  map('n', '<C-c>', '<cmd>qa!<cr>')
  -- Confirm
  map('n', '<space>q', '<cmd>qa<cr>')
  -- Cancel and confirm all in one
  map('n', '<space>x', '<cmd>wqa<cr>')
end

return M
