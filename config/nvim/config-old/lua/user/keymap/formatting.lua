local registerKeyMap = require('user.lib.map').registerKeyMap

local M = {}

function M.setup()
  registerKeyMap({ 'n' }, {
    ['<space>f'] = {
      f = { '<cmd>lua vim.lsp.buf.format({ async = true })<enter>', 'Format document' },
      F = { '<cmd>lua vim.lsp.buf.range_formatting()<enter>', 'Format selected range' },
      j = { 'J', 'Join lines' },
      -- s = { '<cmd>lua require("trevj").format_at_cursor()<enter>', 'Spread lines' },
      ['>'] = { '>', 'Indent with text object and motion support' },
      ['<'] = { '<', 'Dedent with text object and motion support' },

      -- Configured directly in comment plugin
      -- c = { 'gc', 'Comment with text object and motion support' },
    }
  })
end

return M
