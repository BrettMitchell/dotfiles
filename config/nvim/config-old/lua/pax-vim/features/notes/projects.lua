local frontmatter = require('pax-vim.features.notes.frontmatter.v2')
local explore     = require('pax-vim.features.notes.explore.plumbing')
local pickNote    = require('pax-vim.features.notes.telescope').pickNote
local fsOps       = require('pax-vim.features.notes.fsOps')
local mergeLeft   = require('pax-vim.util.table.merge').mergeLeft
local TAGS        = require('pax-vim.features.notes.special_tags')

local M           = {}

function M.pickProject(opts)
  opts = mergeLeft({ title = 'Projects' }, opts or {})

  explore.getNotesByTags(
    {
      tags = {
        TAGS.EFFORT .. '.*',
        'NOT ' .. TAGS.TAXON,
        'NOT ' .. TAGS.TAXONOMY,
      },

      select = {
        'absPath',
        'path',
        'title',
        'metadata',
        'tags',
        'rawContent',
      }
    },

    function(err, notes)
      if (err) then
        vim.notify('Encountered an error while finding projects: ' .. vim.inspect(err), 'error')
        return
      end

      pickNote(notes, opts)
    end)
end

function M.gotoProjectRoot(opts)
  local currentFrontmatter = frontmatter.get_buf_frontmatter()
  if not currentFrontmatter then return end

  local tags = currentFrontmatter.tags
  local tagQuery = {}
  for _, tag in ipairs(tags) do
    if tag:find('^' .. TAGS.EFFORT .. '%.') then
      table.insert(tagQuery, tag)
    end
  end

  if #tagQuery == 0 then
    vim.notify('Note is not a member of any projects')
    return
  end

  table.insert(tagQuery, TAGS.EFFORT)

  explore.getNotesByTags({ tags = tagQuery }, function(errByTitle, projectNotes)
    if (errByTitle) then
      vim.notify('Encountered an error while finding project root: ' .. vim.inspect(errByTitle), 'error')
      return
    end

    if #projectNotes == 1 then
      vim.cmd('e ' .. projectNotes[1].absPath)
      return
    end

    pickNote(projectNotes, opts)
  end)
end

function M.pickWithinProject(opts)
  opts = mergeLeft({ title = 'Project peer notes' }, opts or {})

  local currentFrontmatter = frontmatter.get_buf_frontmatter()
  if not currentFrontmatter then return end

  local tags = currentFrontmatter.tags

  local allContainingProjects = {}
  for _, tag in ipairs(tags) do
    if tag:find('^' .. TAGS.EFFORT .. '%.') then
      table.insert(allContainingProjects, tag)
    end
  end

  if #allContainingProjects == 0 then
    vim.notify('Note is not a member of any projects')
    return
  end

  explore.getNotesByTags({ tags = allContainingProjects }, function(errByTitle, projectNotes)
    if (errByTitle) then
      vim.notify('Encountered an error while finding project root: ' .. vim.inspect(errByTitle), 'error')
      return
    end

    pickNote(projectNotes, opts)
  end)
end

local function createProjectNote(parentNote)
  fsOps.createOrOpen(
    {
      title = parentNote.title .. ' :: TITLE_SUFFIX',
      skipFrontmatterGeneration = false
    },

    function()
      local tags = {}
      for _, tag in ipairs(parentNote.tags) do
        if tag ~= TAGS.EFFORT then
          table.insert(tags, tag)
        end
      end

      frontmatter.map_buf_frontmatter({
        bufnr = 0,
        fn = function(fm)
          fm.title = parentNote.title .. ' :: TITLE_SUFFIX'
          fm.tags = tags
        end
      })
      vim.cmd('write')
    end
  )
end

local createNotePickerOpts = {
  mappings = {
    select_default = function(selectedParent)
      createProjectNote(selectedParent)
    end
  }
}

function M.newProjectNote()
  local noteFrontmatter = frontmatter.get_buf_frontmatter()
  if not noteFrontmatter then
    vim.notify('Could not parse note frontmatter', 'error')
    return
  end

  if type(noteFrontmatter.tags) ~= 'table' then
    vim.notify('Invalid frontmatter tags: ' .. vim.inspect(noteFrontmatter), 'error')
    return
  end

  local searchTags = {}
  for _, tag in ipairs(noteFrontmatter.tags) do
    if (tag:find('^' .. TAGS.EFFORT .. '%.')) then
      table.insert(searchTags, tag)
    end
  end

  if #searchTags == 0 then
    show('Note is not part of a project, please pick a project to add a new note to')
    M.pickProject(createNotePickerOpts)
    return
  end

  table.insert(searchTags, TAGS.EFFORT)

  explore.getNotesByTags(
    {
      tags = searchTags,
      select = {
        'absPath',
        'path',
        'title',
        'metadata',
        'tags',
        'rawContent',
      }
    },

    function(err, notes)
      if (err) then
        vim.notify('Encountered an error while finding project root: ' .. vim.inspect(err), 'error')
        return
      end

      if #notes == 0 then
        vim.notify('Could not find project root for current file', 'warn')
        M.pickProject(createNotePickerOpts)
      elseif #notes > 1 then
        vim.notify('More than one containing project found for current note, please pick from among them')
        pickNote(notes, createNotePickerOpts)
      else
        createProjectNote(notes[1])
      end
    end)
end

function M.focus_project_as_context()
  local note_fm = frontmatter.get_buf_frontmatter()
  if not note_fm then
    vim.notify('Could not parse note frontmatter', 'error')
    return
  end

  local project_tags = {}
  for _, tag in ipairs(note_fm.tags) do
    if (tag:find('^' .. TAGS.EFFORT .. '%.')) then
      table.insert(project_tags, tag)
    end
  end

  local context = require('pax-vim.features.notes.context')
  context.context = project_tags
  context.context_mode = 'any-of'

  vim.notify('Context set to ' .. context.context_mode .. ': ' .. vim.inspect(context.context))
end

return M
