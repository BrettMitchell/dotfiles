local make_feature = require('pax-vim.util.feature').make_feature
local protect = require('pax-vim.util.function.protect').protect

local M = make_feature({
  name = 'Harpoon',

  description = [[
  This feature sets up harpoon, a plugin for marking a working set
  of files and navigating between them very quickly.
  ]],

  plugins = {
    {
      'https://github.com/ThePrimeagen/harpoon',
      config = function ()
        -- TODO: Do this in a VimResize (?) autocommand to keep harpoon in sync with window size
        require('harpoon').setup({
          menu = {
            width = math.min(120, vim.api.nvim_win_get_width(0) - 8),
            height = 10,
          }
        })
      end
    }
  },

  keymaps = {
    { { 'n', 'i', 'v' }, '<M-S-m>',    protect(function() require('harpoon.ui').toggle_quick_menu() end), { desc = 'Open harpoon quick menu' } },
    { { 'n', 'i', 'v' }, '<M-m>',      protect(function() require('harpoon.mark').add_file() end),        { desc = 'Mark file (harpoon)' } },
    { { 'n', 'i', 'v' }, '<M-1>',      protect(function() require('harpoon.ui').nav_file(1) end),         { desc = 'Go to file 1 (harpoon)' } },
    { { 'n', 'i', 'v' }, '<M-2>',      protect(function() require('harpoon.ui').nav_file(2) end),         { desc = 'Go to file 2 (harpoon)' } },
    { { 'n', 'i', 'v' }, '<M-3>',      protect(function() require('harpoon.ui').nav_file(3) end),         { desc = 'Go to file 3 (harpoon)' } },
    { { 'n', 'i', 'v' }, '<M-4>',      protect(function() require('harpoon.ui').nav_file(4) end),         { desc = 'Go to file 4 (harpoon)' } },
    { { 'n', 'i', 'v' }, '<M-5>',      protect(function() require('harpoon.ui').nav_file(5) end),         { desc = 'Go to file 5 (harpoon)' } },
    { { 'n', 'i', 'v' }, '<M-6>',      protect(function() require('harpoon.ui').nav_file(6) end),         { desc = 'Go to file 6 (harpoon)' } },
    { { 'n', 'i', 'v' }, '<M-7>',      protect(function() require('harpoon.ui').nav_file(7) end),         { desc = 'Go to file 7 (harpoon)' } },
    { { 'n', 'i', 'v' }, '<M-8>',      protect(function() require('harpoon.ui').nav_file(8) end),         { desc = 'Go to file 8 (harpoon)' } },
    { { 'n', 'i', 'v' }, '<M-9>',      protect(function() require('harpoon.ui').nav_file(9) end),         { desc = 'Go to file 9 (harpoon)' } },
    { { 'n', 'i', 'v' }, '<M-0>',      protect(function() require('harpoon.ui').nav_file(10) end),        { desc = 'Go to file 10 (harpoon)' } },
    { { 'n', 'v' },      '<space>M',   protect(function() require('harpoon.ui').toggle_quick_menu() end), { desc = 'Open harpoon quick menu' } },
  }
})

return M
