local zk = require('zk.api')
local previewers = require('telescope.previewers')

local noteUtil = require('user.notes.util')
local makeLines = require('user.lib.makeLines').makeLines

local function makeZkPreviewer(opts)
  opts = opts or {}
  local getNotesDir = opts.getNotesDir or noteUtil.getNotesDir
  local getQuery = opts.getQuery or function() return { select = { 'title', 'tags' } } end
  local getText = opts.getText or function(self, notes) return vim.inspect(notes) end

  return previewers.new_buffer_previewer({
    define_preview = function(self, entry)
      local query = getQuery(self, entry)

      zk.list(getNotesDir(), query, function(err, notes)
        local text = ''
        if (err) then
          text = 'An error occurred while running zk:\n\n' .. vim.inspect(err.message or err)
        else
          text = getText(self, notes)
        end

        local lines = makeLines(text)

        vim.api.nvim_buf_set_lines(self.state.bufnr, 0, -1, false, lines)
      end)
    end
  })
end

return {
  makeZkPreviewer = makeZkPreviewer
}
