#!/usr/bin/env bash

set -a
. "$(dirname "$0")/lib.sh"
set +a

function get_install_scripts {
  fd --type x install ./config ./scripts ./src \
    | sed -E 's:^\./::' \
    | sed -E 's:/install\.sh$::'
}

function install {
  local target=`cat -`
  if [[ -z "$target" ]] ; then
    return 0
  fi

  printf "\n> Installing module '$target'"
  printf "\n\n"

  export module="$root/$target"
  "$module/install.sh"

  printf "\nDONE\n"
}

function main {
  local target="$1"

  if [[ "$target" == all ]] ; then
    get_install_scripts | install
    return 0
  fi

  if [[ ! -z "$target" ]] ; then
    printf '%s' "$target" | install
    return 0
  fi

  get_install_scripts | sk | install
}

main "$@"
