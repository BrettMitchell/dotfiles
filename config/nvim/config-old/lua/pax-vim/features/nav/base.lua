local keymap = require('pax-vim.util.keymap')

local M = {
  feature = {
    name = 'Base Motion Keymaps',

    description = [[
    This feature defines custom motion keymaps for faster navigation
    ]],

    mode_1_keymap = keymap {
      start_of_line = { { 'n', 'x' }, 'H', '^', { desc = 'Start of line' } },
      end_of_line = { { 'n', 'x' }, 'L', '$', { desc = 'End of line' } },
      down = { { 'n', 'x' }, 'J', '15j', { desc = '15 lines down' } },
      up = { { 'n', 'x' }, 'K', '15k', { desc = '15 lines up' } },
    },

    command_keymap = keymap {
      { 'c', '<C-h>',   '<left>',    { desc = 'Move left by one character', silent = false } },
      { 'c', '<C-l>',   '<right>',   { desc = 'Move right by one character', silent = false } },
      { 'c', '<C-j>',   '<down>',    { desc = 'Go back in command history by one entry', silent = false } },
      { 'c', '<C-k>',   '<up>',      { desc = 'Go forward in command history by one entry', silent = false } },
      { 'c', '<C-e>',   '<C-right>', { desc = 'Move forward by one word', silent = false } },
      { 'c', '<C-b>',   '<C-left>',  { desc = 'Move back by one word },', silent = false } },
      { 'c', '<C-S-h>', '<home>',    { desc = 'Go to the beginning of the line', silent = false } },
      { 'c', '<C-S-l>', '<end>',     { desc = 'Go to the end of the line', silent = false } },
    },

    insert_keymap = keymap {
      { 'i', '<C-h>', '<left>',  { desc = 'Left in insert mode' } },
      { 'i', '<C-j>', '<down>',  { desc = 'Down in insert mode' } },
      { 'i', '<C-k>', '<up>',    { desc = 'Up in insert mode' } },
      { 'i', '<C-l>', '<right>', { desc = 'Right in insert mode' } },
    }
  }
}

function M.setup()
  M.feature.insert_keymap.apply()
  M.feature.command_keymap.apply()
  M.feature.mode_1_keymap.apply()
  -- registerKeyMap({ 'n' }, {
  --   ['<space>;'] = { toggleMovementMode, 'Toggle movement mode' }
  -- })
end

return M
