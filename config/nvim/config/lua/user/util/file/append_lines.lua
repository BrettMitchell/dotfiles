local make_lines = require('user.util.buffer.make_lines')

return function(opts)
  if type(opts.newLines) == 'string' then opts.newLines = { opts.newLines } end

  vim.fn.writefile(
    make_lines(opts.text or opts.lines),
    opts.filename,
    'a'
  )
end
