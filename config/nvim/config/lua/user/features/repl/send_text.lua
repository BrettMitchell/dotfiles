local feat_util = require('user.util.feature')
local add_plugins = require('user.util.plugins.lazy').add_plugins
local keymap = require('user.util.keymap')

local M = {
  feature = {
    name = 'Send Text to REPL',

    description = [[
    This feature adds support for sending text to REPLs.

    NOTE: At the moment, this feature integrates directly with tmux, and does not include
    support for native vim terminal buffers. This may change in the future for completeness'
    sake, but for now, you must use tmux to use this feature.
    ]],
  }
}

function M.setup(config)
  feat_util.apply_config(config or {}, M.feature)

  vim.g.slime_no_mappings = 1
  vim.g.slime_target = 'tmux'
  vim.g.slime_paste_file = vim.fn.tempname()
  vim.g.slime_bracketed_paste = 1

  -- Default slime config, should work for the most common use case of 2 splits: 1 nvim + 1 REPL
  vim.g.slime_default_config = { socket_name = 'default', target_pane = '{next}' }
  vim.g.slime_dont_ask_default = 1

  -- Works for markdown code fences. Will need to be made dynamic on filetype in order to support
  -- ipython etc.
  vim.g.slime_cell_delimiter = '```'

  add_plugins({
    { 'https://github.com/jpalardy/vim-slime' }
  })

  M.feature.keymap.apply()
end

function M.pick_repl_pane()
  local cmd_out = vim.fn.system({ 'tmux', 'display-panes', '-d', '0', "display-message -p '%%'" })
  local pane = cmd_out:match('%%%d+')
  if pane then
    vim.g.slime_default_config = { socket_name = 'default', target_pane = pane }
    vim.b.slime_config = { socket_name = 'default', target_pane = pane }
  end
end

function M.clear_repl()
  vim.fn['slime#send']('')
end

function M.send(text)
  vim.fn['slime#send'](text)
end

M.feature.keymap = keymap {
  send_block = { 'n', '<space>cc', '<plug>SlimeSendCell', { desc = 'Send current code block to REPL' } },
  send_visual_selection = { 'x', '<space>cc', '<plug>SlimeRegionSend', { desc = 'Send current visual selection to REPL' } },
  pick_repl_pane = { 'n', '<space>cp', M.pick_repl_pane, { desc = 'Pick tmux pane to send text to' } },
  clear_repl = { 'n', '<space>cl', M.clear_repl, { desc = 'Clear REPL (requires support for clear on Ctrl-L)' } }
}

return M
