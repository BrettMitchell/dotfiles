local make_feature = require('pax-vim.util.feature').make_feature_v2
local keymap = require('pax-vim.util.keymap')
local make_augroups = require('pax-vim.util.autocmd').make_augroup

local gutter_enabled = true

-- local function getVisibleBuffers()
--   local wins = vim.api.nvim_list_wins()
--   local bufs = {}
--
--   for _, win in ipairs(wins) do
--     table.insert(bufs, vim.api.nvim_win_get_buf(win))
--   end
--
--   return bufs
-- end

local function getWinForFilenamePattern(pattern)
  for _, win in ipairs(vim.api.nvim_list_wins()) do
    local buf = vim.api.nvim_win_get_buf(win)
    local filename = vim.api.nvim_buf_get_name(buf)
    if filename:find(pattern) then
      return win, buf, filename
    end
  end

  return nil, nil, nil
end

local M = make_feature({
  name = 'Git',

  description = [[
  This feature adds git integration through vim-fugitive and Gitsigns.
  ]],

  plugins = {
    { 'https://github.com/tpope/vim-fugitive' },

    {
      'https://github.com/lewis6991/gitsigns.nvim',
      config = function()
        require('gitsigns').setup({
          current_line_blame_opts = {
            delay = 0
          },

          on_attach = function(bufnr)
            local function map(mode, l, r, opts)
              opts = opts or {}
              opts.buffer = bufnr
              vim.keymap.set(mode, l, r, opts)
            end

            local n = { 'n' }
            local nv = { 'n', 'v' }
            local ox = { 'o', 'x' }

            local merge_keymap = nil
            merge_keymap = keymap {
              close_merge_ui = {
                'n',
                '<space>q',
                function()
                  local targetWin = getWinForFilenamePattern('^fugitive://.*//2')
                  local mergeWin = getWinForFilenamePattern('^fugitive://.*//3')

                  -- In merge mode, we want to quit merge mode before closing the entire file
                  if targetWin then vim.api.nvim_win_close(targetWin, false) end
                  if mergeWin then vim.api.nvim_win_close(mergeWin, false) end

                  -- Restore previous keymap
                  merge_keymap.remove_overlay(0)
                end,
                { desc = 'Close 3-way merge' },
              }
            }

            local function open_merge()
              vim.cmd('Gvdiffsplit!')
              merge_keymap.overlay(0)
            end

            map(n, '<space>gm', open_merge) -- Open a 3-way merge editor

            map(ox, 'ih', ':<C-U>Gitsigns select_hunk<CR>')

            map(n, '<space>ghp', '<cmd>Gitsigns preview_hunk<cr>') -- Preview previous version of hunk in a floating window

            map(nv, '<space>gs', '<cmd>Gitsigns stage_hunk<cr>') -- Stage hunk under cursor
            map(n, '<space>gS', '<cmd>Gitsigns stage_buffer<cr>') -- Stage current buffer
            map(nv, '<space>gr', '<cmd>Gitsigns reset_hunk<cr>') -- Reset hunk under cursor
            map(n, '<space>gR', '<cmd>Gitsigns reset_buffer<cr>') -- Reset current buffer

            map(n, '[h', '<cmd>Gitsigns next_hunk<cr>')          -- Next hunk in the current file
            map(n, ']h', '<cmd>Gitsigns prev_hunk<cr>')          -- Previous hunk in the current file

            -- Merge-specific bindings

            local filename = vim.fn.expand('%')
            local isFugitive = filename:match('^fugitive://')
            -- local isWorkingCopy = filename:match('^fugitive://.*//1')
            local isTarget = filename:match('^fugitive://.*//2')
            local isMerge = filename:match('^fugitive://.*//3')

            -- Saving inside a Fugitive merge window should update the actual file using the working copy
            -- if isFugitive then
            --   map(n, 'KK')
            -- end

            if isFugitive and (isTarget or isMerge) then
              map(n, '<space>ga', '<cmd>diffput<cr>') -- Put changes under cursor in working copy
            end

            map(n, '<space>gah', '<cmd>diffget //2<cr>') -- Accept //2 conflict contents
            map(n, '<space>gal', '<cmd>diffget //3<cr>') -- Accept //3 conflict contents

            -- Maybe incorporate stage hunk into process here, but maybe just use the existing binding

            return gutter_enabled
          end
        })
      end,
    }
  },

  global_keymaps = keymap {
    { 'n', '<space>gg', '<cmd>Git<cr>', {
      desc =
      'Open Fugitive equivalent of git status'
    } },
    { 'n', '<space>gp', '<cmd>Git pull<cr>', {
      desc =
      'Pull from remote'
    } },
    { 'n', '<space>gP', '<cmd>Git push<cr>', {
      desc =
      'Push to remote'
    } },
    { 'n', '<space>gc', '<cmd>Git commit<cr>', {
      desc =
      'Push to remote'
    } },
    { 'n', '<space>gB', '<cmd>Git blame<cr>', {
      desc =
      'Run git blame on current file'
    } },
    { 'n', '<space>gl',
      "<cmd>Git log --date=format-local:'%Y-%m-%d %H:%M:%S' --format='%C(auto)%h %ad %C(Cyan)%aN%C(auto) : %s'<cr>",
      {
        desc =
        'Open git log'
      } },
    { 'n', '<space>gL',
      "<cmd>Git log --date=format-local:'%Y-%m-%d %H:%M:%S' --format='%C(auto)%h %ad %C(Cyan)%aN%C(auto) : %s' -- %<cr>",
      {
        desc =
        'Open git log for current file'
      } },
  },

  -- Fugitive overwrites some motion mappings
  fugitive_buffer_keymaps = keymap {
    -- Remap existing hunk navigation to C-j/C-k
    { 'n', '<C-j>', '<plug>fugitive:J', { desc = 'Previous hunk' } },
    { 'n', '<C-k>', '<plug>fugitive:K', { desc = 'Next hunk' } },

    -- Re-instate overwritten movement keymaps
    { 'n', 'J',     '15j',              { desc = 'Quick motion down' } },
    { 'n', 'K',     '15k',              { desc = 'Quick motion up' } },
  },

  init = function(self)
    self.augroups = make_augroups {
      {
        name = 'ApplyCustomFugitiveBufferKeymaps',
        autocmds = { {
          events = 'BufWinEnter',
          opts = {
            callback = function()
              if vim.bo.filetype ~= 'fugitive' and vim.bo.filetype ~= 'git' then return end
              local bufnr = vim.fn.bufnr('%')
              self.fugitive_buffer_keymaps.apply(bufnr)
            end
          }
        } }
      }
    }
  end,

  apply = function(self)
    if self.global_keymaps then self.global_keymaps.apply() end
    if self.augroups then self.augroups:apply() end
  end
})

function M.toggleGutterSigns()
  vim.cmd('Gitsigns toggle_signs')
  vim.notify('Toggled git gutter signs')
end

function M.toggleCurrentLineBlame()
  vim.cmd('Gitsigns toggle_current_line_blame')
  vim.notify('Toggled git blame')
end

function M.toggle()
  gutter_enabled = not gutter_enabled
  if gutter_enabled then
    require('gitsigns').attach()
  else
    require('gitsigns').detach()
  end
  vim.notify('Toggled gitsigns plugin')
end

function M.enable()
  gutter_enabled = true
  require('gitsigns').attach()
  vim.notify('Enabled gitsigns plugin')
end

function M.disable()
  gutter_enabled = false
  require('gitsigns').detach()
  vim.notify('Disabled gitsigns plugin')
end

return M
