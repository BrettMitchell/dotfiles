local feat_util = require('pax-vim.util.feature')
local add_plugins = require('pax-vim.util.plugins.lazy').add_plugins
local keymap = require('pax-vim.util.keymap')

local M = {
  feature = {
    name = 'Word Motions',

    description = [[
    This feature adds support for more fine-grained 'e', 'w', and 'b' motions.
    Using 'e', 'w', and 'b' will jump between camelCase and underscore_words.

    Without this, 'e', 'w', and 'b' behave as follows (from the official README):

      CamelCaseACRONYMWords_underscore1234
      w--------------------------------->w
      e--------------------------------->e
      b<---------------------------------b

    With this plugin, they behave as follows:

      CamelCaseACRONYMWords_underscore1234
      w--->w-->w----->w---->w-------->w->w
      e-->e-->e----->e--->e--------->e-->e
      b<---b<--b<-----b<----b<--------b<-b
    ]],

    header = nil,
    buttons = nil,

    keymap = keymap {
      forward = { { 'n', 'x', 'o' }, 'b', '<Plug>WordMotion_b', { desc = 'Word motion forward' } },
      backward = { { 'n', 'x', 'o' }, 'e', '<Plug>WordMotion_e', { desc = 'Word motion backward' } },
    }
  }
}

function M.setup(userConfig)
  userConfig = userConfig or {}

  add_plugins({ {
    'https://github.com/chaoren/vim-wordmotion',
  } })

  feat_util.apply_config(userConfig, M.feature)
  M.feature.keymap.apply()

  if userConfig.wordmotion_prefix ~= nil then
    vim.g.wordmotion_prefix = userConfig.wordmotion_prefix
  end

  if userConfig.wordmotion_mappings ~= nil then
    vim.g.wordmotion_mappings = userConfig.wordmotion_mappings
  end

  if userConfig.wordmotion_nomap ~= nil then
    vim.g.wordmotion_nomap = userConfig.wordmotion_nomap
  end

  if userConfig.wordmotion_spaces ~= nil then
    vim.g.wordmotion_spaces = userConfig.wordmotion_spaces
  end

  if userConfig.wordmotion_uppercase_spaces ~= nil then
    vim.g.wordmotion_uppercase_spaces = userConfig.wordmotion_uppercase_spaces
  end
end

return M
