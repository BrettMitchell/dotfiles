local M = {}

function M.mergeLeft(o1, o2)
  local result = {}

  for k, v in pairs(o1) do
    result[k] = v
  end

  for k, v in pairs(o2) do
    result[k] = v
  end

  return result
end

function M.mergeRight(o1, o2)
  return M.mergeLeft(o2, o1)
end

function M.deepMergeLeft(o1, o2)
  local result = {}

  for k, v1 in pairs(o1) do
    local v2 = o2[k]
    if type(v1) == 'table' and type(v2) == 'table' then
      local merged = M.deepMergeLeft(v1, v2)
      result[k] = merged
    else
      result[k] = v1
    end
  end

  for k, v2 in pairs(o2) do
    if result[k] == nil then
      result[k] = v2
    end
  end

  return result
end

function M.deepMergeRight(o1, o2)
  return M.deepMergeLeft(o2, o1)
end

return M
