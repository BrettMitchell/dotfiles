local concat = require('user.util.table.concat')
local TAGS = require('user.features.notes.special_tags')

local M = {}

M.query = {
  frontmatter = function(prev, _, cb)
    return cb(nil, {
      title = prev.title or 'QUERY',
      tags = concat(prev.tags or {}, { TAGS.QUERY }),
      queries = { { tags = { 'wont-match', 'NOT wont-match' } } }
    })
  end,
  build = function(existingBody, _, cb) return cb(nil, existingBody) end
}

return M
