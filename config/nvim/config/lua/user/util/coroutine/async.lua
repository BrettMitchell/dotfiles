local unpack = require('user.util.table.unpack')

-- Turns a callback style async function into
-- one that supports coroutines
return function (f)
  return function(...)
    local params = { ... }
    local parent_coroutine = nil
    local ret = nil

    table.insert(params, function(...)
      if not parent_coroutine or coroutine.status(parent_coroutine) == 'dead' then
        ret = { ... }
      else
        local ok, err = coroutine.resume(parent_coroutine, ...)
        if not ok then error(debug.traceback(parent_coroutine, err)) end
      end
    end)

    f(unpack(params))

    if ret then return unpack(ret) end

    parent_coroutine = coroutine.running()
    return coroutine.yield()
  end
end
