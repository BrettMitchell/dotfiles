local make_feature = require('pax-vim.util.feature').make_feature_v2
local keymap = require('pax-vim.util.keymap')

local quit_normal = make_feature({
  name = 'System Commands: Quit (normal)',

  description = [[
  This feature adds easier keymaps for exiting the editor.

  This version is intended for long editing sessions where
  you likely want to be more careful about unsaved changes.
  ]],

  close_window = function(winnr)
    pcall(vim.api.nvim_win_close, winnr, false)
  end,

  init = function(self)
    self.keymap = keymap {
      { 'n', '<space>q', function() self.close_window(0) end, { desc = 'Close window' } },
      { 'n', '<space>Q', '<cmd>qa<cr>', { desc = 'Exit editor' } },
    }
  end,

  apply = function (self)
    self.keymap.apply()
  end
})

return quit_normal
