local make_feature = require('pax-vim.util.feature').make_feature

local config = {
  qf_config = {}
}

local M = make_feature({
  name = 'Quickfix',

  description = [[
  This feature adds to the quickfix list experience.
  https://github.com/ten3roberts/qf.nvim
  ]],

  plugins = {
    {
      'https://github.com/ten3roberts/qf.nvim',
      config = function ()
        require('qf').setup(config.qf_config)
      end
    }
  },

  config = config,

  -- keymaps = {},
})

return M
