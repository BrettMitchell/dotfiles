local make_feature = require('user.util.feature').make_feature_v2
local cmp_feature = require('user.features.completion.cmp')
local table_util = require('user.util.table')

local cmp_dap = make_feature({
  name = 'Completion :: Emmet Abbreviations',

  description = [[
  Completion source for Emmet abbreviations in appropriate filetypes
  ]],

  plugins = {
    { 'https://github.com/mattn/emmet-vim', event = 'VeryLazy' },
    { 'https://github.com/dcampos/cmp-emmet-vim', event = 'VeryLazy' },
  },

  apply = function()
    local dflt = cmp_feature.sources.default
    local sources = {
      table_util.concat(dflt[1], { { name = 'emmet_vim' } }),
      dflt[2],
    }

    local built_sources = require('cmp').config.sources(
      table_util.unpack(
        table_util.deep_copy(sources)
      )
    )

    require('cmp').setup.filetype(
      {
        'html',
        'xml',
        'typescriptreact',
        'javascriptreact',
        'css',
        'sass',
        'scss',
        'less',
        'heex',
        'tsx',
        'jsx',
      },
      { sources = built_sources }
    )
  end
})

return cmp_dap
