{ nixpkgs, ... }:

{
  imports = [
    ./firefox/advanced.nix
  ];

  home.packages = with nixpkgs.stable; [
    # Ick. Only installed b/c Excalidraw is stupid and won't run properly on FF
    google-chrome
    brave
    librewolf-wayland
  ];
}
