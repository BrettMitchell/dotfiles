local make_feature = require('pax-vim.util.feature').make_feature_v2
local includes = require('pax-vim.util.table.includes')
local dap = require('pax-vim.features.dap')

local config = {
  install_dir = nil
}

local function get_adapter()
  local installDir = config.install_dir or dap.adapter_install_dir .. '/vscode-node-debug2'

  return {
    type = 'executable',
    command = 'node',
    args = { installDir .. '/out/src/nodeDebug.js' },
  }
end

local function get_dap_config ()
  return {
    {
      name = 'Launch',
      type = 'node2',
      request = 'launch',
      program = '${file}',
      cwd = vim.fn.getcwd(),
      sourceMaps = true,
      protocol = 'inspector',
      console = 'integratedTerminal',
    },

    {
      -- For this to work you need to make sure the node process is started with the `--inspect` or `--inspect-brk` flag.
      name = 'Attach (auto)',
      type = 'node2',
      request = 'attach',
      cwd = vim.fn.getcwd(),
      sourceMaps = true,
      protocol = 'inspector',
      skipFiles = { '<node_internals>/**/*.js' },
    },

    {
      -- For this to work you need to make sure the node process is started with the `--inspect` or `--inspect-brk` flag.
      name = 'Attach (manual port entry)',
      type = 'node2',
      request = 'attach',
      port = function ()
        local port = vim.fn.input('Enter a port: ')
        if port:len() == 0 then
          -- Unfortunately, this is the only way to get nvim-dap to stop what it's doing and abort the connect operation
          error('No port chosen, aborting connect')
        end
        return port
      end
    },

    {
      -- For this to work you need to make sure the node process is started with the `--inspect` or `--inspect-brk` flag.
      name = 'Attach (pick)',
      type = 'node2',
      request = 'attach',
      processId = require 'dap.utils'.pick_process,
    },
  }
end

local function init()
  local _dap = require('dap')

  _dap.adapters.node2 = get_adapter()
  _dap.configurations.javascript = get_dap_config()
end

local methods = {
  get_adapter = get_adapter,
  get_dap_config = get_dap_config,
  init = init,

  remove = function (opts)
    opts = opts or {}

    if not config.install_dir then
      vim.notify("Cannot remove directory '" .. config.install_dir .. "'", 'error')
      return
    end

    if not opts.no_confirm then
      local confirmation = vim.fn.input("Are you sure you want to permanantly delete the directory '" .. config.install_dir .. "'? (y/n) : ")
      if not includes(confirmation, { 'y', 'Y' }) then
        vim.notify('Aborted by user')
        return
      end
    end

    vim.notify("Removing directory '" .. config.install_dir .. "'")
    vim.fn.system({ 'rm', '-rf', config.install_dir })
  end,

  install = function ()
    local shell = os.getenv('SHELL')

    if (vim.fn.isdirectory(config.install_dir) == 1) then
      vim.notify('https://github.com/microsoft/vscode-node-debug2.git already installed to ' .. config.install_dir, 'info')
      return
    end

    dap.hooks.do_hook('pre_install', { name = 'node2' })

    vim.notify('Installing node2 debug adapter: ' .. vim.inspect({ shell = shell, installDir = config.install_dir }))

    -- TODO: Use plenary to make this async. This is a pretty awful experience right now.
    vim.notify(vim.fn.system({ 'mkdir', '-p', config.install_dir }))
    vim.notify(vim.fn.system({ 'git', 'clone', 'https://github.com/microsoft/vscode-node-debug2.git', config.install_dir }))
    vim.notify(
      vim.fn.system({ shell, '-c',
        ("cd '%s' && npm install && NODE_OPTIONS=--no-experimental-fetch npm run build"):format(config.install_dir) })
    )

    vim.notify('Finished installing node2 debug adapter')

    dap.hooks.do_hook('post_install', { name = 'node2' })
  end,
}

local M = make_feature({
  name = "DAP :: Adapter 'node2'",
  deps = { dap },

  description = [[
  This feature adds utilities to aid in the installation and startup of
  the 'node2' debug adapter.
  ]],

  config = config,

  apply = function(self)
    if not config.install_dir then
      self.config.install_dir = dap.adapter_install_dir .. '/node2'
    end

    dap.methods.add_installable('node2', methods.install)
    methods.init()
  end
})

M.methods = methods

return M
