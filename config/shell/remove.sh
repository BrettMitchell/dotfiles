#!/usr/bin/env bash

rm_safe "$HOME/.profile"

rm_safe "$HOME/.bash_profile"
rm_safe "$HOME/.bashrc"

rm_safe "$HOME/.zprofile"
rm_safe "$HOME/.zshenv"
rm_safe "$HOME/.zshrc"
rm_safe "$HOME/.config/zsh"
