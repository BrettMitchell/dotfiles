#!/usr/bin/env bash

ln_safe "$module/conf.hjson" "$HOME/.config/broot/conf.hjson"
ln_safe "$module/conf.keys.hjson" "$HOME/.config/broot/conf.keys.hjson"
ln_safe "$module/conf.keys-mux.hjson" "$HOME/.config/broot/conf.keys-mux.hjson"
ln_safe "$module/conf.lf.hjson" "$HOME/.config/broot/conf.lf.hjson"
ln_safe "$module/conf.shell.hjson" "$HOME/.config/broot/conf.shell.hjson"
ln_safe "$module/conf.mux.hjson" "$HOME/.config/broot/conf.mux.hjson"
ln_safe "$module/conf.template.hjson" "$HOME/.config/broot/conf.template.hjson"
