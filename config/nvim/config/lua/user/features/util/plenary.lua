local add_plugins = require('user.util.plugins.lazy').add_plugins

local plenary = {
  name = 'Utility: Plenary',

  description = [[
  This feature adds the commonly used plugin 'plenary'
  ]],
}

function plenary.setup()
  add_plugins({
    'nvim-lua/plenary.nvim',
  })
end

return plenary
