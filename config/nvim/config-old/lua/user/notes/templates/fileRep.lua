local M = {}

M.fileRep = {
  frontmatter = function(existingFrontmatter, _, cb)
    return cb(nil, {
      title = 'File representative :: FILE',
      tags = { 'meta.type.file-rep' },
      file = existingFrontmatter.file or 'FILE_GOES_HERE',
      bindings = {
        [';;o'] = {
          lua = "vim.fn.system(('u-open-item %s'):format(require('user.notes.frontmatter').parseFrontmatter().file))"
        },
      }
    })
  end,
  build = function(existingBody, _, cb) return cb(nil, existingBody) end
}

return M
