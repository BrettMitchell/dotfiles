local explore = require('user.features.notes.explore.plumbing')
local frontmatter = require('user.features.notes.frontmatter.v2')
local util = require('user.features.notes.util')

local M = {}

-- Internal
local function makeNote(filename, skipFrontmatterGeneration)
  M.open(filename)
  -- If this is a new file, make sure it exists before generating frontmatter
  if not skipFrontmatterGeneration then
    local fm = frontmatter.generate_frontmatter()
    frontmatter.write_buf_frontmatter({
      bufnr = 0,
      frontmatter = fm
    })
  end
end

local function uuid()
  local template = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'
  return string.gsub(template, '[xy]', function(c)
    local v = (c == 'x') and math.random(0, 0xf) or math.random(8, 0xb)
    return string.format('%x', v)
  end)
end

-- Open a note
function M.open(filename)
  local notesDir = util.getNotesDir()
  if not notesDir then return end

  local fullPath = filename
  if not fullPath:match('^/') then
    fullPath = notesDir .. '/' .. filename
  end

  -- vim.cmd('! mkdir -p ' .. directory)
  vim.cmd('silent edit ' .. fullPath)
end

function M.generateNewFilename()
  local filename = nil

  while true do
    local noteName = uuid()
    filename = 'zettelkasten/' .. noteName .. '.qmd'

    if vim.fn.filereadable(filename) == 0 then
      break
    else
      -- This will only happen once in a million blue moons, so I figure it might be nice
      -- to see a notification if this ever does happen.
      vim.notify('Filename conflict, regenerating UUID: ' .. filename)
    end
  end

  return filename
end

-- Create a new blank note
function M.newNote(skipFrontmatterGeneration)
  local filename = M.generateNewFilename()
  makeNote(filename, skipFrontmatterGeneration)
  return { filename = filename }
end

-- Open a note, creating it if necessary
function M.createOrOpen(opts, cb)
  opts = opts or {}
  local title = opts.title
  local skipFrontmatterGeneration = opts.skipFrontmatterGeneration
  explore.getNotesByTitle({ title = title }, function(err, notes)
    if err then
      vim.notify('An error occurred: ' .. vim.inspect(err), 'error')
      return
    end

    if not notes or #notes == 0 then
      local res = M.newNote(skipFrontmatterGeneration)
      return cb({ created = true, filename = util.getNotesDir() .. res.filename })
    end

    if #notes > 1 then
      vim.notify('More than one file has the title: ' .. title .. '!', 'warn')
    end

    local note = notes[1]
    M.open(note.path)
    return cb({ created = false, filename = note.path })
  end)
end

return M
