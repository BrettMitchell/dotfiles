local M = {}

function M.loadJson(filename)
  local hasLunaJson, lunajson = pcall(require, 'lunajson')
  if not hasLunaJson then
    error("JSON lib requires 'lunajson' from LuaRocks")
  end

  local file = io.open(filename, 'rb')
  if not file then return nil end

  local content = file:read '*a'
  file:close()

  local json = lunajson.decode(content)
  return json
end

function M.saveJson(filename, data)
  local hasLunaJson, lunajson = pcall(require, 'lunajson')
  if not hasLunaJson then
    error("JSON lib requires 'lunajson' from LuaRocks")
  end

  local file = io.open(filename, 'wb')
  if not file then return end

  local serialized = lunajson.encode(data)
  file:write(serialized)
end

return M
