local make_feature = require('pax-vim.util.feature').make_feature

local config = {
  treesitter_config = {}
}

local M = make_feature({
  name = 'Treesitter',

  description = [[
  This feature configures Treesitter.
  ]],

  config = config,

  plugins = {
    {
      'https://github.com/nvim-treesitter/nvim-treesitter',
      build = ':TSUpdate',

      config = function()
        require('nvim-treesitter.install').update { with_sync = true }
        require('nvim-treesitter.configs').setup({
          auto_install = true,
          ensure_installed = 'all',

          highlight = { enable = true },
          autotag = { enable = true },

          incremental_selection = {
            enable = true,
            keymaps = {
              init_selection = '<CR>',
              node_incremental = '<CR>',
              node_decremental = '<BS>',
              scope_incremental = '<TAB>',
            },
          },
        })
      end
    }
  }
})

M.methods = {
  install_parser = function(parser)
    vim.cmd('TSInstall ' .. parser)
  end
}

return M
