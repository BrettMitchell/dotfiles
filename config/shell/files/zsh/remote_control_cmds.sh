
function set-buf {
  BUFFER="$1"
  CURSOR="${#1}"
  if [[ "$KEYMAP" == vicmd ]] ; then
    (( CURSOR-=1 ))
  fi
  zle -R
}

function show-param {
  zle -M "${(P)1}"
  zle -R
}

function show-msg {
  zle -M "$1"
  zle -R
}

function stash-buf {
  zle push-input
  zle -R
}

function pop-buf {
  zle get-line
  zle -R
}

function open-in-nvim {
  nvim "$@" <"$TTY"
}

function open-in-lf {
  local target="$1"
  if [[ -e "$target" ]] ; then
    lfcd -command "focus '$target'" <"$TTY"
    zle reset-prompt
  fi
}

