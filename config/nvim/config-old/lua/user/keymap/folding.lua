local map = require('user.lib.map').map

local M = {}

function M.setup()
  map('n', '<C-f>', 'za') -- Toggle fold under cursor
end

return M
