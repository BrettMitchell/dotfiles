local make_feature = require('pax-vim.util.feature').make_feature_v2

local M = make_feature({
  name = 'Autopairs',

  description = [[
  This feature autopair support.
  ]],

  plugins = {
    { 'https://github.com/windwp/nvim-autopairs', },
    { 'https://github.com/windwp/nvim-ts-autotag', },
  },

  apply = function()
    local npairs = require("nvim-autopairs")

    npairs.setup {
      enable_check_bracket_line = false
    }

    npairs.add_rules(require('nvim-autopairs.rules.endwise-elixir'))
    npairs.add_rules(require('nvim-autopairs.rules.endwise-lua'))
    npairs.add_rules(require('nvim-autopairs.rules.endwise-ruby'))

    require('nvim-ts-autotag').setup {}
  end
})

return M
