local feat_util = require('user.util.feature')
local add_plugins = require('user.util.plugins.lazy').add_plugins

local M = {
  name = 'Better Quick Fix',

  description = [[
  This feature configures the Better Quick Fix plugin.

  Some of the features included:
  - Quickfix list preview window
  - Quickfix list filtering
  - Bulk replace
  - Undo bulk action
  ]],

  bqf_config = {},
}

function M.setup(config)
  feat_util.apply_config(config or {}, M)
  add_plugins({
    {
      'https://github.com/kevinhwang91/nvim-bqf',
      ft = 'qf',
      config = function()
        require('bqf').setup(M.bqf_config)
      end
    }
  })
end

return M
