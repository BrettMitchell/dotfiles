local hl = require('user.util.highlight')

return function(theme)
  hl('FlashMatch', {
    fg = theme.color.text_selected,
    bg = theme.color.background_selected,
  })

  hl('FlashCurrent', {
    fg = theme.color.foreground1,
    bg = theme.color.background2,
  })

  hl('FlashLabel', {
    fg = theme.color.accent1,
    bg = theme.color.background2,
  })

  hl('FlashBackdrop', {
    fg = theme.color.accent3,
    bg = theme.color.background1,
  })
end
