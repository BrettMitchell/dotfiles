local keymap = require('user.util.keymap')
local lsp = require('user.features.lsp')
local lspsaga = require('user.features.lsp.lspsaga')

local methods = {
  rename_symbol = function()
    require('lspsaga.rename'):lsp_rename()
  end
}

local M = {
  name = 'LSP :: Rename symbol',

  dependencies = { lsp, lspsaga },

  keymaps = keymap {
    code_action = { 'n', '<space>r', methods.rename_symbol, { desc = 'Rename symbol' } }
  },

  description = [[
  This feature adds an LSP-powered rename action
  ]],

  on_activate = function()
  end
}

function M.setup()
  lsp.methods.add_hook({
    hook_name = 'on_attach',
    callback = function(_, bufnr)
      M.keymaps.apply(bufnr)
    end,
  })
end

return M
