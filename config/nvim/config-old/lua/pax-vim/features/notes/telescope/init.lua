return {
  pickNote = require('pax-vim.features.notes.telescope.pickNote').pickNote,
  multi_search = require('pax-vim.features.notes.telescope.multiSearch').multiSearch,
  pickNoteByTag = require('pax-vim.features.notes.telescope.zkBuiltins').pickNoteByTag,
  pick_note_by_tag_raw = require('pax-vim.features.notes.telescope.zkBuiltins').pick_note_by_tag_raw,
  pickNoteByTitle = require('pax-vim.features.notes.telescope.zkBuiltins').pickNoteByTitle,
  pickTagOrphans = require('pax-vim.features.notes.telescope.pickNote').pickTagOrphans,
  pickLinkOrphans = require('pax-vim.features.notes.telescope.pickNote').pickLinkOrphans,
  -- pickLinkGraph = require('pax-vim.features.notes.telescope.pickNote').pickLinkGraph,
}
