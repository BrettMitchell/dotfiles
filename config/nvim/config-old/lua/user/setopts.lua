-- Load local vimrc from project to support custom
--  per-project config
vim.opt.exrc = true

-- Leader
vim.g.mapleader = ' '
vim.g.maplocalleader = ';'

-- Indents
vim.opt.autoindent = true
vim.opt.expandtab = true
vim.opt.tabstop = 2
vim.opt.softtabstop = 2
vim.opt.shiftwidth = 2

-- Command height
-- vim.opt.cmdheight = 0

-- Line numbers
vim.opt.nu = true
vim.opt.relativenumber = true

-- Keep buffers open when you navigate away from them
vim.opt.hidden = true

-- Turn off audible alerts
vim.opt.errorbells = false

-- Don't wrap text by default
-- TODO: Override for less structured file types like markdown
vim.opt.wrap = false

-- Searching should be case sensitive by default
-- This is the default setting; this exists just to make that
--  explicit
-- TODO: Add command to do case insensitive search. Likely to
--        be useful for file types like markdown.
vim.opt.ignorecase = false

-- Turn off swap files
vim.opt.swapfile = false

-- Turn off backups
vim.opt.backup = false

-- Keep undo history for undo tree
vim.opt.undodir = vim.fn.getenv('HOME') .. "/.vim/undo_history"
vim.opt.undofile = true

-- Search while you type
vim.opt.incsearch = true
-- Highlight search terms
vim.opt.hlsearch = true
vim.opt.wrapscan = true

-- Allows nvim to access the clipboard without manual
--  keymaps
-- vim.opt.clipboard = 'unnamedplus'

-- netrw
-- vim.g.netrw_keepdir = 0
vim.g.netrw_winsize = 30
vim.g.netrw_banner = 0
vim.g.netrw_list_hide = '.*\\.swp$,.DS_Store,*/tmp/*,*.so,*.swp,*.zip,*.git,^\\.\\.\\=/\\=$'

-- Keep the cursor centered
vim.opt.scrolloff = 1000
vim.opt.sidescrolloff = 8

-- Add a column for feedback from plugins like linters etc.
vim.opt.signcolumn = "yes"

-- One space after punctuation
vim.opt.joinspaces = false

vim.opt.backspace = { "indent", "eol", "start" }

-- Yank highlight
-- vim.g.highlightedyank_highlight_duration = 120

-- Store 'cut' operations in yoink history
vim.g.yoinkIncludeDeleteOperations = 0

-- Floaterm
vim.g.floaterm_width = 0.8
vim.g.floaterm_height = 0.8
vim.g.lf_map_keys = 0

-- vim-move
-- Use custom keybinds for vim-move
vim.g.move_key_modifier = 'C-M'
vim.g.move_key_modifier_visualmode = 'C-M'
vim.g.move_map_keys = 1

-- Term colors
vim.opt.termguicolors = true

-- vim-wordmotion
vim.g.wordmotion_nomap = 1

-- status bar
vim.opt.showmode = false
vim.opt.showcmd = false

vim.diagnostic.config({
  virtual_text = false,
  -- Default diagnostics between lines to off. Toggle with <space>ml
  virtual_lines = false,
})

