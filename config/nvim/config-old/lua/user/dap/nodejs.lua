local M = {}

local installContainer = os.getenv('HOME') .. '/.build/neovim-artifacts/dap-install'
local installDir = installContainer .. '/vscode-node-debug2'

function M.install()
  local shell = os.getenv('SHELL')

  if (vim.fn.isdirectory(installDir) == 1) then
    vim.notify('https://github.com/microsoft/vscode-node-debug2.git already installed to ' .. installDir)
    return
  end

  vim.notify({ shell = shell, installDir = installDir })
  vim.notify(vim.fn.system({ 'mkdir', '-p', installContainer }))
  vim.notify(vim.fn.system({ 'git', 'clone', 'https://github.com/microsoft/vscode-node-debug2.git', installDir }))
  vim.notify(vim.fn.system({ shell, '-c',
    ("cd '%s' && npm install && NODE_OPTIONS=--no-experimental-fetch npm run build"):format(installDir) }))
end

M.adapter = {
  type = 'executable',
  command = 'node',
  args = { installDir .. '/out/src/nodeDebug.js' },
}

M.configuration = {
}

function M.setup(dap)
  dap.adapters.node2 = M.adapter
  dap.configurations.javascript = M.configuration
end

function M.attach()
  local dap = require('dap');

  dap.run({
    type = 'node2',
    request = 'attach',
    cwd = vim.fn.getcwd(),
    sourceMaps = true,
    protocol = 'inspector',
    skipFiles = { '<node_internals>/**/*.js' },
  });
end

return M
