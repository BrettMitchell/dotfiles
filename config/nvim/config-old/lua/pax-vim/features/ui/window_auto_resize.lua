local feat_util = require('pax-vim.util.feature')
local add_plugins = require('pax-vim.util.plugins.lazy').add_plugins
local keymap = require('pax-vim.util.keymap')

local M = {
  feature = {
    name = 'Window Auto-resize',

    description = [[
    Automatically expands the current focused window. Useful when using a lot
    of splits
    ]],

    enabled = true,
  },
}

function M.toggle()
  if M.feature.enabled then
    M.feature.enabled = false
    require('windows.autowidth').disable()
    vim.notify('Disabled window auto-width')
  else
    M.feature.enabled = true
    require('windows.autowidth').enable()
    vim.notify('Enabled window auto-width')
  end
end

M.feature.keymap = keymap {
  toggle = { { 'n' }, '<space>uW', M.toggle, { desc = 'Toggle window auto-width' } }
}

function M.setup(config)
  feat_util.apply_config(config or {}, M.feature)

  M.feature.keymap.apply()

  add_plugins({ {
    'https://github.com/anuvyklack/windows.nvim',
    dependencies = { 'https://github.com/anuvyklack/middleclass' },
    config = function()
      require('windows').setup({ enabled = M.feature.enabled })
    end
  } })
end

return M
