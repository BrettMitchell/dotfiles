return {
  concat       = require('pax-vim.util.table.concat'),
  filter       = require('pax-vim.util.table.filter'),
  find         = require('pax-vim.util.table.find'),
  includes     = require('pax-vim.util.table.includes'),
  join         = require('pax-vim.util.table.join'),
  map          = require('pax-vim.util.table.map'),
  merge        = require('pax-vim.util.table.merge'),
  shallow_copy = require('pax-vim.util.table.shallow_copy'),
  deep_copy    = require('pax-vim.util.table.deep_copy'),
  slice        = require('pax-vim.util.table.slice'),
  unpack       = require('pax-vim.util.table.unpack'),
}
