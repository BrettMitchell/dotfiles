local make_feature = require('user.util.feature').make_feature

local function auto_indent(normalKey)
  return function ()
    if #vim.fn.getline('.') == 0 then
      return '"_cc'
    else
      return normalKey
    end
  end
end

local M = make_feature({
  name = 'Auto-indent on Insert',

  description = [[
  This feature automatically indents the current line when entering
  insert mode if the current line is empty.
  ]],

  keymaps = {
    { 'n', 'i', auto_indent('i'), { expr = true, desc = 'Enter insert mode (auto-indents current line if empty)' } },
    { 'n', 'a', auto_indent('a'), { expr = true, desc = 'Enter insert mode (auto-indents current line if empty)' } },
    { 'n', 'A', auto_indent('A'), { expr = true, desc = 'Enter insert mode (auto-indents current line if empty)' } },
  }
})

M.methods = {
  i_auto_indent = auto_indent('i'),
  a_auto_indent = auto_indent('a'),
  A_auto_indent = auto_indent('A'),
}

return M

