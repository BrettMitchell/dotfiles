local feat_util = require('pax-vim.util.feature')
local add_plugins = require('pax-vim.util.plugins.lazy').add_plugins
local keymap = require('pax-vim.util.keymap')

local M = {
  feature = {
    name = 'Clever-F',

    description = [[
    This feature enables and configures the clever-f plugin, which 
    provides improvements on top of the base behavior of f/F/t/F.
    ]],

    header = nil,
    buttons = nil,

    keymap = keymap {
      find_forward = { { 'n', 'x', 'o' }, 'f', '<plug>(clever-f-f)', { desc = 'Find character forward' } },
      find_backward = { { 'n', 'x', 'o' }, 'F', '<plug>(clever-f-F)', { desc = 'Find character backward' } },
      to_forward = { { 'n', 'x', 'o' }, 't', '<plug>(clever-f-t)', { desc = 'To character forward' } },
      to_backward = { { 'n', 'x', 'o' }, 'T', '<plug>(clever-f-T)', { desc = 'To character backward' } },
    }
  }
}

function M.setup(userConfig)
  userConfig = userConfig or {}

  add_plugins({ {
    'https://github.com/rhysd/clever-f.vim',
  } })

  feat_util.apply_config(userConfig, M.feature)
  M.feature.keymap.apply()

  if userConfig.clever_f_not_overwrites_standard_mappings ~= nil then
    vim.g.clever_f_not_overwrites_standard_mappings = userConfig.clever_f_not_overwrites_standard_mappings
  end

  if userConfig.clever_f_across_no_line ~= nil then
    vim.g.clever_f_across_no_line = userConfig.clever_f_across_no_line
  end

  if userConfig.clever_f_smart_case ~= nil then
    vim.g.clever_f_smart_case = userConfig.clever_f_smart_case
  end

  if userConfig.clever_f_fix_key_direction ~= nil then
    vim.g.clever_f_fix_key_direction = userConfig.clever_f_fix_key_direction
  end
end

return M
