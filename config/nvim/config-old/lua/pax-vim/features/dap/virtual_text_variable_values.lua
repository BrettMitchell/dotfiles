local make_feature = require('pax-vim.util.feature').make_feature_v2
local dap = require('pax-vim.features.dap')

local M = make_feature({
  name = 'DAP :: Variable Values in Virtual Text',
  dependencies = { dap },

  description = [[
  This feature shows variable values in virtual text at the end of the
  line where the variable is defined or referenced.
  ]],

  plugins = {
    {
      'https://github.com/theHamsta/nvim-dap-virtual-text',
      config = function ()
        require('nvim-dap-virtual-text').setup()
      end
    }
  }
})

return M

