{ nixpkgs, ... }:
{
  # TODO:
  # - arion
  # - nix-snapshotter
  # https://nixos.wiki/wiki/Docker
  home.packages = with nixpkgs.stable; [
    dive
    lazydocker # Applies to podman and docker
  ];
}
