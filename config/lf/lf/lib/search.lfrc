
cmd broot ${{
  if [ `uname` == Darwin ] ; then
    # For some reason, broot pegs the CPU when run as a child of lf on MacOS
    # Substitute with regular old skim in that case
    res=`mux-pick-dir "$PWD"`
    if [ -n "$res" ] ; then
      lf -remote "send $id focus $res"
    fi
    exit
  fi

  conf="$HOME/.config/broot/conf.lf.hjson"
  if [ -n "$TMUX" ] ; then
    conf+=";$HOME/.config/broot/conf.keys-mux.hjson"
  fi
  broot --conf "$conf" "${1:-$PWD}"
}}

cmd broot-root ${{
  dir="$(tmux display -p '#{session_path}')"

  if [ `uname` == Darwin ] ; then
    # For some reason, broot pegs the CPU when run as a child of lf on MacOS
    # Substitute with regular old skim in that case
    res=`mux-pick-dir "$dir"`
    if [ -n "$res" ] ; then
      lf -remote "send $id focus '$dir/$res'"
    fi
    exit
  fi

  conf="$HOME/.config/broot/conf.lf.hjson"
  if [ -n "$TMUX" ] ; then
    conf+=";$HOME/.config/broot/conf.keys-mux.hjson"
  fi
  broot --conf "$conf" "$dir"
}}

cmd search-dir-open ${{
  res=`mux-popup -w '100%' -h '100%' -- mux-search "$PWD"`

  if [[ -n "$res" ]] ; then
    pane=`mux-open-pick-pane --ref-type file-line-column "$res"`
    tmux select-pane -t "$pane"
  fi
}}

cmd search-dir-goto ${{
  res=`mux-popup -w '100%' -h '100%' -- mux-search "$PWD" | cut -d: -f1`

  if [[ -n "$res" ]] ; then
    lf -remote "send $id focus $res"
  fi
}}

map b broot
map B broot-root
map / search
map ? filter
map <esc> setfilter ''
map ff search-dir-open
map fs search-dir-goto

